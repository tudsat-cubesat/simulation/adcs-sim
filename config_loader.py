import yaml
from yaml.loader import SafeLoader
import logging
import copy

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

# main functions

def get_config(user_parameters_filename: str) -> dict:

    default_parameters = import_config_file("default.yaml")

    try:
        user_parameters = import_config_file(demand_user_config_filename(
            user_parameters_filename))
    except Exception as e:
        raise e

    log.debug(bcolors.OKCYAN + "default pre_simulation:\n" 
        + yaml.dump(default_parameters["pre_simulation"]) + bcolors.ENDC)

    final_parameters = default_parameters.copy()

    """
    Parameter Dict Creation
    -----------------------

    1. __Create pre_simulation parameters__: If there is a valid
    pre_simulation dict in user_parameters, its components list replaces
    the default pre_simulation components list. If there is a non-valid 
    pre_simulation item in user_parameters, no pre_simulation item is 
    created in final_parameters and therefore no pre_simulation will be executed.
    If there is no pre_simulation item in user_parameters at all, the
    default pre_simulation specified in default.yaml will be executed.

    2. __Create simulation parameters__: Nearly the same as for
    pre_simulation except the simulation will be executed nonetheless.

    3. __finalization of parameters__: For every component in 
    pre_simulation and simulation dicts the not specified parameters 
    will be added from the Components dict from final_parameters. After 
    that, the Components dict will be removed from the final_parameters 
    dict.
    If there is one, a dict named `component_tables` will be copied from
    the user_parameters, or else a empty one is created.
    """

    # 1. Create pre_simulation parameters

    create_config(final_parameters, user_parameters, "pre_simulation")

    # 2. Create simulation parameters

    create_config(final_parameters, user_parameters, "simulation")

    # 3. finalization of parameters

    final_parameters["pre_simulation"] = finalise_sim_dict("pre_simulation",
        final_parameters, user_parameters)
    final_parameters["simulation"] = finalise_sim_dict("simulation",
        final_parameters, user_parameters)

    final_parameters["component_tables"] = create_comp_tables(final_parameters,
        user_parameters)

    final_parameters["time_parameters"] = create_time_parameters(
        final_parameters,
        user_parameters)

    # get rid of Components dict
    final_parameters.pop('Components', None)

    log.debug(bcolors.BOLD + bcolors.OKGREEN + "final_parameters:\n"
        + yaml.dump(final_parameters, sort_keys=False,
            default_flow_style=False)
        + bcolors.ENDC)

    return final_parameters



# utilities

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def import_config_file(filename: str):
    if filename == "":
        return dict()
    else:
        with open(filename, 'r') as f:
            return list(yaml.load_all(f, Loader=SafeLoader))[0]


def demand_user_config_filename(user_parameters_filename: str) -> str:
    default_user_parameters_filename = ""
    if len(user_parameters_filename) == 0:
        user_parameters_filename = default_user_parameters_filename
    elif user_parameters_filename[-5:] != ".yaml":
        user_parameters_filename += ".yaml"
    print(bcolors.OKGREEN + "filename is " + user_parameters_filename
        + bcolors.ENDC)
    return user_parameters_filename


def create_config(final_config:dict, user_config: dict, target:str):
    def check_user_simulation(key: str) -> tuple:
        # log.debug(bcolors.OKGREEN + "check_user_simulation:\n" 
        #         + yaml.dump(user_config[key]) + bcolors.ENDC)
        try:
            sim_user = user_config[key]
            if "components" in sim_user:
                comp_user = sim_user["components"]
            else:
                comp_user = None
            if "outputs" in sim_user:
                outp_user = sim_user["outputs"]
            else:
                outp_user = None
        except KeyError:
            sim_user = None
            comp_user = None
            outp_user = None
        except TypeError:
            sim_user = dict()
            comp_user = None
            outp_user = None
        return (sim_user, comp_user, outp_user)

    if target == "pre_simulation":
        # 1. Create pre_simulation parameters

        # check for valid user pre_simulation
        (pre_simulation_user, pre_components_user, pre_outputs_user
            ) = check_user_simulation(
            "pre_simulation")

        if (pre_simulation_user != None 
                and (type(pre_components_user) == list
                    or type(pre_outputs_user) == dict)):
            # use pre_simulation from user
            final_config["pre_simulation"]["components"] = (
                pre_simulation_user["components"])

            log.debug(bcolors.OKGREEN + "prefinal pre_simulation:\n" 
                + yaml.dump(final_config["pre_simulation"]) + bcolors.ENDC)
        elif (pre_simulation_user != None and type(pre_components_user)
                != list and type(pre_outputs_user) != dict):
            # use no pre_simulation
            final_config.pop('pre_simulation', None)
            log.debug(bcolors.OKGREEN + "no pre_simulation" + bcolors.ENDC)
        else:
            # use pre_simulation from default
            log.debug(bcolors.OKGREEN + "prefinal pre_simulation:\n" 
                + yaml.dump(final_config["pre_simulation"]) + bcolors.ENDC)
    elif target == "simulation":
        # 2. Create simulation parameters

        # check for valid user simulation
        (simulation_user, sim_components_user, sim_outputs_user
            ) = check_user_simulation(
            "simulation")

        if simulation_user == None:
            # use simulation from default
            pass
        else:
            if type(sim_components_user) == list:
                # use simulation components from user
                final_config["simulation"]["components"] = sim_components_user
            else:
                # use no simulation components
                final_config["simulation"].pop('components', None)
            if type(sim_outputs_user) == dict:
                # use simulation outputs from user
                final_config["simulation"]["outputs"] = sim_outputs_user
            else:
                # use no simulation outputs
                final_config["simulation"].pop('outputs', None)
        log.debug(bcolors.OKGREEN + "prefinal simulation:\n" + yaml.dump(final_config["simulation"]) + bcolors.ENDC)

    elif type(target) != str:
        raise TypeError("Only 'pre_simulation' or 'simulation' are allowed as"
            "input values for second input named 'target'.")
    else:
        raise ValueError("Only 'pre_simulation' or 'simulation' are allowed as"
            "input values for second input named 'target'.")


def normalise_params(component:dict, use_dict:bool = True):
    try:
        component["parameters"]
    except KeyError:
        log.debug("no direct parameter attribute of component: \n"
            + yaml.dump(component, sort_keys=False, 
                default_flow_style=False))
        component["parameters"] = dict()
    else:
        param_type = type(component["parameters"])
        if param_type == list:
            if use_dict == True:
                component["parameters"] = component["parameters"][0]
            else:
                # expected behavior
                pass
        elif param_type == dict:
            if use_dict == True:
                # expected behavior
                pass
            else:
                component["parameters"] = [component["parameters"],]
        else:
            # error
            log.info("parameters type is " + str(param_type)
                + ", should be list or dict. Component: \n"
                + yaml.dump(component, sort_keys=False, 
                    default_flow_style=False))
            return None
        try:
            component["name"]
        except KeyError:
            # give name
            try:
                if use_dict == True:
                    name:str = component["parameters"]["name"]
                else:
                    name:str = component["parameters"][0]["name"]
                if type(name) != str:
                    raise TypeError
            except KeyError:
                # error
                log.info("Component has no name attribute. Component: \n"
                    + yaml.dump(component, sort_keys=False, 
                        default_flow_style=False))
                return None
            except TypeError:
                log.info("Component has no valid name attribute. "
                    "name: " + str(name) +
                    "\nComponent: \n" + yaml.dump(component, sort_keys=False, 
                        default_flow_style=False))
                return None
            else:
                component["name"] = name
        else:
            pass
        return component


def finalise_sim_dict(sim_dict_key:str, final_config:dict, 
        user_config: dict) ->dict:

    # creation of presimulation dict
    sim_dict = final_config[sim_dict_key].copy()
    sim_dict["components"] = list()

    # creation of list of components
    components_list = list()

    # adding components for sim_dict from final_config
    try:
        final_config[sim_dict_key]
    except KeyError:
        log.debug("no " + sim_dict_key)
    else:
        components_list.extend(final_config[sim_dict_key]["components"])

    # normalise component parameters
    for comp in components_list:
        normalise_params(comp)

    log.debug(bcolors.BOLD + sim_dict_key + " components:\n"
        + yaml.dump(components_list, sort_keys=False,
            default_flow_style=False)
        + bcolors.ENDC)

    # adding components from components_list to sim_dict
    for i, comp in enumerate(components_list):
        # check for name and class properties
        try:
            try:
                comp["name"]
            except KeyError:
                comp["name"] = comp["parameters"]["name"]
            comp["class"]
        except KeyError:
            log.critical(bcolors.WARNING + "component #" + str(i+1) +
                " is not valid, add next…\nComponents must have name and "
                "class: \n" + bcolors.ENDC + yaml.dump(comp))
            continue
        else:
            # Copy data from Components in default config data.
            try:
                final_config["Components"]
            except Exception as e:
                final_config["Components"] = import_config_file(
                    "default.yaml")["Components"]
            try:
                final_config["Components"][comp["class"]]
            except Exception as e:
                raise e
            else:
                new_component: dict = final_config["Components"][
                    comp["class"]].copy()
                sim_dict["components"].append(new_component)
                log.debug(bcolors.BOLD + bcolors.OKBLUE
                    + 'new_component with default data:\n'
                    + yaml.dump(new_component, sort_keys=False,
                        default_flow_style=False)
                    + bcolors.ENDC)

            # replace default data with user_config data
            user_component:dict = final_config[sim_dict_key][
                "components"][i].copy()
            log.debug(bcolors.BOLD + bcolors.OKCYAN
                + 'user_component with user_config data:\n'
                + yaml.dump(user_component, sort_keys=False,
                    default_flow_style=False)
                + bcolors.ENDC)
            for key, val in user_component.items():
                if key != "parameters":
                    log.debug(bcolors.BOLD + bcolors.OKBLUE
                        + 'key:\n'
                        + key
                        + bcolors.ENDC)
                    new_component[key] = user_component[key]
                else:
                    log.debug(bcolors.BOLD + bcolors.FAIL
                        + 'key:\n'
                        + key
                        + bcolors.ENDC)
                    log.debug(bcolors.BOLD + bcolors.OKBLUE
                        + "user_component[" + key + "]:\n"
                        + yaml.dump(user_component[key], sort_keys=False,
                            default_flow_style=False)
                        + bcolors.ENDC)
                    for pkey, pval in user_component[key].items():
                        new_component[key][pkey] = pval
                    if "name" in user_component:
                        new_component[key]["name"] = user_component["name"]
    log.debug(bcolors.BOLD + bcolors.OKGREEN
        + "final " + sim_dict_key + " components:\n"
        + yaml.dump(sim_dict, sort_keys=False,
            default_flow_style=False)
        + bcolors.ENDC)
    return sim_dict



def create_comp_tables(final_config:dict, user_config: dict) ->dict:
    comp_tabs_final = dict()

    # creation of list of components
    components_list = list()

    # adding components for component_tables from final_config component
    #     tables.
    try:
        final_config["component_tables"]
    except KeyError:
        pass
    else:
        components_list.extend(final_config["component_tables"].values())

    # adding components for component_tables from user_config component
    #     tables.
    try:
        user_config["component_tables"]
    except KeyError:
        log.debug("no component_tables")
    else:
        components_list.extend(user_config["component_tables"].values())

    # adding components for component_tables from pre_simulation
    try:
        final_config["pre_simulation"]
    except KeyError:
        log.debug("no pre_simulation")
    else:
        components_list.extend(copy.deepcopy(final_config["pre_simulation"]["components"]))

    # adding components for component_tables from simulation
    try:
        final_config["simulation"]
        final_config["simulation"]["components"]
        if type(final_config["simulation"]["components"]) != list:
            raise KeyError
    except KeyError:
        log.debug("no simulation components")
    else:
        components_list.extend(copy.deepcopy(final_config["simulation"]["components"]))

    # normalise component parameters
    for comp in components_list:
        normalise_params(comp, use_dict = False)
        # get rid of initialisation parameters
        for ii, param in enumerate(comp["parameters"]):
            if "simulation_time" in param \
                    and param["simulation_time"] > 0:
                pass
            else:
                comp["parameters"].pop(ii)

    # add components to comp_tabs_final
    for comp in components_list:
        table_name: str = comp["name"] + "_parameters"
        if table_name not in comp_tabs_final:
            comp_tabs_final[table_name] = comp
        elif len(comp["parameters"]) > 0:
            comp_tabs_final[table_name]["parameters"].extend(
                comp["parameters"])

    log.debug(bcolors.BOLD + "comp_tabs_final:\n"
        + yaml.dump(comp_tabs_final, sort_keys=False,
            default_flow_style=False)
        + bcolors.ENDC)

    return comp_tabs_final



def create_time_parameters(final_config:dict, user_config: dict) ->dict:
    if "time_parameters" in user_config:
        time_parameters = dict()
        for key, val in user_config["time_parameters"].items():
            time_parameters[key] = val
        return time_parameters
    else:
        # no user time_parameters
        if "time_parameters" in final_config:
            return final_config["time_parameters"].copy()
        else:
            log.critical(bcolors.FAIL + "No time_parameters found. Unable to start simulation")
            return None
