var hierarchy =
[
    [ "config_loader.bcolors", "classconfig__loader_1_1bcolors.html", null ],
    [ "main.bcolors", "classmain_1_1bcolors.html", null ],
    [ "json_grabber.MakeDict", "classjson__grabber_1_1_make_dict.html", null ],
    [ "object", null, [
      [ "Sensor.Sensor", "class_sensor_1_1_sensor.html", [
        [ "acceleration_sensor.MEMS3AxisAccelerationSensor", "classacceleration__sensor_1_1_m_e_m_s3_axis_acceleration_sensor.html", null ],
        [ "earth_sensor.EarthSensor", "classearth__sensor_1_1_earth_sensor.html", null ],
        [ "gyroscope.MEMS3AxisGyroscope", "classgyroscope_1_1_m_e_m_s3_axis_gyroscope.html", null ],
        [ "magnetometer.FluxgateMagnetometer", "classmagnetometer_1_1_fluxgate_magnetometer.html", null ],
        [ "magnetometer.Magnetometer", "classmagnetometer_1_1_magnetometer.html", null ],
        [ "sunsensor.Sunsensor", "classsunsensor_1_1_sunsensor.html", [
          [ "sunsensor.FineSunSensor", "classsunsensor_1_1_fine_sun_sensor.html", null ],
          [ "sunsensor.Photodiode", "classsunsensor_1_1_photodiode.html", null ]
        ] ]
      ] ],
      [ "dynamics.Dynamics", "classdynamics_1_1_dynamics.html", null ],
      [ "environment.Environment", "classenvironment_1_1_environment.html", null ]
    ] ],
    [ "internal_transfer.OutputLists", "classinternal__transfer_1_1_output_lists.html", null ],
    [ "main.task_object", "classmain_1_1task__object.html", null ],
    [ "QMainWindow", null, [
      [ "view.App", "classview_1_1_app.html", null ],
      [ "widgets.App", "classwidgets_1_1_app.html", null ]
    ] ],
    [ "QObject", null, [
      [ "main.Main", "classmain_1_1_main.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "view.Tabs", "classview_1_1_tabs.html", null ],
      [ "widgets.MyTableWidget", "classwidgets_1_1_my_table_widget.html", null ],
      [ "widgets.PyQtGraph2dPlot", "classwidgets_1_1_py_qt_graph2d_plot.html", null ],
      [ "widgets.YamlEditorWindow", "classwidgets_1_1_yaml_editor_window.html", null ]
    ] ],
    [ "Satrec", null, [
      [ "orbit.Orbit", "classorbit_1_1_orbit.html", null ]
    ] ]
];