import numpy as np
import matplotlib.pyplot as plt
from pyquaternion import Quaternion
import utilities
import Arrow3D

df = utilities.get_table_from_database('test.db', 'position')
r_table = df.to_numpy()
r_table = r_table[:, 2:5] / 100

r_ell = 6370

u = np.linspace(0, 2 * np.pi, 100)
v = np.linspace(0, np.pi, 100)

g1 = r_ell / 100 * np.outer(np.cos(u), np.sin(v))
g2 = r_ell / 100 * np.outer(np.sin(u), np.sin(v))
g3 = r_ell / 100 * np.outer(np.ones(np.size(u)), np.cos(v))

# Plotting arguments for Earth Frame
fig3 = plt.figure(figsize=(10, 10))
ax3 = fig3.add_subplot(111, projection='3d')
ax3.grid(False)
ax3.plot_surface(g1, g2, g3, linewidth=1, rstride=5, cstride=5, color='k', alpha=0.1, edgecolor='k')
ax3.plot3D(r_table[:, 0], r_table[:, 1], r_table[:, 2], linestyle='dotted', color='k')

# Earth Inertial Cartesian Coordinate System
arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
sc = 50
a = Arrow3D.Arrow3D([0, sc * 1], [0, 0], [0, 0], **arrow_prop_dict, color='r')
ax3.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, sc * 1], [0, 0], **arrow_prop_dict, color='b')
ax3.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, 0], [0, sc * 1], **arrow_prop_dict, color='g')
ax3.add_artist(a)

ax3.text(0.0, 0.0, sc * -0.1, r'$0$')
ax3.text(sc * 1.1, 0, 0, r'$x$')
ax3.text(0, sc * 1.1, 0, r'$y$')
ax3.text(0, 0, sc * 1.1, r'$z$')

dm = utilities.get_table_from_database('test.db', 'magnetic_field_strength')
dmc = utilities.get_table_from_database('test.db', 'FluxMag_X')
dyn = utilities.get_table_from_database('test.db', 'state_sat')

dyn = dyn.to_numpy()
dyn = dyn[:, 2:6]

igrf_table = dm.to_numpy()
igrf_table = igrf_table[:, 2:5] / 1000

igrf_table = igrf_table + r_table

mc_table = dmc.to_numpy()
mc_table = mc_table[:, 2:5] / 1000

# Plot of Magnetic Field Calculation (red) and pyIGRF magnetic field (black)

for i in range(0, 270000, 10000):
    igrf = Arrow3D.Arrow3D([r_table[i, 0], igrf_table[i, 0]],
                           [r_table[i, 1], igrf_table[i, 1]],
                           [r_table[i, 2], igrf_table[i, 2]],
                           **arrow_prop_dict, color='k')

    igrf_x = Arrow3D.Arrow3D([r_table[i, 0], igrf_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='k')
    igrf_y = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], igrf_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='k')
    igrf_z = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], igrf_table[i, 2]],
                             **arrow_prop_dict, color='k')
    ax3.add_artist(igrf_x)
    ax3.add_artist(igrf_y)
    ax3.add_artist(igrf_z)

    orientation_sat = Quaternion(dyn[int(i / 5), :])
    mc_vec = mc_table[int(i / 5)]
    mc_sat = Quaternion(np.array([1, 0, 0, 0])).rotate(mc_table[int(i / 5)])
    mc_in = orientation_sat.rotate(mc_sat)
    mc_in = mc_in + r_table[i]

    mgn_sen = Arrow3D.Arrow3D([r_table[i, 0], mc_in[0]],
                              [r_table[i, 1], mc_in[1]],
                              [r_table[i, 2], mc_in[2]],
                              **arrow_prop_dict, color='r')
    # ax3.add_artist(mgn_sen)

    mgn_sen_x = Arrow3D.Arrow3D([r_table[i, 0], mc_in[0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='r')
    mgn_sen_y = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], mc_in[1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='r')
    mgn_sen_z = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], mc_in[2]],
                             **arrow_prop_dict, color='r')
    ax3.add_artist(mgn_sen_x)
    ax3.add_artist(mgn_sen_y)
    ax3.add_artist(mgn_sen_z)

    # Rotated Satellite coordinate system
    sat_coord = False
    if sat_coord is True:
        sc3 = 10
        ax_sat_in = orientation_sat.rotate([sc3 * 1, 0, 0]) + np.transpose(r_table[i])
        ay_sat_in = orientation_sat.rotate([0, sc3 * 1, 0]) + np.transpose(r_table[i])
        az_sat_in = orientation_sat.rotate([0, 0, sc3 * 1]) + np.transpose(r_table[i])

        b = Arrow3D.Arrow3D([r_table[i, 0], ax_sat_in[0]], [r_table[i, 1], ax_sat_in[1]], [r_table[i, 2], ax_sat_in[2]],
                            **arrow_prop_dict, color='r')
        ax3.add_artist(b)
        b = Arrow3D.Arrow3D([r_table[i, 0], ay_sat_in[0]], [r_table[i, 1], ay_sat_in[1]], [r_table[i, 2], ay_sat_in[2]],
                            **arrow_prop_dict, color='b')
        ax3.add_artist(b)
        b = Arrow3D.Arrow3D([r_table[i, 0], az_sat_in[0]], [r_table[i, 1], az_sat_in[1]], [r_table[i, 2], az_sat_in[2]],
                            **arrow_prop_dict, color='g')
        ax3.add_artist(b)

# z scales the limit of the axes
z = 1
ax3.set_xlim(z * [-100, 100])
ax3.set_ylim(z * [-100, 100])
ax3.set_zlim(z * [-100, 100])

plt.show()