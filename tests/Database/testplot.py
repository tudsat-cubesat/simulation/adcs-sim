import pandas as pd
import numpy as np
import sqlite3
import matplotlib.pyplot as plt


while True:
    conn = sqlite3.connect('test.db')
    c = conn.cursor()

    df = pd.read_sql_query("SELECT * FROM gyroscope", conn)

    plt.plot(df["gyro_1"], 'k')
    plt.plot(df["gyro_2"], 'b')
    plt.plot(df["gyro_3"], 'r')
    plt.xlim(len(df["gyro_1"])-40, len(df["gyro_1"]))
    plt.pause(0.005)
    c.close()