import pandas as pd
import pyIGRF
import utilities
import numpy as np
from astropy import coordinates as coord, units as u
from astropy.time import Time
import datetime as dt
from sgp4.api import jday
import time
import logging


class Environment(object):
    def __init__(self, sim_time_offset: tuple, database: str, sampling_rate: float, outputs):
        """Environment class that sets up all the required environmental circumstances including magnetic field,
        atmospheric density, solar pressure, sun position and angular components.

        :param sim_time_offset: (tuple) the simulation start date and time in the form (YYYY, m, d, h, m, s)
        :param database: (str) the database the environment can write to and read from
        :param sampling_rate: (float) the frequency the environment creates new values and writes them into the
                                        database
        """
        self.sim_time_offset = sim_time_offset
        self.database = database
        self.sampling_rate = sampling_rate
        self.dict_env = outputs

        # create table for each output of the environment in the database, if they don't already exist
        utilities.create_table(self.database, 'magnetic_field_strength', 'real_time, simulation_time, M_x, M_y, M_z')
        utilities.create_table(self.database, 'atmospheric_density', 'real_time, simulation_time, density')
        utilities.create_table(self.database, 'solar_pressure', 'real_time, simulation_time, solar_pressure')
        utilities.create_table(self.database, 'earth_to_sun', 'real_time, simulation_time, sun_vector_x, '
                                                              'sun_vector_y, sun_vector_z')
        utilities.create_table(self.database, 'eclipse', 'real_time, simulation_time, eclipse')
        utilities.create_table(self.database, 'radius_ellipsoid', 'real_time, simulation_time, r_ell')

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, 'environment_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, 'environment_parameters', columns_content)

    def calc_mag_field(self, sim_time, commit: bool = True):
        """Compute the magnetic field strength components in a TEME coordinate frame. This method requires a position
        vector in the table 'position' in the database at the sim_time. It either commits the computed values to the
        database or simply returns the calculated vector. You can use this to propagate the magnetic field of a whole
        position table by setting sim_time to 'all'.

        :param sim_time: (float) the simulation time the magnetic field is supposed to be calculated for, use 'all' for
                                    calculating the entire position table
        :param commit: (bool) if True, it writes into the database directly after being done, if False, it returns a
                                vector containing all three components of the magnetic field
        :return: (np.ndarray) vector that contains all three components of the magnetic field in TEME coordinates
        """
        if sim_time != 'all':
            # connect to database and read the position of the satellite
            df = utilities.interpolate_two_rows(self.database, name='position', sim_time=sim_time)
            position = df.to_numpy()[0][1:].reshape(3)

            # calculate the simulated time in julian date
            epoch = list(self.sim_time_offset)
            epoch[5] += sim_time
            jd, fr = jday(*epoch)
            sim_time_julian = jd + fr

            # compute geodetic coordinates that are needed for pyIGRF package
            geodetic_coord = utilities.TEME_to_geodetic(position[0], position[1], position[2], sim_time_julian)
            lat, lon, alt = geodetic_coord[0], geodetic_coord[1], geodetic_coord[2]

            # compute magnetic field
            time_dec = utilities.julian_to_decimal(sim_time_julian)
            mag_field = np.array(pyIGRF.calculate.igrf12syn(date=time_dec,
                                                            itype=1,
                                                            alt=alt,
                                                            lat=lat,
                                                            elong=lon)[:3])

            # rotate the magnetic field from north east down system to ECEF
            r = utilities.ned2ecef(lon, lat)
            mag_field = r.rotate(mag_field)

            # transform ECEF coordinates to TEME
            mag_field = utilities.ECEF_to_TEME(mag_field[0], mag_field[1], mag_field[2], sim_time_julian)

            if commit:
                # write magnetic field in TEME coordinates into the database
                columns_content = (str(dt.datetime.now(tz=None)), sim_time, mag_field[0], mag_field[1], mag_field[2])
                utilities.write_line_into_database_table(self.database, 'magnetic_field_strength', columns_content)
            else:
                return mag_field

        else:
            df = utilities.get_table_from_database(self.database, 'position')
            position = df.to_numpy()
            sim_time_julian = np.zeros(position.shape[0])
            # calculate the simulated time in julian date
            for i in range(position.shape[0]):
                epoch = list(self.sim_time_offset)
                epoch[5] += position[i, 1]
                jd, fr = jday(*epoch)
                sim_time_julian[i] = jd + fr

            # transform julian dates in astropy.Time type in order to use coordinate transformations properly
            sim_time_julian = Time(sim_time_julian.tolist(), format='jd')

            # compute geodetic coordinates that are needed for pyIGRF package
            geodetic_coord = utilities.TEME_to_geodetic(position[:, 2], position[:, 3], position[:, 4], sim_time_julian)
            lat, lon, alt = geodetic_coord[0], geodetic_coord[1], geodetic_coord[2]

            # longitude values are given in range [-180, 180] but are required in [0, 360]
            for i in range(len(lon)):
                if lon[i] < 0:
                    lon[i] += 360

            # compute magnetic field
            time_dec = list(map(lambda x: x.to_value('decimalyear'), sim_time_julian))
            mag_field = np.zeros((position.shape[0], 3))
            for i in range(position.shape[0]):
                mag_field[i, :] = np.array(pyIGRF.calculate.igrf12syn(date=time_dec[i],
                                                                      itype=1,
                                                                      alt=alt[i],
                                                                      lat=lat[i],
                                                                      elong=lon[i])[:3])

            # rotate the magnetic field from north east down system to ECEF
            r = utilities.ned2ecef(lon, lat)
            mag_field = r.apply(mag_field)

            # transform ECEF coordinates to TEME
            mag_field = utilities.ECEF_to_TEME(mag_field[:, 0], mag_field[:, 1], mag_field[:, 2], sim_time_julian)

            if commit:
                # write magnetic field in TEME coordinates into the database
                columns_content = []
                real_times = [str(dt.datetime.now(tz=None)) for i in range(position.shape[0])]
                for i in range(position.shape[0]):
                    columns_content.append([real_times[i], position[i, 1], mag_field[i, 0], mag_field[i, 1],
                                            mag_field[i, 2]])
                    col_name = ['real_time', 'simulation_time', 'M_x', 'M_y', 'M_z']
                    self.dict_env.safe_output(
                        'magnetic_field_strength', col_name,
                        [real_times[i], position[i, 1], mag_field[i, 0], mag_field[i, 1],
                         mag_field[i, 2]])

                utilities.write_line_into_database_table(self.database, 'magnetic_field_strength',
                                                         tuple(columns_content))
            else:
                return mag_field

    def calc_atmospheric_density(self, sim_time):
        """
        this very simple static model is a first approximation. altitude in km, density in kg/m^3
        a more precise model should be implemented in a final version
        TODO: upgrade model
        """
        # the values are taken from Fundamentals of Spacecraft Attitude Determination and Control
        # by f. Markley and John Crassidis, table D.1
        const = {"p_0": [2.418e-11, 9.158e-12, 3.725e-12, 1.585e-12, 6.967e-13, 1.454e-13, 3.614e-14],
                 "h_0": [300, 350, 400, 450, 500, 600, 700],
                 "H": [52.5, 56.4, 59.4, 62.2, 65.8, 79, 109]}
        # connect to database and read the position of the satellite
        df = utilities.get_last_line_from_database_table(self.database, 'position')
        position = df.to_numpy().reshape(3)

        # calculate the simulated time in julian date
        # TODO: convert sim_time to julian date (or just initialize offset as julian date)
        sim_time_julian = self.sim_time_offset + sim_time

        altitude = utilities.TEME_to_geodetic(position[0], position[1], position[2], sim_time_julian)[2]

        if altitude < 300 or altitude > 800:
            raise Exception("the altitude is outside the range for the atmospheric model.\n" + \
                            "it is:{}, but it should be between 300 and 800".format(altitude))
        else:
            # depending on the altitude a different set of constants needs to be used
            # the altitude should be bigger than h_0 but smaller than the next value of h_0
            i = [j > altitude for j in const["h_0"]].index(True) - 1

        density = const["p_0"][i] * np.exp(-(altitude - const["h_0"][i]) / const["H"][i])

        # write atmospheric density at the given position into the database
        columns_content = (str(dt.datetime.now(tz=None)), sim_time, density)
        utilities.write_line_into_database_table(self.database, 'atmospheric_density', columns_content)

    def calc_solar_pressure(self, sim_time):
        """
        returns the amount of solar radiation pressure at the position. only considers light
        coming directly from the sun.
        from: Fundamentals of Spacecraft Attitude Determination and Control
        by f. Markley and John Crassidis Appendix D.3
        """

        # connect to database and read the position of the satellite
        df = utilities.get_last_line_from_database_table(self.database, 'position')
        position = df.to_numpy().reshape(3)
        # connect to database and read sun vector
        df = utilities.get_last_line_from_database_table(self.database, 'earth_to_sun')
        sun_pos_TEME = df.to_numpy()[2:].reshape(3)

        position_AU = (position * u.km).to(u.AU)
        sat_to_sun_AU = np.array(sun_pos_TEME) - np.array(position_AU)
        # speed of light in m/s
        c = 299792458
        # solar constant in W/m^2, flux density of solar radiation at adistance of 1 AU from the Sun
        # can vary up to 5 W/m^2 daily which wont matter for this purpose
        solar_const = 1362
        solar_pressure = solar_const / (c * (np.linalg.norm(sat_to_sun_AU) * 1000) ** 2)

        # write atmospheric density at the given position into the database
        columns_content = (str(dt.datetime.now(tz=None)), sim_time, solar_pressure)
        utilities.write_line_into_database_table(self.database, 'solar_pressure', columns_content)

    def earth_to_sun(self, sim_time, commit=True):
        """Calculate the position of the sun as a vector pointing from earth to sun. The vector is given in a TEME
        coordinate system. This method only requires a sim_time. It either commits the computed values to the database
        or simply returns the calculated vector. You can use this method to propagate the sun's position over a whole
        'position' table by setting sim_time to 'all'.

        :param sim_time: (float) the simulation time at which the sun's position is supposed to be calculated. Set to
                                    'all' in order to compute the sun vector for a whole given 'position' table where
                                    column 'simulation_time' is used to assign each position vector a sun vector.
        :param commit: (bool) if True, computed values are directly committed to the database, if False, computed
                                    values are returned
        :returns: (np.ndarray) the computed sun vector(s) in TEME coordinate frame

        """
        if sim_time != 'all':
            # calculate the simulated time in julian date
            epoch = list(self.sim_time_offset)
            epoch[5] += sim_time
            jd, fr = jday(*epoch)
            sim_time_julian = jd + fr

            sim_time_julian = Time(sim_time_julian, format="jd")

        else:
            df = utilities.get_table_from_database(self.database, 'position')
            simulation_time = df['simulation_time'].to_numpy()
            sim_time_julian = np.zeros(simulation_time.shape[0])
            # calculate the simulated time in julian date
            for i in range(simulation_time.shape[0]):
                epoch = list(self.sim_time_offset)
                epoch[5] += simulation_time[i]
                jd, fr = jday(*epoch)
                sim_time_julian[i] = jd + fr

            sim_time_julian = Time(sim_time_julian[:].tolist(), format="jd")

        sun_pos_GCRS = coord.get_sun(sim_time_julian)
        # the vector from earth to the sun in AU in the correct frame
        sun_pos_TEME = np.array(sun_pos_GCRS.transform_to(coord.builtin_frames.TEME()).cartesian.xyz).transpose()

        if sim_time != 'all':
            if commit:
                # write atmospheric density at the given position into the database
                columns_content = (
                    str(dt.datetime.now(tz=None)), sim_time, sun_pos_TEME[0], sun_pos_TEME[1], sun_pos_TEME[2])
                utilities.write_line_into_database_table(self.database, 'earth_to_sun', columns_content)
            else:
                return sun_pos_TEME
        else:
            if commit:
                # write sun vector in TEME coordinates into the database
                columns_content = []
                real_times = [str(dt.datetime.now(tz=None)) for i in range(simulation_time.shape[0])]
                for i in range(simulation_time.shape[0]):
                    columns_content.append([real_times[i], simulation_time[i], sun_pos_TEME[i, 0], sun_pos_TEME[i, 1],
                                            sun_pos_TEME[i, 2]])

                    col_name = ['real_time', 'simulation_time', 'sun_vector_x',
                                'sun_vector_y', 'sun_vector_z']
                    self.dict_env.safe_output(
                        'earth_to_sun', col_name,
                        [real_times[i], simulation_time[i], sun_pos_TEME[i, 0],
                         sun_pos_TEME[i, 1],
                         sun_pos_TEME[i, 2]])

                utilities.write_line_into_database_table(self.database, 'earth_to_sun',
                                                         tuple(columns_content))
            else:
                return sun_pos_TEME

    def is_in_shadow(self, sim_time, commit=True):
        """
        returns a boolean describing whether the satellite is in the shadow
        of the earth. uses the cylindrical shadow projection.
        from: Fundamentals of Spacecraft Attitude Determination and Control
        by f. Markley and John Crassidis Appendix D.3
        """
        if sim_time != 'all':
            # connect to database and read the position of the satellite
            df_pos = utilities.get_last_line_from_database_table(self.database, 'position')

            # connect to database and read the earth to sun vector
            df_sun = utilities.get_last_line_from_database_table(self.database, 'earth_to_sun')
        else:
            # connect to database and read the position of the satellite
            df_pos = utilities.get_table_from_database(self.database, 'position')

            # connect to database and read the earth to sun vector
            df_sun = utilities.get_table_from_database(self.database, 'earth_to_sun')

        df_pos['r'] = np.sqrt(df_pos['x'] ** 2 + df_pos['y'] ** 2 + df_pos['z'] ** 2)
        position = np.array([df_pos['x'], df_pos['y'], df_pos['z']]).transpose()

        df_sun['length'] = np.sqrt(df_sun['sun_vector_x'] ** 2 + df_sun['sun_vector_y'] ** 2
                                   + df_sun['sun_vector_z'] ** 2)
        df_sun['x_norm'] = df_sun['sun_vector_x'] / df_sun['length']
        df_sun['y_norm'] = df_sun['sun_vector_y'] / df_sun['length']
        df_sun['z_norm'] = df_sun['sun_vector_z'] / df_sun['length']

        sun_position_unit = np.array([df_sun['x_norm'], df_sun['y_norm'], df_sun['z_norm']]).transpose()
        earth_radius = 6371  # km

        is_in_shadow = list(np.array(list(map(lambda x, y: np.dot(x, y), position, sun_position_unit))) < \
                            - np.sqrt(df_pos['r'] ** 2 - earth_radius ** 2))

        if sim_time != 'all':
            if commit:
                # write atmospheric density at the given position into the database
                columns_content = (str(dt.datetime.now(tz=None)), sim_time, is_in_shadow[0])
                utilities.write_line_into_database_table(self.database, 'eclipse', columns_content)
            else:
                return is_in_shadow[0]

        else:
            if commit:
                # write bool into the database whether CubeSat is in shadow or not
                columns_content = []
                real_times = [str(dt.datetime.now(tz=None)) for i in range(position.shape[0])]
                for i in range(position.shape[0]):
                    columns_content.append([real_times[i], df_pos['simulation_time'].to_numpy()[i], is_in_shadow[i]])

                    col_name = ['real_time', 'simulation_time', 'eclipse']
                    self.dict_env.safe_output(
                        'eclipse', col_name,
                        [real_times[i],
                         df_pos['simulation_time'].to_numpy( )[i],
                         is_in_shadow[i]])
                utilities.write_line_into_database_table(self.database, 'eclipse',
                                                         tuple(columns_content))


    def earth_disk_ellipsoid(self, sim_time, commit=True):

        """Calculate the earth disk radius of a WGS84 ellipsoid which can be seen from the earth sensor.
        This method only requires a sim_time. It either commits the computed values to the database or simply returns
        the calculated vector.

        :param sim_time: (float) the simulation time at which the Earth Disk Radius is supposed to be calculated. Set to
                                    'all' in order to compute the sun vector for a whole given 'position' table where
                                    column 'simulation_time' is used to assign each position vector a sun vector.
        :param commit: (bool) if True, computed values are directly committed to the database, if False, computed
                                    values are returned
        :returns: (float) the computed earth radius

        """

        if sim_time != 'all':
            # connect to database and read the position of the satellite
            df = utilities.interpolate_two_rows(self.database, name='position', sim_time=sim_time)
            position = df.to_numpy()[0][1:].reshape(3)

            # calculate the simulated time in julian date
            epoch = list(self.sim_time_offset)
            epoch[5] += sim_time
            jd, fr = jday(*epoch)
            sim_time_julian = jd + fr

            # compute geodetic latitude and transform it to geocentric latitude to compute the Earth Disk Radius
            geodetic_coord = utilities.TEME_to_geodetic(position[0], position[1], position[2], sim_time_julian)
            geodetic_lat, geodetic_lon, geodetic_alt = geodetic_coord[0], geodetic_coord[1], geodetic_coord[2]

            geocentric_lat = np.arctan((1 - (1 / 298.257223560)) ** 2 * np.tan(geodetic_lat * np.pi / 180))
            r_ell = 6378137 * (1 - (1 / 298.257223560) * (np.sin(geocentric_lat)) ** 2)
            if commit:
                # write Earth Disk Radius into the database
                columns_content = (str(dt.datetime.now(tz=None)), sim_time, r_ell)
                utilities.write_line_into_database_table(self.database, 'radius_ellipsoid', columns_content)
            else:
                return r_ell

        else:
            df = utilities.get_table_from_database(self.database, 'position')
            position = df.to_numpy()
            sim_time_julian = np.zeros(position.shape[0])
            # calculate the simulated time in julian date
            for i in range(position.shape[0]):
                epoch = list(self.sim_time_offset)
                epoch[5] += position[i, 1]
                jd, fr = jday(*epoch)
                sim_time_julian[i] = jd + fr

            # transform julian dates in astropy.Time type in order to use coordinate transformations properly
            sim_time_julian = Time(sim_time_julian.tolist(), format='jd')

            # compute geodetic coordinates that are needed for pyIGRF package
            geodetic_coord = utilities.TEME_to_geodetic(position[:, 2], position[:, 3], position[:, 4], sim_time_julian)
            geodetic_lat, geodetic_lon, geodetic_alt = geodetic_coord[0], geodetic_coord[1], geodetic_coord[2]

            r_ell = np.zeros(position.shape[0])

            for i in range(position.shape[0]):
                geocentric_lat = np.arctan((1 - (1 / 298.257223560)) ** 2 * np.tan(geodetic_lat[i] * np.pi / 180))
                r_ell[i] = 6378137 * (1 - (1 / 298.257223560) * (np.sin(geocentric_lat)) ** 2)

            if commit:
                # write earth disk radius value into the Database
                columns_content = []
                real_times = [str(dt.datetime.now(tz=None)) for i in range(position.shape[0])]
                for i in range(position.shape[0]):
                    columns_content.append([real_times[i], position[i, 1], r_ell[i]])
                    col_name = ['real_time', 'simulation_time', 'r_ell']
                    self.dict_env.safe_output(
                        'radius_ellipsoid', col_name,
                        [real_times[i], position[i, 1], r_ell[i]])

                utilities.write_line_into_database_table(self.database, 'radius_ellipsoid',
                                                         tuple(columns_content))
            else:
                return r_ell

    def propagate_environment(self):
        total_start = time.time()
        logging.info('start sun position calculation')
        self.earth_to_sun(sim_time='all')
        logging.info('done, needed %s seconds' % (time.time() - total_start))
        start = time.time()
        logging.info('start eclipse calculation')
        self.is_in_shadow(sim_time='all')
        logging.info('done, needed %s seconds' % (time.time() - start))
        start = time.time()
        logging.info('start Earth Disk Radius calculation')
        self.earth_disk_ellipsoid(sim_time='all')
        logging.info('done, needed %s seconds' % (time.time() - start))
        start = time.time()
        logging.info('start magnetic field calculations')
        self.calc_mag_field(sim_time='all')
        logging.info('done, needed %s seconds' % (time.time() - start))
        logging.info('all in all needed %s seconds for building up orbit and environment' % (time.time() - total_start))
