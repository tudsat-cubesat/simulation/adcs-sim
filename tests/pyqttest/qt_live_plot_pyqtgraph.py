from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from PyQt5.QtCore import QObject, QThread, pyqtSignal
import multiprocessing as mp
import collections
import random
import time
import math
import numpy as np
import sqlite3
import utilities

# todo:
#   zoom x und y independet
#   multiprocessing
#   Qthread
#   event_process

class DynamicPlotter():

    def __init__(self, sampleinterval=0.1, timewindow=10., size=(600,350)):
        # Data stuff
        self._interval = int(sampleinterval*1000)
        self._bufsize = int(timewindow/sampleinterval)
        self.databuffer = collections.deque([0.0]*self._bufsize, self._bufsize)
        self.x = [0]
        self.y = [0]

        # PyQtGraph stuff
        self.app = QtGui.QApplication([])
        self.plt = pg.plot(title='Dynamic Plotting with PyQtGraph')
        self.plt.resize(*size)
        self.plt.showGrid(x=True, y=True)
        self.plt.setLabel('left', 'amplitude', 'V')
        self.plt.setLabel('bottom', 'time', 's')
        self.curve = self.plt.plot(self.x, self.y, pen=(255,0,0))
        # self.vb = self.curve.getViewBox()
        # self.vb.setAspectLocked(lock=False)
        # self.vb.enableMouse = True
        # self.vb.setMouseEnabled(y=False)
        # QTimer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(100)
        self.i = 1

    def updateplot(self):
        df = np.array(utilities.get_table_from_database('mp_test.db', 'test_data')['output'])
        self.y = df
        self.x = range(len(df))
        self.curve.setData(self.x, self.y)
        self.app.processEvents()

    def run(self):
        self.app.exec_()

def write_some_numbers():
    utilities.create_table('mp_test.db', 'test_data', 'output, input')
    i = 1
    while True:
        number = np.sin(np.pi/20*i)
        utilities.write_line_into_database_table('mp_test.db', 'test_data', (number, 1))
        i += 1
        time.sleep(0.01)




if __name__ == '__main__':
    p1 = mp.Process(target=write_some_numbers, args=())
    p1.daemon = True
    p1.start()
    time.sleep(1)
    #
    m = DynamicPlotter(sampleinterval=0.05, timewindow=10.)
    m.run()