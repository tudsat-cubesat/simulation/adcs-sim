import json
import numpy as np
import pandas as pd
import sqlite3
import datetime as dt
import time
from uuid import uuid4
from scipy.spatial.transform import Rotation as R
import utilities
from Sensor import Sensor

# todo: neue function ohne while schleife
# utilities funktion für datenbank aufruf
# beschleunigungssensor

class Sensor1(Sensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray,
            database: str, mode: int, digital: bool, sampling_rate: float, 
            active: bool = True):
        """Dummy Simulation of a Sensor

        :param name: (string) name of the sensor
        :param position: (numpy array, 3x1 x y z) position of the sensor in the
                satellite frame in m
        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the 
                sensor in quaternions in the satellite frame
        :param database: (string, .db) name of the database to read/write 
                from/in
        :param digital: (bool) defines if the sensor is digital or analog
        :param mode: (int) chooses between a different set of predefined 
                parameter for the sensor
        :param active: (Boolean) activates or deactivates the sensor
        :param sampling_rate: (float) sampling rate of the sensor in Hz
        """

        self.sampling_rate = sampling_rate
        # angular random walk in deg/s
        super().__init__(name, position, orientation, database, mode, digital, active)

        # create table for the output of the sensor in the database, if it doesn't already exist
        utilities.create_table(self.database, self.name, 'real_time, simulation_time, W_x_1')

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        utilities.create_table(self.database, self.name + '_parameters', 'real_time, simulation_time, changed_parameter, parametervalue')

        # write the parameter into parameter table of the database
        conn_3 = sqlite3.connect(str(self.database))
        c_3 = conn_3.cursor()
        # create list of all parameters
        parameter = ["name", "position", "orientation", "database", "digital", "mode", "active", "sampling_rate"]
        parameter_value = [self.name, self.position.tolist(), self.orientation.tolist(), self.database,
                           self.digital, self.mode, self.active, sampling_rate]

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, json.dumps(parameter), json.dumps(parameter_value))

        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)


    def set_position(self, position: np.ndarray):
        """Changes the position of the sensor in the satellite frame.

        :param position: (numpy array, 3x1 x y z) position of the sensor in the satellite frame in m
        """
        super(Sensor1, self).set_position(position)

    def set_orientation(self, orientation: np.ndarray):
        """Changes the orientation of the sensor in the satellite frame.

        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the sensor in quaternions in the satellite frame
        """
        super(Sensor1, self).set_orientation(orientation)

    def set_mode(self, mode: int):
        """Switches between different predefined modes of the Sensor. Everey mode has a set of Parameter.

        :param mode: (int, describtion) different modes of the
        """
        super(Sensor1, self).set_mode(mode)

    def set_active(self, active: bool):
        """Activates or deactivates the sensor. To start a Sensor this function needs a thread.

        :param active: (Boolean) activates or deactivates the sensor
        """
        super(Sensor1, self).set_active(active)

    def set_sampling_rate(self, sampling_rate: float):
        """Change the frequency of the sensor.

        :param sampling_rate: (float) sampling rate of the sensor in Hz
        """
        self.sampling_rate = sampling_rate

        # write the change into parameter table of the database
        conn_6 = sqlite3.connect(str(self.database))
        c_6 = conn_6.cursor()
        c_6.executemany('insert into ' + self.name + '_parameters VALUES (?, ?, ?, ?)',
                        [(str(dt.datetime.now(tz=None)), 0, "sensor_range", json.dumps(self.sampling_rate))])

        conn_6.commit()
        c_6.close()

    def output(self, simulated_time: float):
        if self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            try:
                df = utilities.get_last_line_from_database_table(self.database, "test_value")
                w_x_0 = float(df["test_value"].tail(1))

                w_x_1 = w_x_0 * (1 + np.random.rand()*0.01)
                
                # write sensor output into the database
                columns_content = (str(dt.datetime.now(tz=None)), simulated_time, w_x_1)
                utilities.write_line_into_database_table(self.database, self.name, columns_content)

                print(self.name + ": " + str(w_x_1))

                #
                if time.time() - current_time_stamp < 1 / self.sampling_rate:
                    # time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
                    pass
                else:
                    print("script too slow for the set sampling rate of the Gyroscope")
            except Exception as e:
                print('No values yet for ' + self.name + ': ' + str(e))