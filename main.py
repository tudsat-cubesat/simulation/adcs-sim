import sqlite3
import time
import numpy as np
import environment
import dynamics
import internal_transfer
import orbit
import gyroscope
import acceleration_sensor
import magnetometer
import sunsensor
import earth_sensor
import json_grabber
import logging
from scipy.spatial.transform import Rotation as R
import fast_plot
from PyQt5.QtCore import QObject, pyqtSignal
import inspect
import yaml
import datetime as dt
import utilities
import json_grabber

class bcolors:
    """ Colors for console logs """ 
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class task_object:
    def __init__(self, sampling_rate: float = -1, task_mode: str = "main", 
            scope = None, component_dict:dict = dict(), param_key: int = 0):
        """
        :param sampling_rate: (float) sampling rate of the task object in 
            Hertz. If sampling_rate >= 0, then the task of the object will not
            be renewed automatically by the task loop.
        :param task_mode: (str)
        :param scope: None
        :param component_dict: (dict) 
        :param param_key: 
        """
        self.sampling_rate = sampling_rate
        self.scope = scope
        self.task_mode = task_mode
        self.component_dict = component_dict
        self.param_key = param_key

    def param_change(self, simulated_time: float = 0):
        """
        The method changing the parameters of a component. The new parameters
        are stored in the `param_key` property.

        param simuladed_time: (float) the current simulated time for the 
            parameter change
        """
        parameters = self.component_dict["parameters"][self.param_key]
        # check for name
        if "name" not in self.component_dict:
            self.scope.log.critical(str(simulated_time)
                 + " ss: no `name` attribute for component. "
                "Parameter change unsuccessful.")
            return
        # check for database
        elif "database" in parameters:
            database: str = parameters["database"]
        elif self.component_dict["name"] in self.scope.comp_dict \
                and "database" in self.scope.comp_dict[
                self.component_dict["name"]]["parameters"]:
            database: str = self.scope.comp_dict[self.component_dict[
                "name"]]["parameters"]["database"]
        else:
            self.scope.log.critical(str(simulated_time)
                 + " ss: no `database` parameter for component named "
                + self.component_dict["name"] + ". Parameter change"
                " unsuccessful.")
            return
        # check for module
        if "module" in self.component_dict:
            module_name: str = self.component_dict["module"]
        elif self.component_dict["name"] in self.scope.comp_dict \
                and "module" in self.scope.comp_dict[self.component_dict[
                "name"]]:
            module_name: str = self.scope.comp_dict[self.component_dict[
                "name"]]["module"]
        else:
            self.scope.log.critical(str(simulated_time)
                 + " ss: no `module` attribute for component named "
                + self.component_dict["name"] + ". Parameter change"
                " unsuccessful.")
            return
        try:
            module = globals()[module_name]
        except:
            self.scope.log.critical(str(simulated_time)
                + " ss: no module found with name `" + module_name
                + "` for component named " + self.component_dict["name"]
                + ". Parameter change unsuccessful.")
            return
        # check for class
        if "class" in self.component_dict:
            class_name: str = self.component_dict["class"]
        elif self.component_dict["name"] in self.scope.comp_dict \
                and "class" in self.scope.components[self.component_dict[
                "name"]]:
            class_name: str = self.scope.components[self.component_dict[
                "name"]]["class"]
        else:
            self.scope.log.critical(str(simulated_time)
                 + " ss: no `class` attribute for component named "
                + self.component_dict["name"] + ". Parameter change"
                " unsuccessful.")
            return

        self.scope.log.debug(str(simulated_time)
                 + " ss: new parameters:\n" + yaml.dump(parameters))

        utilities.write_line_into_database_table(
            database = database,
            name = self.component_dict["name"] + "_parameters",
            columns_content = parameters)


class Main(QObject):
    finished_prepare = pyqtSignal( )
    finished_simulate = pyqtSignal( )

    def __init__(self):

        super( ).__init__( )
        # self.logging levels (from [self.logging HOWTO]
        # (https://docs.python.org/3/howto/self.logging.html#self.logging-basic-tutorial)):

        # - DEBUG: Detailed information, typically of interest only when 
        #       diagnosing problems.
        # - INFO: (recomended) Confirmation that things are working as 
        #       expected.
        # - WARNING:  An indication that something unexpected happened, or 
        #       indicative of some problem in the near future (e.g. ‘disk 
        #       space low’). The software is still working as expected.
        # - ERROR: Due to a more serious problem, the software has not been 
        #       able to perform some function.
        # - CRITICAL: A serious error, indicating that the program itself may 
        #       be unable to continue running.

        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)
        self.log.setLevel(logging.INFO)

        self.task_list: list = []
        self.components: dict = {}
        self.comp_dict: dict = {}
        self.parameters = ""
        self.orbit_env_outputs = ""
        self.simulation_outputs = ""
        self.progress_value = 0


        """
        variable_str[0]: parameter value
        variable_str[1]: variable name
        """
        self.variable_list = [
            ('sim_time_offset','sim_time_offset'),
            ]


    def output_progress(self, steps_total: int, simulated_time_position: float,
                        last_simulated_time_position: float, simulated_duration: float):
        """
        Outputs the progress as a logging output in the console in percentage 
        values.
        """
        if steps_total <= 0:
            return
        aa: float = simulated_duration / steps_total
        bb: float = simulated_time_position % aa
        cc: float = last_simulated_time_position % aa
        if bb < cc:
            value = np.floor(simulated_time_position / simulated_duration * 100)
            self.log.info(bcolors.OKBLUE + str(value) + "%"
                         + "==============================================" + bcolors.ENDC)
            self.progress_value = (50 + value/2)



    def task_loop(self):
        """
        Organizes the calls of the different components via the task_list.
        """

        self.prepare_simulation()

        self.log.info(bcolors.HEADER + "starting simulation…"
             + bcolors.ENDC)
        if self.real_time_factor > 0:
            real_time_position: float = self.simulated_time_position / self.real_time_factor
            real_time_start: float = time.time() - real_time_position
            estimated_real_time_end: float = (real_time_start +
                                              self.simulated_duration / self.real_time_factor)
            self.log.info(
                bcolors.OKBLUE + "Estimated simulation end: "
                + str(time.gmtime(estimated_real_time_end)[0:6]) + ", in "
                + str(self.simulated_duration * self.real_time_factor) + " seconds."
                + bcolors.ENDC)
        else:
            real_time_start: float = time.time()

        i: int = 0
        task_count: int = 0
        if self.real_time_factor > 0:
            schedule_warning_max: int = 100
        last_simulated_time_position = self.simulated_time_position
        while self.simulated_time_position < self.simulated_duration and len(self.task_list) > 0:
            i += 1

            self.output_progress(
                steps_total=np.maximum(5, self.simulated_duration * self.real_time_factor),
                simulated_time_position=self.simulated_time_position,
                last_simulated_time_position=last_simulated_time_position,
                simulated_duration=self.simulated_duration)
            last_simulated_time_position = self.simulated_time_position
            # get next step

            next_time_position: float = self.task_list[-1][0]
            next_step: float = next_time_position - self.simulated_time_position
            if next_step > 0:
                # wait till next task...

                self.log.debug("wait for " + str(next_step)
                              + " ss (simulated seconds) till next time position: "
                              + str(next_time_position) + ", now is: "
                              + str(self.simulated_time_position))

                if self.real_time_factor > 0:
                    real_time_position = time.time() - real_time_start
                    next_time_position_realscaled: float = (
                            next_time_position * self.real_time_factor)
                    sleep_time: float = (
                            next_time_position_realscaled - real_time_position)
                    if sleep_time > 0:
                        self.log.debug("sleep for " + str(sleep_time) + " s…")
                        time.sleep(sleep_time)
                    else:
                        warning_text: str = (
                                bcolors.WARNING
                                + "task for " + str(self.task_list[-1][1])
                                + " is \n" + bcolors.BOLD
                                + str(round((-1 * sleep_time), 5))
                                + " s " + bcolors.ENDC + bcolors.WARNING
                                + "behind schedule: \ndue simulated_time: "
                                + str(round((self.task_list[-1][0]), 5))
                                + ", \nnow: "
                                + str(round((real_time_position
                                    / self.real_time_factor), 5))
                                + bcolors.ENDC)
                        if schedule_warning_max > 0:
                            self.log.warning(warning_text)
                            schedule_warning_max -= 1
                        elif schedule_warning_max == 0:
                            self.log.warning(bcolors.WARNING +
                                            "maximum number of schedule warnings exceeded"
                                            + bcolors.ENDC)
                            self.log.debug(warning_text)
                            schedule_warning_max -= 1
                        else:
                            self.log.debug(warning_text)

                self.simulated_time_position += next_step
            else:
                # time is up for task!
                self.log.debug(
                    "--------------------------------------------------------")
                for line in self.task_list:
                    self.log.debug("task: " + str(line[0]) + " – " + str(line[1]))
                self.log.debug(
                    "--------------------------------------------------------\n")

                task = self.task_list.pop()
                task_count += 1
                task_duration = -time.perf_counter()
                task_name = str(task[1])
                self.run_task(task)
                task_duration += time.perf_counter()
                self.log.debug(task_name + ": taken total " + bcolors.WARNING +
                              str(task_duration) + "s\n" + bcolors.ENDC)
                self.add_task(*self.get_next_task(old_task=task))

        # simulation end
        self.progress_value = 100
        real_time_position = time.time() - real_time_start
        self.log.debug("")
        self.log.info(bcolors.OKGREEN +
                     "=============================================================="
                     + bcolors.ENDC)
        self.log.info(bcolors.OKGREEN +
                     "==============================================================\n"
                     + bcolors.ENDC)
        self.log.info("real time duration: " + str(real_time_position) + " seconds")
        self.log.info("loop operations: " + str(i))
        self.log.info("tasks handled: " + str(task_count) + "\n\n")

        self.end()


    def update_simulation(self, parameters:dict):
        """
        changes the parameters of the instanced components of the simulation 
        while it is still running. The method instances an new task_object for 
        every component that gets its parameters changed and adds this task to 
        the task_list. 

        :param parameters: (dict) Parameters dictionary derived from a changed
            configuration
        """

        components_list: list = list()
        # simulation components
        try:
            sim = parameters["simulation"]
        except KeyError:
            pass
        else:
            try:
                simcomp = sim["components"]
            except KeyError:
                pass
            else:
                if type(simcomp) == list and len(simcomp) > 0:
                    components_list.extend(simcomp)
        # component tables
        try:
            comp_tabs = parameters["component_tables"]
        except KeyError:
            pass
        else:
            components_list.extend(comp_tabs.values())

        # clean components
        for i, component in enumerate(components_list):
            if "parameters" in component:
                param_type = type(component["parameters"])
                if param_type == dict:
                    component["parameters"] = [component["parameters"].copy(),]
                elif param_type == list:
                    # expected behavior
                    pass
                else:
                    # error
                    log.info("Parameter Change: parameters type is "
                        + str(param_type)
                        + ", should be list or dict. Component: \n"
                        + yaml.dump(component, sort_keys=False, 
                            default_flow_style=False))
                    continue
                for param in component["parameters"]:
                    if "simulation_time" not in param:
                        param["simulation_time"] = self.simulated_time_position
                    if "real_time" not in param:
                        param["real_time"] = str(dt.datetime.now(tz=None))
                    if "name" not in component:
                        if "name" in param:
                            component["name"] = param["name"]
                        else:
                            components_list.pop(i)
                            self.log.critical(bcolors.FAIL + "Parameter Change "
                                "failed for component (no name): \n"
                                + yaml.dump(component) + bcolors.ENDC)
                            continue
                    for key, parameter in param.items():
                        if type(parameter) == list or type(parameter) == dict:
                            param[key] = str(parameter)
            else:
                components_list.pop(i)
                self.log.critical(bcolors.FAIL + "Parameter Change "
                    "failed for component (no parameters): \n"
                    + yaml.dump(component) + bcolors.ENDC)


        self.log.info(bcolors.BOLD + "Parameter Change: \n" + bcolors.ENDC
            + yaml.dump(components_list) )
        # create param change tasks
        for component in components_list:
            for i, param in enumerate(component["parameters"]):
                # DIRTY FIX BEGIN
                if component["name"] == "dynamics":
                    continue
                # DIRTY FIX END
                comp_task_obj = task_object(
                    task_mode = "object",
                    scope = self,
                    component_dict = component,
                    param_key = i)
                self.add_task(
                    param["simulation_time"],
                    comp_task_obj,
                    "param_change")



    def add_task(self, simulated_time: float, task_object, method_name: str):
        """
        Adds a task to the task_list if it has a valid simulated time.

        :param simulated_time: (float) due simulated time for the task to be
            executed
        :param task_object: (object) the containing object of the method
        :param method_name: (string) the name of the method to be executed
        """
        if simulated_time >= 0:
            self.task_list.append((simulated_time, task_object, method_name))
            # sort with decreasing simulated time
            self.task_list.sort(key=lambda i: i[0], reverse=True)
        else:
            self.log.debug("add_task: No following tasks " + bcolors.BOLD
                          + method_name + bcolors.ENDC + " for " + bcolors.BOLD
                          + str(task_object) + bcolors.ENDC)


    def run_task(self, task: tuple):
        """
        Executes the specified task.

        :param task: (tuple) a tuple with the length of three consisting of
            1: simulated time: (float) due simulated time for the task to be
            executed
            2: task object: (object) the containing object of the method
            3: method_name: (string) the name of the method to be executed
        """
        simulated_time: float = task[0]
        task_object = task[1]
        method_name: str = task[2]
        if task_object == None:
            try:
                globals()[method_name](simulated_time)
            except Exception as e:
                self.log.critical(str(Exception))
            else:
                pass
        else:
            task_mode = getattr(task_object, "task_mode", "object")
            if task_mode == "object":
                method = getattr(task_object, method_name, False)
                if method != False:
                    method(simulated_time)
                else:
                    self.log.critical(bcolors.FAIL + "run_task: No method found"
                                                    " with name " + method_name + " and object "
                                     + str(task_object) + bcolors.ENDC)
            elif task_mode == "main":
                try:
                    globals()[method_name](simulated_time)
                except Exception as e:
                    self.log.critical(str(Exception))


    def get_next_task(self, old_task: tuple) -> tuple:
        """
        Creates a new tuple basing on the specified old task. The new task gets
        a due simulated time depending on the sampling rate of the task object.

        :param old_task: (tuple) a tuple with the length of three consisting of
            1: simulated time: (float) due simulated time for the task to be
            executed
            2: task object: (object) the containing object of the method
            3: method_name: (string) the name of the method to be executed
        """
        old_simulated_time: float = old_task[0]
        task_object = old_task[1]
        method_name: str = old_task[2]
    
        sampling_rate: float = getattr(task_object, "sampling_rate", False)
    
        if sampling_rate > 0:
            new_simulated_time: float = old_simulated_time + 1 / sampling_rate
        else:
            # no more following tasks
            new_simulated_time = -1
    
        return (new_simulated_time, task_object, method_name)


    def get_time_parameters(self) -> tuple:
        """
        Returns the time parameters from the parameters property.
        """
        simulated_time_start = self.parameters["time_parameters"]["simulated_time_start"]
        real_time_factor = self.parameters["time_parameters"]["real_time_factor"]
        simulated_duration = self.parameters["time_parameters"]["simulated_duration"]
        sim_time_offset = time.gmtime(simulated_time_start)[0:6]
        return (simulated_time_start, real_time_factor, simulated_duration,
            sim_time_offset)

    def filter_arguments(self, function, arguments: dict) -> tuple:
        """
        returns arguments from arguments specified in dict `arguments` 
        without arguments not specified by the function.
        """
        filtered_arguments = {
            k: v for k, v in arguments.items() if k in [
                p.name for p in inspect.signature(function
                    ).parameters.values()]}
        return filtered_arguments

    def filtered_call(self, function, arguments: dict):
        """
        calls `function` with arguments specified in dict `arguments` 
        without arguments not specified by the function.
        """
        return function(**self.filter_arguments(function, arguments))

    def replace_str_with_variables(self, parameter:dict):
        """
        Replaces variable names with variable values specified in property 
        variable_list.

        :param parameter: (dict) Parameter set to be processed
        """
        for variable_str in self.variable_list:
            if parameter == variable_str[0]:
                self.log.debug("variable string " + str(variable_str[0]) + ": " + str(getattr(self, variable_str[1])))
                return getattr(self, variable_str[1])
        return parameter

    def instance_component(self, component_name:str, module_name:str, 
            class_name:str, parameters:dict):
        """
        Instances a component with a dictionary as parameter set savely, even
        if there are not used parameters by the function.
        """
        self.comp_dict[component_name] = {
            "name": component_name,
            "module": module_name,
            "class": class_name,
            "parameters": parameters.copy()}
    
        for key, val in parameters.items():
            if type(val) == str:
                parameters[key] = self.replace_str_with_variables(val)
        if "simulation_time" in parameters:
            if parameters["simulation_time"] != 0:
                self.log.warning(bcolors.WARNING + "instance_component: First"
                    " parameter set of component " + class_name + " named " 
                    + component_name + " is not set for "
                    "simulation time = 0 but for simulation time = "
                    + str(parameters["simulation_time"]) + bcolors.ENDC)
                return

        module = globals()[module_name]

        self.components[component_name] = self.filtered_call(
            function = getattr(module, class_name),
            arguments = parameters)

        self.log.info("Instancing component "
            "successfull: " + module_name + " " + class_name + " named `"
            + component_name + "`")


    # Initialisation
    # ==============

    def start_presimulation(self):
        """
        Executes the pre-simulation.
        """
        self.progress_value = 0

        (self.simulated_time_start, self.real_time_factor, self.simulated_duration,
            self.sim_time_offset) = self.get_time_parameters()

        self.log.debug(bcolors.BOLD + bcolors.OKGREEN + "final_parameters:\n"
            + yaml.dump(self.parameters, sort_keys=False,
            default_flow_style=False)
            + bcolors.ENDC)

        self.log.info(bcolors.HEADER + "starting presimulation…"
             + bcolors.ENDC)



        # Instancing
        # ----------

        # Outputs

        for key, output in self.parameters["pre_simulation"]["outputs"
                ].items():
            setattr(self, key, internal_transfer.OutputLists(
                output["database_presim"],
                output["database_sim"],
                output["orbit_env"]))
            self.variable_list.append((key, key))

        # Components

        for component in self.parameters["pre_simulation"]["components"]:
            self.log.debug(bcolors.BOLD + bcolors.OKCYAN + "component:\n"
                + yaml.dump(component, sort_keys=False,
                default_flow_style=False)
                + bcolors.ENDC)
            try:
                component["parameters"]
            except KeyError:
                self.log.warning(bcolors.WARNING + "no parameters for"
                    "component " + component["name"] + " in pre_simulation."
                    " Initialisation of the component impossible. Continue "
                    "with folloing…" + bcolors.ENDC)
            else:
                self.instance_component( component_name = component["name"],
                    module_name = component["module"], 
                    class_name = component["class"], 
                    parameters = component["parameters"].copy())

        self.log.info(bcolors.BOLD + "simulation time parameters:\n"
            "simuated_time_start: " + str(self.simulated_time_start)
            + "\nreal_time_factor: " + str(self.real_time_factor)
            + "\nsimulated_duration: " + str(self.simulated_duration) 
            + "\n(sim_time_offset: " + str(self.sim_time_offset) + ")" + bcolors.ENDC)


        # Pre Simulation
        # --------------

        for component in self.parameters["pre_simulation"]["components"]:
            self.log.info(bcolors.OKBLUE + "calculate " + component["name"]
            + "…" + bcolors.ENDC)
            try:
                if component["class"] == "Orbit":
                    getattr(self.components[component["name"]], 
                        component["method"])(max_time=self.simulated_duration)
                else:
                    getattr(self.components[component["name"]], 
                        component["method"])()
            except Exception as e:
                raise e

        self.log.info(bcolors.HEADER + "presimulation completed.\n"
             + bcolors.ENDC)
        self.finished_prepare.emit()
        self.progress_value = 50


    def prepare_simulation(self):
        """
        Prepares the main simulation as it instances components and adds its
        tasks. Does not start the simulation.
        """

        self.log.info(bcolors.HEADER + "preparing simulation…" + bcolors.ENDC)

        # time properties
        # ---------------


        (self.simulated_time_start, self.real_time_factor, 
            self.simulated_duration, self.sim_time_offset
            ) = self.get_time_parameters()


        # Instancing
        # ----------


        # Outputs

        for key, output in self.parameters["simulation"]["outputs"
                ].items():
            setattr(self, key, internal_transfer.OutputLists(
                output["database_presim"],
                output["database_sim"],
                output["orbit_env"]))
            self.variable_list.append((key, key))

        # Components

        for component in self.parameters["simulation"]["components"]:
            self.log.debug(bcolors.BOLD + bcolors.OKCYAN + "component:\n"
                + yaml.dump(component, sort_keys=False,
                default_flow_style=False)
                + bcolors.ENDC)
            try:
                component["parameters"]
            except KeyError:
                self.log.warning(bcolors.WARNING + "no parameters for"
                    "component " + component["name"] + " in simulation."
                    " Initialisation of the component impossible. Continue "
                    "with folloing…" + bcolors.ENDC)
            else:
                self.instance_component( component_name = component["name"],
                    module_name = component["module"], 
                    class_name = component["class"], 
                    parameters = component["parameters"])


        self.simulated_time_end: float = self.simulated_time_start + self.simulated_duration

        try:
            self.simulated_time_position
        except AttributeError:
            self.simulated_time_position: float = 0.0
        if self.simulated_time_position >= self.simulated_duration:
            self.simulated_time_position = 0.0


        # Task Creation
        # -------------


        for component in self.parameters["simulation"]["components"]:
            self.log.info(bcolors.OKBLUE + "Adding task for "
                + component["name"] + "…" + bcolors.ENDC)
            self.add_task(
                simulated_time=component["start_simulation_time"],
                task_object=self.components[component["name"]],
                method_name="output")

        # Parameter Changes

        self.update_simulation({"component_tables": self.parameters[
            "component_tables"]})

        self.log.info(bcolors.HEADER + "Simulation prepared.\n" + bcolors.ENDC)

    # Task Loop
    # =========

    def start(self):
        """ Starts orbit and env simulation without sensor simulation."""
        self.start_presimulation()

    def start_all(self):
        """ Starts orbit and env simulation and sensor simulation afterwards"""

        self.start_presimulation()
        self.task_loop()


    def end(self):
        """
        Is executed at the end of the task_loop.
        """
        for key, output in self.parameters["simulation"]["outputs"
                ].items():
            getattr(self, key).write_into_sensor_database()


        self.log.info(bcolors.HEADER + "simulation completed."
             + bcolors.ENDC)
        self.finished_simulate.emit( )



if __name__ == "__main__":
    """
    If main.py is called independenly from GUI, the following commands
    are called as a replacement of the actions performed by the GUI:

     - loading of the default configuration ("default.yaml")
     - loading of custom configuration (defaults to "")
     - parsing the parameters and calling the functions 
        `start_presimulation`, `prepare_simulation` subsequently
     - asking for start of task loop and executing `task_loop`

    """
    start_sim_automatically = False
    a = Main()
    import config_loader


    parameters = config_loader.get_config("test.yaml")
    a.parameters = parameters
    a.start()
    print("type(main): " + str(type(a)))


    if start_sim_automatically == False:
        start_sim_input = input(bcolors.BOLD + bcolors.OKBLUE + "Start simulation? (Y/n)\n")
    else:
        start_sim_input = "Y"
    if start_sim_input in ["Y", "y", ""]:
        a.task_loop()
    else:
        a.log.info("Simulation aborted.")