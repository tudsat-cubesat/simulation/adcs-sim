from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

import collections
import random
import time
import math
import numpy as np
import sqlite3
import utilities
import utilities


class DynamicPlotter():

    def __init__(self, sampleinterval=0.1, timewindow=10., size=(600,350)):
        # Data stuff
        self._interval = int(sampleinterval*100)
        self._bufsize = int(timewindow/sampleinterval)
        self.databuffer = collections.deque([0.0]*self._bufsize, self._bufsize)
        self.x = np.linspace(-timewindow, 0.0, self._bufsize)
        self.y = np.zeros(self._bufsize, dtype=float)
        # PyQtGraph stuff
        self.app = QtGui.QApplication([])
        self.app.setStyle('Windows')
        self.plt = pg.plot(title='Dynamic Plotting with PyQtGraph')
        self.plt.resize(*size)
        self.plt.showGrid(x=True, y=True)
        self.plt.setLabel('left', 'amplitude', 'V')
        self.plt.setLabel('bottom', 'time', 's')
        self.curve = self.plt.plot(self.x, self.y, pen=(255,0,0))
        # QTimer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(self._interval)

    def updateplot(self):
        self.databuffer.append(float(utilities.get_last_line_from_database_table('test.db', 'angular_components')[["omega_x"]].to_numpy()))
        self.y[:] = self.databuffer
        self.curve.setData(self.x, self.y)
        self.app.processEvents()

    def run(self):
        self.app.exec_()


def test_write():
    i = 0
    while True:
        conn_10 = sqlite3.connect('test.db')
        c_10 = conn_10.cursor()
        try:
            c_10.execute('create table angular_components (omega_x, omega_y, omega_z, alpha_x, alpha_y, alpha_z)')
        except:
            pass
        c_10.executemany('insert into angular_components VALUES (?, ?, ?, ?, ?, ?)',
                         [(np.sin(i * np.pi / 18), np.sin(i * np.pi / 18 + np.pi / 4),
                           np.sin(i * np.pi / 18 + np.pi / 2), 0, 0, 0)])
        conn_10.commit()
        c_10.close()
        i += 1
        time.sleep(0.01)
if __name__ == '__main__':
    import multiprocessing as mp




    mp.set_start_method('spawn')
    t1 = mp.Process(target=test_write, args=())
    t1.start()

    m = DynamicPlotter(sampleinterval=0.05, timewindow=10.)
    m.run()