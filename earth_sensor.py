import numpy as np
from numpy import linalg as la
import math
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.spatial.transform import Rotation as R
from pyquaternion import Quaternion
import sympy as sym
import sqlite3
from Sensor import Sensor
import utilities
import datetime as dt
import json
import time
import logging
import pandas as pd


# TODO  Pitch and Roll d.h. rotation around x,y axis, nadir angle im sensor system bestimmen nicht im sat system evtl position + nadir vector

class EarthSensor(Sensor):

    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, digital: bool, mode,
                 outputs, fov_shape: str, fov, detected_area: float, accuracy: np.array, sampling_rate: float,
                 active: bool = True):
        """

        :param name: (string) name of the sensor
        :param position: (np.array 3x1) position of the sensor on the satellite in satellite frame
        :param orientation: (np.array 4x1) orientation of the sensor in quaternions (q_r, q_i, q_j, q_k)
        :param database: (string) name of the database
        :param digital: (bool)
        :param mode: (bool)
        :param outputs: (class object) class object of the internal_transfer class in the main script
        :param fov_shape: (string) describes the shape of the field of view and can either be a 'circle'
                or a 'rectangle'
        :param fov: for circular (float) in degrees for rectangle (list) with [Vertical, Horizontal] in degrees
        :param active_area: (float) area which has to be detected to provide sensor output
        :param accuracy: (np.array)
        :param sampling_rate: (float)
        :param active: (bool)
        """
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)

        self.fov_shape = fov_shape
        if self.fov_shape == 'circle':
            self.fov = fov * np.pi / 180
        elif self.fov_shape == 'square':
            self.fov = np.array(fov) * np.pi / 180
        self.active_area = detected_area
        self.acc = accuracy
        self.sampling_rate = sampling_rate
        self.list = []
        self.column_names = ['real_time', 'simulation_time', 'Real_Angle', 'Estimated_Angle']

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

        # create dataframe
        self.outputs.output_dict[self.name] = \
            pd.DataFrame(columns=["real_time", "simulation_time", "Real_Angle", "Estimated_Angle"])

    def get_nadirvector(self, pos_sat, orientation_sat):
        # Function returns the Nadir Vector in Sat Coordinates
        orientation_earth_to_sat = orientation_sat.inverse

        nadir_r_in = -pos_sat
        nadir_r_sat = orientation_earth_to_sat.rotate(nadir_r_in)

        logging.debug('Nadir_sat ' + str(nadir_r_sat))
        return nadir_r_sat

    def is_earth_in_fov(self, r_ell, pos_sat, orientation_sat):
        # Center of Earth Disk is defined by the Nadir Vector
        nadir_r_sat = self.get_nadirvector(pos_sat, orientation_sat)

        # Calculation of the Earth Disk Radius r_disk
        # Earth is seen as an ellipsoid and the Earth Radius at the Horizon is simplified as the Ellipsoid Radius
        # beneath the satellite
        rho = np.arcsin(r_ell / (la.norm(pos_sat)))

        # Transformation from Sensor -> Satellite Frame
        tr_sen_to_sat = Quaternion(self.orientation)
        tr_sat_to_sen = tr_sen_to_sat.inverse
        # Transformation of Nadir vector Satellite -> Sensor Frame
        nadir_r_sen = tr_sat_to_sen.rotate(nadir_r_sat)

        # Definition of Boresight Vector and Projections
        bore_sen = np.array([1, 0, 0])
        xz_sen = np.array([1, 0])
        xy_sen = xz_sen

        # Nadir Vector from sensor origin
        nadir_sen_0 = tr_sat_to_sen.rotate(np.array(nadir_r_sat - np.array(self.position)))

        nadir_xz_sen = np.array([nadir_sen_0[0], nadir_sen_0[2]])
        nadir_xy_sen = np.array([nadir_sen_0[0], nadir_sen_0[1]])
        ang_bn = np.arccos((np.matmul(nadir_sen_0, bore_sen)) / (la.norm(nadir_sen_0) * la.norm(bore_sen)))
        ang_bn_xy = np.arccos((np.matmul(nadir_xy_sen, xy_sen) / (la.norm(nadir_xy_sen) * la.norm(xy_sen))))
        ang_bn_xz = np.arccos((np.matmul(nadir_xz_sen, xz_sen) / (la.norm(nadir_xz_sen) * la.norm(xz_sen))))
        self.list = np.array([ang_bn, ang_bn_xy, ang_bn_xz])
        if self.fov_shape == 'circle':
            if ang_bn <= rho - self.fov / 2:
                logging.debug('Only Earth is recognized by Sensors fov')
                return False
            elif ang_bn >= rho + self.fov / 2:
                logging.debug('Earth is not recognized by Sensors fov because Sensor is mounted towards space')
                return False
            else:
                logging.debug('Horizon is recognized')

                # Comparison of the EarthDisk Area Vector and the fov Area Vector
                # to determine the area recognized by the Sensor
                if self.active_area > 0 and self.fov < np.pi:
                    area_fov = 2 * np.pi * (1 - np.cos(self.fov / 2))
                    area = 2 * (np.pi - np.cos(rho) *
                                np.arccos((np.cos(self.fov / 2) - np.cos(rho) * np.cos(ang_bn)) / (
                                        np.sin(rho) * np.sin(ang_bn)))
                                - np.cos(self.fov / 2) *
                                np.arccos((np.cos(rho) - np.cos(self.fov / 2) * np.cos(ang_bn)) / (
                                        np.sin(self.fov / 2) * np.sin(ang_bn)))
                                - np.arccos(
                                (np.cos(ang_bn) - np.cos(self.fov / 2) * np.cos(rho)) / (
                                        np.sin(self.fov / 2) * np.sin(rho))))
                    logging.debug('Area of sensor which detects earth surface' + str(area / area_fov * 100) + '%')

                    if area / area_fov <= self.active_area:
                        logging.debug('Not enough pixels detected')
                        return False

                return True

        elif self.fov_shape == 'rectangle':
            if ang_bn_xy <= rho - self.fov[1] / 2 and ang_bn_xz <= rho - self.fov[0] / 2:
                logging.debug('Only Earth is recognized by Sensors fov')
                return False
            elif ang_bn_xy >= rho + self.fov[1] / 2 and ang_bn_xz >= rho + self.fov[0] / 2:
                logging.debug('Earth is not recognized by Sensors fov because Sensor is mounted towards space')
                return False
            else:
                logging.debug('Horizon is recognized')
                return True

    def nadir_angle_estimation(self, pos_sat, orientation_sat, r_ell):
        """ The function measures the angle between the sensors boresight and the nadir angle if the Earth Ellipsoid
        is in the field of view of the sensor."""

        sigma_rad = self.acc[0] * math.pi / 180 / self.acc[1]
        if self.is_earth_in_fov(r_ell, pos_sat, orientation_sat) is True:
            ang_bn = self.list[0] * 180/np.pi
            ang_bn_est = np.random.normal(loc=ang_bn, scale=sigma_rad)
            logging.debug('Real angle ' + str(ang_bn) + ' Estimated angle '
                          + str(ang_bn_est))
        else:
            ang_bn = self.list[0] * 180/np.pi
            ang_bn_est = 0.0

        return ang_bn, ang_bn_est

    def output(self, sim_time: float):
        # check for updates in the parameters
        self.check_parameter_update()

        if self.active is True:
            # Read Position of Satellite in km from Database
            # df = utilities.interpolate_two_rows(self.database, name='position', sim_time=sim_time)
            # pos_sat = df.to_numpy()[0][1:].reshape(3)
            # logging.debug('Position_sat ' + str(pos_sat))
            df = utilities.interpolate_dataframe(self.outputs.satellite_position, sim_time)
            pos_sat = df.to_numpy()[1:].reshape(3)

            # Read Orientation of Satellite from Database
            df = utilities.interpolate_dataframe(pd.read_json('json_communication/state_sat.json', orient='records',
                                                              lines=True), sim_time)
            orientation_sat = df.to_numpy().reshape(8)
            orientation_sat = Quaternion(orientation_sat[2:6])

            # Read Earth Disk Radius in m from Database and convert it to km
            df = utilities.interpolate_dataframe(self.outputs.radius_ellipsoid, sim_time)
            r_ell = df.to_numpy().reshape(2)
            r_ell = r_ell[1] / 1000
            logging.debug('Earth Disk Ellipsoid ' + str(r_ell))

            # Get output
            ang_real, ang_est = self.nadir_angle_estimation(pos_sat, orientation_sat, r_ell)

            # write sensor output into the database
            columns_content = (str(dt.datetime.now(tz=None)), sim_time, ang_real, ang_est)
            # utilities.write_line_into_database_table(self.database, self.name, columns_content)
            self.outputs.safe_output(name=self.name,
                                     column_names=self.column_names,
                                     column_content=list(columns_content))
            return ang_real, ang_est

    def run(self):
        """ Starts sensor in while Loop."""

        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output(0.1)
            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                logging.warning("script too slow for the set sampling rate of the " + self.name)


if __name__ == "__main__":
    import threading
    import internal_transfer
    import logging

    #logging.getLogger().setLevel(logging.DEBUG)

    h_ec = np.sqrt(((6378.136 + 500) * 1000) ** 2 / 3)

    ori = np.array([1, 0, 0, 0])
    a = EarthSensor(outputs=internal_transfer.OutputLists('presim.db',
                                                          'simdata.db'),
                    name='earth_sensor',
                    position=np.array([0.001, 0.001, 0.001]),
                    orientation=np.array([0.0, 0.0, 0.7071, -0.7071]),
                    database='test.db',
                    digital=True,
                    mode=1,
                    fov_shape='circle',
                    fov=160,
                    detected_area=1,
                    accuracy=np.array([2, 1]),
                    sampling_rate=1.,
                    active=True)
    for i in np.linspace(0, 1, 1):
        a.output(i)
