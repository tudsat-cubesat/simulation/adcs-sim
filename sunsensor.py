import numpy as np
import utilities
import environment
from orbit import Orbit
from Sensor import Sensor
import json
import pandas as pd
import sqlite3
from pyquaternion import Quaternion
import time
import datetime as dt
import random

from statistics import stdev

"""
TODO:

- [ ] change `utilities.create_table('test.db',…)` with table creation 
      command with self.database instead of `'test.db'`
"""

class Sunsensor(Sensor):
    # pass

    # utilities.create_table('test.db', 'sat_to_sun',
    #                        'real_time, simulation_time, sat_to_sun_x, sat_to_sun_y, sat_to_sun_z')

    def satellite_to_sun(self, sim_time, commit: bool = True):
        """
        returns a unit vector pointing to the sun in the satellite frame.
        """

        # df = utilities.interpolate_two_rows(self.database, name='state_sat', sim_time=sim_time)
        df = utilities.interpolate_dataframe(
            pd.read_json('json_communication/state_sat.json', orient='records',
                         lines=True),
            sim_time)
        attitude = df.to_numpy()[1:].reshape(7)
        attitude = Quaternion(attitude[0:4])
        # print('attitude= ', attitude)

        # connect to database and read earth to sun vector in TEME frame
        # df = utilities.interpolate_two_rows(self.database, name='earth_to_sun', sim_time=sim_time)
        df = utilities.interpolate_dataframe(self.outputs.earth_to_sun,
            sim_time)
        earth_to_sun_TEME = df.to_numpy()[1:].reshape(3)
        earth_to_sun_TEME = earth_to_sun_TEME * 1.495978707 * 10 ** 8  # convert from AU in km
        # print('earth_to_sun_TEME= ', earth_to_sun_TEME)

        # connect to database and get the satellite position in TEME
        # df = utilities.interpolate_two_rows(self.database, name='position', sim_time=sim_time)
        df = utilities.interpolate_dataframe(self.outputs.satellite_position,
                                             sim_time)
        sat_position_TEME = df.to_numpy()[1:].reshape(3)
        # print('sat_position_TEME= ', sat_position_TEME)
        # sat_position_TEME = np.array([float(df_pos["x"]), float(df_pos["y"]), float(df_pos["z"])])

        sat_to_sun_TEME = earth_to_sun_TEME - sat_position_TEME
        # print('sat_to_sun_TEME= ', sat_to_sun_TEME)
        sat_to_sun_body = attitude.rotate(sat_to_sun_TEME)  # satellite to sun vector in satellite frame
        sat_to_sun = sat_to_sun_body / np.linalg.norm(sat_to_sun_body)  # normalized sat_to_sun vector
        # print('sat_to_sun= ', sat_to_sun)

        # if commit:
        #     conn = sqlite3.connect(self.database)
        #     c = conn.cursor()
        #     c = c.execute("""SELECT EXISTS (SELECT 1 FROM sat_to_sun WHERE simulation_time=? LIMIT 1)""",
        #                     (sim_time,)).fetchone()[0]
        #
        #     if c:
        #         pass
        #     else:

        column_names = ['real_time', 'simulation_time', 'sat_to_sun_x', 'sat_to_sun_y', 'sat_to_sun_z']
        columns_content = (str(dt.datetime.now(tz=None)), sim_time, sat_to_sun[0], sat_to_sun[1], sat_to_sun[2])
        # utilities.write_line_into_database_table('test.db', 'sat_to_sun', columns_content)
        self.outputs.safe_output('sat_to_sun', column_names,
                                 list(columns_content))


        return sat_to_sun

    def is_side_in_sun(self, orientation, sat_to_sun):
        """
        If dot product <=0: the angle between the normal vector of the side where the sensor
        is mounted and satellite to sun vector is >= 90°.
        Therefore true if sensor is illuminated by the sun
        // Assuming that sensor normal vector is identical to
        normal vector of side of satellite where the sensor is mounted.
        """
        dot = np.dot(sat_to_sun, orientation)

        if dot <= 0:
            return False,
        else:
            return True,

    def compute_sunvector_from_photo_currents(self, sim_time, photo_currents):
        """Equation to determine sunvector from:
        Fundamentals of Spacecraft Attitude Determination by Markley and Crassidis (eq. 4.22) """

        photo_current_Xpos = float(photo_currents[0])
        photo_current_Xneg = float(photo_currents[1])
        photo_current_Ypos = float(photo_currents[2])
        photo_current_Yneg = float(photo_currents[3])
        photo_current_Zpos = float(photo_currents[4])
        photo_current_Zneg = float(photo_currents[5])

        photo_normal1 = [1, 0, 0]
        photo_normal2 = [0, 1, 0]
        photo_normal3 = [0, 0, 1]

        a = (photo_current_Xpos - photo_current_Xneg) * np.cross(photo_normal2, photo_normal3) + \
            (photo_current_Ypos - photo_current_Yneg) * np.cross(photo_normal3, photo_normal1) + \
            (photo_current_Zpos - photo_current_Zneg) * np.cross(photo_normal1, photo_normal2)

        b = self.max_current * np.dot(photo_normal1, np.cross(photo_normal2, photo_normal3))

        photo_measured_sunvector = np.divide(a, b)

        columns_content = (str(dt.datetime.now(tz=None)), sim_time, photo_measured_sunvector[0],
                           photo_measured_sunvector[1], photo_measured_sunvector[2])
        # utilities.write_line_into_database_table(self.database, 'photo_measured_sunvector', columns_content)
        column_names = ['real_time', 'simulation_time', 'sat_to_sun_measured_x', 'sat_to_sun_measured_y', 'sat_to_sun_measured_z']
        self.outputs.safe_output('photo_measured_sunvector', column_names,
                                 list(columns_content))
        # print('measured sunvector from photodiodes:', photo_measured_sunvector)
        return photo_measured_sunvector

    def compute_sunvector_from_photo_currents_alternative(self, sim_time, photo_currents):
        """Equation to determine sunvector from:
        Satellite Attitude Estimation using Sun Sensors, Horizon Sensors and Gyros - Vaibhav Vasant Unhelkar"""
        # TODO:
        # - access threshold from sim.config via read_config function

        threshold = 1

        photo_current_Xpos = float(photo_currents[0])
        photo_current_Xneg = float(photo_currents[1])
        photo_current_Ypos = float(photo_currents[2])
        photo_current_Yneg = float(photo_currents[3])
        photo_current_Zpos = float(photo_currents[4])
        photo_current_Zneg = float(photo_currents[5])

        # A: a matrix containing the axis vectors of the sensors selected for the calculation
        # b: a vector containing the measurements of the sensors
        # photo_measured_sunvector: contains the measured sat to sun vector, returned as np.zeros(3)
        # if there are not enough measurements for the calculation
        A = np.zeros((3, 3))
        b = np.zeros(3)
        photo_measured_sunvector_2 = np.zeros(3)

        # the following code assumes that the normal of photocurrent1 & photocurrent2 are in +-x direction respectively
        # photocurrent3 & photocurrent4 are in +-y direction
        # photocurrent5 & photocurrent6 are in +-z direction

        # determination of strongest measurement in x-direction & update of A and b accordingly
        if photo_current_Xpos > photo_current_Xneg:
            b[0] = photo_current_Xpos
            A[0] = [1, 0, 0]
        else:
            b[0] = photo_current_Xneg
            A[0] = [-1, 0, 0]

        # determination of strongest measurement in y-direction & update of A and b accordingly
        if photo_current_Ypos > photo_current_Yneg:
            b[1] = photo_current_Ypos
            A[1] = [0, 1, 0]
        else:
            b[1] = photo_current_Yneg
            A[1] = [0, -1, 0]

        # determination of strongest measurement in z-direction & update of A and b accordingly
        if photo_current_Zpos > photo_current_Zneg:
            b[2] = photo_current_Zpos
            A[2] = [0, 0, 1]
        else:
            b[2] = photo_current_Zneg
            A[2] = [0, 0, -1]

        # calculation of sunvector should enough diodes face the sun
        if b[0] > threshold and b[1] > threshold and b[2] > threshold:
            photo_measured_sunvector_2 = np.linalg.inv(A).dot(b)
            print('measured sunvector from photodiodes:', photo_measured_sunvector_2)
        # else:
        #     print("not enough diodes facing the sun")

        # return photo_measured_sunvector_2



class Photodiode(Sunsensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, mode: int,
                 outputs , accuracy: float, precision: float, fov: int, max_current: float,
                 sampling_rate: float, digital: bool = False, active: bool = True):
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)

        """
                   :param name: (str) a unique name for each sensor
                   :param position: (numpy array, 3x1 x y z) position of the sensor in the satellite frame in m
                   :param orientation: (numpy array, 3x1 x y z) orientation of sensor in satellite frame
                   :param database: (string, .db) name of the database to read/write from/in
                   :param mode: (int) chooses between a different set of predefined parameter for the sensor
                   :param digital: (bool) defines if the sensor is digital or analog
                   :param accuracy: (float) systematic errors e.g. offset or calibration error (in deg)
                   :param precision: (float) random error / gaussian noise of measured entity 
                   :param fov: (int) field of view of the sensor in degree
                   :param max_current: (float) max output current if sun rays are orthogonal to sensor
                   :param active: (bool) activates or deactivates the sensor
                """

        self.fov = fov
        self.max_current = max_current
        self.active = active
        self.digital = digital
        self.mode = mode
        self.database = database
        self.accuracy = accuracy
        self.precision = precision
        self.sampling_rate = sampling_rate
        # self.noise = super().make_noise(1)

        # create table for the output of the sensor in the database, if it doesn't already exist
        # utilities.create_table(self.database, self.name, 'real_time, simulation_time, Xpos, Xneg, Ypos, Yneg, Zpos, Zneg')
        #
        # utilities.create_table(self.database, 'photo_measured_sunvector',
        #                 'real_time, simulation_time, sat_to_sun_measured_x, sat_to_sun_measured_y, sat_to_sun_measured_z')
        self.column_names = ['real_time', 'simulation_time', 'Xpos', 'Xneg',
                             'Ypos', 'Yneg', 'Zpos', 'Zneg']


        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def get_parameters(self):
        """
        Returns all parameters that an instance of class Photodiode possess
        """
        return self.name, self.position.tolist(), self.orientation.tolist(), self.database, \
               self.mode, self.accuracy.tolist(), self.precision, self.fov, \
               self.max_current, self.digital, self.active

    def set_fov(self, fov: int):
        """ Change Field of View of the sun sensor """
        self.fov = fov
        # write the new parameters into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, "fov", json.dumps(self.fov))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def get_photo_current_noise(self, current_ideal):
        """ Returns gaussian noise as a function of output current."""

        accuracy_dist = np.random.uniform(-self.accuracy, self.accuracy)
        # print('accuracy_dist= ', accuracy_dist)
        current_acc = current_ideal + accuracy_dist
        current_with_noise = np.random.normal(current_acc, self.precision, 1)
        photo_current_noise = current_with_noise - current_ideal

        return photo_current_noise

    def set_max_current(self):
        """ Set upper limit for output current  """
        self.max_current = max_current
        columns_content = (str(dt.datetime.now(tz=None)), 0, "max_current", json.dumps(self.max_current))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def set_sampling_rate(self, sampling_rate: float):
        """
        Change the frequency of the sensor.

        :param sampling_rate: (float) sampling rate of the sensor in Hz
        """
        self.sampling_rate = sampling_rate
        # write the new parameters into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, "sampling_rate", json.dumps(self.sampling_rate))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def output(self, sim_time: float):
        '''
        returns photo currents from photodiodes
        '''

        # check for updates in the parameters
        self.check_parameter_update()
        if self.active:

            # read eclipse entries from database; eclipse=1 sat in shadow, eclipse=0 sat illuminated
            # df = utilities.interpolate_two_rows(self.database, name='eclipse', sim_time=sim_time)
            df = utilities.interpolate_dataframe(self.outputs.eclipse, sim_time)
            eclipse = float(df.to_numpy()[1:].reshape(1))
            # print('eclipse= ', eclipse)

            # print('sim_time= ', sim_time)

            sat_to_sun = self.satellite_to_sun(sim_time)

            # normal vectors in satellite frame
            normal = {"+X": [1, 0, 0],
                      "-X": [-1, 0, 0],
                      "+Y": [0, 1, 0],
                      "-Y": [0, -1, 0],
                      "+Z": [0, 0, 1],
                      "-Z": [0, 0, -1]}

            side = ['+X', '-X', '+Y', '-Y', '+Z', '-Z']


            photo_currents_list = []
            photo_currents_ideal_list = []

            for i in range(len(side)):
                if side[i] in normal:
                    photo_normal = normal[side[i]]  # normal vector of each photodiode

                    # the dot product of the sun vector and the sensor normal is bigger than 0
                    # if the sensor can see the sun
                    dot = np.dot(sat_to_sun, photo_normal)
                    current_ideal = self.max_current * dot  # ideal current without any noise

                    # the angle needs to be computed to ensure that the incident sun ray
                    # is in the FOV of the sensor
                    theta = np.arccos(dot / (np.linalg.norm(sat_to_sun) * np.linalg.norm(
                        photo_normal))) * 180 / np.pi  # theta and FOV in degrees
                    # print('theta= ', theta)

                    # if the satellite is in the shadow of the earth or the side on which
                    # the sensor is mounted is not in the sun only noise is returned
                    # print('is_side_in_sun =', self.is_side_in_sun(self.orientation, sat_to_sun))
                    if eclipse >= 0.5 or not self.is_side_in_sun(photo_normal, sat_to_sun):
                        current_ideal = 0
                        photo_current = float(self.get_photo_current_noise(current_ideal))
                    elif dot <= 0 or theta > self.fov / 2:
                        photo_current = float(self.get_photo_current_noise(current_ideal))
                        current_ideal = 0
                    else:
                        photo_current = float(current_ideal + self.get_photo_current_noise(current_ideal))

                    if photo_current < 0:  # suppress negative currents
                        photo_current = 0

                    photo_currents = np.zeros(6)
                    photo_currents_ideal_list.append(current_ideal)
                    photo_currents_list.append(photo_current)
                    if len(photo_currents_list) == 6:
                        photo_currents = np.hstack(photo_currents_list)
                        photo_currents = self.position * photo_currents

                        current_ideal = np.hstack(photo_currents_ideal_list)
                        current_ideal = self.position * current_ideal
                        columns_content = (str(dt.datetime.now(tz=None)), sim_time, *photo_currents)
                        # utilities.write_line_into_database_table(self.database, self.name, columns_content)
                        self.outputs.safe_output(self.name, self.column_names,
                                                 columns_content)

                        self.compute_sunvector_from_photo_currents(sim_time, photo_currents)

        else:
            photo_currents = np.zeros(6)           # set photo current to zero if photodiode is not activated
            current_ideal = np.zeros(6)

    def run(self):
        """ Starts the sensor.

        """
        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output()

            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script too slow")

if __name__ == "__main__":
    import threading

    photo = Photodiode(name='photodiode', position= np.array([1,1,1,1,1,1]), orientation=np.array([1, 0, 0]),
                       database='test.db', mode=1, accuracy=0.3, precision=0.05, fov=140,
                       max_current=100, sampling_rate=1,  active=True)

    photo.check_parameter_update()


    # photo.output()
    #
    # for sim_time in np.linspace(0, 5400, 540):
    #     photo.output(sim_time)

    # print(photo.active)

    # def test_write():
    #     i = 0
    #     while True:
    #
    #         conn_10 = sqlite3.connect('test.db')
    #         c_10 = conn_10.cursor()
    #         try:
    #             c_10.execute('create table photo_current (I_1, I_2, I_3, I_4, I_5, I_6)')
    #         except:
    #             pass
    #
    #         I_1 = 50  # TODO: get photo_current[i] when output is running
    #         I_2 = 0
    #         I_3 = 20
    #         I_4 = 0
    #         I_5 = 0
    #         I_6 = 60
    #
    #         c_10.execute('insert into photo_current VALUES (?, ?, ?, ?, ?, ?)', [I_1, I_2, I_3, I_4, I_5, I_6])
    #         conn_10.commit()
    #         c_10.close()
    #         i += 1
    #         time.sleep(0.02)
    #
    #
    # t1 = threading.Thread(target=test_write, args=())
    # t1.start()
    # print('thread 1 aktiv')
    # time.sleep(1)
    # t2 = threading.Thread(target=photo.run, args=())
    # t2.start()
    # print('thread 2 aktiv')
    # time.sleep(6)
    # photo.set_sampling_rate(1)



class FineSunSensor(Sunsensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, mode: int,
                 outputs, accuracy: float, precision: float, fov: int, max_current: float,
                 sampling_rate: float, digital: bool = False, active: bool = True):
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)

        self.fov = fov
        self.max_current = max_current
        self.active = active
        self.digital = digital
        self.mode = mode
        self.database = database
        self.accuracy = accuracy
        self.precision = precision
        self.sampling_rate = sampling_rate
        # self.noise = super().make_noise(1)

        # create table for the output of the sensor in the database, if it doesn't already exist
        # utilities.create_table(self.database, self.name, 'real_time, simulation_time, fine_sun_current_1, fine_sun_current_2')
        self.column_names = ['real_time', 'simulation_time', 'fine_sun_current_1',
                             'fine_sun_current_2']
        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def get_fine_current_noise(self, current_ideal):
        """ Returns gaussian noise as a function of output current."""

        # fine_current = np.random.normal(current_ideal, self.noise, 1)
        # fine_current_noise = fine_current - current_ideal
        # print('fine_current_noise= ', fine_current_noise)

        accuracy_dist = np.random.uniform(-self.accuracy, self.accuracy)
        # print('accuracy_dist= ', accuracy_dist)
        current_acc = current_ideal + accuracy_dist
        current_with_noise = np.random.normal(current_acc, self.precision, 1)
        fine_current_noise = current_with_noise - current_ideal
        # print('fine_current_noise= ', fine_current_noise)

        # columns_content = (str(dt.datetime.now(tz=None)), 0, "fine_current_noise", json.dumps(fine_current_noise))
        # utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)
        return fine_current_noise

    def measure_fine_sun_sensor_angles(self, sat_to_sun, position):
        '''
        Assumes that orientation = normal vector of fine sun sensor in satellite frame
        '''

        normal = {"+X": [1, 0, 0],
                  "-X": [-1, 0, 0],
                  "+Y": [0, 1, 0],
                  "-Y": [0, -1, 0],
                  "+Z": [0, 0, 1],
                  "-Z": [0, 0, -1]}

        # position = '+X'
        if position in normal:
            normal_vector = normal[position]

            # Sources:
            # Datasheet SolarMEMS SSOC-A60
            # Integrated Navigation System Design for Micro Planetary Rovers: Comparison of Absolute
            # Heading Estimation Algorithms and Nonlinear Filtering

            # determine the projection of sat_to_sun onto two orthogonal planes
            # the angles between the normal vector and the projected vector are alpha and beta
            xy_proj = sat_to_sun.copy()
            xy_proj[2] = 0
            xy_proj_norm = xy_proj / np.linalg.norm(xy_proj)

            xz_proj = sat_to_sun.copy()
            xz_proj[1] = 0
            xz_proj_norm = xz_proj / np.linalg.norm(xz_proj)

            yz_proj = sat_to_sun.copy()
            yz_proj[0] = 0
            yz_proj_norm = yz_proj / np.linalg.norm(yz_proj)

            if 'X' in position:
                dot_1 = np.dot(normal_vector, xy_proj_norm)    #xy
                dot_2 = np.dot(normal_vector, xz_proj_norm)    #xz
                alpha = np.arccos(dot_1) * 180 / np.pi
                beta = np.arccos(dot_2) * 180 / np.pi
                # print('alpha, beta', alpha, beta)
                return alpha, beta, dot_1, dot_2
            elif 'Y' in position:
                dot_1 = np.dot(normal_vector, xy_proj_norm)    #xy
                dot_2 = np.dot(normal_vector, yz_proj_norm)    #yz
                alpha = np.arccos(dot_1) * 180 / np.pi
                beta = np.arccos(dot_2) * 180 / np.pi
                # print('alpha, beta', alpha, beta)
                return alpha, beta, dot_1, dot_2
            elif 'Z' in position:
                dot_1 = np.dot(normal_vector, xz_proj_norm)    #xz
                dot_2 = np.dot(normal_vector, yz_proj_norm)    #yz
                alpha = np.arccos(dot_1) * 180 / np.pi
                beta = np.arccos(dot_2) * 180 / np.pi
                # print('alpha, beta', alpha, beta)
                return alpha, beta, dot_1, dot_2

    def output(self, sim_time: float):
        """ returns fine sun sensor currents """

        self.check_parameter_update()

        # read eclipse entries from database; eclipse=1 sat in shadow, eclipse=0 sat illuminated
        # df = utilities.interpolate_two_rows(self.database, name='eclipse', sim_time=sim_time)
        df = utilities.interpolate_dataframe(self.outputs.eclipse, sim_time)
        eclipse = float(df.to_numpy()[-1].reshape(1))

        # position = '+X'
        sat_to_sun = self.satellite_to_sun(sim_time)
        alpha, beta, dot_1, dot_2 = self.measure_fine_sun_sensor_angles(sat_to_sun, self.position)
        # print('alpha beta dot_1 dot_2= ', alpha, beta, dot_1, dot_2)

        dot = np.dot(sat_to_sun, self.orientation)
        current_ideal_1 = self.max_current * dot_1
        current_ideal_2 = self.max_current * dot_2
        # print('fine_sun_currents_ideal= ', current_ideal_1, current_ideal_2)

        # if the satellite is in the shadow of the earth or the side on which
        # the sensor is mounted is not in the sun only noise is returned
        if eclipse >= 0.5 or not self.is_side_in_sun(self.orientation, sat_to_sun):
            I_1 = float(self.get_fine_current_noise(current_ideal_1))
            I_2 = float(self.get_fine_current_noise(current_ideal_2))
            # return I_1, I_2
            # return self.get_fine_current_noise(current_ideal_1)

        elif dot_1 <= 0 or dot_2 <= 0 or alpha > self.fov/2 or beta > self.fov/2:
            I_1 = float(self.get_fine_current_noise(current_ideal_1))
            I_2 = float(self.get_fine_current_noise(current_ideal_2))
        else:
            I_1 = float(current_ideal_1 + self.get_fine_current_noise(current_ideal_1))
            I_2 = float(current_ideal_2 + self.get_fine_current_noise(current_ideal_2))

        if I_1 < 0:       # suppress negative currents
            I_1 = 0.0
        if I_2 < 0:
            I_2 = 0.0

        fine_sun_current = np.array([I_1, I_2])
        # print('fine_sun_currents= ', fine_sun_current)
        # print('sim_time= ', sim_time)


        # write sensor output into the database
        columns_content = (str(dt.datetime.now(tz=None)), sim_time, fine_sun_current[0], fine_sun_current[1])
        # utilities.write_line_into_database_table(self.database, self.name, columns_content)
        self.outputs.safe_output(self.name, self.column_names, columns_content)

        return fine_sun_current, current_ideal_1, current_ideal_2

    def run(self):
        """
        Starts the sensor.
        """
        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output()

            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script too slow")


if __name__ == "__main__":
    import threading

    fine = FineSunSensor(name='fine_sun_sensor', position=np.array([1, 0, 0]), orientation=np.array([1, 0, 0]),
                       database='test.db', mode=1, accuracy=1, precision=0, fov=120,
                       max_current=100, sampling_rate=1,  active=True)

    fine.check_parameter_update()

    # for sim_time in np.linspace(0, 5400, 540):
    #     fine.output(sim_time)

    # fine.output()

