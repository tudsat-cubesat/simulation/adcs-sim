from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication
import os
import sqlite3
import collections
import numpy as np
import utilities
import time
import threading


class Window(QWidget):
    def __init__(self, database):
        super().__init__()
        self._bufsize = 100
        self.databuffer = collections.deque([0.0]*self._bufsize, self._bufsize)
        self.database = database
        self.x = []
        self.y = []
        self.setWindowTitle("QVBoxLayout Example")
        # Create a QVBoxLayout instance
        self.layout = QVBoxLayout()
        # Add widgets to the layout
        self.layout.addWidget(QPushButton("Top"))
        self.layout.addWidget(QPushButton("Center"))
        self.layout.addWidget(QPushButton("Bottom"))
        self.layout.addStretch()
        conn = sqlite3.connect(self.database)
        c = conn.cursor()
        c.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = c.fetchall()
        c.close()
        # creates a list with the table names as content
        self.sensor_table_names = []
        self.orbit_environment_tables = ['magnetic_field_strength', 'temperature', 'earth_to_sat', 'sat_to_sun',
                                         'earth_to_sun']

        for table_name in tables:
            self.sensor_table_names.append(table_name[0])
        # removes parameter tables and pre calculated tables
        for table_name in self.sensor_table_names:
            if '_parameters' in table_name:
                self.sensor_table_names.remove(table_name)
            # removes pre calculated tables
            for pre_calculated_table in self.orbit_environment_tables:
                if table_name is pre_calculated_table:
                    self.sensor_table_names.remove(table_name)
        print(self.sensor_table_names)
        for index, name in enumerate(self.sensor_table_names):
            self.plt = pg.plot(title=name)
            self.x.append(np.linspace(0, 100, self._bufsize))
            self.y.append(np.zeros(self._bufsize, dtype=float))
            self.layout.addWidget(self.plt)
            self.curve = self.plt.plot(self.x[index], self.y[index], pen=(255, 0, 0))
        # Set the layout on the application's window
        self.setLayout(self.layout)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(100)

    def updateplot(self):
        for index, name in enumerate(self.sensor_table_names):
            self.databuffer.append(float(utilities.get_last_line_from_database_table(self.database, name)[["omega_x"]].to_numpy()))
            self.y[index][:] = self.databuffer
            self.curve.setData(self.x[index], self.y[index])
            self.app.processEvents()


def main():
    app = QtWidgets.QApplication(sys.argv)
    main = Window('testplot.db')
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    def test_write():
        i = 0
        while True:
            for i in range(10):
                conn_10 = sqlite3.connect('testplot.db')
                c_10 = conn_10.cursor()

                try:
                    c_10.execute('create table angular_components_'+str(i)+' (omega_x, y)')
                except:
                    pass

                c_10.executemany('insert into angular_components_'+str(i)+' VALUES (?, ?)',
                                 [(np.sin(i * np.pi / 18), np.sin(i * np.pi / 18))])
                conn_10.commit()
                c_10.close()
            i += 1
            time.sleep(0.02)


    t1 = threading.Thread(target=test_write, args=())
    t1.setDaemon(True)
    t1.start()
    main()