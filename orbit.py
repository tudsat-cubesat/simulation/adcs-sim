import logging

import pandas as pd
from sgp4.api import jday, Satrec, WGS84
import numpy as np
import pandas
import datetime as dt
import utilities
import time
import environment

class Orbit(Satrec):
    def __init__(self, database: str, sampling_rate: float, epoch: tuple, a: float, i: float, e: float, argpo: float,
                 RAAN: float, outputs, mean_anomaly: float =0.0, deg: bool =True):
        """Class of a sgp4 orbit that can be initialized with Kepler elements rather than TLEs. Might be useless if
        we decide to let the user define TLEs instead of Kepler elements.

        @param database: (str) the database the orbit is connected with and is sending data to
        @param sampling_rate: (float) the sampling rate (real time) that the propagator is supposed to compute a value
                                in Hz
        @param epoch: (tuple) the epoch that all Kepler elements are referring to, Form: (yyyy, m, d, h, m, s)
        @param a: (float) semi-major axis of the orbit in km
        @param i: (float) inclination of the orbit in deg or rad -> default is deg
        @param e: (float) eccentricity of the orbit -> must be between 0 and 1
        @param argpo: (float) argument of perigee of the orbit in deg or rad -> default is deg
        @param RAAN: (float) right ascension of ascending node of the orbit in deg or rad -> default is deg
        @param mean_anomaly: (float) mean anomaly at the given epoch in deg or rad -> default is deg
        @param deg: (bool) indicate whether angles are given in deg or rad
        """
        self.database = database
        self.sampling_rate = sampling_rate
        self.sim_time_offset = epoch
        self.dict_orbit = outputs

        # create table for the output of the orbit propagator in the database, if it doesn't already exist
        utilities.create_table(self.database, 'position', 'real_time, simulation_time, x, y, z')
        utilities.create_table(self.database, 'velocity', 'real_time, simulation_time, v_x, v_y, v_z')

        # epoch: days since 1949 December 31 00:00 UT
        jd, fr = jday(*epoch)
        epoch = jd + fr - 2433281.5
        #TODO: see if we can calculate bstar coefficient with given Kepler elements
        # bstar: drag coefficient (1/earth radii)
        bstar = 2.8098e-05
        # ndot (NOT USED): ballistic coefficient (revs/day)
        ndot = 0.0
        # nddot (NOT USED): mean motion 2nd derivative (revs/day^3)
        nddot = 0.0
        # no_kozai: mean motion (radians/minute)
        no_kozai = utilities.get_mean_motion(a)

        # propagator needs radians values so transform deg in rad
        if deg:
            i = i * np.pi / 180.
            argpo = argpo * np.pi / 180.
            RAAN = RAAN * np.pi / 180.
            mean_anomaly = mean_anomaly * np.pi / 180.

        self.sgp4init(WGS84, 'i', 5, epoch, bstar, ndot, nddot, e, argpo, i, mean_anomaly, no_kozai, RAAN)

    def output(self, sim_time, commit: bool =True):
        """Computes the position and velocity vector of the orbit at a given time epoch and writes it to the
        database.

        :param sim_time: (float) the simulation time that has been passed since start of the simulation (NOT REAL TIME!)
        :param commit: (bool) if True, computed vectors are committed to database, if False, computed vectors are
                                returned
        :return: (list, list) returns position and velocity vector if commit is False
        """
        epoch = list(self.sim_time_offset)
        epoch[5] += sim_time

        jd, fr = jday(*epoch)
        e, r, v = self.sgp4(jd, fr)
        if e != 0:
            # a dict of errors is in sgp4.api import SGP4_ERRORS
            raise Exception("something in the sgp4 propagation went wrong. check the date and orbital elements")

        if commit:
            # write position vector into the database
            columns_content1 = (str(dt.datetime.now(tz=None)), jd + fr, r[0], r[1], r[2])
            utilities.write_line_into_database_table(self.database, 'position', columns_content1)

            # write velocity vector into the database
            columns_content2 = (str(dt.datetime.now(tz=None)), jd + fr, v[0], v[1], v[2])
            utilities.write_line_into_database_table(self.database, 'velocity', columns_content2)

        else:
            return r, v

    def run(self):
        """Runs the orbit simulation. Uses a while True loop with no abort option -> should be adapted.
        sim_time_factor is for scaling the real time to simulation time. With a sim_time_factor of 2 e.g. the simulation
        time passes twice as fast as the real time. Therefore, the sampling_rate of the simulation time is actually
        only half the sampling_rate that refers to the real time and that is given to the constructor.
        """
        start = time.time()
        sim_time_factor = 45
        while True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor on time
            self.output((current_time_stamp - start) * sim_time_factor)

            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print('Script to slow for sampling rate')

    def propagate_orbit(self, max_time: float):
        """Propagates an orbit trajactory for the time beginning with the simulation offset indicated when initializing
        the orbit until the maximum time that needs to be specified in seconds. The frequency allows to adjust the
        level of detail of the orbit trajectory. The more detailed the orbit is required, the higher the frequency has
        to be set. After having fully propagated the orbit, it is written into the database.

        :param max_time: (float) the duration of the propagated orbit in seconds
        :param freq: (float) the frequency of the orbit in Hz
        """
        freq = self.sampling_rate
        logging.info('calculating orbit')
        steps = int(max_time * freq + 1)
        r_ = []
        v_ = []
        start = time.time()
        for sec in np.linspace(0, max_time, steps):
            r, v = self.output(sim_time=sec, commit=False)
            r_.append((str(dt.datetime.now(tz=None)), sec, *r))
            v_.append((str(dt.datetime.now(tz=None)), sec, *v))
            # write in position data
            pos_col_names = ['real_time', 'simulation_time', 'x', 'y', 'z']
            self.dict_orbit.safe_output('position', pos_col_names,
                                        [str(dt.datetime.now(tz=None)), sec, *r])
            # write in velocity data
            vel_col_names = ['real_time', 'simulation_time', 'v_x', 'v_y', 'v_z']
            self.dict_orbit.safe_output('velocity', vel_col_names,
                                        [str(dt.datetime.now(tz=None)), sec, *v])
        utilities.write_line_into_database_table(self.database, 'position', r_)
        utilities.write_line_into_database_table(self.database, 'velocity', v_)
        logging.info('done, needed %s seconds' % (time.time() - start))


if __name__ == "__main__":
    orbit = Orbit(database='test.db', sampling_rate=50, epoch=(2020, 1, 1, 15, 0, 0), a=6771., i=51.3, e=0.0015,
                  argpo=0., RAAN=0., mean_anomaly=0.)

    env = environment.Environment(sim_time_offset=(2020, 1, 1, 15, 0, 0), database='test.db', sampling_rate=50)

    orbit.propagate_orbit(max_time=5400, freq=10)
    env.propagate_environment()