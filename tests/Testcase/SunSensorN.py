import environment
import numpy as np, utils
import math as m
from pyquaternion import Quaternion

class Sensor(object):


    def __init__(self, name, rel_position, orientation, noise, n_steps):
        """
        name(string): a unique name for each sensor
        rel_position(3x1): the position in the satellite in meter
        orientation(3x1): a vector describing the orientation of the sensor.
                    meaning changes slightly with each sensor
        noise(float): a measure of noise. in the same unit as the measurements
                           refer to the measure function of each sensor for more info
        n_steps(int): number of user defined integration steps
        """
        self.name = name
        self.rel_position = rel_position
        self.orientation = orientation
        self.noise = noise
        self.n_steps = n_steps
    
    def make_noise_cache(self, dimensions):
        """
        caching the noise values is necessary because the integrator would get confused
        otherwise. between the user defined steps it does smaller steps to ensure 
        the accuracy of the results. without caching the sensors would change its
        measurement with each small step slightly which would slightly alter the
        applied control torque. this would prevent the integrator from staying
        within an acceptable error for the ode solutions.
        """
        return np.random.normal(0, self.noise, (self.n_steps, dimensions))
    

class Magnetometer(Sensor):

    def __init__(self, name, rel_position, x_axis, resolution, n_steps):
        """
        differences to the parent constructor:
        x_axis(3x1 array): the x_axis of the sensor in the satellite body frame
        """
        super().__init__(name, rel_position, x_axis, resolution, n_steps)
        self.noise_cache = super().make_noise_cache(3)
    

    def measure(self, satellite):
        """
        measure the magnetic field as seen by the sensor. 
        the returned vector has some noise.
        the noise is given in nT.
        """
        #the magnetic field produced by the earth in nT
        mag_field = np.array(environment.magnetic_field(satellite.r, satellite.real_time))
        # rotate from TEME to body frame
        mag_field = satellite.attitude.rotate(mag_field)
        #the magnetic field produced by the magnetorquers and the intrinsic magnetic moment
        #of the satellite will also influence the magnetometer measurements
        mag_field += satellite.get_intrinsic_magnetic_field(self.rel_position)
        # apply noise. gaussian around zero with a std. deviation equal to the resolution
        mag_field += self.noise_cache[int(satellite.sim_time)]
        # rotate from satellite body frame to sensor frame
        rel_attitude = utils.get_attitude([1,0,0], self.orientation)
        mag_field = rel_attitude.rotate(mag_field)
        return mag_field


class Photodiode(Sensor):

    def __init__(self, name, side, normal, noise, max_current, FOV, n_steps):
        """
        differences to the parent constructor:
        normal(3x1 array): the normal vector on the plane of the detector
        max_current(float): the maximum current created by the diode in direct
                            sun light, i.e. when normal and sun vector are equal
        FOV(float): the total field of view of the sensor in degrees, 
                    i.e. between 0 and 180
        """
        super().__init__(name, side, normal, noise, n_steps)
        self.noise_cache = super().make_noise_cache(1)
        self.max_current = max_current
        self.FOV = FOV





    def measure(self, satellite):
        """
        the intensity of the light as measured by the photodiode
        """
        def is_side_in_sun(side, sun):
            """
            wether the side is illuminated by the sun.
            assumes that the satellite is in the sun
            """
            dot = np.dot(sun, side)
            if dot <= 0:
                return False
            else:
                return True
        sat_to_sun = satellite.to_sun()
        #if the satellite is in the shadow of the earth or the side on wich
        #the sensor is mounted is not in the sun only noise is returned
        if satellite.is_in_shadow() or not is_side_in_sun(self.rel_position, sat_to_sun):
            return self.noise_cache[int(satellite.sim_time)]
        #the dot product of the sun vector and the sensor normal is bigger than 0
        #if the sensor can see the sun
        dot = np.dot(sat_to_sun, self.orientation)
        #the angle needs to be computed to ensure that the incident sun ray
        #is in the FOV of the sensor
        angle = np.arccos(dot) * 180 / np.pi #angle in degrees since FOV in deg
        if dot <= 0 or angle > self.FOV / 2:
            #the sensor only measures noise, there is no sun ray coming into it
            return self.noise_cache[int(satellite.sim_time)]
        else:
            return self.max_current * dot + self.noise_cache[int(satellite.sim_time)]


class PositionSensitiveDevice(Sensor):

    def __init__(self, name, side, normal, noise, max_current, FOV, n_steps, accuracy):
        """
        differences to the parent constructor:
        normal(3x1 array): the normal vector on the plane of the detector
        max_current(float): the maximum current created by the diode in direct
                            sun light, i.e. when normal and sun vector are equal
        FOV(float): the total field of view of the sensor in degrees,
                    i.e. between 0 and 180
        accuracy(float): the maximum deviation of the sensor in deg
        """
        super().__init__(name, side, normal, noise, n_steps)
        self.noise_cache = super().make_noise_cache(1)
        self.max_current = max_current
        self.FOV = FOV
        self.accuracy = accuracy


    def measure(self, satellite):
       """
        the Solar vecor as measured by the PSD
        """

        def is_side_in_sun(side, sun):
            """
            wether the side is illuminated by the sun.
            assumes that the satellite is in the sun
            """
            dot = np.dot(sun, side)
            if dot <= 0:
                return False
            else:
                return True

        sat_to_sun = satellite.to_sun()
        # if the satellite is in the shadow of the earth or the side on wich
        # the sensor is mounted is not in the sun only noise is returned




        if satellite.is_in_shadow() or not is_side_in_sun(self.rel_position, sat_to_sun):
            print("sun not in FOV")

        # the dot product of the sun vector and the sensor normal is bigger than 0
        # if the sensor can see the sun
        dot = np.dot(sat_to_sun, self.orientation)
        # the angle needs to be computed to ensure that the incident sun ray
        # is in the FOV of the sensor
        angle = np.arccos(dot) * 180 / np.pi  # angle in degrees since FOV in deg


        psi = np.mod((Sensor.noise * 180), self.accuracy)
        tau = np.mod((Sensor.noise * 180), self.accuracy)

        sat_to_sun_noisy = sat_to_sun * rot_z(psi) * rot_y(tau)



        if dot <= 0 or angle > self.FOV / 2:
            # sat_sun vector is returned if sun is in FOV
            return print("sun not in FOV")
        else:
            return sat_to_sun_noisy




def rot_x(theta):
    return np.matrix([[1, 0, 0],
                      [0, m.cos(theta), -m.sin(theta)],
                      [0, m.sin(theta), m.cos(theta)]])


def rot_y(theta):
    return np.matrix([[m.cos(theta), 0, m.sin(theta)],
                      [0, 1, 0],
                      [-m.sin(theta), 0, m.cos(theta)]])


def rot_z(theta):
    return np.matrix([[m.cos(theta), -m.sin(theta), 0],
                      [m.sin(theta), m.cos(theta), 0],
                      [0, 0, 1]])


def coarseSunVector(*sensors, satellite):
    """ computes the sat-to-sun vector in the spacecraft reference frame.
    The function gets passed an arbitrary number of sensors and selects
    he best ones for the computation of the solar vector."""

    # A is a matrix containing the axis vectors of the sensors selected for the calculation
    # b is a vector containing the measurements of the sensors
    A = np.zeros((10,4))
    b = np.zeros((3))

    #threshhold of noise which has to be surpassed in order for a measurement to be considered
    thresh = 0.05
    x_max = thresh
    y_max = thresh
    z_max = thresh


    # select the sensor with the highest measurement and fill A & b accordingly
    for i in sensors:
        measurement = photodiode.measure(sensors[i], satellite)
        orientation = sensors[i].orientation

        if orientation == [1, 0, 0] or orientation == [-1, 0, 0]:
            if measurement > x_max:
                b[0]=measurement

                if orientation == [1, 0, 0]:
                    A[0]= [1, 0, 0]
                else:
                    A[0]= [-1, 0, 0]

        if orientation == [0, 1, 0] or orientation == [0, -1, 0]:
            if measurement > y_max:
                b[1] = measurement

                if orientation == [0, 1, 0]:
                    A[0] = [0, 1, 0]
                else:
                    A[0] = [0, -1, 0]

        if orientation == [0, 0, 1] or orientation == [0, 0, -1]:
            if measurement > z_max:

                b[2] = measurement

                if orientation == [0, 0, 1]:
                    A[0] = [0, 0, 1]
                else:
                    A[0] = [0, 0, -1]

    # check if threshhold was surpassed in every direction, only then construct the sat_sun vector and return it
    if (x_max > thresh and y_max > thresh) and z_max > thresh:
        return numpy.linalg.inv(A).dot(b)

    else:
        println("not enough diodes facing the sun")