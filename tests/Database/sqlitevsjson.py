import pandas as pd
import numpy as np
import csv
import sqlite3
import json
import time
import matplotlib.pyplot as plt


# create sqlite
conn = sqlite3.connect('sqdatabase.db')
c = conn.cursor( )
# create table, if it doesn't already exists
try:
    c.execute('create table written_data (a, b, c, d)')

except:
    pass

conn.commit( )
c.close( )

# create json file
d = {'a': [0], 'b': [0], 'c': [0], 'd': [0]}
df = pd.DataFrame(data=d)
df.to_json('jasonbase.json', orient='records', lines=True)
# deleting and writing time sqlite
sqlite_dw_time = []

# overwriting time json
json_w_time = []

factor = []



for i in range(10000):
    # create data
    data_s = []
    data_j = []

    for j in range(4):
        d = np.random.normal(0, 2, i+2)
        data_j.append(list(d))
    for k in range(i):
        d = np.random.normal(0, 2, 4)
        data_s.append(tuple(d))




    inserting = {'a': data_j[0], 'b': data_j[1], 'c': data_j[2], 'd': data_j[3]}


    # clear and write sqlite
    start = time.time()
    conn = sqlite3.connect('sqdatabase.db')
    sql = 'DELETE FROM written_data'
    cur = conn.cursor()
    cur.execute(sql)
    cur.executemany('INSERT INTO written_data VALUES(?,?,?,?);', data_s)
    conn.commit()
    conn.close()
    end = time.time()
    calc = end - start
    sqlite_dw_time.append(calc * 1000)

    # overwriting json file
    start = time.time()
    df = pd.DataFrame(data=inserting)
    df.to_json('jasonbase.json', orient='records', lines=True)
    end = time.time( )
    calc = end - start
    json_w_time.append(calc * 1000)
    factor.append(sqlite_dw_time[-1]/json_w_time[-1])

# create csv
df1 = {'sqlite_dw_time': sqlite_dw_time, 'json_time': json_w_time,
       'factor': factor}
df1 = pd.DataFrame(data=df1)
df1.to_csv('time_measurement.csv', sep=';')

df1.plot()
plt.show()


