# Standardization

## Tools and Packages

- python: Version 3.9.5
    + scipy.spatial.transform for quaternionen: Version 1.7.0
    + astropy: Version 4.2.1
    + numpy: Version 1.21.0
    + matplotlib: Version 3.4.2
    + pyIGRF: Version 0.3.3
    + pandas: Version 1.2.5
    + [logging](https://docs.python.org/3/howto/logging.html)
    + sqlite3: Version 2.6.0 (`import sqlite3; print(sqlite3.version)`)
    + sgp4: 2.19
    + mpmath: 1.2.1 
    + sympy: 1.8
    + pyyaml: 5.4.1
    + ruamel.yaml 0.17.10
- SQLite3: Version 3.22.0 (`import sqlite3; print(sqlite3.sqlite_version)`)

---

## Naming and Type Conventions

### Super class Sensor

#### Parameters

- name: str
- position: array, 3x1
- orientation: array, quat
- active: bool
- digital: bool
- database: str
- mode: int

#### Methods

- set_mode
- set_postion
- set_orientation
- set_active


### Database

#### Sensor

sensor_name
> | real_time | simulated_time | Output 1 | Output ... |
> | -- |-- | -- | -- |
> | "  | " | "  | " |
> | "  | " | "  | " |

#### Environment

> table name
> | real_time | simulated_time | Output 1 | Output ... |
>| -- | -- | -- | -- | 
>| "  | "  | "  | " | 
>| "  | "  | "  | " | 

#### Parameter table

Parameter bzw parameter werte am besten als json string
> sensor_name_parameters
> | real_time | simulated_time | changed parameter | parameter value |
>| -- | -- | -- | -- | 
>| "  | "  | "  | " | 
>| "  | "  | "  | " | 

### Table names

- magnetic_field_strength
- temperature
- earth_to_sat
- sat_to_sun
- earth_to_sun

### Outputs

- magnetic_field_strength: 3x1 in nT
- angular_velocity: array, 3x1 in rad/s
- temperature: float in K
- earth_to_sat: array, 3x1 in TEME
- sat_to_sun: array, 3x1 in sat frame
- earth_to_sun: array, 3x1 in TEME

### Inputs
- orbit: 

---

## Functional

## Environment
The Environment table is precalculated for a given orbit before the simulation. should the data needed for a timestep (simulated time) not be in the database it will be interpolated by the two adjacent values.

---

# Latex 

Zukünftige Ausarbeitungen in Latex

## Konventionen

- siunitx für einheiten
- Literatur in Zotero und dort taggen -> einfacher Export der Bibliographie



# Plots for GUI

## Sunsensor:

- error (sat_to_sun/ sat_to_sun_measured) plotten - **error calculation needs to be implemented**
- In one graph: photocurrent (1-6) plotten
- In one graph: fine sun current (1-2) plotten

