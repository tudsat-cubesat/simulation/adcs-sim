var files_dup =
[
    [ "acceleration_sensor.py", "acceleration__sensor_8py.html", "acceleration__sensor_8py" ],
    [ "config_loader.py", "config__loader_8py.html", "config__loader_8py" ],
    [ "dynamics.py", "dynamics_8py.html", "dynamics_8py" ],
    [ "earth_sensor.py", "earth__sensor_8py.html", "earth__sensor_8py" ],
    [ "environment.py", "environment_8py.html", [
      [ "environment.Environment", "classenvironment_1_1_environment.html", "classenvironment_1_1_environment" ]
    ] ],
    [ "fast_plot.py", "fast__plot_8py.html", "fast__plot_8py" ],
    [ "gyroscope.py", "gyroscope_8py.html", "gyroscope_8py" ],
    [ "internal_transfer.py", "internal__transfer_8py.html", [
      [ "internal_transfer.OutputLists", "classinternal__transfer_1_1_output_lists.html", "classinternal__transfer_1_1_output_lists" ]
    ] ],
    [ "json_grabber.py", "json__grabber_8py.html", [
      [ "json_grabber.MakeDict", "classjson__grabber_1_1_make_dict.html", "classjson__grabber_1_1_make_dict" ]
    ] ],
    [ "magnetometer.py", "magnetometer_8py.html", "magnetometer_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "orbit.py", "orbit_8py.html", "orbit_8py" ],
    [ "Sensor.py", "_sensor_8py.html", [
      [ "Sensor.Sensor", "class_sensor_1_1_sensor.html", "class_sensor_1_1_sensor" ]
    ] ],
    [ "sunsensor.py", "sunsensor_8py.html", "sunsensor_8py" ],
    [ "utilities.py", "utilities_8py.html", "utilities_8py" ],
    [ "view.py", "view_8py.html", "view_8py" ],
    [ "widgets.py", "widgets_8py.html", "widgets_8py" ]
];