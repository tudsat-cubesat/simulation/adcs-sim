#####################################################################################
#                                                                                   #
#                PLOT A LIVE GRAPH IN A PYQT WINDOW                                 #
#                EXAMPLE 2                                                          #
#               ------------------------------------                                #
# This code is inspired on:                                                         #
# https://learn.sparkfun.com/tutorials/graph-sensor-data-with-python-and-matplotlib/speeding-up-the-plot-animation  #
#                                                                                   #
#####################################################################################

from __future__ import annotations
from typing import *
import sqlite3
import utilities
import sys
import time
import os
from matplotlib.backends.qt_compat import QtCore, QtWidgets
# from PyQt5 import QtWidgets, QtCore
from matplotlib.backends.backend_qt5agg import FigureCanvas
# from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib as mpl
import matplotlib.figure as mpl_fig
import matplotlib.animation as anim
import numpy as np


class ApplicationWindow(QtWidgets.QMainWindow):
    '''
    The PyQt5 main window.

    '''
    def __init__(self):
        super().__init__()
        # 1. Window settings
        self.setGeometry(300, 300, 800, 400)
        self.setWindowTitle("Matplotlib live plot in PyQt - example 2")
        self.frm = QtWidgets.QFrame(self)
        self.frm.setStyleSheet("QWidget { background-color: #eeeeec; }")
        self.lyt = QtWidgets.QVBoxLayout()
        self.frm.setLayout(self.lyt)
        self.setCentralWidget(self.frm)

        # 2. Place the matplotlib figure
        self.myFig = MyFigureCanvas(x_len=200, y_range=[0, 100], interval=20)
        self.lyt.addWidget(self.myFig)

        # 3. Show
        self.show()
        return

class MyFigureCanvas(FigureCanvas, anim.FuncAnimation):
    '''
    This is the FigureCanvas in which the live plot is drawn.

    '''
    def __init__(self, x_len:int, y_range:List, interval:int) -> None:
        '''
        :param x_len:       The nr of data points shown in one plot.
        :param y_range:     Range on y-axis.
        :param interval:    Get a new datapoint every .. milliseconds.

        '''
        FigureCanvas.__init__(self, mpl_fig.Figure())
        # Range settings
        df = utilities.get_last_line_from_database_table("test.db", "angular_components", x_len)
        output = df[["omega_x"]].to_numpy()
        self._x_len_ = x_len
        # self._y_range_ = y_range
        # self._y_range_ = output
        print(self._x_len_)

        # Store two lists _x_ and _y_
        x = list(range(self._x_len_))
        y = list(np.transpose(output)[0])
        print(len(x))
        print(len(y))

        # Store a figure and ax
        self._ax_  = self.figure.subplots()
        self._ax_.set_ylim(ymin=min(output), ymax=max(output))
        self._line_, = self._ax_.plot(x, y)

        # Call superclass constructors
        anim.FuncAnimation.__init__(self, self.figure, self._update_canvas_, fargs=(y,), interval=interval, blit=True)
        return

    def _update_canvas_(self, i, y) -> None:
        '''
        This function gets called regularly by the timer.

        '''
        df = utilities.get_last_line_from_database_table("test.db", "angular_components")
        y.append(float(df["omega_x"]))
        y = y[-self._x_len_:]                        # Truncate list _y_
        self._line_.set_ydata(y)
        return self._line_,



if __name__ == "__main__":
    import threading

    def test_write():
        i = 0
        while True:
            conn_10 = sqlite3.connect('test.db')
            c_10 = conn_10.cursor()
            try:
                c_10.execute('create table angular_components (omega_x, omega_y, omega_z, alpha_x, alpha_y, alpha_z)')
            except:
                pass
            c_10.executemany('insert into angular_components VALUES (?, ?, ?, ?, ?, ?)',
                             [(np.sin(i * np.pi / 18), np.sin(i * np.pi / 18 + np.pi / 4),
                               np.sin(i * np.pi / 18 + np.pi / 2), 0, 0, 0)])
            conn_10.commit()
            c_10.close()
            i += 1
            time.sleep(0.01)




    t1 = threading.Thread(target=test_write, args=())
    t1.start()

    qapp = QtWidgets.QApplication(sys.argv)
    app = ApplicationWindow()
    qapp.exec_()

