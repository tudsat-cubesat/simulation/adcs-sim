import matplotlib.pyplot as plt
from numpy.linalg import norm
"""
a wrapper for all the different plotting specified in the sim.conf
"""
def make_plots(config, logs):
    if bool(int(config["plotting"]["attitude"])):
        plt.figure("attitude")
        plt.subplot(411)
        plt.title(r"Evolution of the attitude over time")
        plt.plot(logs["times"], logs["attitudes"][:, 0])
        plt.ylabel(r"$Q_0$")
        plt.subplot(412)
        plt.plot(logs["times"], logs["attitudes"][:, 1])
        plt.ylabel(r"$Q_1$")
        plt.subplot(413)
        plt.plot(logs["times"], logs["attitudes"][:, 2])
        plt.ylabel(r"$Q_2$")
        plt.subplot(414)
        plt.plot(logs["times"], logs["attitudes"][:, 3])
        plt.ylabel(r"$Q_3$")
        plt.xlabel(r"Time (s)")
        plt.subplots_adjust(
            left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)
    
    if bool(int(config["plotting"]["angular_velocity"])):
        plt.figure("angular_velocity")
        plt.subplot(411)
        plt.title(r"Evolution of the angular velocity over time ")
        plt.plot(logs["times"], logs["angular_velocities"][:, 0], label="actual")
        plt.ylabel(r"$\omega_x$ (rad/s)")
        plt.legend()
        plt.subplot(412)
        plt.plot(logs["times"], logs["angular_velocities"][:, 1], label="actual")
        plt.ylabel(r"$\omega_y$ (rad/s)")
        plt.legend()
        plt.subplot(413)
        plt.plot(logs["times"], logs["angular_velocities"][:, 2], label="actual")
        plt.ylabel(r"$\omega_z$ (rad/s)")
        plt.xlabel(r"Time (s)")
        plt.subplot(414)
        plt.plot(logs["times"], [norm(i) for i in logs["angular_velocities"]], label="total")
        plt.ylabel(r"$\omega$ (rad/s)")
        plt.xlabel(r"Time (s)")
        plt.legend()
        plt.subplots_adjust(
            left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)
    
    if bool(int(config["plotting"]["position"])):
        plt.figure("position")
        plt.subplot(311)
        plt.title(r"Evolution of the position over time ")
        plt.plot(logs["times"], logs["positions"][:, 0], label="actual")
        plt.ylabel(r"$r_x$ (km)")
        plt.legend()
        plt.subplot(312)
        plt.plot(logs["times"], logs["positions"][:, 1], label="actual")
        plt.ylabel(r"$r_y$ (km)")
        plt.legend()
        plt.subplot(313)
        plt.plot(logs["times"], logs["positions"][:, 2], label="actual")
        plt.ylabel(r"$r_z$ (km)")
        plt.xlabel(r"Time (s)")
        plt.legend()
        plt.subplots_adjust(
            left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)
    
    if bool(int(config["plotting"]["velocity"])):
        plt.figure("velocity")
        plt.subplot(311)
        plt.title(r"Evolution of the velocity over time ")
        plt.plot(logs["times"], logs["velocities"][:, 0], label="actual")
        plt.ylabel(r"$v_x$ (km/s)")
        plt.legend()
        plt.subplot(312)
        plt.plot(logs["times"], logs["velocities"][:, 1], label="actual")
        plt.ylabel(r"$v_y$ (km/s)")
        plt.legend()
        plt.subplot(313)
        plt.plot(logs["times"], logs["velocities"][:, 2], label="actual")
        plt.ylabel(r"$v_z$ (km/s)")
        plt.xlabel(r"Time (s)")
        plt.legend()
        plt.subplots_adjust(
            left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)

    plot_torque(logs, config, "gravity_gradient", "gravity_torque")
    
    plot_torque(logs, config, "solar", "solar_torque")
    
    plot_torque(logs, config, "atmosphere", "atmospheric_torque")

    plot_torque(logs, config, "random", "random_torque")
    
    if bool(int(config["plotting"]["magnetometer"])):
        for name in logs["sensor_measurements"].keys():
            if "mag" in name:
                plt.figure(name)
                plt.subplot(411)
                plt.title(r"Measurements of the Magnetometer over time ")
                plt.plot(logs["times"], logs["sensor_measurements"][name][:, 0])
                plt.ylabel(r"B_x in nT")
                plt.subplot(412)
                plt.plot(logs["times"], logs["sensor_measurements"][name][:, 1])
                plt.ylabel(r"B_y in nT")
                plt.subplot(413)
                plt.plot(logs["times"], logs["sensor_measurements"][name][:, 2])
                plt.ylabel(r"B_z in nT")
                plt.xlabel(r"Time (s)")
                plt.subplot(414)
                plt.plot(logs["times"], [norm(i) for i in logs["sensor_measurements"][name]], label="total")
                plt.ylabel(r"B in nT")
                plt.xlabel(r"Time (s)")
                plt.legend()
                plt.subplots_adjust(
                    left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)
    
    if bool(int(config["plotting"]["photodiode"])):
        for name in logs["sensor_measurements"].keys():
            if "photo" in name:
                plt.figure(name)
                plt.subplot(111)
                plt.title(r"Measurements of the Photodiode over time ")
                plt.plot(logs["times"], logs["sensor_measurements"][name])
                plt.ylabel(r"Intensity")
                plt.xlabel(r"Time (s)")
                plt.subplots_adjust(
                    left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)

    if bool(int(config["plotting"]["fineSunSensor"])):
        for name in logs["sensor_measurements"].keys():
            if "fine" in name:
                plt.figure(name)
                plt.subplot(111)
                plt.title(r"Measurements of the fine sun sensor over time ")
                plt.plot(logs["times"], logs["sensor_measurements"][name])
                plt.ylabel(r"Intensity")
                plt.xlabel(r"Time (s)")
                plt.subplots_adjust(
                    left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)

    plt.show()




def plot_torque(logs, config, config_name, logs_name):
    if bool(int(config["plotting"][config_name])):
        plt.figure("{} torque".format(config_name))
        plt.subplot(411)
        plt.title("Evolution of the {} torque over time".format(config_name))
        plt.plot(logs["times"], logs[logs_name][:, 0])
        plt.ylabel(r"$\tau_x$ (Nm)")
        plt.subplot(412)
        plt.plot(logs["times"], logs[logs_name][:, 1])
        plt.ylabel(r"$\tau_y$ (Nm)")
        plt.subplot(413)
        plt.plot(logs["times"], logs[logs_name][:, 2])
        plt.ylabel(r"$\tau_z$ (Nm)")
        plt.xlabel(r"Time (s)")
        plt.subplot(414)
        plt.plot(logs["times"], [norm(i) for i in logs[logs_name]], label="total")
        plt.ylabel(r"$\tau$ (Nm)")
        plt.xlabel(r"Time (s)")
        plt.legend()
        plt.subplots_adjust(
            left=0.08, right=0.94, bottom=0.08, top=0.94, hspace=0.3)