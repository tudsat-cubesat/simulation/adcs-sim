
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, \
    QAction, QTabWidget, QVBoxLayout, QLabel, \
    QGridLayout, QLineEdit, QGroupBox, QPlainTextEdit, QStyle
from PyQt5.QtCore import pyqtSlot, QTimer, Qt
from PyQt5.QtGui import QKeyEvent, QFont
import pyqtgraph as pg
import logging
import utilities
from ruamel import yaml
import config_loader


class PyQtGraph2dPlot(QWidget):
    def __init__(self, parent, app, database, plot_title: str,
                 table_name: str, columns: dict, refresh_rate: int,
                 left_label: list, bottom_label: list,
                 communication_mode: str):
        """ Pyqtgraph 2D-live-plot from sqlite database.

        :param parent: QWidget from PyQt5
        :param app: Application from GUI to process Events
        :param database: (str) name of database to read from / or Outputs class
        :param plot_title: (str) shown plot title in Graph
        :param table_name: (str) name of table in database to read from
        :param columns: (dict) name and color of every line in the plot
        :param refresh_rate: (int) update rate of the live plot in ms
        :param left_label: (list) name and unit of y label
        :param bottom_label: (list) name and unit of x label
        :param communication_mode: (string) choose between diffrent communications

        """
        super(QWidget, self).__init__(parent)
        self.app = app
        self.database = database
        self.communication_mode = communication_mode
        self.plot_title = plot_title
        self.table_name = table_name
        self.columns = columns
        self.refresh_rate = refresh_rate
        self.left_label = left_label
        self.bottom_label = bottom_label
        # initialize plot
        self.plt = pg.plot(title=self.plot_title)
        self.plt.setLabels(title=self.plot_title)
        self.plt.addLegend()
        self.plt.setLabel('left', self.left_label[0], self.left_label[1])
        self.plt.setLabel('bottom', self.bottom_label[0], self.bottom_label[1])
        # disable automatic unit scaling
        self.plt.getAxis('left').enableAutoSIPrefix(enable=False)

        # plot default curves
        self.x = []
        self.y = []
        self.curve = []
        for count, key in enumerate(columns):
            self.x.append([0])
            self.y.append([0])
            self.curve.append(self.plt.plot(self.x[count], self.y[count],
                                            name=key, pen=self.columns[key]))
        # call update function with a timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_plots)
        self.timer.start(refresh_rate)

    def update_plots(self):
        """ Update the plot """
        # try to read data out of database
        if self.communication_mode == 'sqlite':
            try:
                df = utilities.get_table_from_database(self.database,
                                                       self.table_name)
                for count, key in enumerate(self.columns):
                    self.curve[count].setData(df["simulation_time"], df[key])
            # give logging hint if connection to database failed
            except:
                logging.debug("problems with reading database table: " +
                              self.table_name)
        # try read data from internal list sensors
        elif self.communication_mode == 'sensors':
            try:
                df = self.database.simulation_outputs.output_dict[self.table_name]
                for count, key in enumerate(self.columns):
                    self.curve[count].setData(df["simulation_time"], df[key])

            except:
                logging.debug("problems with reading internal table: " +
                              self.table_name)
        # try read data from internal list sensors
        elif self.communication_mode == 'orbit_env':
            try:
                df = self.database.orbit_env_outputs.output_dict[self.table_name]
                for count, key in enumerate(self.columns):
                    self.curve[count].setData(df["simulation_time"], df[key])
            except:
                logging.debug("problems with reading internal table: " +
                              self.table_name)
        elif self.communication_mode == 'external_json':
            try:
                df = self.database.components['json_grabber'].internal_dict[self.table_name]
                for count, key in enumerate(self.columns):
                    self.curve[count].setData(df["simulation_time"], df[key])
            except:
                logging.debug("problems with reading internal table: " +
                              self.table_name)



        # process event
        self.app.processEvents()


class YamlEditorWindow(QWidget):
    def __init__(self, parent, simulation):
        """ Editor for YAML files """

        super(QWidget, self).__init__(parent)
        # make layout
        self.simulation = simulation
        self.groupbox = QGroupBox("Configuration")
        self.groupbox.layout = QGridLayout()
        self.groupbox.setLayout(self.groupbox.layout)
        # Status bar
        self.update_label = QLabel("Status: ready for user input")
        self.groupbox.layout.addWidget(self.update_label, 0, 0, 1, -1)
        # read button
        self.read_button = QPushButton("read file")
        self.read_button.setToolTip("Reads a file into the Box beneath")
        self.groupbox.layout.addWidget(self.read_button, 1, 2)
        self.read_button.clicked.connect(self.read_yaml)
        # file name
        self.describtion_yaml = QLabel("file name:")
        self.groupbox.layout.addWidget(self.describtion_yaml, 1, 0)
        self.yaml_name = QLineEdit( )
            # monospace
        f = QFont('Andalé Mono', 10)
        f.setStyleHint(QFont.Monospace)
        self.yaml_name.setFont(f)
        self.yaml_name.setPlaceholderText('default.yaml')
        self.groupbox.layout.addWidget(self.yaml_name, 1, 1)
            # enter event
        self.yaml_name.returnPressed.connect(self.read_button.click)
        # update button
        self.update_button = QPushButton("overwrite configuration")
        self.update_button.setToolTip("Creates or updates a file with the\n"
                                      "text form the box beneath")
        self.update_button.clicked.connect(self.write_yaml)
        self.groupbox.layout.addWidget(self.update_button, 1, 3)
        # text editor
        self.text_editor = QPlainTextEdit()
        self.text_editor.setFont(f)
        self.groupbox.layout.addWidget(self.text_editor, 2, 0, -1, -1)
        self.text_editor.zoomIn(10)
        self.parameters = ""

    def read_yaml(self):
        """ Read in a yaml file """
        # check if no file name was inserted, use then default
        if self.yaml_name.text() == '':
            with open('default.yaml', 'r') as stream:
                self.parameters = yaml.safe_load(stream)
                yaml_text = yaml.dump(self.parameters,
                                      Dumper=yaml.RoundTripDumper)
                self.text_editor.setPlainText(yaml_text)
                self.update_label.setText("Status: file 'default.yaml' "
                                          "has been read")
                self.update_label.setStyleSheet(
                    "background-color: green")
        else:
            try:
                # read in file if possible
                 with open(self.yaml_name.text( ), 'r') as stream:
                    self.parameters = yaml.safe_load(stream)
                    yaml_text = yaml.dump(self.parameters, Dumper=yaml.RoundTripDumper)
                    self.text_editor.setPlainText(yaml_text)
                    self.update_label.setText("Status: file '"+
                                              self.yaml_name.text( )+
                                              "' has been read")
                    self.update_label.setStyleSheet(
                        "background-color: green")

            except:
                # give error if reading wasn't possible
                self.update_label.setText(
                    "Status: no such file name")
                self.update_label.setStyleSheet("background-color: red")
                logging.info("no or wrong yaml name")



    def write_yaml(self):
        """ Override yaml file with informations from plain text."""
        # read plain text
        changed_text = self.text_editor.toPlainText()
        # don't allow to override default.yal
        if self.yaml_name.text() == "default.yaml":
            self.update_label.setText("you can't override the 'default.yaml'")
            self.update_label.setStyleSheet("background-color: red")
            return
        # check if tect isn't empty
        if changed_text != "":
            # try updating the file with the new text
            try:
                with open(self.yaml_name.text(), 'w+') as file:
                    dct = yaml.safe_load(changed_text)
                    yaml.dump(dct, file, Dumper=yaml.RoundTripDumper)
                    self.update_label.setText("Status: file '"+
                                              self.yaml_name.text()+
                                              "' has been updated")
                    self.update_label.setStyleSheet(
                        "background-color: green")
                    # update parameter if simulation is running
                    try:

                        self.simulation.update_simulation(dct)
                        self.update_label.setText("Status: file '" +
                                                  self.yaml_name.text( ) +
                                                  "' has been updated and"
                                                  " live parameter have been"
                                                  " changed")
                    except:
                        logging.debug("sim isn't running")

            # give error if updating the file failed
            except:
                self.update_label.setText("error: file couldn't be updated")
                self.update_label.setStyleSheet("background-color: red")
        # give error if no file name was given
        else:
            logging.warning("no text for file")
            self.update_label.setText("no text for file")
            self.update_label.setStyleSheet("background-color: red")





if __name__ == '__main__':
    class App(QMainWindow):

        def __init__(self, database: str):
            """ Create main window.

            :param database: (str) name of database to read data from
            :param sensor_names, (list) table name of sensors
            """
            super( ).__init__( )
            self.app = app
            self.app.setStyle('Fusion')
            self.title = 'ADCS GUI'
            self.database = database
            self.setWindowTitle(self.title)
            self.table_widget = MyTableWidget(self, self.database, self.app)
            self.setCentralWidget(self.table_widget)
            self.showMaximized( )


    class MyTableWidget(QWidget):

        def __init__(self, parent, database: str, app):
            """ Create the tabs for the main Window

            :param parent: class of main window
            :param database: name of database to read data from
            """
            super(QWidget, self).__init__(parent)
            self.app = app
            self.layout = QVBoxLayout(self)

            self.database = database
            # Initialize tab screen
            self.tabs = QTabWidget( )

            self.tab1 = QWidget( )
            self.tab2 = QWidget( )

            self.tabs.addTab(self.tab1, "environment")
            self.tabs.addTab(self.tab2, "magnetometer")
            self.tab1.layout = QGridLayout(self)
            self.tab1.setLayout(self.tab1.layout)
            self.tab2.layout = QGridLayout(self)
            self.tab2.setLayout(self.tab2.layout)

            lines = {"M_x" : (255, 0, 0), "M_y" : (0, 255, 0), "M_z" : (0, 0, 255)}

            self.plot_1 = PyQtGraph2dPlot(self, app=self.app, database=database, plot_title="magnetometer", table_name="FluxMag_X", columns=lines, refresh_rate=100, left_label=["mag", "Tesla"],
                                     bottom_label=["time", "s"])

            self.tab1.layout.addWidget(self.plot_1.plt, 0, 0)

            self.text = YamlEditorWindow(self, yaml_file="default.yaml")
            self.tab2.layout.addWidget(self.text.groupbox, 0, 0)

            self.layout.addWidget(self.tabs)
            self.setLayout(self.layout)


    app = QApplication(sys.argv)

    ex = App('test.db')
    sys.exit(app.exec_( ))













