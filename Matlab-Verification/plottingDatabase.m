%%%%%%%%%%%%%%%%%%%% Verification Case %%%%%%%%%%%%%%%%%%%%%%%%%%


%% import tables from database new
dbfile = fullfile(pwd,'test.db');
% dbfile = fullfile(pwd,'2_fullOrbit_new_nadirPointing1_test.db');

conn = sqlite(dbfile,'readonly')


% environment, orbit and dynamics 
sqlquery = 'SELECT * FROM eclipse';
eclipse = fetch(conn,sqlquery);

sim_time_environment = cell2mat(eclipse(:,2)); % sim_time environment (different sampling rates...)

eclipse = cell2mat(eclipse(:,3));

sqlquery = 'SELECT * FROM position';
position_TEME = fetch(conn,sqlquery);
position_TEME = cell2mat(position_TEME(:,3:end));

sqlquery = 'SELECT * FROM state_sat';
state_sat = fetch(conn,sqlquery);
sim_time_dynamics = cell2mat(state_sat(:,3));
angular_rate_dynamics = cell2mat(state_sat(:,8:end));
attitude_quat_dynamics = cell2mat(state_sat(:,4:7));


sqlquery = 'SELECT * FROM earth_to_sun';
earth2sun_TEME = fetch(conn,sqlquery);
earth2sun_TEME = cell2mat(earth2sun_TEME(:,3:end));

sqlquery = 'SELECT * FROM sat_to_sun';
sat2sun_body = fetch(conn,sqlquery);
sat2sun_body = cell2mat(sat2sun_body(:,4:end));
sat2sun_body(2:2:end,:) = [];

sqlquery = 'SELECT * FROM magnetic_field_strength';
magnetic_field = fetch(conn,sqlquery);
magnetic_field = cell2mat(magnetic_field(:,4:end));



% sensors 
sqlquery = 'SELECT * FROM photodiode';
photo_currents = fetch(conn,sqlquery);
sim_time = cell2mat(photo_currents(:,3));
photo_currents = cell2mat(photo_currents(:,4:end));

sqlquery = 'SELECT * FROM earth_sensor';
nadir_angle = fetch(conn,sqlquery);
nadir_angle = cell2mat(nadir_angle(:,4));

sqlquery = 'SELECT * FROM gyro';
angular_rate_gyro = fetch(conn,sqlquery);
sim_time_gyro = cell2mat(angular_rate_gyro(:,3));
angular_rate_gyro = cell2mat(angular_rate_gyro(:,4:end));

sqlquery = 'SELECT * FROM fluxgate';
fluxMag__x = fetch(conn,sqlquery);
fluxMag__x = cell2mat(fluxMag__x(:,4:end));

sqlquery = 'SELECT * FROM photo_measured_sunvector';
photo_vector = fetch(conn,sqlquery);
photo_vector = cell2mat(photo_vector(:,4:end));

CurrentDate=datestr(now,'dd-mm-yyyy-HH:MM');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% import tables from database old
% % dbfile = fullfile(pwd,'test.db');
% % dbfile = fullfile(pwd,'1_fullOrbit_nadirPointing1_test.db');
% dbfile = fullfile(pwd,'1_fullOrbit_nadirPointing2_test.db');
% % dbfile = fullfile(pwd,'1_fullOrbit_body2ECI-5degs_test.db');
% % dbfile = fullfile(pwd,'1_fullOrbit_body2ECI-noRotation_test.db');
% 
% conn = sqlite(dbfile,'readonly')
% 
% 
% % environment, orbit and dynamics 
% sqlquery = 'SELECT * FROM eclipse';
% eclipse = fetch(conn,sqlquery);
% 
% sim_time_environment = cell2mat(eclipse(:,2)); % sim_time environment (different sampling rates...)
% 
% eclipse = cell2mat(eclipse(:,3));
% 
% sqlquery = 'SELECT * FROM position';
% position_TEME = fetch(conn,sqlquery);
% position_TEME = cell2mat(position_TEME(:,3:end));
% 
% sqlquery = 'SELECT * FROM state_sat';
% state_sat = fetch(conn,sqlquery);
% sim_time_dynamics = cell2mat(state_sat(:,2));
% angular_rate_dynamics = cell2mat(state_sat(:,7:end));
% attitude_quat_dynamics = cell2mat(state_sat(:,3:6));
% 
% 
% sqlquery = 'SELECT * FROM earth_to_sun';
% earth2sun_TEME = fetch(conn,sqlquery);
% earth2sun_TEME = cell2mat(earth2sun_TEME(:,3:end));
% 
% sqlquery = 'SELECT * FROM sat_to_sun';
% sat2sun_body = fetch(conn,sqlquery);
% sat2sun_body = cell2mat(sat2sun_body(:,3:end));
% sat2sun_body(2:2:end,:) = [];
% 
% sqlquery = 'SELECT * FROM magnetic_field_strength';
% magnetic_field = fetch(conn,sqlquery);
% magnetic_field = cell2mat(magnetic_field(:,3:end));
% 
% 
% 
% % sensors 
% sqlquery = 'SELECT * FROM photo_currents';
% photo_currents = fetch(conn,sqlquery);
% sim_time = cell2mat(photo_currents(:,2));
% photo_currents = cell2mat(photo_currents(:,3:end));
% 
% sqlquery = 'SELECT * FROM EHS';
% nadir_angle = fetch(conn,sqlquery);
% nadir_angle = cell2mat(nadir_angle(:,3));
% 
% sqlquery = 'SELECT * FROM Gyroscope_sensor';
% angular_rate_gyro = fetch(conn,sqlquery);
% sim_time_gyro = cell2mat(angular_rate_gyro(:,2));
% angular_rate_gyro = cell2mat(angular_rate_gyro(:,3:end));
% 
% sqlquery = 'SELECT * FROM FluxMag_X';
% fluxMag__x = fetch(conn,sqlquery);
% fluxMag__x = cell2mat(fluxMag__x(:,3:end));
% 
% sqlquery = 'SELECT * FROM photo_measured_sunvector';
% photo_vector = fetch(conn,sqlquery);
% photo_vector = cell2mat(photo_vector(:,3:end));
% 
% CurrentDate=datestr(now,'dd-mm-yyyy-HH:MM');
% 
% 

%% import results from matlab simulation


time_matSim = linspace(1,size(out.mat_photo_current,1),size(out.mat_photo_current,1))';


% time_matSim = linspace(0,5600,56001)';  % for 0.1s timesteps 



% for i=1:size(out,1)
% end 
    
    
magField_NED = out.mat_magneticField_NED;
latlon = out.mat_lat_lon;
[x_ecef, y_ecef, z_ecef] = ned2ecefv(magField_NED(:,1),magField_NED(:,2),magField_NED(:,3),latlon(:,1),latlon(:,2));
magField_ECEF = [x_ecef, y_ecef, z_ecef];

utc = [2020 1 1 15 0 0];
% magField_ECI = ecef2eci(utc, magField_ECEF)
%  [x_eci, y_eci, z_eci]



%%%%%%%%%%%%%%%%%%%%%%%% plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% error plots


% error_pos_TEME = sqrt((position_TEME(1:50:end,:)-out.mat_earth2sat_TEME).^2);
error_pos_TEME = (position_TEME(1:50:end,:)-out.mat_earth2sat_TEME);
error_pos_TEME = vecnorm(error_pos_TEME,2,2);


f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time, error_pos_TEME(1:end-2,1),'b-','LineWidth',1.3)
% plot(sim_time, error_pos_TEME(1:end-1,1),'b--',sim_time, error_pos_TEME(1:end-1,2),'b:',...
%     sim_time, error_pos_TEME(1:end-1,3),'b-.','LineWidth',1.3)  
hold on
% legend('difference...','location','southeast')
set(gca,'FontSize',18)
xlabel('t in s')
ylabel('distance in km')
grid on 
% title('satellite position in TEME frame ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/error_position_TEME_',CurrentDate]) % for png




%% %%%%%%%%%%%%%% photodiode %%%%%%%%%%%%%%%%%%%%%%%%%%

% figure(1)
% plot(sim_time, photo_currents(:,5),'b',linspace(1,size(out.mat_photo_current,1),size(out.mat_photo_current,1))',out.mat_photo_current,'r','LineWidth',1.3)
% hold on
% legend('Python','Matlab','location','southeast')
% xlabel('t in s')
% ylabel('current in mA')
% grid on 
% title('photo current (Z+) ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/sunSensor/A_',CurrentDate]) % for png



% figure(2)
% % index =(t_3um_1>=0.01);
% plot(sim_time,photo_curr_Zpos,'r',sim_time, photo_curr_Zneg,'b','LineWidth',1.3)
% hold on
% legend('Z+','Z-','location','southeast')
% xlabel('t in s')
% ylabel('current in mA')
% grid on 
% title('photo currents python ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/sunSensor/B_',CurrentDate]) % for png



% f = figure('WindowState','maximized');
% pause(1);
% f.Position;
% plot(sim_time, photo_currents(:,3),'b',linspace(1,size(out.mat_photo_current,1),size(out.mat_photo_current,1))',out.mat_photo_current,'r','LineWidth',1.3)
% hold on
% legend('Python','Matlab','location','southeast')
% xlabel('t in s')
% ylabel('current in mA')
% grid on 
% title('photo current (Ypos) ')
% % print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/sunSensor/C_',CurrentDate]) % for png


% mat_photo_current = out.mat_photo_current(1:10:end);

f = figure('WindowState','maximized');
pause(1);
f.Position;
rectangle('Position',[518 0 2089 50], 'FaceColor',[220,220,220]/255, 'EdgeColor','w') %, 'FaceAlpha','[0.5]') 
rectangle('Position',[2590 0 2720 50], 'FaceColor',[215,126,44]/255, 'EdgeColor','w') %, 'FaceAlpha','[0.5]') 
hold on
plot(sim_time, photo_currents(:,6),'b-',sim_time,out.mat_photo_current(1:end-1),'r-','LineWidth',1.3)
hold on
set(gca,'FontSize',18)
legend('Framework (Python model)','Verification (Matlab model)','location','northeast')
xlabel('t in s')
ylabel('current in mA')
grid on 
% title('Output current photodiode')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/sunSensor/photoCurrent_Zneg_nadir_',CurrentDate]) % for png


% f = figure('WindowState','maximized');
% pause(1);
% f.Position;
% rectangle('Position',[518 0 2089 50], 'FaceColor',[220,220,220]/255, 'EdgeColor','w') %, 'FaceAlpha','[0.5]') 
% hold on
% plot(sim_time, photo_currents(:,6),'b-',time_matSim(1:end-100),out.mat_photo_current(1:end-100),'r-','LineWidth',1.3)
% hold on
% set(gca,'FontSize',18)
% legend('Python','Matlab','location','southeast')
% xlabel('t in s')
% ylabel('current in mA')
% grid on 
% % title('photo current (Zneg)')
% % print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/sunSensor/photoCurrent_Zneg_nadir_',CurrentDate]) % for png

%%

f = figure('WindowState','maximized');
pause(1);
f.Position;
rectangle('Position',[518 -1.2 2089 3], 'FaceColor',[220,220,220]/255, 'EdgeColor','w') %, 'FaceAlpha','[0.5]') 
hold on
plot(sim_time, sat2sun_body(:,1),'b--',sim_time,sat2sun_body(:,2),'b:',...
    sim_time,sat2sun_body(:,3),'b-.',sim_time,photo_vector(:,1)...
    ,'r--',sim_time,photo_vector(:,2),'r:',...
    sim_time,photo_vector(:,3),'r-.','LineWidth',1.3)  
% ylim([-1.2 1.7])
ylim([-1.0 1.0])
hold on
set(gca,'FontSize',18)
legend('x_{dyn}','y_{dyn}','z_{dyn}','x_{photo}','y_{photo}','z_{photo}','location','northeast','NumColumns',2)
xlabel('t in s')
% ylabel('')
grid on 
% title('sat2sun_{body} - dynamics simulation vs. measured vector from currents ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/photoVector_',CurrentDate]) % for png


%% %%%%%%%%%%%%%%% earth sensor %%%%%%%%%%%%%%%%%%%%%%%%%%

% nadirAngle_offset =out.mat_nadirAngle - 90;
% nadir_angle = (nadir_angle -180).*-1 ;

f = figure('WindowState','maximized');
pause(1);
f.Position;
% plot(sim_time, nadir_angle,'b',time_matSim,out.mat_nadirAngle,'r','LineWidth',1.3)
plot(sim_time, nadir_angle,'b-.',time_matSim,out.mat_nadirAngle,'r-.','LineWidth',1.3)
% ylim([150 210])
hold on
set(gca,'FontSize',18)
legend('Python','Matlab','location','southeast')
xlabel('t in s')
ylabel('angle in deg')
grid on 
% title('nadir angle ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/earthSensor/nadirAngle_stationary_',CurrentDate]) % for png

%% %%%%%%%%%%%%%%% magnetometer %%%%%%%%%%%%%%%%%%%%%%%%%%

f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time, fluxMag__x(:,1),'b--',sim_time,fluxMag__x(:,2),'b:',...
    sim_time,fluxMag__x(:,3),'b-.',time_matSim,out.mat_magneticField_body(:,1)...
    ,'r--',time_matSim,out.mat_magneticField_body(:,2),'r:',...
    time_matSim,out.mat_magneticField_body(:,3),'r-.','LineWidth',1.4)  
hold on
legend('x_{py}','y_{py}','z_{py}','x_{mat}','y_{mat}','z_{mat}','location','southeast','NumColumns',2)
set(gca,'FontSize',18)
xlabel('t in s')
ylabel('magnetic field strength in nT')
grid on 
% title('Magnetometer output in body frame')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/magnetometer/fluxMag_x_stationary_',CurrentDate]) % for png


%% %%%%%%%%%%%%%%% gyroscope %%%%%%%%%%%%%%%%%%%%%%%%%%

f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_gyro, angular_rate_gyro(:,1),'b-',sim_time_gyro,angular_rate_gyro(:,2),'b:',...
    sim_time_gyro,angular_rate_gyro(:,3),'b-.',time_matSim,out.mat_angularRate_noNoise(:,1)...
    ,'r-',time_matSim,out.mat_angularRate_noNoise(:,2),'r:',...
    time_matSim,out.mat_angularRate_noNoise(:,3),'r-.','LineWidth',1.3)
hold on
plot(sim_time_dynamics, angular_rate_dynamics(:,1),'g-',sim_time_dynamics,...
    angular_rate_dynamics(:,2),'g:',sim_time_dynamics, angular_rate_dynamics(:,3),'g-.', 'LineWidth', 1.3)
hold on
set(gca,'FontSize',18)
legend('x_{gyro}','y_{gyro}','z_{gyro}','x_{mat} dynamics-sim','y_{mat}',...
    'z_{mat}','x_{py} dynamics-sim','y_{py}','z_{py}','location','southeast')
% ylim([-0.01 0.01])
xlabel('t in s')
ylabel('angular rate in rad/s')
grid on 
title('Gyroscope, Python- and Matlab-Dynamics')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/gyro/gyroscope1_',CurrentDate]) % for png

%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_gyro, angular_rate_gyro(:,1),'b-',sim_time_gyro,angular_rate_gyro(:,2),'b:',...
    sim_time_gyro,angular_rate_gyro(:,3),'b-.',time_matSim,out.mat_angularRate_noNoise(:,1)...
    ,'r-',time_matSim,out.mat_angularRate_noNoise(:,2),'r:',...
    time_matSim,out.mat_angularRate_noNoise(:,3),'r-.','LineWidth',1.3)
hold on
% plot(sim_time_dynamics, angular_rate_dynamics(:,1),'g-',sim_time_dynamics,...
%     angular_rate_dynamics(:,2),'g:',sim_time_dynamics, angular_rate_dynamics(:,3),'g-.', 'LineWidth', 1.3)
% hold on
set(gca,'FontSize',18)
legend('x_{gyro}','y_{gyro}','z_{gyro}','x_{mat} dynamics-sim','y_{mat}',...
    'z_{mat}','x_{py} dynamics-sim','y_{py}','z_{py}','location','southeast')
% ylim([-0.01 0.01])
xlabel('t in s')
ylabel('angular rate in rad/s')
grid on 
title('Gyroscope, Python- and Matlab-Dynamics')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/gyro/gyroscope1_',CurrentDate]) % for png

%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_gyro, angular_rate_gyro(:,1),'b-',sim_time_gyro,angular_rate_gyro(:,2),'b:',...
    sim_time_gyro,angular_rate_gyro(:,3),'b-.',time_matSim,out.mat_angularRate_noNoise(:,1)...
    ,'r-',time_matSim,out.mat_angularRate_noNoise(:,2),'r:',...
    time_matSim,out.mat_angularRate_noNoise(:,3),'r-.','LineWidth',1.3)
hold on
set(gca,'FontSize',18)
legend('x_{gyro}','y_{gyro}','z_{gyro}','x_{mat}','y_{mat}',...
    'z_{mat}','location','southeast','NumColumns',2)
ylim([-0.0015 0.0015])
xlabel('t in s')
ylabel('angular rate in rad/s')
grid on 
% title('Gyroscope, Python- and Matlab-Dynamics')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/gyro/gyroscope1_',CurrentDate]) % for png
% 
% f = figure('WindowState','maximized');
% pause(1);
% f.Position;
% plot(sim_time_gyro, angular_rate_gyro(:,1),'b',time_matSim,out.mat_angularRate_noNoise(:,1)...
%     ,'r-','LineWidth',1.5)
% hold on
% plot(sim_time_dynamics, angular_rate_dynamics(:,1),'g', 'LineWidth', 0.8)
% % ylim([0.015 0.02])
% hold on
% set(gca,'FontSize',18)
% legend('x_{gyro}','x_{mat} dynamics-sim','x_{py} dynamics-sim','location','southeast')
% xlabel('t in s')
% ylabel('angular rate in rad/s')
% grid on 
% title('Gyroscope, Python- and Matlab-Dynamics')
% % print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/gyro/gyroscope2_',CurrentDate]) % for png

%% %%%%%%%%%%%%%% environment and orbit %%%%%%%%%%%%%%%%%%%%%%%%%%



f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time, sat2sun_body(:,1),'b--',sim_time,sat2sun_body(:,2),'b:',...
    sim_time,sat2sun_body(:,3),'b-.',time_matSim,out.mat_sat2sun_body(:,1)...
    ,'r--',time_matSim,out.mat_sat2sun_body(:,2),'r:',...
    time_matSim,out.mat_sat2sun_body(:,3),'r-.','LineWidth',1.3)  
hold on
set(gca,'FontSize',18)
legend('x_{py}','y_{py}','z_{py}','x_{mat}','y_{mat}','z_{mat}','location','northeast','NumColumns',2)
xlabel('t in s')
% ylabel('')
grid on 
% title('satellite to sun vector in body frame ')
print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/sat2sun_nadir_',CurrentDate]) % for png


%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_environment, earth2sun_TEME(:,1),'b--',sim_time_environment,earth2sun_TEME(:,2),'b:',...
    sim_time_environment,earth2sun_TEME(:,3),'b-.',time_matSim,out.mat_earth2sun_TEME(:,1)...
    ,'r--',time_matSim,out.mat_earth2sun_TEME(:,2),'r:',...
    time_matSim,out.mat_earth2sun_TEME(:,3),'r-.','LineWidth',1.3)  
hold on
legend('x_{py}','y_{py}','z_{py}','x_{mat}','y_{mat}','z_{mat}','location','southeast')
xlabel('t in s')
% ylabel('')
grid on 
title('earth to sun vector in TEME frame ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/earth2sun_',CurrentDate]) % for png
%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_environment, position_TEME(:,1),'b--',sim_time_environment,position_TEME(:,2),'b:',...
    sim_time_environment,position_TEME(:,3),'b-.',time_matSim,out.mat_earth2sat_TEME(:,1)...
    ,'r--',time_matSim,out.mat_earth2sat_TEME(:,2),'r:',...
    time_matSim,out.mat_earth2sat_TEME(:,3),'r-.','LineWidth',1.3)  
hold on
legend('x_{py}','y_{py}','z_{py}','x_{mat}','y_{mat}','z_{mat}','location','southeast','NumColumns',2)
set(gca,'FontSize',18)
xlabel('t in s')
ylabel('distance in km')
grid on 
% title('satellite position in TEME frame ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/position_TEME_',CurrentDate]) % for png

%%

% f = figure('WindowState','maximized');
% pause(1);
% f.Position;
% plot(sim_time_environment, magnetic_field(:,1),'b--',sim_time_environment,magnetic_field(:,2),'b:',...
%     sim_time_environment,magnetic_field(:,3),'b-.',time_matSim,out.mat_magneticField_TEME(:,1)...
%     ,'r--',time_matSim,out.mat_magneticField_TEME(:,2),'r:',...
%     time_matSim,out.mat_magneticField_TEME(:,3),'r-.','LineWidth',1.3)  
% hold on
% legend('x_{py}','y_{py}','z_{py}','x_{mat}','y_{mat}','z_{mat}','location','southeast')
% xlabel('t in s')
% ylabel('magnetic field strength in nT')
% grid on 
% title('IGRF - Magnetic Field (TEME)')
% % print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/magneticFieldStrength_',CurrentDate]) % for png


%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
plot(sim_time_dynamics, attitude_quat_dynamics(:,1),'b--',sim_time_dynamics,attitude_quat_dynamics(:,2),'b+',...
    sim_time_dynamics,attitude_quat_dynamics(:,3),'r-.',sim_time_dynamics,attitude_quat_dynamics(:,4),'g','LineWidth',1.3)  
hold on
legend('q_{r,py}','q_{py}','q_{py}','q_{py}','location','southeast')
xlabel('t in s')
% ylabel('')
grid on 
title('quaternion eci2body ')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/quaternion_eci2body_',CurrentDate]) % for png


%%
f = figure('WindowState','maximized');
pause(1);
f.Position;
% figure('Color',[0.8 0.8 0.8]); set(gca, 'color', [1 1 0])
plot(sim_time_environment, eclipse,'bo',time_matSim,out.mat_eclipse,'r--','MarkerSize',1.2,'LineWidth',1.3)  
ylim([-0.1 1.1])
hold on
legend('Python','Matlab','location','southeast')
xlabel('t in s')
ylabel('eclipse')
% set(gca, 'color', [1 1 0])
grid on 
title('if eclipse=1: satellite in earth shadow')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/eclipse_',CurrentDate]) % for png



%%

% light grey: [220,220,220]/255
% 

f = figure('WindowState','maximized');
pause(1);
f.Position;
% figure('Color',[0.8 0.8 0.8]); set(gca, 'color', [1 1 0])
rectangle('Position',[518 0 2089 1.1], 'FaceColor',[220,220,220]/255, 'EdgeColor','w') %, 'FaceAlpha','[0.5]') 
hold on
plot(sim_time_environment, eclipse,'bo',time_matSim,out.mat_eclipse,'r--','MarkerSize',1.2,'LineWidth',1.3)  
ylim([-0.1 1.1])
hold on
legend('Python','Matlab','location','southeast')
xlabel('t in s')
ylabel('eclipse')
% set(gca, 'color', [1 1 0])
grid on 
title('if eclipse=1: satellite in earth shadow')
% print('-dpng', '-r500', ['/home/felix/Documents/Uni/ADP_CubeSat/plots/environment/eclipse_rectangle_',CurrentDate]) % for png
