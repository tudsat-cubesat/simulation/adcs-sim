import json
import numpy as np
import pandas as pd
import sqlite3
import datetime as dt
import time
from uuid import uuid4
from scipy.spatial.transform import Rotation as R
import utilities
from Sensor import Sensor
import logging

# todo: update parameter funktion nicht gefunden
#   rad ord deg von dynamik
#   debug run wgen timestamp change
#   falschrum geschwindigkeit ist gespiegelt
#   Nulldivision bug: Wenn state_sat nur initiale Werte mit 
#       `simulation_time: 0` hat, wird Fehler aus apply_sensitivity,
#       utilities.py line 33 ausgegeben: "ValueError: cannot convert 
#       float NaN to integer".


class MEMS3AxisGyroscope(Sensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, mode: int,
                 digital: bool, outputs, sensor_range: np.ndarray, sensitivity: float, sampling_rate: float,
                 angular_random_walk: float, bias_instability: float, active: bool = True):
        """Simulation of a 3-axis MEMS-Gyroscope

        The error model is based on a random noise, that is added to the real angular velocity
        :param name: (string) name of the sensor
        :param position: (numpy array, 3x1 x y z) position of the sensor in the satellite frame in m
        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the sensor in quaternions in the
                                                                                                    satellite frame
        :param database: (string, .db) name of the database to read/write from/in
        :param digital: (bool) defines if the sensor is digital or analog
        :param mode: (int) chooses between a different set of predefined parameter for the sensor
        :param active: (Boolean) activates or deactivates the sensor
        :param sensor_range: (numpy array, 1x2 +range -range) maximum measurable angular velocity in deg/s
        :param sensitivity: (float) sensitivity of the sensor in deg/s
        :param sampling_rate: (float) sampling rate of the sensor in Hz
        :param angular_random_walk: (float) angular random walk error in deg/sqrt(hr)
        :param bias_instability: (float) bias instability error in deg/hr
        """

        self.sensor_range = sensor_range
        self.sensitivity = sensitivity
        self.sampling_rate = sampling_rate
        # angular random walk
        self.angular_random_walk = angular_random_walk
        # bias instability
        self.bias_instability = bias_instability
        # import init from sensor
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)

        # create table for the output of the sensor in the database, if it doesn't already exist
        # utilities.create_table(self.database, self.name, 'real_time, simulation_time, omega_x_1, omega_y_1, omega_z_1')
        self.column_names = ['real_time', 'simulation_time', 'omega_x_1', 'omega_y_1', 'omega_z_1']

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def output(self, simulated_time):
        """ Run Sensor one time.

        Basic procedure: read in angular velocity of satellite -> transform it into the sensor frame
        -> add bias instability -> add angular random walk -> apply sensitivity -> limit to range -> write in database
        """
        # connect to database and read the angular velocity of the satellite
        try:
            angular_components = utilities.calc_angular_motion_parameters(
                self.database)
        except:
            logging.error("no dynamic simulation table found")
            return
        self.check_parameter_update()
        omega_x_0 = angular_components[0]
        omega_y_0 = angular_components[1]
        omega_z_0 = angular_components[2]
        # create angular velocity vector of the satellite frame
        omega_0 = np.array([omega_x_0, omega_y_0, omega_z_0])
        # transform angular velocity into sensor frame
        omega_1 = utilities.get_quaternion_angular_velocity_transformation(
            self.orientation, omega_0)
        # split up the angular velocity of the sensor frame
        omega_x_1 = omega_1[0]
        omega_y_1 = omega_1[1]
        omega_z_1 = omega_1[2]

        # apply the sensitivity to the output
        omega_x_1 = utilities.apply_sensitivity(omega_x_1, self.sensitivity * np.pi / 180)
        omega_y_1 = utilities.apply_sensitivity(omega_y_1, self.sensitivity * np.pi / 180)
        omega_z_1 = utilities.apply_sensitivity(omega_z_1, self.sensitivity * np.pi / 180)

        # add bias instability error
        omega_x_1 = np.random.normal(omega_x_1, self.bias_instability * np.pi / 180 / 3600, 1)
        omega_y_1 = np.random.normal(omega_y_1, self.bias_instability * np.pi / 180 / 3600, 1)
        omega_z_1 = np.random.normal(omega_z_1, self.bias_instability * np.pi / 180 / 3600, 1)

        # add angular random walk error
        omega_x_1 = np.random.normal(omega_x_1, self.angular_random_walk * np.pi / 180 / 60, 1)
        omega_y_1 = np.random.normal(omega_y_1, self.angular_random_walk * np.pi / 180 / 60, 1)
        omega_z_1 = np.random.normal(omega_z_1, self.angular_random_walk * np.pi / 180 / 60, 1)

        # set the max sensible angular velocity
        if omega_x_1[0] > self.sensor_range[0] * np.pi / 180:
            omega_x_1[0] = self.sensor_range[0] * np.pi / 180
        if omega_x_1[0] < self.sensor_range[1] * np.pi / 180:
            omega_x_1[0] = self.sensor_range[1] * np.pi / 180
        if omega_y_1[0] > self.sensor_range[0] * np.pi / 180:
            omega_y_1[0] = self.sensor_range[0] * np.pi / 180
        if omega_y_1[0] < self.sensor_range[1] * np.pi / 180:
            omega_y_1[0] = self.sensor_range[1] * np.pi / 180
        if omega_z_1[0] > self.sensor_range[0] * np.pi / 180:
            omega_z_1[0] = self.sensor_range[0] * np.pi / 180
        if omega_z_1[0] < self.sensor_range[1] * np.pi / 180:
            omega_z_1[0] = self.sensor_range[1] * np.pi / 180
        # write sensor output into the database
        if self.active is True:
            columns_content = (str(dt.datetime.now(tz=None)), simulated_time, omega_x_1[0], omega_y_1[0], omega_z_1[0])
            # utilities.write_line_into_database_table(self.database, self.name, columns_content)

            self.outputs.safe_output(self.name, self.column_names,
                                     list(columns_content))

    def run(self):
        """ Starts sensor in while Loop.

        """
        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output(float(current_time_stamp))
            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script to slow for the set sampling rate of the "+ self.name)


if __name__ == "__main__":
    import threading

    r = R.from_matrix([[1, 0, 0],
                       [0, 1, 0],
                       [0, 0, 1]])
    a = MEMS3AxisGyroscope("Gyroscope_sensor", np.array([0]), r.as_quat(),
                           'gyro_degbug.db', 1, False, np.array([400, -400]), 0.05, 0.5, 0.3, 0.15)


    def test_write():
        i = 1
        while True:

            conn_10 = sqlite3.connect('gyro_degbug.db')
            c_10 = conn_10.cursor()
            try:
                c_10.execute('create table state_sat (real_time, simulation_time, q_r, q_i, q_j, q_k, w_x, w_y, w_z)')
            except:
                pass

            c_10.executemany('insert into state_sat VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                             [(0, i, np.sin(i * np.pi / 18), np.sin(i * np.pi / 18 + np.pi / 4),
                               np.sin(i * np.pi / 18 + np.pi / 2), np.sin(i * np.pi / 18 + np.pi / 2), 0, 0, 0)])
            conn_10.commit()
            c_10.close()
            i += 1
            time.sleep(0.02)


    t1 = threading.Thread(target=test_write, args=())
    t1.start()
    print('thread 1 aktiv')
    time.sleep(1)
    t2 = threading.Thread(target=a.run, args=())
    t2.start()
    print('thread 2 aktiv')
    time.sleep(6)


