import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, \
    QAction, QTabWidget, QVBoxLayout, QLabel, \
    QGridLayout, QLineEdit, QGroupBox, QComboBox, QProgressBar, QScrollBar
import widgets
from PyQt5.QtGui import QIcon, QPalette, QColor
from PyQt5.QtCore import pyqtSlot, QTimer, Qt, QThread, QProcess, QRect
import utilities
import collections
import numpy as np
import pyqtgraph as pg
import logging
import multiprocessing as mp
import main
from ruamel import yaml
import config_loader

class App(QMainWindow):
    def __init__(self):
        """ Create main window. """
        self.app = QApplication(sys.argv)
        super().__init__()
        self.app.setStyle('Fusion')

        # dark theme for no obvious reason
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(25, 25, 25))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, Qt.black)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)
        palette.setColor(QPalette.Link, QColor(42, 130, 218))
        palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        palette.setColor(QPalette.HighlightedText, Qt.black)
        self.app.setPalette(palette)

        self.title = 'ADCS GUI'
        self.setWindowTitle(self.title)
        # database
        self.database = "test.db"
        self.table_widget = Tabs(self, self.app, self.database)
        self.setCentralWidget(self.table_widget)
        self.showMaximized()

    def run(self):
        sys.exit(self.app.exec_())


class Tabs(QWidget):
    def __init__(self, parent, app, database):
        """ Create the tabs for the main Window

        :param app: Application
        :param parent: class of main window
        :param database: name of database to read data from
        """
        super(QWidget, self).__init__(parent)
        self.app = app
        self.database = database
        self.layout = QVBoxLayout(self)
        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tab4 = QWidget()
        self.landing_tab = QWidget()
        self.tab5 = QWidget()
        self.tab6 = QWidget()
        self.simulation = main.Main()
        self.parameters = ""

        # initialize threads
        self.thread_start_sim = QThread()
        self.worker_start_sim = self.simulation
        self.thread_prep_sim = QThread()
        self.worker_prep_sim = self.simulation
        self.thread_full_sim = QThread()
        self.worker_full_sim = self.simulation

        # Add tabs
        self.tabs.addTab(self.landing_tab, "Initialization")
        self.tabs.addTab(self.tab1, "Environment")
        self.tabs.addTab(self.tab2, "Magnetometer")
        self.tabs.addTab(self.tab3, "Earth and sun sensor")
        self.tabs.addTab(self.tab4, "Gyro and accelerometer")
        self.tabs.addTab(self.tab5, "Actuators")
        self.tabs.addTab(self.tab6, "Real sensors")

        # region: landing tab
        self.landing_tab.layout = QGridLayout(self)
        self.status_sim = QLabel("Progress bar:")
        self.chose_sim = QComboBox()
        self.chose_sim.addItem('Full simulation')
        self.chose_sim.addItem('Orbit and environment simulation')
        self.chose_sim.addItem('Sensor simulation')
        self.start_button = QPushButton('Start simulation')
        self.start_button.clicked.connect(self.start)
        self.progress_bar_landing = QProgressBar(self)
        self.progress_timer = QTimer( )
        self.progress_timer.timeout.connect(self.update_progress_bar)
        self.progress_timer.start(3000)
        # mode for progress bar
        self.sim_mode = ""
        self.landing_tab.layout.addWidget(self.chose_sim, 0, 0)
        self.landing_tab.layout.addWidget(self.start_button, 0, 1)
        self.landing_tab.layout.addWidget(self.status_sim, 0, 2)
        self.landing_tab.layout.addWidget(self.progress_bar_landing, 0, 3)

        self.parameter_editor = widgets.YamlEditorWindow(
            self, self.simulation)
        self.landing_tab.layout.addWidget(self.parameter_editor.groupbox, 1, 0,
                                          -1, -1)
        self.landing_tab.setLayout(self.landing_tab.layout)
        # endregion

        # region FIRST TAB: ORBIT AND ENVIRONMENT
        self.tab1.layout = QGridLayout(self)
        self.tab1.setLayout(self.tab1.layout)
        self.progress_bar_tab1 = QProgressBar(self)
        self.tab1.layout.addWidget(self.progress_bar_tab1, 0, 0, 1, -1)

        # magnetic field
        env_mag_columns = {"M_x": (255, 0, 0), "M_y": (0, 255, 0),
                           "M_z": (0, 0, 255)}
        env_mag_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation,
            plot_title="magnetic field", table_name="magnetic_field_strength",
            columns=env_mag_columns, refresh_rate=2000,
            left_label=["magnetic field strength", "nT"],
            bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_mag_plot.plt, 1, 0, 1, 2)
        # density
        env_density_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation, plot_title="density",
            table_name="atmospheric_density", columns={"density": (255, 0, 0)},
            refresh_rate=2000, left_label=["density", "kg/m³"],
            bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_density_plot.plt, 1, 2, 1, 2)
        # position
        env_velocity_columns = {"v_x": (255, 0, 0), "v_y": (0, 255, 0),
                                "v_z": (0, 0, 255)}
        env_velocity_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation, plot_title="velocity",
            table_name="velocity", columns=env_velocity_columns,
            refresh_rate=2000, left_label=["velocity", "km/s"],
            bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_velocity_plot.plt, 2, 0, 1, 2)

        # position
        env_position_columns = {"x": (255, 0, 0), "y": (0, 255, 0),
                                "z": (0, 0, 255)}
        env_position_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation, plot_title="position",
            table_name="position", columns=env_position_columns,
            refresh_rate=2000, left_label=["position", "km"],
            bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_position_plot.plt, 2, 2, 1, 2)
        # earth to sun vector
        env_earth_to_sun_columns = {"sun_vector_x": (255, 0, 0),
                                    "sun_vector_y": (0, 255, 0),
                                    "sun_vector_z": (0, 0, 255)}
        env_earth_to_sun_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation,
            plot_title="earth to sun vector", table_name="earth_to_sun",
            columns=env_earth_to_sun_columns, refresh_rate=2000,
            left_label=["earth to sun", ""],
            bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_earth_to_sun_plot.plt, 3, 0, 1, 2)

        # solar pressure
        env_solar_pressure_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation,
            plot_title="solar pressure", table_name="solar_pressure",
            columns={"solar_pressure": (255, 0, 0)}, refresh_rate=2000,
            left_label=["density", "Pa"], bottom_label=["simulation time", "s"],
            communication_mode='orbit_env'
        )
        self.tab1.layout.addWidget(env_solar_pressure_plot.plt, 3, 2, 1, 2)
        # endregion

        # region SECOND TAB: MAGNETOMETER
        self.tab2.layout = QGridLayout(self)
        self.tab2.setLayout(self.tab2.layout)
        self.progress_bar_tab2 = QProgressBar(self)
        self.tab2.layout.addWidget(self.progress_bar_tab2, 0, 0, 1, -1)
        # magnetometer
        magnetometer_columns = {"M_x": (255, 0, 0), "M_y": (0, 255, 0),
                                "M_z": (0, 0, 255)}
        magnetometer_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app,
            database=self.simulation,
            plot_title="Fluxgate Magnetometer", table_name="FluxMag_X",
            columns=magnetometer_columns, refresh_rate=100,
            left_label=["magnetic field", "nT"],
            bottom_label=["simulation time", "s"],
            communication_mode='sensors'
        )
        self.tab2.layout.addWidget(magnetometer_plot.plt, 1, 0)
        # endregion

        # region THIRD TAB: SUN AND EARTH SENSOR
        self.tab3.layout = QGridLayout(self)
        self.tab3.setLayout(self.tab3.layout)
        self.progress_bar_tab3 = QProgressBar(self)
        self.tab3.layout.addWidget(self.progress_bar_tab3, 0, 0, 1, -1)
        # sun sensor
        fine_sun_sensor_columns = {"fine_sun_current_1": (255, 0, 0),
                                   "fine_sun_current_2": (0, 255, 0)}

        fine_sun_sensor_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app,
            database=self.simulation,
            plot_title="Fine Sun Sensor", table_name="finesunsensor",
            columns=fine_sun_sensor_columns, refresh_rate=100,
            left_label=["Output current", "Ampere"],
            bottom_label=["simulation time", "s"],
            communication_mode='sensors'
        )
        self.tab3.layout.addWidget(fine_sun_sensor_plot.plt, 1, 0)
        photo_sun_sensor_columns = {"Xpos": (255, 0, 0), "Xneg": (128, 0, 0),
                                    "Ypos": (0, 255, 0), "Yneg": (0, 128, 0),
                                    "Zpos": (0, 255, 255), "Zneg": (0, 0, 255)}
        photo_sun_sensor_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app,
            database=self.simulation,
            plot_title="Photodiode", table_name="photodiode",
            columns=photo_sun_sensor_columns, refresh_rate=100,
            left_label=["Output current", "Ampere"],
            bottom_label=["simulation time", "s"],
            communication_mode='sensors'
        )
        self.tab3.layout.addWidget(photo_sun_sensor_plot.plt, 2, 0)
        # earth sensor
        earth_sensor_sensor_columns = {"Real_Angle": (255, 0, 0), "Estimated_Angle": (0, 0, 255)}

        earth_sensor_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app,
            database=self.simulation,
            plot_title="earth sensor", table_name="earth_sensor",
            columns=earth_sensor_sensor_columns, refresh_rate=100,
            left_label=["earth radius", "deg"],
            bottom_label=["simulation time", "s"],
            communication_mode='sensors'
        )
        self.tab3.layout.addWidget(earth_sensor_plot.plt, 3, 0)
        # endregion

        # region FOURTH TAB: GYRO AND ACCELERATION SENSOR
        self.tab4.layout = QGridLayout(self)
        self.tab4.setLayout(self.tab4.layout)
        self.progress_bar_tab4 = QProgressBar(self)
        self.tab4.layout.addWidget(self.progress_bar_tab4, 0, 0, 1, -1)
        # gyroscope
        gyro_columns = {"omega_x_1": (255, 0, 0), "omega_y_1": (0, 255, 0),
                        "omega_z_1": (0, 0, 255)}
        gyro_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation,
            plot_title="Gyroscope", table_name="gyro", columns=gyro_columns,
            refresh_rate=100, left_label=["angular velocity", "rad/s"],
            bottom_label=["simulation time", "s"], communication_mode='sensors')
        self.tab4.layout.addWidget(gyro_plot.plt, 1, 0)
        # acceleration sensor
        acceleration_columns = {"a_x_1": (255, 0, 0),
                                "a_y_1": (0, 255, 0),
                                "a_z_1": (0, 0, 255)}
        acceleration_plot = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation,
            plot_title="Acceleration Sensor", table_name="acceleration",
            columns=acceleration_columns, refresh_rate=100,
            left_label=["acceleration", "m/s²"],
            bottom_label=["simulation time", "s"], communication_mode='sensors')
        self.tab4.layout.addWidget(acceleration_plot.plt, 2, 0)
        # region: FIFTH TAB ACTUATORS
        self.tab5.layout = QGridLayout(self)
        self.tab5.setLayout(self.tab5.layout)
        self.progress_bar_tab5 = QProgressBar(self)
        self.tab5.layout.addWidget(self.progress_bar_tab5, 0, 0, 1, -1)
        actuator_columns = {"F_x": (255, 0, 0),
                            "F_y": (0, 255, 0),
                            "F_z": (0, 0, 255)}
        actuator = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation, plot_title="Actuator",
            table_name="./json_communication/actuator.json", columns=actuator_columns,
            refresh_rate=4000,
            left_label=["force", "N"], bottom_label=["simulation time", "s"],
            communication_mode='external_json')
        self.tab5.layout.addWidget(actuator.plt, 1, 0)
        # endregion
        # region: SIXTH TAB REAL SENSORS
        self.tab6.layout = QGridLayout(self)
        self.tab6.setLayout(self.tab6.layout)
        self.progress_bar_tab6 = QProgressBar(self)
        self.tab6.layout.addWidget(self.progress_bar_tab6, 0, 0, 1, -1)
        real_sensors_columns = {"a_x": (255, 0, 0),
                                "a_y": (0, 255, 0),
                                "a_z": (0, 0, 255)}
        real_sensors = widgets.PyQtGraph2dPlot(
            self, app=self.app, database=self.simulation, plot_title="Sensor",
            table_name="./json_communication/sensor.json", columns=real_sensors_columns,
            refresh_rate=4000, left_label=["acceleration", "m/s²"],
            bottom_label=["simulation time", "s"],
            communication_mode='external_json')
        self.tab6.layout.addWidget(real_sensors.plt, 1, 0)
        # endregion

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def start(self):
        """ Start Simulation in thread"""

        # if no yaml name was chosen, read in default.yaml
        if self.parameter_editor.yaml_name.text() == "":
            with open('default.yaml', 'r') as file:
                test = yaml.load(file, Loader=yaml.RoundTripLoader)
            self.status_sim.setText("Progress bar:")
            self.parameters = config_loader.get_config(
                self.parameter_editor.yaml_name.text())
            self.simulation.parameters = self.parameters
        # if one was chosen read it in
        else:
            try:
                with open(self.parameter_editor.yaml_name.text(), 'r') as file:
                    test = yaml.load(file, Loader=yaml.RoundTripLoader)
                self.parameters = config_loader.get_config(
                    self.parameter_editor.yaml_name.text())
                self.simulation.parameters = self.parameters

            except:
                self.parameter_editor.update_label.setText("Status: wrong file "
                                                           "name in "
                                                           "Initialization tab")
                self.parameter_editor.update_label.setStyleSheet(
                    "background-color: red")
                logging.info("no or wrong yaml name")
                return

        # start pre sim (orbit and env)
        self.sim_mode = self.chose_sim.currentText( )
        if self.chose_sim.currentText() == 'Orbit and environment simulation':
            self.thread_prep_sim = QThread()
            self.simulation.parameters = self.parameters
            self.worker_prep_sim.moveToThread(self.thread_prep_sim)

            self.parameter_editor.update_label.setText("Status: orbit and "
                                                       "environment"
                                                       "simulation started")
            self.parameter_editor.update_label.setStyleSheet(
                "background-color: green")

            self.thread_prep_sim.started.connect(
                self.worker_prep_sim.start_presimulation)
            self.worker_prep_sim.finished_prepare.connect(
                self.worker_prep_sim.disconnect)
            self.thread_prep_sim.finished.connect(
                self.thread_prep_sim.disconnect)

            self.thread_prep_sim.start()
            # disable update button
            self.parameter_editor.update_button.setDisabled(True)

        # Start sensor simulation
        if self.chose_sim.currentText() == 'Sensor simulation':
            self.thread_start_sim = QThread()
            self.worker_start_sim.moveToThread(self.thread_start_sim)
            self.thread_start_sim.started.connect(
                self.worker_start_sim.task_loop)

            self.parameter_editor.update_label.setText("Status: orbit and "
                                                       "environment "
                                                       "simulation started")
            self.parameter_editor.update_label.setStyleSheet(
                "background-color: green")

            self.worker_start_sim.finished_simulate.connect(
                self.worker_start_sim.disconnect)
            self.thread_start_sim.finished.connect(
                self.thread_start_sim.disconnect)

            self.thread_start_sim.start()

        # start full simulation
        if self.chose_sim.currentText() == 'Full simulation':
            self.thread_full_sim = QThread()
            self.worker_full_sim.moveToThread(self.thread_full_sim)
            self.thread_full_sim.started.connect(
                self.worker_full_sim.start_all)

            self.parameter_editor.update_label.setText("Status: "
                                                       "simulation started")
            self.parameter_editor.update_label.setStyleSheet(
                "background-color: green")

            self.worker_full_sim.finished_simulate.connect(
                self.worker_full_sim.disconnect)
            self.thread_full_sim.finished.connect(
                self.thread_full_sim.disconnect)
            self.thread_full_sim.start()
            # disable update button
            self.parameter_editor.update_button.setDisabled(True)




    def update_progress_bar(self):
        """ Updates the progress bar of the simulation.

        :param i: current percentage of the simulation
        """
        progress = -1
        if self.sim_mode == 'Orbit and environment simulation':
            progress = self.simulation.progress_value * 2
            if progress >= 100:
                self.start_button.setEnabled(True)
                self.parameter_editor.update_label.setText(
                    "Status: Orbit and environment simulation finished")
                self.parameter_editor.update_label.setStyleSheet(
                    "background-color: green")
                # enable update button
                self.parameter_editor.update_button.setEnabled(True)
        if self.sim_mode == 'Sensor simulation':
            progress = self.simulation.progress_value * 2 - 50 * 2
            self.start_button.setEnabled(True)
            if progress >= 100:
                self.parameter_editor.update_label.setText(
                    "Status: Sensor simulation finished")
                self.parameter_editor.update_label.setStyleSheet(
                    "background-color: green")

        if self.sim_mode == 'Full simulation':
            progress = self.simulation.progress_value
            if 50 <= progress < 100:
                self.parameter_editor.update_label.setText(
                    "Status: orbit and environment simulation finished, "
                    "sensor simulation started")
                self.parameter_editor.update_label.setStyleSheet(
                    "background-color: green")
                # enable update button
                self.parameter_editor.update_button.setEnabled(True)
            if progress >= 100:
                self.start_button.setEnabled(True)
                self.parameter_editor.update_label.setText("Status: simulation "
                                                           "finished")
                self.parameter_editor.update_label.setStyleSheet(
                    "background-color: green")
        if progress == -1:
            self.progress_bar_landing.reset()
            self.progress_bar_tab1.reset()
            self.progress_bar_tab2.reset()
            self.progress_bar_tab3.reset()
            self.progress_bar_tab4.reset()
            self.progress_bar_tab5.reset()
            self.progress_bar_tab6.reset()
        else:
            self.progress_bar_landing.setValue(int(progress))
            self.progress_bar_tab1.setValue(int(progress))
            self.progress_bar_tab2.setValue(int(progress))
            self.progress_bar_tab3.setValue(int(progress))
            self.progress_bar_tab4.setValue(int(progress))
            self.progress_bar_tab5.setValue(int(progress))
            self.progress_bar_tab6.setValue(int(progress))


if __name__ == "__main__":
    ex = App()
    ex.run()
