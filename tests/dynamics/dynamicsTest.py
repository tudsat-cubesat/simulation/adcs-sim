from time import time  # , sleep
import numpy as np
from scipy.integrate import odeint
from math import pow, degrees, radians, pi, sin, cos, sqrt
import matplotlib.pyplot as plt

from pyquaternion import Quaternion
from numpy.linalg import inv
from scipy import integrate
#from matplotlib import plot, ion, show
from matplotlib.pylab import *


def euler_to_quaternion(euler):
    """
    Convert euler angles to a quaternion with scalar first representation
    """
    # aerospace standard Euler angles sequence Z,Y,state = yaw, pitch, roll
    # 1, psi   - z (yaw,heading)
    # 2, theta - y (pitch)
    # 3, phi   - x (roll)

    # check if pitch is not in [-90, 90] deg domain
    if euler[1] >= pi / 2:
        print(">>> WARNING! Pitch is more than 90 deg. Results may not be accurate")

    if euler[1] <= -pi / 2:
        print(">>> WARNING! Pitch is less than -90 deg. Results may not be accurate")

    # angles = array([r, p, y])
    c = np.cos(euler / 2.)
    s = np.sin(euler / 2.)

    # from book: Quaternions and Rotation Sequences pg 167
    # scalar first representation
    q0 = c[0] * c[1] * c[2] + s[0] * s[1] * s[2]
    q1 = c[0] * c[1] * s[2] - s[0] * s[1] * c[2]
    q2 = c[0] * s[1] * c[2] + s[0] * c[1] * s[2]
    q3 = s[0] * c[1] * c[2] - c[0] * s[1] * s[2]

    # scalar first
    return np.array([q0, q1, q2, q3])


def quaternion_to_euler(q):
    # from book: Quaternions and Rotation Sequences pg 168
    # assumes q = [q0, q1, q2, q3] (scalar first)

    dcm = quaternion_to_dcm(q)

    psi = np.arctan2(dcm[0, 1], dcm[0, 0])  # yaw
    theta = np.arcsin(-dcm[0, 2])  # pitch
    phi = np.arctan2(dcm[1, 2], dcm[2, 2])  # roll

    return np.array([psi, theta, phi])


def quatNorm(q):
 # return normalized quaternion
    return q / np.sqrt(np.dot(q, q))



def quaternion_to_dcm(q):
    # from book: Quaternions and Rotation Sequences pg 168
    q0, q1, q2, q3 = q  # [0],q[1],q[2],q[3]

    return np.array([
        [2 * q0 ** 2 - 1 + 2 * q1 ** 2, 2 * (q1 * q2 + q0 * q3), 2 * (q1 * q3 - q0 * q2)],
        [2 * (q1 * q2 - q0 * q3), 2 * q0 ** 2 - 1 + 2 * q2 ** 2, 2 * (q2 * q3 + q0 * q1)],
        [2 * (q1 * q3 + q0 * q2), 2 * (q2 * q3 - q0 * q1), 2 * q0 ** 2 - 1 + 2 * q3 ** 2]
    ])


def state_derivative(state, t, torque, J):

    # state vector: [q0,q1,q2,q3, wx,wy,wz]
    q = state[0:4]
    w = state[4:7]  # w: angular velocity of body frame with respect to the inertial frame

    w_quat = Quaternion([0, *w])
    q_quat = Quaternion(q)
    # q_dot = 0.5 * w_quat * q_quat # in fixed reference frame
    q_dot = 0.5 * q_quat * w_quat # satellite frame
    q_dot = np.array([q_dot.scalar, *q_dot.vector]) # transform to scalar values for integrator
    print('q_dot= ', q_dot)


    # torque = np.array([0, 0, 0])
    w_dot = (-np.matmul(inv(J), np.cross(w, np.matmul(J, w))) + np.matmul(inv(J), torque))
    # print('w_dot= ', w_dot)

    state_dot = np.concatenate((q_dot, w_dot), axis=None)

    # print('state_dot= ', state_dot)
    # print('state_dot_test= ', state_dot_test)

    # return time derivative of state vector
    return state_dot

mu = 3.986004e14  # m J /kg
r_earth = 6378.14e3

n_orbits = 0.1  # number of orbits
delta_t = 1.0  # time step

# euler sequence: yaw, pitch, roll
euler_0 = np.array([100, 0, 0]) * np.pi / 180.0
w_initial = np.array([5.0, 0.0, 0.0]) * np.pi / 180.0  # w_initial: angular rate in rad/s

I_xx = 0.5448
I_yy = 2.4444
I_zz = 2.6052

torque_initial = np.array([0.0, 0.0, 0.0])


def simulate_dynamics(euler_0, delta_t, n_orbits, w_initial, I_xx, I_yy, I_zz, torque_initial):

    # convert euler angles to quaternions
    q_0 = euler_to_quaternion(euler_0) # q1,q2 and q3 are imaginary components [q0 q1 q2 q3]

    # TODO: import orbit data to compute orbital_period
    orbital_period = 5400

    stop_sim_time = n_orbits * orbital_period  # sec

    J = np.diag([I_xx, I_yy, I_zz])

    # state vector
    state_initial = np.concatenate([q_0, w_initial])
    # t  = 0:delta_t:stop_sim_time;
    time_data = np.arange(0, stop_sim_time, delta_t)

    state_data = np.zeros([len(time_data), 7])
    attitude_error_data = np.zeros([len(time_data), 3])
    torque_data = np.zeros([len(time_data), 3])

    # initial values for simulation
    state = state_initial
    torque = torque_initial

    # desired attitude
    euler_ref = np.array([0.0, 0.0, 0.0])

    error_last = np.array([0.0, 0.0, 0.0])


    # start attitude propagation simulation
    # state_data = integrate.odeint(state_derivative, state_initial, time_data, args=(torque,J))

    for i in range(time_data.size):
        # print time_data[i]
        state_i = odeint(state_derivative, state, [0, delta_t / 2., delta_t], args=(torque, J))
        state = state_i[-1]
        state_data[i, :] = state

        print('state= ', state)


        q = state[0:4]
        euler = quaternion_to_euler(q)

        # compute error
        error = euler_ref - euler
        derror = (error - error_last) / delta_t
        error_last = error

        # for symetric J = 1
        kp = 0.0
        kd = 0.0

        tx = kp * error[2] + kd * derror[2]
        ty = kp * error[1] + kd * derror[1]
        tz = kp * error[0] + kd * derror[0]

        torque = np.array([tx, ty, tz])
        torque_data[i, :] = torque

    # assign
    q_data = state_data[:, 0:4]
    w_data = state_data[:, 4:7]
    # w_data = w_data * 180 / np.pi

    # convert quaternions to euler
    euler_data = np.zeros([len(time_data), 3])
    for i in range(time_data.size):
        euler_data[i, :] = quaternion_to_euler(q_data[i, :])

    return q_data, w_data, torque_data, euler_data

simulate_dynamics(euler_0, delta_t, n_orbits, w_initial, I_xx, I_yy, I_zz, torque_initial)







# fig = plt.figure(1)  # figsize=(6.5, 9.5))
#
# # clear the figure
# fig.clf()
#
# time_data = time_data / 60
#
# plt.subplot(411)
# plt.plot(time_data, q_data)
# plt.legend(['$q_0$', '$q_1$', '$q_2$', '$q_3$'])
# plt.ylim(-1.1, 1.1)
#
# plt.subplot(412)
# plt.plot(time_data, euler_data * 180 / np.pi)
# plt.legend(['$\\psi (yaw)$', '$\\theta (pitch)$', '$\phi (roll)$'])
# plt.ylim(-200, 200)
#
# plt.subplot(413)
# plt.plot(time_data, w_data)
# legend(['$\omega_x$','$\omega_y$','$\omega_z$'])
# ylabel('$\omega$ [rad/s]')
# plt.ylim(-4, 4)
#
# plt.subplot(414)
# plt.plot(time_data, torque_data)
# plt.legend(['$T_x$', '$T_y$', '$T_z$'])
# plt.ylim(-0.200, 0.200)
#
# plt.show()
