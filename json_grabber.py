import pandas as pd
import logging


class MakeDict:
    def __init__(self, json_path: list, sampling_rate: float, name: str = "json_grabber"):
        """ Reads a bunch of json files into an internal dict of pd.dataframes

        :param json_path: list of json path strings
        :param sampling_rate: sampling rate for the main
        """
        self.name = name
        self.internal_dict = {}
        self.json_path = json_path
        self.sampling_rate = sampling_rate

    def output(self, _simulation_time):
        """ Read json into internal dict, append dict only with not known values
        """
        # for every json file in list
        for path in self.json_path:
            try:
                # read in json
                df = pd.read_json(path, orient="records", lines=True)
                # create new key if it doesn't exist yet
                self.internal_dict.setdefault(path, {})
                # if key was read previously
                if len(self.internal_dict[path]) > 0:
                    try:
                        # find index of last saved time step in new dataframe
                        index = df['simulation_time'][
                            df['simulation_time'] ==
                            self.internal_dict[path]['simulation_time'].iloc[
                                -1]].index[0]
                        # append old dataframe with new values
                        self.internal_dict[path] = self.internal_dict[path].append(
                            df.iloc[index:], ignore_index=True)
                    except Exception as e:
                        # case json is overwritten faster, than grabber can read
                        self.internal_dict[path] = self.internal_dict[path].append(
                            df, ignore_index=True)
                        logging.debug('json faster than json_grabber ' +
                                      path + "\n" + str(e))

                else:
                    # special case first read of json
                    self.internal_dict[path] = df
            except:
                logging.debug("external json not found")
