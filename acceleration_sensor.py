import utilities
from Sensor import Sensor
import datetime as dt
import numpy as np
import time
import json
import logging

# todo:
# - Nulldivision bug: Wenn state_sat nur initiale Werte mit 
#       `simulation_time: 0` hat, wird Fehler aus apply_sensitivity,
#       utilities.py line 33 ausgegeben: "ValueError: cannot convert 
#       float NaN to integer".
# - erdgravitation gleicht zentripedalkraft aus in docu

class MEMS3AxisAccelerationSensor(Sensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray,
                 database: str, mode: int, digital: bool,
                 sensor_range: list, sensitivity: float,
                 sampling_rate: float, scale_factor_error: list,
                 zero_offset: float, random_noise: list, outputs,
                 cross_axis_coupling_factor: list = [0, 0, 0],
                 vibro_pendulous_error: float = 0, active: bool = True):
        """Simulation of a MEMS-Gyroscope.

        :param name: (string) name of the sensor
        :param position: (numpy array, 1x3 x y z) position of the sensor in the
        satellite frame in m
        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the
        sensor in quaternions in the satellite frame
        :param database: (string, .db) name of the database to read/write
        from/in
        :param digital: (bool) defines if the sensor is digital or analog
        :param mode: (int) chooses between a different set of predefined
        parameter for the sensor
        :param active: (Boolean) activates or deactivates the sensor
        :param sensor_range: (list, 1x2 +range -range) maximum measurable
        angular velocity in m/s^2
        :param sensitivity: (float) sensitivity of the sensor in m/s^2
        :param sampling_rate: (float) sampling rate of the sensor in Hz
        :param scale_factor_error: (list, 1x3 x y z) scale factor error
        in %
        :param zero_offset: (float) measurement bias or zero offset in g
        :param random_noise: (float) random bias in g
        :param cross_axis_coupling_factor: (float) cross axis coupling factor
        in %
        :param vibro_pendulous_error: (float) vibro-pendulous error coefficient
        """
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)
        self.sensor_range = sensor_range
        self.sensitivity = sensitivity
        self.sampling_rate = sampling_rate
        self.scale_factor_error = scale_factor_error
        self.zero_offset = zero_offset
        self.random_noise = random_noise
        self.cross_axis_coupling_factor = cross_axis_coupling_factor
        self.vibro_pendulous_error = vibro_pendulous_error
        self.g = 9.80665

        # create table for the output of the sensor in the database,
        # if it doesn't already exist
        # utilities.create_table(self.database, self.name,
        #                        'real_time, simulation_time, a_x_1, a_y_1, a_z_1')
        self.column_names = ['real_time', 'simulation_time', 'a_x_1',
                                'a_y_1', 'a_z_1']
        # create table for the parameters of the sensor in the database,
        # if it doesn't already exist
        columns = 'real_time, simulation_time, ' + \
                  ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)
        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0,
                           *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name +
                                                 '_parameters', columns_content)

    def output(self, simulated_time):
        """ Run Sensor one time.

        Basic procedure: read in angular velocity and acceleration of satellite
        -> transform it into the sensor frame
        -> apply sensitivity -> limit to range -> write in database
        """

        # connect to database and read the angular components from dynamic
        # simulation
        self.check_parameter_update( )
        try:
            angular_components = utilities.calc_angular_motion_parameters(
                self.database)
        except:
            logging.error("no dynamic simulation table found")
            return
        # create angular velocity vector of the satellite frame
        omega_0 = np.array([angular_components[0], angular_components[1],
                            angular_components[2]])
        alpha_0 = np.array([angular_components[3], angular_components[4],
                            angular_components[5]])


        # transform angular velocity into sensor frame
        acceleration = utilities.get_acceleration_frame_transformation(
            self.position, self.orientation, omega_0, alpha_0)

        # apply the sensitivity to the output
        acceleration_x = utilities.apply_sensitivity(acceleration[0],
                                                     self.sensitivity)
        acceleration_y = utilities.apply_sensitivity(acceleration[1],
                                                     self.sensitivity)
        acceleration_z = utilities.apply_sensitivity(acceleration[2],
                                                     self.sensitivity)

        # calculate an error for a MEMS-based accelerometer
        # mathematical equation of error:
        # a_x_out = (1+S_x)*a_x + M_Y*a_y + M_z*a_z + B_f + B_v*a_x*a_y + n_x
        # with: a_x_out = sensor out, a_x a_y a_z = sensor in,
        # S_x = scale factor error, M_x M_y M_z = cross axis
        # coupling factor, B_f = measurement zero offset bias,
        # B_v = vibro pendulous error coefficient, n_x = random noise

        acceleration_x = (1 + self.scale_factor_error[0] * 100) * acceleration_x \
                         + self.cross_axis_coupling_factor[1] / 100 * acceleration_y \
                         + self.cross_axis_coupling_factor[2] / 100 * acceleration_z \
                         + self.zero_offset * self.g \
                         + self.vibro_pendulous_error * acceleration_x * acceleration_y \
                         + np.random.normal(0, self.random_noise[0], 1) * self.g
        acceleration_y = (1 + self.scale_factor_error[1] * 100) * acceleration_y \
                         + self.cross_axis_coupling_factor[0] / 100 * acceleration_x \
                         + self.cross_axis_coupling_factor[2] / 100 * acceleration_z \
                         + self.zero_offset * self.g \
                         + self.vibro_pendulous_error * acceleration_x * acceleration_y \
                         + np.random.normal(0, self.random_noise[1], 1) * self.g

        acceleration_z = (1 + self.scale_factor_error[2] * 100) * acceleration_y \
                         + self.cross_axis_coupling_factor[0] / 100 * acceleration_x \
                         + self.cross_axis_coupling_factor[1] / 100 * acceleration_y \
                         + self.zero_offset * self.g \
                         + self.vibro_pendulous_error * acceleration_x * acceleration_y \
                         + np.random.normal(0, self.random_noise[2], 1) * self.g

        # set the max sensible angular velocity
        if acceleration_x > self.sensor_range[0]:
            acceleration_x = self.sensor_range[0]
        if acceleration_x < self.sensor_range[1]:
            acceleration_x = self.sensor_range[1]
        if acceleration_y > self.sensor_range[0]:
            acceleration_y = self.sensor_range[0]
        if acceleration_y < self.sensor_range[1]:
            acceleration_y = self.sensor_range[1]
        if acceleration_z > self.sensor_range[0]:
            acceleration_z = self.sensor_range[0]
        if acceleration_z < self.sensor_range[1]:
            acceleration_z = self.sensor_range[1]

        # convert numpy number to normal float
        acceleration_x = float(acceleration_x)
        acceleration_y = float(acceleration_y)
        acceleration_z = float(acceleration_z)
        # write sensor output into the database
        if self.active is True:
            columns_content = (str(dt.datetime.now(tz=None)), simulated_time,
                               acceleration_x, acceleration_y,
                               acceleration_z)
            # utilities.write_line_into_database_table(self.database, self.name,
            #                                          columns_content)
            self.outputs.safe_output(self.name, self.column_names,
                                     list(columns_content))

    def run(self):
        """ Starts sensor in while Loop.

        """
        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output(float(current_time_stamp))
            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script to slow for the set sampling rate of the " + self.name)


if __name__ == "__main__":
    import threading
    import sqlite3
    from scipy.spatial.transform import Rotation as R

    r = R.from_matrix([[1, 0, 0],
                       [0, 1, 0],
                       [0, 0, 1]])

    a = MEMS3AxisAccelerationSensor("Acceleration_sensor", [1, 1, 1], [1, 0, 0, 0], 'acceleration_degbug.db', 0, True,
                                    [400, -400], 100, 1, [0.01, 0.01, 0.01], 1, [1, 1, 1])


    def test_write():
        i = 1
        while True:

            conn_10 = sqlite3.connect('acceleration_degbug.db')
            c_10 = conn_10.cursor( )
            try:
                c_10.execute(
                    'create table state_sat (real_time, simulation_time, q_r, q_i, q_j, q_k, w_x, w_y, w_z)')
            except:
                pass

            c_10.executemany(
                'insert into state_sat VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                [(0, i, np.sin(i * np.pi / 18),
                  np.sin(i * np.pi / 18 + np.pi / 4),
                  np.sin(i * np.pi / 18 + np.pi / 2),
                  np.sin(i * np.pi / 18 + np.pi / 2), 0, 0, 0)])
            conn_10.commit( )
            c_10.close( )
            i += 1
            time.sleep(0.02)


    t1 = threading.Thread(target=test_write, args=())
    t1.start()
    print('thread 1 aktiv')
    time.sleep(1)
    t2 = threading.Thread(target=a.run, args=())
    t2.start()
    print('thread 2 aktiv')


