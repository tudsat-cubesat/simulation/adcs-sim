import sqlite3
import pandas as pd
import numpy as np
import gyroscope

conn = sqlite3.connect('test.db')
c = conn.cursor()

# create table, if it doesn't already exist
try:
    c.execute('create table gyroscope (gyro_1, gyro_2, gyro_3)')
except:
    pass

conn.commit()

# instance sensors
gyro1 = gyroscope.GyroscopeBasic(bias_instability=0, angular_random_walk=0)
gyro2 = gyroscope.GyroscopeBasic(bias_instability=0.1, angular_random_walk=0.1)
gyro3 = gyroscope.GyroscopeBasic(bias_instability=0.2, angular_random_walk=0.2)
c.close()
i = 0

while True:
    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    i += 1

    # append table in database
    c.executemany('insert into gyroscope VALUES (?, ?, ?)',
                  [(gyro1.get_output(100 * np.sin(i / 20)), gyro2.get_output(100 * np.sin(i / 20 + 2*np.pi / 4)),
                    gyro3.get_output(100 * np.sin(i / 20 + np.pi / 4)))])

    conn.commit()
    c.close()
