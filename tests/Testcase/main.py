from pyquaternion import Quaternion
import numpy as np
import matplotlib.pyplot as plt
import configparser, plotting
# import actuators

# import perturbations, environment
from pyquaternion import Quaternion

import wmm2020 as wmm
# import utils
import numpy as np
import pymap3d as pm
from astropy import coordinates as coord, units as u
from astropy.time import Time
from sgp4.api import jday, Satrec, WGS72
from sgp4.conveniences import sat_epoch_datetime
import math, datetime

from scipy.integrate import ode
from tqdm import tqdm
import numpy as np


# import controller

################################# Utils-package ######################################


def TEME_to_geodetic(x, y, z, t):
    """
    convert coordinates(km) given in TEME frame and a time(julian day)
    to latitude(deg), longitude(deg) and height(km)
    """
    t = Time(t, format="jd")
    # convert from the ECI frame TEME to ITRS which is an ECEF frame
    teme = coord.builtin_frames.TEME(x * u.km, y * u.km, z * u.km,
                                     obstime=t, representation_type='cartesian')
    itrs = teme.transform_to(coord.ITRS(obstime=t)).cartesian
    # ITRS is the default frame for EarthLocation which enables a geodetic representation
    # the WGS84 should be used since it is also used by the magnetic model
    earth_loc = coord.EarthLocation(x=itrs.x, y=itrs.y,
                                    z=itrs.z).to_geodetic(ellipsoid="WGS84")
    lat = earth_loc.lat.degree
    lon = earth_loc.lon.degree
    height = earth_loc.height.to_value(u.km)
    return [lat, lon, height]


def ECEF_to_TEME(x, y, z, t):
    """
    convert the coordinates given in an ECEF to the TEME frame at
    the given time in julian days
    """
    t = Time(t, format="jd")
    ecef = coord.ITRS(x, y, z, obstime=t)
    teme = ecef.transform_to(coord.builtin_frames.TEME(obstime=t))
    return np.array(teme.cartesian.xyz)


def julian_to_decimal(time):
    """converts a julian day date to a decimal year"""
    t = Time(time, format="jd")
    return float(t.to_value("decimalyear"))


def julian_to_gps(time):
    """converts a julian day fractional to gps time"""
    t = Time(time, format="jd")
    return float(t.to_value("gps"))


def step_to_sensor(data, n_steps):
    """
    convert the data which is an array of dictionaries into a dictionary of arrays.
    in the array of dictionaries the array is a list of timesteps with the dictionary
    assigning each sensor a measurement.
    in the dictionary of arrays the dictionary assigns each sensor an array of measurements.
    """
    sensors = {}
    names = data[0].keys()
    for name in names:
        # intialize the arrays for each sensor
        sensors[name] = np.empty((n_steps, len(data[0][name])))
        for i in range(n_steps):
            sensors[name][i] = data[i][name]
    return sensors


def get_attitude(vector1, vector2):
    """
    returns the quaternion that rotates one vector into the other.
    (modified code from TUDSat script)
    Args:
        vector1 (numpy ndarray): first vector
        vector2 (numpy ndarray): second vector

    Returns:
        quaternion (pyquaternion.Quaternion): A quaternion that represents a rotation from first to second vector
    """

    unit_vector1 = vector1 / np.linalg.norm(vector1)
    unit_vector2 = vector2 / np.linalg.norm(vector2)

    # check if arrays are equal and if so, return a quaternion that does not rotate any vector
    if np.array_equal(unit_vector1, unit_vector2):
        quaternion = Quaternion([1., 0., 0., 0.])
    else:
        rotation_axis = np.cross(unit_vector1, unit_vector2)
        rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))

        # check if vectors are pointing into the exact opposite direction
        if np.array_equal(rotation_axis, np.zeros(3)):
            # if vectors are pointing towards opposite directions cross product is a zero vector, which is why another
            # method for finding an orthogonal vector is used
            rotation_axis = np.array([1., 1., 1.])
            rotation_axis -= rotation_axis.dot(unit_vector1) * unit_vector1
            rotation_axis /= np.linalg.norm(rotation_axis)
            rotation_angle = np.pi

        # the length of the vector represents the angle of rotation
        quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
    return quaternion


# def get_attitude(vector1, vector2):
#     """
#     returns the quaternion that rotates one vector into the other.
#     vectors are unitized after input
#     """
#     if np.array(vector1).all() == np.array(vector2).all():
#         return Quaternion([1, 0, 0, 0])
#     unit_vector1 = vector1 / np.linalg.norm(vector1)
#     unit_vector2 = vector2 / np.linalg.norm(vector2)
#     rotation_axis = np.cross(unit_vector1, unit_vector2)
#     rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))
#     # the length of the vector represents the angle of rotation
#     quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
#     return quaternion


def make_sensors(config):
    """
    returns a dictionary of sensors that are specified in the config
    """

    def get_orientation(orientation, name):
        # a dictionary connecting the names of the axes to their vectors
        vectors = {"+X": [1, 0, 0],
                   "-X": [-1, 0, 0],
                   "+Y": [0, 1, 0],
                   "-Y": [0, -1, 0],
                   "+Z": [0, 0, 1],
                   "-Z": [0, 0, -1]}
        if "," in orientation:
            vec = read_config_vector(orientation)
            return vec / np.linalg.norm(vec)
        elif orientation in vectors.keys():
            return np.array(vectors[orientation])
        else:
            raise Exception("the orientation of sensor {} is not correct".format(name))

    sensor_dict = {}
    n_steps = int(int(config["simulation"]["duration"]) \
                  / float(config["simulation"]["delta_t"])) + 1
    # magnetometers first:
    for i in range(int(config["sensors"]["n_magnetometers"])):
        # the name of the subsection that specifies each magnetometer
        identifier = "magnetometer" + str(i + 1)
        subsection = config[identifier]
        # the _mag is added to distinguish magnetometers from other sensors
        name = subsection["name"] + "_mag"
        rel_position = read_config_vector(subsection["position"])
        orientation = get_orientation(subsection["orientation"], name)
        resolution = float(subsection["resolution"])
        sensor_dict[name] = Magnetometer(name, rel_position, orientation,
                                         resolution, n_steps)

    # photodiodes:
    for i in range(int(config["sensors"]["n_photodiodes"])):
        identifier = "photodiode" + str(i + 1)
        subsection = config[identifier]
        name = subsection["name"] + "_photo"
        side = get_orientation(subsection["side"], name)
        normal = read_config_vector(subsection["normal"])
        FOV = float(subsection["FOV"])
        max_current = float(subsection["max_current"])
        noise = float(subsection["noise"])
        sensor_dict[name] = Photodiode(name, side, normal, noise,
                                       max_current, FOV, n_steps)

    # fine sun sensors
    for i in range(int(config["sensors"]["n_fineSunSensors"])):
        identifier = "fineSunSensor" + str(i + 1)
        subsection = config[identifier]
        name = subsection["name"] + "_fine"
        side = get_orientation(subsection["side"], name)
        normal = read_config_vector(subsection["normal"])
        FOV = float(subsection["FOV"])
        max_current = float(subsection["max_current"])
        noise = float(subsection["noise"])
        sensor_dict[name] = FineSunSensor(name, side, normal, noise,
                                          max_current, FOV, n_steps)

    return sensor_dict


# def make_actuators(config):
#     """
#     returns a dicitionary of actuators that are specified in the config
#     """
#     n_steps = int(int(config["simulation"]["duration"]) \
#                   / float(config["simulation"]["delta_t"])) + 1
#     actuator_dict = {}
#     for i in range(int(config["actuators"]["n_magnetorquers"])):
#         identifier = "magnetorquer" + str(i + 1)
#         subsection = config[identifier]
#         # TODO. add some sort of type attribute to sensors and actuators
#         name = subsection["name"] + "_mgt"
#         rel_position = read_config_vector(subsection["rel_position"])
#         normal = read_config_vector(subsection["normal"])
#         normal = normal / np.linalg.norm(normal)
#         area = float(subsection["area"])
#         n_windings = int(subsection["n_windings"])
#         max_current = float(subsection["max_current"])
#         noise = float(subsection["noise"])
#         actuator_dict[name] = actuators.Magnetorquer(name, rel_position, normal, area,
#                                                      n_windings, max_current, noise, n_steps)
#     return actuator_dict


def read_config_vector(string):
    """
    convert the vector in the string into a numpy array
    """
    vector = string.split(",")
    return np.array(list(map(float, vector)))


def read_config_quaternion(string):
    """
    convert the vector in the string into a quaternion
    """
    quat = string.split(",")
    return Quaternion(list(map(float, quat)))


def dipole_field(position, moment):
    """
    the magnetic field vector in nT outside a dipole given a position relative to the dipole.
    position and moment as numpy array
    """
    # vacuum permeability
    mu_0 = 1.256637062 * 1e-6
    pos_norm = np.linalg.norm(position)
    pos_unit = position / pos_norm
    field = (mu_0 / (4 * np.pi)) * ((3 * pos_unit * np.dot(moment, pos_unit) - moment) / pos_norm ** 3)
    # convert to nT
    return field * 1e9


################################### Sensors-package ######################################


class Sensor(object):

    def __init__(self, name, rel_position, orientation, noise, n_steps):
        """
        name(string): a unique name for each sensor
        rel_position(3x1): the position in the satellite in meter
        orientation(3x1): a vector describing the orientation of the sensor.
                    meaning changes slightly with each sensor
        noise(float): a measure of noise. in the same unit as the measurements
                           refer to the measure function of each sensor for more info
        n_steps(int): number of user defined integration steps
        """
        self.name = name
        self.rel_position = rel_position
        self.orientation = orientation
        self.noise = noise
        self.n_steps = n_steps

    def make_noise_cache(self, dimensions):
        """
        caching the noise values is necessary because the integrator would get confused
        otherwise. between the user defined steps it does smaller steps to ensure
        the accuracy of the results. without caching the sensors would change its
        measurement with each small step slightly which would slightly alter the
        applied control torque. this would prevent the integrator from staying
        within an acceptable error for the ode solutions.
        """
        return np.random.normal(0, self.noise, (self.n_steps, dimensions))


class Magnetometer(Sensor):

    def __init__(self, name, rel_position, x_axis, resolution, n_steps):
        """
        differences to the parent constructor:
        x_axis(3x1 array): the x_axis of the sensor in the satellite body frame
        """
        super().__init__(name, rel_position, x_axis, resolution, n_steps)
        self.noise_cache = super().make_noise_cache(3)

    def measure(self, satellite):
        """
        measure the magnetic field as seen by the sensor.
        the returned vector has some noise.
        the noise is given in nT.
        """
        # the magnetic field produced by the earth in nT
        mag_field = np.array(magnetic_field(satellite.r, satellite.real_time))
        # rotate from TEME to body frame
        mag_field = satellite.attitude.rotate(mag_field)
        # the magnetic field produced by the magnetorquers and the intrinsic magnetic moment
        # of the satellite will also influence the magnetometer measurements
        mag_field += satellite.get_intrinsic_magnetic_field(self.rel_position)
        # apply noise. gaussian around zero with a std. deviation equal to the resolution
        mag_field += self.noise_cache[int(satellite.sim_time)]
        # rotate from satellite body frame to sensor frame
        rel_attitude = get_attitude([1, 0, 0], self.orientation)
        mag_field = rel_attitude.rotate(mag_field)
        return mag_field


class Sunsensor(Sensor):
    """
       differences to the parent constructor:
       normal(3x1 array): the normal vector on the plane of the detector
       max_current(float): the maximum current created by the diode in direct
                           sun light, i.e. when normal and sun vector are equal
       FOV(float): the total field of view of the sensor in degrees,
                   i.e. between 0 and 180
       """

    def __init__(self, name, side, normal, noise, max_current, FOV, n_steps):
        super().__init__(name, side, normal, noise, n_steps)
        self.noise_cache = super().make_noise_cache(1)
        self.max_current = max_current
        self.FOV = FOV


class FineSunSensor(Sunsensor):
    def measure(self, satellite):
        def is_side_in_sun(side, sun):
            dot = np.dot(sun, side)
            if dot <= 0:
                return False
            else:
                return True

        sat_to_sun = satellite.to_sun()
        # if the satellite is in the shadow of the earth or the side on which
        # the sensor is mounted is not in the sun only noise is returned
        if satellite.is_in_shadow() or not is_side_in_sun(self.rel_position, sat_to_sun):
            return self.noise_cache[int(satellite.sim_time)]
        # the dot product of the sun vector and the sensor normal is bigger than 0
        # if the sensor can see the sun
        dot = np.dot(sat_to_sun, self.orientation)
        # the angle needs to be computed to ensure that the incident sun ray
        # is in the FOV of the sensor
        angle = np.arccos(dot) * 180 / np.pi  # angle in degrees since FOV in deg
        if dot <= 0 or angle > self.FOV / 2:
            # the sensor only measures noise, there is no sun ray coming into it
            return self.noise_cache[int(satellite.sim_time)]
        else:
            return self.max_current * dot + self.noise_cache[int(satellite.sim_time)]


class Photodiode(Sunsensor):

    def measure(self, satellite):
        """
        the intensity of the light as measured by the photodiode
        """

        def is_side_in_sun(side, sun):
            """
            whether the side is illuminated by the sun.
            assumes that the satellite is in the sun
            """
            dot = np.dot(sun, side)
            if dot <= 0:
                # print('Side NOT in sun: ', dot)
                return False
            else:
                # print('Side in sun: ', dot)
                return True

        sat_to_sun = satellite.to_sun()
        # if the satellite is in the shadow of the earth or the side on which
        # the sensor is mounted is not in the sun only noise is returned
        if satellite.is_in_shadow() or not is_side_in_sun(self.rel_position, sat_to_sun):
            return self.noise_cache[int(satellite.sim_time)]
        # the dot product of the sun vector and the sensor normal is bigger than 0
        # if the sensor can see the sun
        dot = np.dot(sat_to_sun, self.orientation)
        # the angle needs to be computed to ensure that the incident sun ray
        # is in the FOV of the sensor
        ''' euclidian norm was missing in previous version ?!?!'''
        # angle = np.arccos(dot) * 180 / np.pi  # former version (without euclidian norm)
        angle = np.arccos(dot / (np.linalg.norm(sat_to_sun) * np.linalg.norm(
            self.orientation))) * 180 / np.pi  # angle in degrees since FOV in deg
        # print('theta= ', angle)

        if dot <= 0 or angle > self.FOV / 2:
            # the sensor only measures noise, there is no sun ray coming into it

            # print('only noise= ', self.noise_cache[int(satellite.sim_time)])
            # print('noSun: theta= ', angle)

            return self.noise_cache[int(satellite.sim_time)]
        else:
            print('noise + current= ', self.max_current * dot + self.noise_cache[int(satellite.sim_time)])
            # print('In Sun: theta= ', angle)

            return self.max_current * dot + self.noise_cache[int(satellite.sim_time)]


############################## Dynamics-package #####################################


def angular_vel_ode(J, w, M_list=None):
    """
    the angular velocity ode with external torques in M
    """
    if M_list is not None:
        M_total = np.sum(M_list, axis=0)
    else:
        M_total = np.array([0, 0, 0])

    J_inv = np.linalg.inv(J)
    return (-np.matmul(J_inv, np.cross(w, np.matmul(J, w))) +
            np.matmul(J_inv, M_total))


########################### Kinematics-package #########################################


def kin_attitude_ode(attitude, w):
    """
    the kinematic attitude ode
    quat: attitude quaternion
    w: angular velocity vector (rad/s)
    returns quaternion representing attitude as numpy array
    """
    w_quat = Quaternion([0, *w])
    q_dot = 0.5 * w_quat * attitude
    return np.array(q_dot.elements)


########################## Environment-package ##################################


def magnetic_field(position, time):
    """
    returns the magnetic field vector (nT) at the given position(km, TEME frame) and time(julian day)
    refer to https://www.ngdc.noaa.gov/geomag/WMM/soft.shtml for more information
    """
    geo_pos = TEME_to_geodetic(*position, time)
    time_dec = julian_to_decimal(time)
    mag_field = wmm.wmm_point(*geo_pos, time_dec)
    # the wmm returns the coordinates in a north, east, down (NED) frame
    # they are converted to ITRS(whih is an ECEF) and then to the TEME frame
    mag_field_ECEF = pm.ned2ecef(mag_field["north"], mag_field["east"], mag_field["down"],
                                 *geo_pos, pm.Ellipsoid("wgs84"))
    mag_field_TEME = ECEF_to_TEME(*mag_field_ECEF, time)
    # since the TEME and ECEF frames refer to physical points in space above the earth
    # the magnitude of the magnetic field vector in nT gets messed up
    # the magnetic field will always have the same magnitude, independent of the frame
    mag_field_TEME = (mag_field_TEME / np.linalg.norm(mag_field_TEME)) * mag_field["total"]
    return mag_field_TEME


def atmospheric_density(altitude):
    """
    this very simple static model is a first approximation. altitude in km, density in kg/m^3
    a more precise model should be implemented in a final version
    TODO: upgrade model
    """
    # the values are taken from Fundamentals of Spacecraft Attitude Determination and Control
    # by f. Markley and John Crassidis, table D.1
    const = {"p_0": [2.418e-11, 9.158e-12, 3.725e-12, 1.585e-12, 6.967e-13, 1.454e-13, 3.614e-14],
             "h_0": [300, 350, 400, 450, 500, 600, 700],
             "H": [52.5, 56.4, 59.4, 62.2, 65.8, 79, 109]}
    if altitude < 300 or altitude > 800:
        raise Exception("the altitude is outside the range for the atmospheric model.\n" + \
                        "it is:{}, but it should be between 300 and 800".format(altitude))
    else:
        # depending on the altitude a different set of constants needs to be used
        # the altitude should be bigger than h_0 but smaller than the next value of h_0
        i = [j > altitude for j in const["h_0"]].index(True) - 1

    return const["p_0"][i] * np.exp(-(altitude - const["h_0"][i]) / const["H"][i])


def earth_to_sun(time):
    """
    returns a vector from the earth to the sun in the TEME frame in AU at the given time(julian day)
    """
    time = Time(time, format="jd")
    sun_pos_GCRS = coord.get_sun(time)
    # the vector from earth to the sun in AU in the correct frame
    sun_pos_TEME = sun_pos_GCRS.transform_to(coord.builtin_frames.TEME()).cartesian.xyz
    return sun_pos_TEME


def solar_radiation_pressure(position, time):
    """
    returns the amount of solar radiation pressure at the position. only considers light
    coming directly from the sun.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis Appendix D.3
    """
    sun_pos_TEME = earth_to_sun(time)
    position_AU = (position * u.km).to(u.AU)
    sat_to_sun_AU = np.array(sun_pos_TEME) - np.array(position_AU)
    # speed of light in m/s
    c = 299792458
    # solar constant in W/m^2, flux density of solar radiation at adistance of 1 AU from the Sun
    # can vary up to 5 W/m^2 daily which wont matter for this purpose
    solar_const = 1362
    solar_pressure = solar_const / (c * (np.linalg.norm(sat_to_sun_AU) * 1000) ** 2)
    return solar_pressure


def is_in_shadow(position, time):
    """
    returns a boolean describing whether the satellite is in the shadow
    of the earth. uses the cylindrical shadow projection.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis Appendix D.3
    """
    sun_position_unit = earth_to_sun(time) / np.linalg.norm(earth_to_sun(time))
    earth_radius = 6371  # km
    return np.dot(position, sun_position_unit) < - np.sqrt(np.linalg.norm(position) ** 2 - earth_radius ** 2)


############################ Simulation-package #################################


"""
quaternion convention of pyquaternion: scalar first entry
"""


def simulate_adcs(satellite, sgp4_sat, start_real_time, start_sim_time=0, delta_t=1, stop_sim_time=10):
    """
    the actual simulation function. sets up the propagator and records the data.
    the recorded data is returned as a dictionary.
    satellite(obj): a satellite object from this project
    sgp4_sat(obj): a satellite object for the sgp4 propagation
    start_real_time(2x1 tuple): contains the julian date and a fraction on that day
    start_sim_time(float): the start of the simulation time in seconds, relative to the real start time
    delta_t(float): time between recorded simulation steps in seconds
    stop_sim_time(float): number of seconds since the start_sim_time at which the sim will terminate


    """
    solver = ode(dervatives_func)
    solver.set_integrator(
        'lsoda',
        rtol=(1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-6, 1e-6,
              1e-6),
        atol=(1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-8, 1e-8,
              1e-8),
        nsteps=10000)
    init_state = np.array([*satellite.attitude, *satellite.w])
    solver.set_initial_value(y=init_state, t=start_sim_time)

    # setup logging
    n_steps = int((stop_sim_time - start_sim_time) / delta_t) + 1
    # simulation data logging
    times = np.empty(n_steps)
    real_times = np.empty(n_steps)
    # satellite data logging
    attitudes = np.empty((n_steps, 4))
    ang_vels = np.empty((n_steps, 3))
    positions = np.empty((n_steps, 3))  # position in km, TEME frame
    velocities = np.empty((n_steps, 3))  # velocity in km/s, TEME frame
    # sensor logging
    sensor_measurements = np.empty(n_steps, dict)
    # actuator logging
    # actuator_controls = np.empty(n_steps, dict)
    # perturbation logging
    atmospheric_torques = np.empty((n_steps, 3))
    gravity_torques = np.empty((n_steps, 3))
    solar_torques = np.empty((n_steps, 3))
    magnetic_torques = np.empty((n_steps, 3))

    # setup perturbation noise
    pert_noise = np.random.normal(0, satellite.perturbation_noise, (n_steps + 1, 3))

    # setup progress bar
    pbar = tqdm(total=n_steps)

    i = 0
    solver.set_f_params(satellite, pert_noise)
    while solver.successful() and solver.t <= stop_sim_time:
        # position and velocity calucaltions, refer to orbit.py file
        jd, frac = sim_time_to_real_time(solver.t, delta_t, sgp4_sat)
        satellite.real_time = jd + frac
        r, v = pos_vel(sgp4_sat, jd, frac)
        satellite.r = r
        satellite.v = v

        satellite.sim_time = solver.t

        # calling the controller
        # TODO: is this the best place to call the controller?
        # controller.controller(satellite.sensors, satellite.actuators, r, v,
        #                       julian_to_gps(jd + frac), satellite.desired_attitude)

        # logging
        times[i] = solver.t
        real_times[i] = jd + frac
        attitudes[i] = solver.y[0:4]
        ang_vels[i] = solver.y[4:7]
        positions[i] = r
        velocities[i] = v
        # logging perturbations, these calculations are only for the logging
        # the actual calculations that influence the satellite are in derivatives_func
        atmospheric_torques[i] = satellite.compute_aerodynamic_torque()
        gravity_torques[i] = satellite.compute_gravity_gradient_torque()
        solar_torques[i] = satellite.compute_solar_radiation_torque()
        # magnetic_torques[i] = satellite.compute_magnetic_torque()
        # logging the sensor measurements. a dictionary with key-> sensor name
        # and value-> measurement is saved at each time step
        sensor_measurements[i] = satellite.get_sensor_measurements()
        # logging actuators controls, i.e. the controlling input of the actuator
        # actuator_controls[i] = satellite.get_actuator_controls()

        # update progress bar
        pbar.update(1)
        # continue intergation
        i += 1
        solver.integrate(solver.t + delta_t)

    pbar.close()
    # convert the inconvenient data structure of the sensor measurements
    # into a dictionary keys->sensor names, values->measurments of that sensor
    sensor_measurements = step_to_sensor(sensor_measurements, n_steps)
    # actuator_controls = step_to_sensor(actuator_controls, n_steps)
    logs = {"times": times, "real_times": real_times, "attitudes": attitudes,
            "angular_velocities": ang_vels, "positions": positions, "velocities": velocities,
            "atmospheric_torque": atmospheric_torques, "gravity_torque": gravity_torques,
            "solar_torque": solar_torques,
            "random_torque": pert_noise[:n_steps],
            "sensor_measurements": sensor_measurements,
            }
    # print(logs)
    # print(sensor_measurements.keys())

    ####################### Compute sun unit vector from coarse sun sensor measurements ##############

    max_current = 100  # access from sim.config via read_config_vector function

    photo_current1 = logs['sensor_measurements']['photo1_photo']
    photo_current2 = logs['sensor_measurements']['photo2_photo']
    photo_current3 = logs['sensor_measurements']['photo3_photo']
    photo_current4 = logs['sensor_measurements']['photo4_photo']
    photo_current5 = logs['sensor_measurements']['photo5_photo']
    photo_current6 = logs['sensor_measurements']['photo6_photo']

    photo_normal1 = [1, 0, 0]
    photo_normal2 = [0, 1, 0]
    photo_normal3 = [0, 0, 1]

    a = (photo_current1 - photo_current2) * np.cross(photo_normal2, photo_normal3) + \
            (photo_current3 - photo_current4) * np.cross(photo_normal3, photo_normal1) + \
            (photo_current5 - photo_current6) * np.cross(photo_normal1, photo_normal2)

    b = max_current * np.dot(photo_normal1, np.cross(photo_normal2, photo_normal3))

    measured_sunvector = np.divide(a, b)

    print('measured sunvectors (from photodiodes) for each timestep', measured_sunvector)
    return logs
    # return np.array(float(photo_current1))



def dervatives_func(t, state, satellite, perturbation_noise):
    """
    the combination of the kinematic attitude ode and the angular velocity ode
    t: time (s)
    state: combined state conatining the attitude quaternion and the angular velocity
           state[0:4]: attitude
           state[4:7]: angular velocity
           (numpy array 7*1)
    satellite: (satellite obj)
    """
    satellite.attitude = Quaternion(state[:4]).normalised
    satellite.w = state[4:7]
    del_state = np.zeros((7,))
    del_state[0:4] = kin_attitude_ode(satellite.attitude, satellite.w)

    # pass some noise as a perturnation torque
    # noise_torque = perturbation_noise[int(t)]
    pertubation_torques = satellite.compute_pertubation_torques()
    # call the controller
    noisy_att = np.random.normal(satellite.attitude.elements)
    noisy_att = Quaternion(noisy_att).normalised
    # contr_torque = controller.simple_controller(noisy_att, satellite.desired_attitude)
    # FLAG: add all control or perturbation toruqes here
    del_state[4:7] = angular_vel_ode(satellite.J, satellite.w,
                                     [pertubation_torques])

    return del_state


############################# Pertubations-package ##################################


def aerodynamic_torque(position, velocity, altitude, attitude, side_length, C_D):
    """
    this model of aerodynamic torque assumes a 1U cubesat and models each side of the
    satellite as a flat panel with area 0.01 m^2.
    equations and model from Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis, 3.3.6.3
    """
    d = side_length
    area = d * d

    def force_per_plate(normal, area, altitude, relative_vel_B, C_D):
        """
        the force acting on each plate which is defined by the outward normal vector on that plate
        """
        inclination = np.dot(normal, relative_vel_B) / np.linalg.norm(relative_vel_B)
        rho = atmospheric_density(altitude)
        return -0.5 * rho * C_D * np.linalg.norm(relative_vel_B) * \
               relative_vel_B * area * max(inclination, 0)

    # earths angular velocity in rad/s
    w_earth = 0.000072921158553
    # relative velocity in the ECI frame, atmosphere is not stationary in ECI
    v_rel_ECI = velocity + np.cross([0, 0, w_earth], position)
    # relative velocity in the body frame
    v_rel_B = attitude.rotate(v_rel_ECI)

    # normal vectors of each side of the cube
    plate_normals = [[1, 0, 0], [0, 1, 0], [0, 0, 1], [-1, 0, 0], [0, -1, 0], [0, 0, -1]]
    # the vector from the satellite center of mass to the plate center of mass
    plate_pos = [[d, 0, 0], [0, d, 0], [0, 0, d], [-d, 0, 0], [0, -d, 0], [0, 0, -d]]
    forces = [force_per_plate(i, area, altitude, v_rel_B, C_D) for i in plate_normals]
    torques = [np.cross(plate_pos[i], forces[i]) for i in range(len(plate_pos))]
    return np.sum(torques, axis=0)


def gravity_gradient_torque(position, attitude, inertia_tensor):
    """
    the gravity gradient torque acting on the satellite. with a spherically symmetric gravity field.
    equations and model from Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis, 3.3.6.1
    """
    # gravitational constant * mass of the earth aka standard gravitational parameter of the earth
    mu = 3.986004418e14
    nadir_ECI = -1 * position / np.linalg.norm(position)
    nadir_body = attitude.rotate(nadir_ECI)
    # position now needs to be in meters
    pos_magnitude = np.linalg.norm(position) * 1000
    return (3 * mu / (pos_magnitude ** 3)) * np.cross(nadir_body, np.dot(inertia_tensor, nadir_body))


def solar_radiation_torque(position, attitude, time, R_spec, R_diff, R_abs, side_length):
    """
    the torque produced by the sunlight reflecting off of the satellite. only direct sunlight
    is considered, no albedo.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis 3.3.6.4
    """
    # if the satellite is in shadow there isnt any pertubation torque from this source
    if is_in_shadow(position, time):
        return np.array([0, 0, 0])

    d = side_length
    area = d * d
    if R_spec + R_diff + R_abs != 1:
        raise Exception("the values for the reflection coefficient do not add up to 1!")

    earth_to_sun_vector = earth_to_sun(time)
    sat_to_sun = np.array(earth_to_sun_vector) - np.array(position)
    sat_to_sun_B = attitude.rotate(sat_to_sun)
    sat_to_sun_unit_B = sat_to_sun_B / np.linalg.norm(sat_to_sun_B)

    solar_pressure = solar_radiation_pressure(position, time)

    # normal vectors of each side of the cube
    plate_normals = [[1, 0, 0], [0, 1, 0], [0, 0, 1], [-1, 0, 0], [0, -1, 0], [0, 0, -1]]
    # the vector from the satellite center of mass to the plate center of mass
    plate_pos = [[d, 0, 0], [0, d, 0], [0, 0, d], [-d, 0, 0], [0, -d, 0], [0, 0, -d]]

    def force_per_plate(normal, area, solar_press, sat_to_sun):
        normal = np.array(normal)
        sat_to_sun = np.array(sat_to_sun)
        dot = np.dot(normal, sat_to_sun)
        return -solar_press * area * (2 * (R_diff / 3 + R_spec * dot) * normal \
                                      + (1 - R_spec) * sat_to_sun) * np.max(dot, 0)

    forces = [force_per_plate(i, area, solar_pressure, sat_to_sun_unit_B) for i in plate_normals]
    torques = [np.cross(plate_pos[i], forces[i]) for i in range(len(plate_pos))]
    return np.sum(torques, axis=0)


# def magnetic_torque(position, dipole, time, attitude):
#     """
#     the torque acting on the satellite due to the magnetic field and
#     the dipole of the satellite.
#     """
#     # the WMM model returns the magnetic field in nT
#     field = np.array(magnetic_field(position, time)) * 1e-9
#     field_B = attitude.rotate(field)
#     return np.cross(dipole, field_B)

#################################### Satellite-package ###########################################

"""
Satellite module that contains the satellite class
TEME is very close to J2000 (difference only a few milliarcesonds)
"""


class Satellite(object):

    def __init__(self, J, w, dipole, side_length, attitude, desired_attitude,
                 C_D, R_spec, R_diff, R_abs, sensors,
                 perturbation_noise=0.1):
        """
        J(3x3 array): Inertia tensor
        w(3x1 array): angular velocity
        dipole(3x1 array): magnetic dipole moment
        side_length(float): length of the side of the satellite, which is assumed to be a cube
        att(Quaternion): Quaternion representation of the attitude(TEME frame to body frame)
        desired_attitude(Quaternion): the attitude that the controller will try to attain
        perturbation_noise(float): abs value for sigma of the noise
        contr_noise(float): relative value for sigma of the noise for the attitude
                            passed to the controller and the torque returned by the controller
        C_D(float): drag coefficient
        R_spec(flaot): specular reflection coefficient
        R_diff(float): diffuse reflection coefficient
        R_abs(float): absorption coefficient
        sensors(dictionary): a dictionary of all onboard sensors. key->name, value->sensor obj
        actuators(dictionary): a dictionary of all onboard actuators. key->name, value->actuator obj

        other attributes:
            real_time(float): the real time in julian days
            sim_time(float): the simulation time in seconds
            r(3x1 array): position vector in km (TEME frame)
            v(3x1 array): velocity vector in km/s (TEME frame)
            diag_J(boolean): whether the inertia tensor J is diagonal
        """
        self.real_time = 0
        self.sim_time = 0
        self.J = J
        self.w = w
        self.residual_dipole = dipole
        self.side_length = side_length
        self.r = np.empty(3)
        self.v = np.empty(3)
        self.attitude = attitude
        self.C_D = C_D
        self.R_spec = R_spec
        self.R_diff = R_diff
        self.R_abs = R_abs
        self.desired_attitude = desired_attitude
        self.sensors = sensors
        self.perturbation_noise = perturbation_noise
        self.diag_J = self.J_is_diagonal()

    def compute_aerodynamic_torque(self):
        """
        returns the torque that the satellite experiences due to the atmosphere
        """
        altitude = TEME_to_geodetic(*self.r, self.real_time)[2]
        torque = aerodynamic_torque(self.r, self.v, altitude, self.attitude,
                                    self.side_length, self.C_D)
        return torque

    def compute_gravity_gradient_torque(self):
        """
        returns the torque that the satellite experiences due to gravity and its attitude
        """
        # if the inertia tensor is a diagonal matrix there is no gravity gradient torque
        if self.diag_J:
            return np.zeros(3)
        return gravity_gradient_torque(self.r, self.attitude, self.J)

    def compute_solar_radiation_torque(self):
        """
        returns the torque that the satellite experiences due to the radiation from the sun
        """
        return solar_radiation_torque(self.r, self.attitude, self.real_time, self.R_spec,
                                      self.R_diff, self.R_abs, self.side_length)

    # def compute_magnetic_torque(self):
    #     """
    #     returns the torque on the satellite due to the interaction of the magnetic field and
    #     the magnetic dipole
    #     """
    #     return magnetic_torque(self.r, self.real_time, self.attitude)

    def compute_pertubation_torques(self):
        """
        wrapper function for all the perturbation torques. returns the total perturbation torque
        """
        aerodynamic_torque = self.compute_aerodynamic_torque()
        gravity_gradient_torque = self.compute_gravity_gradient_torque()
        solar_radiation_torque = self.compute_solar_radiation_torque()
        # magnetic_torque = self.compute_magnetic_torque()
        total = np.sum([aerodynamic_torque, gravity_gradient_torque,
                        solar_radiation_torque], axis=0)
        return total

    def get_sensor_measurements(self):
        """
        a convenience function that returns the measurements of all on board sensors at the
        current time step in a dictionary, with key -> name, value -> measurement
        """
        measurements = {}
        for name in self.sensors.keys():
            measurements[name] = self.sensors[name].measure(self)
            # print(measurements)  # print sensor measurements during simulation (print for each time step)
        return measurements


    def to_sun(self):
        """
        returns a unit vector pointing to the sun in the satellite frame.
        """
        earth_to_sun_vector = earth_to_sun(self.real_time)
        sat_to_sun = np.array(earth_to_sun_vector) - self.r
        sat_to_sun_B = self.attitude.rotate(sat_to_sun)
        print('sat to sun vector: ', sat_to_sun_B / np.linalg.norm(sat_to_sun_B))
        return sat_to_sun_B / np.linalg.norm(sat_to_sun_B)

    def is_in_shadow(self):
        return is_in_shadow(self.r, self.real_time)

    def J_is_diagonal(self):
        """
        whether the inertia tensor is a diagonal matrix aka it only has nonzero elements
        on the diagonal
        """
        return 0 == np.count_nonzero(self.J - np.diag(np.diag(self.J)))

        # def get_dipole(self):
        """
        return the total magnetic dipole moments. consist of the residual dipole of the
        satellite and the dipoles produced by the magnetorquers
        # """
        # magnetorquers = [self.actuators[name] for name in self.actuators if "mgt" in name]
        # actuator_dipoles = np.sum([i.dipole_moment(self) for i in magnetorquers], axis=0)
        # return self.residual_dipole + actuator_dipoles

    def get_intrinsic_magnetic_field(self, rel_position):
        #     """
        #     the magnetic field produced by the magnetorquers and the instrinsic magnetic field
        #     of the satellite. the sensor is assumed to be outside any of these components
        #     """
        #     # magnetorquers = [self.actuators[name] for name in self.actuators if "mgt" in name]
        total_field = 0
        #     # every magnetorquer produces a field at the relative position depending on their
        #     # distance and the moment of the torquer
        #     for mgt in magnetorquers:
        #         # the vector from the magnetorquer to relative position
        #         distance_vec = rel_position - mgt.rel_postion
        #         total_field += dipole_field(distance_vec, mgt.dipole_moment(self))
        #     # the intrinsic magnetic moment of the satellite also produces a field
        #     # the dipole for this is assumed to be at the center
        #     total_field += dipole_field(rel_position, self.residual_dipole)
        return total_field


################################ Orbit-package #######################################


def setup_orbit(config):
    """
    returns the sgp4 satellite object with orbit specified in config
    """
    sgp4_sat = Satrec()
    sgp4_sat.sgp4init(WGS72, "i",
                      666,  # satellite number
                      compute_epoch(config),  # epoch (days since 1949 December 31)
                      float(config["orbital_elements"]["b_star"]) / 6371,  # bstar (/earth_radii)
                      float(config["orbital_elements"]["n_dot"]),  # ndot (revs/day)
                      float(config["orbital_elements"]["nd_dot"]),  # nddot (revs/day^3)
                      float(config["orbital_elements"]["ecco"]),  # ecco
                      float(config["orbital_elements"]["argpo"]),  # argpo (radians)
                      float(config["orbital_elements"]["inclo"]) * math.pi / 180,  # inclo (radians)
                      float(config["orbital_elements"]["mo"]),  # mo (radians)
                      float(config["orbital_elements"]["no_kozai"]),  # no_kozai (radians/minute)
                      float(config["orbital_elements"]["nodeo"]) * math.pi / 180,  # nodeo (radians)
                      )

    return sgp4_sat


def pos_vel(sgp4_sat, jd, frac):
    """
    returns position and velocities in km and km/s in TEME frame at specified time
    """
    e, r, v = sgp4_sat.sgp4(jd, frac)
    # e is an error code
    if e != 0:
        # a dict of errors is in sgp4.api import SGP4_ERRORS
        raise Exception("something in the sgp4 propagation went wrong. check the date and orbital elements")
    return r, v


def julian_day_frac(config):
    """
    returns a tuple containing a julian day and the frac of that day
    corresponding to the point in time at start_real_time
    """
    jd, frac = jday(*time_from_config(config))
    return (jd, frac)


def compute_epoch(config):
    """
    returns the number of seconds since 1949 December 31 00:00 UT relative to
    the start time specified in the config
    """
    time = datetime.datetime(*time_from_config(config), tzinfo=datetime.timezone.utc)
    epoch_seconds = (time - datetime.datetime(1949, 12, 31, tzinfo=datetime.timezone.utc)).total_seconds()
    epoch_days = epoch_seconds / (60 * 60 * 24)
    return epoch_days


def time_from_config(config):
    """
    converts the start_time string from the config into a tuple of
    year, month, day, hour, minute and seconds
    """
    real_time = config["simulation"]["start_time"]
    year, mon, day = map(int, real_time.split("T")[0].split("-"))
    hr, minute, sec = map(int, real_time.split("T")[1].split(":"))
    return (year, mon, day, hr, minute, sec)


def sim_time_to_real_time(t, delta_t, sgp4_sat):
    """
    returns the real time in julian date, frac that corresponds to the simulation time t
    """
    dt_curr_time = sat_epoch_datetime(sgp4_sat)
    dt_delta_t = datetime.timedelta(seconds=delta_t + t)
    dt_next_time = dt_curr_time + dt_delta_t
    jd, frac = jday(dt_next_time.year, dt_next_time.month, dt_next_time.day,
                    dt_next_time.hour, dt_next_time.minute, dt_next_time.second)
    return jd, frac


def main(config_file):
    config = configparser.ConfigParser(empty_lines_in_values=False)
    config.read(config_file)

    sgp4_sat = setup_orbit(config)
    # a tuple containing the julian day and the fraction of that day 
    # corresponding to the real time
    start_real_time = julian_day_frac(config)

    if config["satellite"]["inital_attitude"] == "random":
        initial_attitude = Quaternion.random()
        print('initial attitude:', initial_attitude)
    else:
        initial_attitude = read_config_quaternion(config["satellite"]["inital_attitude"])
        print('initial attitude:', initial_attitude)

    desired_attitude = read_config_quaternion(config["satellite"]["desired_attitude"])

    omega = read_config_vector(config["satellite"]["omega"])

    # constructing the inertia tensor
    inertia_row_1 = read_config_vector(config["satellite"]["inertia_row_1"])
    inertia_row_2 = read_config_vector(config["satellite"]["inertia_row_2"])
    inertia_row_3 = read_config_vector(config["satellite"]["inertia_row_3"])
    inertia_tensor = np.array([inertia_row_1, inertia_row_2, inertia_row_3])

    residual_dipole = read_config_vector(config["satellite"]["dipole"])

    # constructing the sensors
    sensors = make_sensors(config)
    # constructing the actuators
    # actuators = make_actuators(config)

    # constructing the satellite
    sat = Satellite(inertia_tensor,
                    omega,
                    residual_dipole,
                    float(config["satellite"]["side_length"]),
                    initial_attitude,
                    desired_attitude,
                    float(config["satellite"]["C_D"]),
                    float(config["satellite"]["R_spec"]),
                    float(config["satellite"]["R_diff"]),
                    float(config["satellite"]["R_abs"]),
                    sensors,
                    perturbation_noise=float(config["simulation"]["perturbation_noise"]))
    logs = simulate_adcs(sat, sgp4_sat, start_real_time,
                         stop_sim_time=int(config["simulation"]["duration"]),
                         delta_t=float(config["simulation"]["delta_t"]))

    f = open("logFile-Test.txt", "w")  # use "a" instead of "w" to append
    print(logs, file=f)
    f.close()

    if bool(int(config["simulation"]["plotting"])):
        plotting.make_plots(config, logs)


if __name__ == "__main__":
    main("sim.conf")

    # # test to print out some attributes and log-files
    # if bool(int(config["plotting"]["photodiode"])):
    #     for name in logs["sensor_measurements"].keys():
    #         if "photo" in name:
    #             print(logs)
