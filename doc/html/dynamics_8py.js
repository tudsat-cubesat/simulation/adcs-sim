var dynamics_8py =
[
    [ "dynamics.Dynamics", "classdynamics_1_1_dynamics.html", "classdynamics_1_1_dynamics" ],
    [ "delta_t", "dynamics_8py.html#a05c4303e190f991d6c4064bc280fa1b1", null ],
    [ "dynamics", "dynamics_8py.html#a04f5df96bc463af8f42a206f5bbec1b0", null ],
    [ "euler_init", "dynamics_8py.html#a699ef2e10342fa3093767c8aaa1f22e1", null ],
    [ "freq", "dynamics_8py.html#addcd2690b8e1895c0a9e32d99ca3b37f", null ],
    [ "I_xx", "dynamics_8py.html#a531f5e21ad26aa5e34d8540ea547574b", null ],
    [ "I_yy", "dynamics_8py.html#ac982197dac5e8787c0a7e54187fc005b", null ],
    [ "I_zz", "dynamics_8py.html#a3112f267b01a76c4cde843d65519fe39", null ],
    [ "level", "dynamics_8py.html#a27f0ccb48966de89950481c9fb376eac", null ],
    [ "mu", "dynamics_8py.html#a92c52a7ded9d56b5126bc8fd92cf5c1d", null ],
    [ "n_orbits", "dynamics_8py.html#a2119438381e64806742211c8eb4ded15", null ],
    [ "r_earth", "dynamics_8py.html#aa01bbf12f7a07e5e529dffe10581b913", null ],
    [ "torque_initial", "dynamics_8py.html#a0a7c49364360609636ecf400d8e393ca", null ],
    [ "w_initial", "dynamics_8py.html#a45b1072784cacc71dc1f5500357c8fbd", null ]
];