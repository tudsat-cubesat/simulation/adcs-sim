import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sqlite3
import utilities


def plot_all(database):
    """Plot all tables from database.

    :param database: name of database
    """
    # connect to database and create list with table names in it
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = c.fetchall()
    c.close()
    # creates a list with the table names as content
    sensor_table_names = []
    # tuples to list
    for table_name in tables:
        sensor_table_names.append(table_name[0])
    # removes parameter tables
    for table_name in sensor_table_names:
        if '_parameters' in table_name:
            sensor_table_names.remove(table_name)
    # plot tables
    for table_name in sensor_table_names:
        df = utilities.get_table_from_database(database, table_name)
        x = []
        y = []
        # name figure
        plt.figure(table_name)

        x = df['simulation_time']
        # delete simulation time and real time column
        df = df.drop(['simulation_time'], 1)
        df = df.drop(['real_time'], 1)

        for column in df:
            plt.plot(x, df[column])
        plt.legend(df)

    plt.show()
