var annotated_dup =
[
    [ "acceleration_sensor", "namespaceacceleration__sensor.html", [
      [ "MEMS3AxisAccelerationSensor", "classacceleration__sensor_1_1_m_e_m_s3_axis_acceleration_sensor.html", "classacceleration__sensor_1_1_m_e_m_s3_axis_acceleration_sensor" ]
    ] ],
    [ "config_loader", "namespaceconfig__loader.html", [
      [ "bcolors", "classconfig__loader_1_1bcolors.html", null ]
    ] ],
    [ "dynamics", "namespacedynamics.html", [
      [ "Dynamics", "classdynamics_1_1_dynamics.html", "classdynamics_1_1_dynamics" ]
    ] ],
    [ "earth_sensor", "namespaceearth__sensor.html", [
      [ "EarthSensor", "classearth__sensor_1_1_earth_sensor.html", "classearth__sensor_1_1_earth_sensor" ]
    ] ],
    [ "environment", "namespaceenvironment.html", [
      [ "Environment", "classenvironment_1_1_environment.html", "classenvironment_1_1_environment" ]
    ] ],
    [ "gyroscope", "namespacegyroscope.html", [
      [ "MEMS3AxisGyroscope", "classgyroscope_1_1_m_e_m_s3_axis_gyroscope.html", "classgyroscope_1_1_m_e_m_s3_axis_gyroscope" ]
    ] ],
    [ "internal_transfer", "namespaceinternal__transfer.html", [
      [ "OutputLists", "classinternal__transfer_1_1_output_lists.html", "classinternal__transfer_1_1_output_lists" ]
    ] ],
    [ "json_grabber", "namespacejson__grabber.html", [
      [ "MakeDict", "classjson__grabber_1_1_make_dict.html", "classjson__grabber_1_1_make_dict" ]
    ] ],
    [ "magnetometer", "namespacemagnetometer.html", [
      [ "FluxgateMagnetometer", "classmagnetometer_1_1_fluxgate_magnetometer.html", "classmagnetometer_1_1_fluxgate_magnetometer" ],
      [ "Magnetometer", "classmagnetometer_1_1_magnetometer.html", null ]
    ] ],
    [ "main", "namespacemain.html", [
      [ "bcolors", "classmain_1_1bcolors.html", null ],
      [ "Main", "classmain_1_1_main.html", "classmain_1_1_main" ],
      [ "task_object", "classmain_1_1task__object.html", "classmain_1_1task__object" ]
    ] ],
    [ "orbit", "namespaceorbit.html", [
      [ "Orbit", "classorbit_1_1_orbit.html", "classorbit_1_1_orbit" ]
    ] ],
    [ "Sensor", "namespace_sensor.html", [
      [ "Sensor", "class_sensor_1_1_sensor.html", "class_sensor_1_1_sensor" ]
    ] ],
    [ "sunsensor", "namespacesunsensor.html", [
      [ "FineSunSensor", "classsunsensor_1_1_fine_sun_sensor.html", "classsunsensor_1_1_fine_sun_sensor" ],
      [ "Photodiode", "classsunsensor_1_1_photodiode.html", "classsunsensor_1_1_photodiode" ],
      [ "Sunsensor", "classsunsensor_1_1_sunsensor.html", "classsunsensor_1_1_sunsensor" ]
    ] ],
    [ "view", "namespaceview.html", [
      [ "App", "classview_1_1_app.html", "classview_1_1_app" ],
      [ "Tabs", "classview_1_1_tabs.html", "classview_1_1_tabs" ]
    ] ],
    [ "widgets", "namespacewidgets.html", [
      [ "App", "classwidgets_1_1_app.html", "classwidgets_1_1_app" ],
      [ "MyTableWidget", "classwidgets_1_1_my_table_widget.html", "classwidgets_1_1_my_table_widget" ],
      [ "PyQtGraph2dPlot", "classwidgets_1_1_py_qt_graph2d_plot.html", "classwidgets_1_1_py_qt_graph2d_plot" ],
      [ "YamlEditorWindow", "classwidgets_1_1_yaml_editor_window.html", "classwidgets_1_1_yaml_editor_window" ]
    ] ]
];