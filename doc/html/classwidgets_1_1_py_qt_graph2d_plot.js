var classwidgets_1_1_py_qt_graph2d_plot =
[
    [ "__init__", "classwidgets_1_1_py_qt_graph2d_plot.html#a5689718c32ce4a9ab3dc7fe0f3446227", null ],
    [ "update_plots", "classwidgets_1_1_py_qt_graph2d_plot.html#a67db33888b8bb15cd9f595a7e9aaa8d9", null ],
    [ "app", "classwidgets_1_1_py_qt_graph2d_plot.html#a7b86adca03919a1885e656fe2291bbfb", null ],
    [ "bottom_label", "classwidgets_1_1_py_qt_graph2d_plot.html#aefacfe42a0acb4dd9bd65258839f3067", null ],
    [ "columns", "classwidgets_1_1_py_qt_graph2d_plot.html#a42d9a1e15029aa9f9ca1885282a9b371", null ],
    [ "communication_mode", "classwidgets_1_1_py_qt_graph2d_plot.html#aa9ce860be6c77c8e7c95a9c71b674a44", null ],
    [ "curve", "classwidgets_1_1_py_qt_graph2d_plot.html#af71a1b4019dbc73752d9b038d21fc83b", null ],
    [ "database", "classwidgets_1_1_py_qt_graph2d_plot.html#a1a1d6ca064179aae69813e0a845966f8", null ],
    [ "left_label", "classwidgets_1_1_py_qt_graph2d_plot.html#a1f36f49de38ed2b7357619c286fcb253", null ],
    [ "plot_title", "classwidgets_1_1_py_qt_graph2d_plot.html#a9266aa703ac7dceede26839110f94592", null ],
    [ "plt", "classwidgets_1_1_py_qt_graph2d_plot.html#a0c026882c8477b6a7226a11e8b74cc03", null ],
    [ "refresh_rate", "classwidgets_1_1_py_qt_graph2d_plot.html#a7fbcbc7ef9b646db5f152e262ce050c9", null ],
    [ "table_name", "classwidgets_1_1_py_qt_graph2d_plot.html#a511efff31e70d1a1c7c21c4af265748a", null ],
    [ "timer", "classwidgets_1_1_py_qt_graph2d_plot.html#a67b3c533dfe0990a5e6dba3d28efe29f", null ],
    [ "x", "classwidgets_1_1_py_qt_graph2d_plot.html#a42c89a251a1697b16f9e8108fca41110", null ],
    [ "y", "classwidgets_1_1_py_qt_graph2d_plot.html#a7e68a16d944d38b4382ed7e50c328950", null ]
];