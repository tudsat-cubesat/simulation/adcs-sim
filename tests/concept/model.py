import sqlite3
import datetime
import time
import numpy as np


class SinusModel:

    def __init__(self, stop=False):
        self.stop = stop

    def set_stop(self, parameter):
        self.stop = parameter

    def start(self):
        i = 0
        # verbindet mit Datenbank und erstellt einen Cursor
        conn = sqlite3.connect('concepttest.db')
        c = conn.cursor()
        # erstellt eine Tabelle, falls diese noch nicht existiert
        try:
            c.execute('create table model (time, sin)')
        except:
            pass
        # commited zur Datenbank
        conn.commit()
        c.close()
        # while schleife, die die ganze Zeit eine Sinuswelle in die Datenbank schreibt
        while True:
            sin = 10*np.sin(np.pi * i)
            i += 1
            conn = sqlite3.connect('concepttest.db')
            c = conn.cursor()
            # executemany funktioniert ähnlich wie ein append bei einer Liste
            c.executemany('insert into model VALUES (?, ?)',
                          [(str(datetime.datetime.now(tz=None)), sin)])

            conn.commit()
            c.close()
            time.sleep(0.01)
            # sollte die schleife stoppen, hab ich aber noch nicht getestet
            if self.stop is True:
                print("Thread stopped")
                break
