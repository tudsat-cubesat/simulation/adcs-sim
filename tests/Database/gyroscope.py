"""
Created on on Mai 25 10:38:08 2021

@author: Thomas Buechner
"""

import numpy as np
import matplotlib.pyplot as plt
import random as rd


# todo: slow drift, alle inputs verwenden

class GyroscopeBasic:
    """ Simulation of gyroscope error """

    def __init__(self, range=0, sensitivity=0.0, bias_instability=0.0, angular_random_walk=0.0,
                 temperature_drift=0.0, name="sensor"):
        self.name = name
        self.range = range
        self.temperature_drift = temperature_drift
        self.sensitivity = sensitivity
        self.bias_instability = bias_instability
        self.angular_random_walk = angular_random_walk

    def get_output(self, angular_rate):
        """Adding the error to an input"""

        output = angular_rate + rd.gauss(0, self.angular_random_walk) \
                 + rd.gauss(0, self.bias_instability)

        return output


if __name__ == "__main__":
    # instantiate gyroscope
    x = GyroscopeBasic(bias_instability=0.3, angular_random_walk=0.3)
    i = 0
    j = 0
    l = 0
    plot = [[0],[0]]

    while True:
        # multiple of pi
        i += np.pi
        # get output of simulated sensor
        m = 10 * np.sin(i / 400)
        k = x.get_output(m)
        # run variable for plot
        j += 1
        plot[0].append(j)
        plot[1].append(k)
        plt.xlim([j - 100, j])
        plt.ylim([m - 4, m + 4])
        plt.plot(plot[0], plot[1])
        # pause simulation so the graph can catch up
        plt.pause(0.005)
