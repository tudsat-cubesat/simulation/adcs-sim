import time
import utilities
import pandas as pd
import sqlite3
import logging
import os



class OutputLists():
    def __init__(self, database_presim, database_sim, orbit_env=False):
        """ Save all output data into lists.

        :param database_presim: name of the environment and orbit simulation
        database
        :param database_sim: name of the sensor+dynamic simulation output
        database
        """
        self.database_presim = database_presim
        self.database_sim = database_sim
        self.orbit_env = orbit_env
        if orbit_env is False:
            # load data form database_presim/orbit
            self.satellite_position = utilities.get_table_from_database(
                database_presim, 'position')
            self.satellite_velocity = utilities.get_table_from_database(
                database_presim, 'velocity')

            # load data from database_presim/environment
            self.radius_ellipsoid = utilities.get_table_from_database(
                database_presim, 'radius_ellipsoid')
            self.magnetic_field_strength = utilities.get_table_from_database(
                database_presim, 'magnetic_field_strength')
            self.earth_to_sun = utilities.get_table_from_database(
                database_presim, 'earth_to_sun')
            self.eclipse = utilities.get_table_from_database(
                database_presim, 'eclipse')
        # initialise output list
        # key is name of table in database and value in pd.dataframe
        self.output_dict = {}

    def write_into_sensor_database(self):
        """ Write the sensor_output_list into the database_sim """
        # try:
        with sqlite3.connect(self.database_sim) as dbcon:
            for key in self.output_dict:
                # col_list = self.sensor_output_dict[key].columns
                # col_names = ', '.join(col_list)
                col_content = self.output_dict[key]
                col_content.to_sql(name=key, con=dbcon, if_exists='append')
        # except:
        #     logging.error("couldn't commit sensor output to database")

    def safe_output(self, name: str, column_names: list, column_content: list,
                    json_length: int=1):
        """ Writes output into dict as pandas frame and creates/overwrites json
            output file

        :param name: name of the sensor, also key of the dict and name+".json"
                     of the output file
        :param column_names: list of column names as string
        :param column_content: list of column content
        :param json_length: rows of the json file
        """
        data_dict = {}
        # appends pandas frame in dict if key already exists
        if name in self.output_dict:
            for counter, column_name in enumerate(column_names):
                data_dict[column_name] = [column_content[counter]]
            data_dict = pd.DataFrame(data=data_dict)
            self.output_dict[name] = \
                self.output_dict[name].append(
                    data_dict, ignore_index=True)
        # creates new pandas frame in dict if key doesn't exist
        else:
            for counter, column_name in enumerate(column_names):
                data_dict[column_name] = [column_content[counter]]
            data_dict = pd.DataFrame(data=data_dict)
            self.output_dict[name] = data_dict
        if self.orbit_env is False:
            # creates/overwrites json output file with 1 line
            if json_length == 1:
                data_dict.to_json('json_communication/'+name+'.json',
                                  orient='records', lines=True)
            # creates/overwrites json output file with more than 1 line
            elif json_length > 1:
                data_dict = self.output_dict.get(name).iloc[-json_length:]
                data_dict.to_json('json_communication/'+name+'.json',
                                  orient='records', lines=True)
