import numpy as np
import pandas as pd
import utilities
import sqlite3
import datetime as dt
import json
from ast import literal_eval
import time
from uuid import uuid4

class Sensor(object):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, digital: bool,
                 mode: int, outputs, active: bool = True):
        """Parameters and functions, that all sensors share

        :param name: (string) name of the sensor
        :param position: (numpy array, 3x1 x y z) position of the sensor in the satellite frame in m
        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the sensor in quaternions in the
                                                                                                    satellite frame
        :param database: (string, .db) name of the database to read/write from/in
        :param digital: (bool) defines if the sensor is digital or analog
        :param mode: (int) chooses between a different set of predefined parameter for the sensor
        :param active: (Boolean) activates or deactivates the sensor
        """
        self.name = name
        self.position = position
        self.orientation = orientation
        self.database = database
        self.active = active
        self.digital = digital
        self.mode = mode
        self.parameter_count = 1
        self.outputs = outputs


    def check_parameter_update(self):
        """Check for updates in the sensors parameter list. When the user has entered a change of parameters, this
        change is written into the parameters table of the respective sensor. This method then compares its parameter
        count value with the amount of rows that re present in the parameter table. If the row count exceeds the
        parameter count value the newest row is read and parameter changes will be done.

        """
        # connect to database and count the number of rows
        con = sqlite3.connect(self.database)
        count = con.cursor().execute('SELECT COUNT(*) FROM ' + self.name + '_parameters;').fetchone()[0]

        # if the row count exceeds the parameter count, that is initially set to 1, start procedure, else pass
        if count > self.parameter_count:
            # read table into df and convert last row of df into dictionary
            df = pd.read_sql_query('SELECT * FROM ' + self.name + '_parameters', con)
            con.close()
            new_line = df[df.columns[2:]].to_dict('records')[-1]
            # renew all parameters as indicated in the new row within sensor's parameter table
            for key in new_line:
                # it has to be checked if attribute type is numpy array, since literal_eval cannot handle them
                if key != 'outputs':
                    if type(self.__dict__[key]) == np.ndarray:
                        setattr(self, key, np.array(literal_eval(new_line[key].replace(' ', ',').replace(',,,', ',')
                                                                 .replace(',,', ',').replace(',]', ']')
                                                                 .replace('[,', '['))))
                    elif type(self.__dict__[key]) == str:
                        setattr(self, key, new_line[key])
                    elif type(self.__dict__[key]) == float:
                        setattr(self, key, float(new_line[key]))
                    elif type(self.__dict__[key]) == int:
                        setattr(self, key, int(new_line[key]))
                    elif type(self.__dict__[key]) == bool:
                        setattr(self, key, bool(new_line[key]))
                    else:
                        setattr(self, key, literal_eval(new_line[key]))

            # increase parameter count up to current row count
            self.parameter_count = count

        else:
            con.close()