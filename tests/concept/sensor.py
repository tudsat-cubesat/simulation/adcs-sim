import sqlite3
import datetime
import time
import numpy as np
import random as rd
import pandas as pd


class ErrorSensor:

    def __init__(self, name, error, sample_rate, stop=False):
        self.name = name
        self.error = error
        self.sample_rate = sample_rate
        self.stop = stop

    def set_stop(self, parameter):
        self.stop = parameter

    def start(self):
        # verbindet mit Datenbank und erstellt einen Cursor
        conn = sqlite3.connect('concepttest.db')
        c = conn.cursor()
        # erstellt eine Tabelle, falls diese noch nicht existiert
        try:
            c.execute('create table '+ self.name+' (time, out)')
        except:
            pass
        # commited zur Datenbank
        conn.commit()
        c.close()
        # while schleife, die die ganze Zeit eine Sinuswelle in die Datenbank schreibt
        while True:
            conn = sqlite3.connect("concepttest.db")
            c = conn.cursor()
            # liest werte aus dem model ein
            df = pd.read_sql_query("SELECT * FROM model", conn)
            out = rd.gauss(float(df["sin"].tail(1)), self.error)



            # executemany funktioniert ähnlich wie ein append bei einer Liste
            c.executemany('insert into '+ self.name+' VALUES (?, ?)',
                          [(str(datetime.datetime.now(tz=None)), out)])

            conn.commit()
            c.close()
            time.sleep(self.sample_rate)
            if self.stop is True:
                print(self.name+"stopped")
                break

if __name__ == "__main__":
    x = ErrorSensor('sensor1', 0.3, 0.5)
    x.start()