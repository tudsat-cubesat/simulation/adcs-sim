var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwxyz",
  1: "abdefmopsty",
  2: "acdefgijmosuvw",
  3: "acdefgijmorsuvw",
  4: "_acdefgijmnopqrstuw",
  5: "abcdefghijlmnoprstuvwxyz",
  6: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

