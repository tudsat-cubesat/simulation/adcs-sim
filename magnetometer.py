import numpy as np
import random
import time
import datetime as dt

import pandas as pd
from pyquaternion import Quaternion
import utilities
import logging
from Sensor import Sensor


class Magnetometer(Sensor):
    pass


class FluxgateMagnetometer(Sensor):
    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, mode: int, outputs,
                 resolution: int, sensor_range: float, analog_range: float, noise_density: float, bandwidth: float,
                 sampling_rate: float, temp_drift: tuple, max_zero_error: float = 0., max_scaling_error: float = 0.,
                 max_orthogonality_error: float = 0., use_max_errors: bool = False, active: bool = True,
                 digital: bool = False, dimension: int = 3):
        """
        Constructor for the class magnetometer:

        :param name: (str) a unique name for each sensor
        :param position: (numpy array, 3x1 x y z) position of the sensor in the satellite frame in m
        :param orientation: (numpy array, 1x4 qi qj qk qr) orientation of the sensor in quaternions in the
                                                                                                    satellite frame
        :param database: (string, .db) name of the database to read/write from/in
        :param mode: (int) chooses between a different set of predefined parameter for the sensor
        :param resolution: (int) the resolution in bit if the sensor is digital
        :param sensor_range: (float) maximum detectable field strength in nT
        :param analog_range: (float) maximum output voltage of the sensor in V
        :param noise_density: (float) spectral noise density in pTrms/sqrt(Hz)
        :param bandwidth: (float) the magnetometers bandwidth in kHz
        :param sampling_rate: (float) sampling rate of the sensor in Hz
        :param temp_drift: (tuple) A list that contains the temperature drift in nT/K as indicated in the datasheet
                            and the temperature at which the drift is zero in K
        :param max_zero_error: (float) the zero error in nT as indicated in the sensor's datasheet
        :param max_scaling_error: (float) the scaling error in % as indicated in the sensor's datasheet
        :param max_orthogonality_error: (float) the orthogonal error in degrees as indicated in the sensor's datasheet
        :param use_max_errors: (bool) if True, sensor is initialized with the maximum errors, if False errors are
                            set randomly within a range that is defined by max values
        :param active: (bool) activates or deactivates the sensor
        :param digital: (bool) defines if the sensor is digital or analog
        """
        super().__init__(name, position, orientation, database, digital, mode,
                         outputs, active)
        self.resolution = resolution
        self.sensor_range = sensor_range
        self.analog_range = analog_range
        self.noise_density = noise_density
        self.bandwidth = bandwidth
        self.sampling_rate = sampling_rate
        self.temp_drift = temp_drift
        self.use_max_errors = use_max_errors
        self.zero_error = self.set_error(max_zero_error, 3)
        self.scaling_error = self.set_error(max_scaling_error / 100, 3)
        self.orthogonality_error = self.set_error_matrix(max_orthogonality_error, 3)
        self.dimension = dimension

        # create table for the output of the sensor in the database, if it doesn't already exist
        if self.dimension == 3:
            value_columns = 'real_time, simulation_time, M_x, M_y, M_z'
        else:
            value_columns = 'real_time, simulation_time, M'

        # utilities.create_table(self.database, self.name, value_columns)
        self.column_names = value_columns.split(", ")

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        columns = 'real_time, simulation_time, ' + ', '.join(self.__dict__.keys())
        utilities.create_table(self.database, self.name + '_parameters', columns)

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, *[str(self.__dict__[i]) for i in self.__dict__])
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def set_error(self, max_error: float, dimension: int):
        """Set an error (dimension x 1) array with random values reaching from -max_error to +max_error.

        :param max_error: (float) the symmetrical maximum error that determines an error range
        :param dimension: (int) dimension of the output array that contains individual error values for each dimension
        :return: (dimension x 1 np.array) an error array that contains individual error values for each dimension
        """
        error = np.zeros(dimension)
        if self.use_max_errors:
            for direction in range(dimension):
                error[direction] = max_error
        else:
            for direction in range(dimension):
                error[direction] = random.uniform(-abs(max_error), abs(max_error))
        return error

    def set_error_matrix(self, max_error: float, dimension: int = 3):
        """Set an orthogonality error matrix with a maximum error that determines the error range. Setting max_error to 0
        would result in returning the identity matrix

        :param max_error: (float) the symmetrical maximum error that determines an error range
        :param dimension: (int) dimension of the symmetrical output matrix
        :return: (dimensions x dimensions np.array) an error matrix with individual error values for each entry of the matrix
        """
        error = (np.ones((dimension, dimension)) - np.eye(dimension)) * np.pi / 2
        for row in range(dimension):
            for col in range(dimension):
                if self.use_max_errors:
                    error[row, col] += max_error * (np.pi / 180)
                else:
                    error[row, col] += random.uniform(-abs(max_error), abs(max_error)) * (np.pi / 180)

        error = np.cos(error)
        return error

    def get_noise(self):
        """Returns a normally distributed random noise where standard deviation is calculated with sensor's
        noise density and bandwidth.

        :return: 3x1 array with normally distributed noise according to sensor's noise density and bandwidth
        """
        # A 1/f noise is assumed with the indicated noise density being assumed as the density @1Hz
        # the cutoff frequency for pink noise is assumed to be @1 Hz
        # the slope of the density is assumed to be -20 db/decade reaching back to 0.01 Hz
        white_noise = self.noise_density * np.sqrt(1.57 * 1000 * self.bandwidth)
        pink_noise = self.noise_density * np.sqrt(np.log(100))
        std = np.sqrt(white_noise ** 2 + pink_noise ** 2)
        noise = np.zeros(3)
        for direction in range(3):
            noise[direction] = np.random.normal(0, std, 1) / 1000.

        return noise

    def output(self, sim_time: float, temperature: float = 293.15):
        """Gets the local magnetic field, converts it to the body frame and applies errors and noise according to
        respective datasheets (if data is available, if not assumptions have to be made) and returns the magnetic field
        as seen from the sensor.

        :param sim_time: (float) current simulation time an output of the magnetometer is required
        :param temperature: (float) the current temperature of the sensor
        :return: a noisy magnetic field as seen by the sensor in nT
        """
        # check for updates in the parameters
        self.check_parameter_update()

        if self.active:
            try:
                # connect to database and read the magnetic field strength of the satellite
                # df = utilities.interpolate_two_rows(self.database, "magnetic_field_strength", sim_time)
                df = utilities.interpolate_dataframe(
                    self.outputs.magnetic_field_strength, sim_time)
                magnetic_field = df.to_numpy()[1:].reshape(3)
                logging.debug(magnetic_field)

                # Convert TEME coordinates to CubeSat Coordinates
                # df = utilities.interpolate_two_rows(self.database, "state_sat", sim_time)
                df = utilities.interpolate_dataframe(pd.read_json('json_communication/state_sat.json', orient='records',
                      lines=True), sim_time)
                sat_attitude = df.to_numpy()[1:5].reshape(4)
                magnetic_field_sat = Quaternion(sat_attitude).inverse.rotate(magnetic_field)
                logging.debug(magnetic_field_sat)

            except:
                logging.warning("Could not find required input from either magnetic_field_strength table or state_sat "
                                "table. \n Make sure a suitable input is written into these tables before calling "
                                "Magnetometer's output method.")

                return

            # rotate magnetic field to sensor's CS
            magnetic_field_sensor = Quaternion(self.orientation).inverse.rotate(magnetic_field_sat)
            logging.debug(magnetic_field_sensor)

            # Linearity error and orthogonality error are expressed in matrix form while zero error is 3x1 array
            # Temperature drift only effects offset so far,
            # datasheets often do not say if other errors are effected as well

            noisy_magnetic_field = np.matmul((np.diag(self.scaling_error) + self.orthogonality_error),
                                             magnetic_field_sensor) \
                                   + self.zero_error + self.temp_drift[0] * (temperature - self.temp_drift[1]) \
                                   + self.get_noise()
            logging.debug(noisy_magnetic_field)

            # sensor range is limited
            for direction in range(3):
                # sensor has a maximal output that cannot be exceeded
                if self.sensor_range < np.abs(noisy_magnetic_field)[direction]:
                    noisy_magnetic_field[direction] = self.sensor_range * np.sign(noisy_magnetic_field)[
                        direction] + self.get_noise()[direction]
                else:
                    pass

            # return only component that is seen from the sensor
            if self.dimension != 3:
                output = noisy_magnetic_field[0]
                if not self.digital:
                    output = output * (self.analog_range / self.sensor_range)
                else:
                    output = utilities.apply_sensitivity(output, 2 * self.sensor_range / (2 ** self.resolution))

                columns_content = (str(dt.datetime.now(tz=None)), sim_time, output)

            else:
                output = noisy_magnetic_field
                if not self.digital:
                    output = output * (self.analog_range / self.sensor_range)
                else:
                    for dimension in range(len(output)):
                        output[dimension] = utilities.apply_sensitivity(output[dimension],
                                                                        2 * self.sensor_range / (2 ** self.resolution))

                columns_content = (str(dt.datetime.now(tz=None)), sim_time, *output)

            # write sensor output into the database
            # utilities.write_line_into_database_table(self.database, self.name, columns_content)
            self.outputs.safe_output(self.name, self.column_names,
                                     columns_content)
            logging.debug(output)

    def run(self):
        """ Starts the sensor.

        Uses a while loop to simulate the sensor, so it has to be started in a separated thread.
        Basic procedure: read in local magnetic field of earth -> call get_mag_field function and write output into
        an instance owned table
        """
        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor on time
            self.output(88888888)

            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script to slow for the set sampling rate of the Magnetometer")


if __name__ == "__main__":
    import sqlite3
    import pyIGRF

    logging.basicConfig(level=logging.INFO)
    mgt = FluxgateMagnetometer(name='Fluxgate', position=np.array([0, 0, 0]), orientation=np.array([1, 0, 0, 0]),
                               database='test.db', mode=1, resolution=24, sensor_range=65000., analog_range=12.,
                               noise_density=12., bandwidth=2., sampling_rate=10, temp_drift=(0.5, 293.15),
                               max_zero_error=5., max_scaling_error=0.0005, max_orthogonality_error=0.0, active=True,
                               digital=True, use_max_errors=True)

    def test_write():
        lons = range(360)
        for lon in lons:
            conn_10 = sqlite3.connect('test.db')
            c_10 = conn_10.cursor()
            try:
                c_10.execute('create table magnetic_field_strength (real_time, simulation_time, M_x, M_y, M_z)')
                c_10.execute('create table state_sat (real_time, simulation_time, q_r, q_i, q_k, q_l, w_x, w_y, w_z)')
            except:
                pass

            M_x = pyIGRF.igrf_value(0., lon, 400., 2023.789)[3]
            M_y = pyIGRF.igrf_value(0., lon, 400., 2023.789)[4]
            M_z = pyIGRF.igrf_value(0., lon, 400., 2023.789)[5]

            c_10.execute('insert into magnetic_field_strength VALUES (?, ?, ?, ?, ?)',
                             [str(dt.datetime.now(tz=None)), lon, (M_x), (M_y), (M_z)])
            c_10.execute('insert into state_sat VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                             [str(dt.datetime.now(tz=None)), lon, 1, 0, 0, 0, 0, 0, 0])
            conn_10.commit()
            c_10.close()

    test_write()
    start = time.time()
    for i in range(500):
        mgt.output(0)

    print('Sensor took {} seconds.'.format(str(time.time() - start)))
    """t1 = threading.Thread(target=test_write, args=())
    t1.start()
    print('thread 1 aktiv')
    time.sleep(1)
    t2 = threading.Thread(target=mgt.run, args=())
    t2.start()
    print('thread 2 aktiv')"""
