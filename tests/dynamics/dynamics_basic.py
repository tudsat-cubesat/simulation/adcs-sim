from pyquaternion import Quaternion
import numpy as np
import orbit
import utilities
from environment import Environment
import datetime as dt
import pandas



J = np.array([[0.00222, 0, 0], [0, 0.00222, 0], [0, 0, 0.00222]])

def dynamics_init():

    # create table for the output of the sensor in the database, if it doesn't already exist
    utilities.create_table('test.db', 'orientation_sat', 'real_time, simulation_time, q_r, q_i, q_j, q_k')

    for i in range(0, 3):

        q_in = Quaternion(axis=[1, 0, 0], degrees=90)
        #q_init = Quaternion.random()

        # write sensor output into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, q_in[0], q_in[1], q_in[2], q_in[3])
        utilities.write_line_into_database_table('test.db', 'orientation_sat', columns_content)

def dynamics(sim_time, freq, w):

    # omega in radians/sec

    df = utilities.get_last_line_from_database_table('test.db', "orientation_sat", 1)
    q_in = df.to_numpy().reshape(6)
    q_in = Quaternion(q_in[2:6])

    print(q_in)

    delta_t = 1 / freq

    delta_qx = w[0] * delta_t
    delta_qy = w[1] * delta_t
    delta_qz = w[2] * delta_t

    print(delta_qx, delta_qy, delta_qz)

    q_rot_x = Quaternion(axis=[1, 0, 0], angle=delta_qx)
    q_rot_y = Quaternion(axis=[0, 1, 0], angle=delta_qy)
    q_rot_z = Quaternion(axis=[0, 0, 1], angle=delta_qz)

    print(q_rot_x, q_rot_y, q_rot_z)

    q_curr = q_rot_x.rotate(q_in)
    q_curr = q_rot_y.rotate(q_curr)
    q_curr = q_rot_z.rotate(q_curr)

    print('Current', q_curr)

    # write sensor output into the database
    columns_content = (str(dt.datetime.now(tz=None)), sim_time, q_curr[0], q_curr[1], q_curr[2], q_curr[3])
    utilities.write_line_into_database_table('test.db', 'orientation_sat', columns_content)

def kin_attitude_ode(q, w):

    """torque-free motion of spacecraft"""

    # """
    # the kinematic attitude ode
    # quat: attitude quaternion
    # w: angular velocity vector (rad/s)
    # returns time derivative of quaternion: q_dot
    # """
    w_quat = Quaternion([0, *w])
    q_dot = 0.5 * w_quat * q
    return np.array(q_dot.elements)


#print('time derivative of quaternion: q_dot= ', kin_attitude_ode(q, w))


dynamics_init()
dynamics(1, 5, [20, 0, 20])
