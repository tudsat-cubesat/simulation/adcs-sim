from time import time
import numpy as np
import pandas as pd
from scipy.integrate import odeint
from math import pow, degrees, radians, pi, sin, cos, sqrt
import matplotlib.pyplot as plt
import utilities
from pyquaternion import Quaternion
from numpy.linalg import inv
from scipy import integrate
from matplotlib.pylab import *
import datetime as dt
import scipy
import logging
import json


# todo:
# - remodel as class
# - rename parameters and methods to fit in framework
# - Type Hints
# - rethink rotation transformations: why use own transformations and 
#   not the ones from scipy.spatial.transform? Are they better than 
#   scipy? If needed, it should be moved to utilities.py

class Dynamics(object):
    def __init__(self, database: str, w_initial: np.array, euler_init: np.array, att_random: bool, sampling_rate: float,
                 I_xx, I_yy, I_zz, torque_initial, outputs):
        self.outputs = outputs
        self.database = database
        self.att_random = att_random
        self.sampling_rate = sampling_rate
        self.I_xx = I_xx
        self.I_yy = I_yy
        self.I_zz = I_zz
        self.w_initial = np.array(w_initial) * np.pi/180
        self.euler_init = euler_init
        self.torque_initial = torque_initial
        # create table for the output of the sensor in the database, if it doesn't already exist
        # utilities.create_table(self.database, 'state_sat', 'real_time, simulation_time, q_r, q_i, q_j, q_k, w_x, w_y, w_z')
        self.column_names = ['real_time', 'simulation_time', 'q_r', 'q_i', 'q_j', 'q_k', 'w_x', 'w_y', 'w_z']

        if self.att_random is True:
            q_in = Quaternion.random()
        else:
            q_in = utilities.euler_to_quaternion(euler_init)

        for i in range(0, 3):
            # write dynamic output into the database
            columns_content = (str(dt.datetime.now(tz=None)), 0, q_in[0], q_in[1], q_in[2], q_in[3],
                               *self.w_initial)
            self.outputs.safe_output('state_sat', self.column_names,
                                     list(columns_content), 3)
            # utilities.write_line_into_database_table(self.database, 'state_sat', columns_content)

    def state_derivative(self, state, t, torque, J):
        """Computes the first derivative of the state vector components from the Euler rotational equation for a rigid
        body in its principle axis frame"""

        # state vector: [q0,q1,q2,q3, wx,wy,wz]
        q = state[0:4]
        w = state[4:7]  # w: angular velocity of body frame with respect to the inertial frame

        w_quat = Quaternion([0, *w])
        q_quat = Quaternion(q)
        # q_dot = 0.5 * w_quat * q_quat # in fixed reference frame
        q_dot = 0.5 * q_quat * w_quat  # satellite frame
        q_dot = np.array([q_dot.scalar, *q_dot.vector])  # transform to scalar values for integrator

        # torque = np.array([0, 0, 0])
        w_dot = (-np.matmul(inv(J), np.cross(w, np.matmul(J, w))) + np.matmul(inv(J), torque))
        # logging.debug('w_dot= ', str(w_dot))

        state_dot = np.concatenate((q_dot, w_dot), axis=None)

        # logging.debug('state_dot= ', str(state_dot))
        # logging.debug('state_dot_test= ', str(state_dot_test))

        # return time derivative of state vector
        return state_dot

    def output(self, simulated_time: float):
        """The first derivative of the state vector components are integrated over the time period of a time-step to
        get the angular velocity components and the quaternion which rotates the satellite relative to the TEME-Frame.
        Note: The orientation of the Satellite in other scripts has to be updated every time-step
                with the last line of table "state_sat" """

        delta_t = 1/self.sampling_rate
        J = np.diag([self.I_xx, self.I_yy, self.I_zz])

        # Get last line from Database
        # df = utilities.get_last_line_from_database_table(self.database, "state_sat", 1)
        df = pd.read_json('json_communication/state_sat.json', orient='records',
                          lines=True)
        df = df.iloc[-1:]
        state = df.to_numpy().reshape(9)
        q_0 = state[2:6]
        # state = np.array(state[2:9])
        logging.debug('Quat=' + str(q_0))
        w_curr = state[6:9]
        logging.debug('Omega=' + str(w_curr))
        # Define state vector
        state = [*q_0, *w_curr]
        logging.debug('State=' + str(state))

        # Torque has to be computed in an other script and has to be read from database
        torque = self.torque_initial

        # start attitude propagation simulation
        state_data = np.zeros([2, 7])
        state_data = odeint(self.state_derivative, state, [0, delta_t], args=(torque, J))
        state_i = state_data[-1]
        logging.debug('state= ' + str(state_data))

        q = state_i[0:4]
        euler = utilities.quaternion_to_euler(q)

        # desired attitude
        euler_ref = np.array([0.0, 0.0, 0.0])

        error_last = np.array([0.0, 0.0, 0.0])

        # compute error
        error = euler_ref - euler
        derror = (error - error_last) / delta_t

        # for symmetric J = 1
        kp = 0.0
        kd = 0.0

        tx = kp * error[2] + kd * derror[2]
        ty = kp * error[1] + kd * derror[1]
        tz = kp * error[0] + kd * derror[0]

        torque_data = np.array([tx, ty, tz])

        # assign
        q_data = state_i[0:4]
        w_data = state_i[4:7]
        # w_data = w_data * 180 / np.pi

        # convert quaternions to euler
        euler_data = utilities.quaternion_to_euler(q_data)

        columns_content = (str(dt.datetime.now(tz=None)), simulated_time, *q_data, *w_data)
        # utilities.write_line_into_database_table(self.database, 'state_sat', columns_content)
        self.outputs.safe_output('state_sat', self.column_names,
                                 list(columns_content), 3)

        return q_data, w_data, torque_data, euler_data

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    mu = 3.986004e14  # m J /kg
    r_earth = 6378.14e3

    n_orbits = 0.1  # number of orbits
    delta_t = 1.0  # time step

    # euler sequence: yaw, pitch, roll
    euler_init = np.array([100, 0, 0]) * np.pi / 180.0
    w_initial = np.array([50.0, 0.0, 0.0]) * np.pi / 180.0  # w_initial: angular rate in rad/s

    I_xx = 0.5448
    I_yy = 2.4444
    I_zz = 2.6052

    torque_initial = np.array([0.0, 0.0, 0.0])
    freq = 1


    dynamics = Dynamics(database = 'test.db', w_initial = w_initial, 
        euler_init = euler_init, att_random=True, sampling_rate = freq,
        I_xx = I_xx, I_yy = I_yy, I_zz = I_zz, torque_initial = torque_initial)
    dynamics.output(0)
    dynamics.output(1)

# fig = plt.figure(1)  # figsize=(6.5, 9.5))
#
# # clear the figure
# fig.clf()
#
# time_data = time_data / 60
#
# plt.subplot(411)
# plt.plot(time_data, q_data)
# plt.legend(['$q_0$', '$q_1$', '$q_2$', '$q_3$'])
# plt.ylim(-1.1, 1.1)
#
# plt.subplot(412)
# plt.plot(time_data, euler_data * 180 / np.pi)
# plt.legend(['$\\psi (yaw)$', '$\\theta (pitch)$', '$\phi (roll)$'])
# plt.ylim(-200, 200)
#
# plt.subplot(413)
# plt.plot(time_data, w_data)
# legend(['$\omega_x$','$\omega_y$','$\omega_z$'])
# ylabel('$\omega$ [rad/s]')
# plt.ylim(-4, 4)
#
# plt.subplot(414)
# plt.plot(time_data, torque_data)
# plt.legend(['$T_x$', '$T_y$', '$T_z$'])
# plt.ylim(-0.200, 0.200)
#
# plt.show()
