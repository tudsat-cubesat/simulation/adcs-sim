# ADCS-HIL Framework

This project simulates the adcs system of a cubesat. It can be used to evaluate different sensor configurations and controllers.

## Current version includes:
* attitude kinematic and dynamic propagation
* orbit propagation using sgp4
* magnetic field model: IGRF-13
* senors: gyroscope, accelerometer, magnetometer, sun senor and eart horizon sensor
* GUI for setup and results
    * setup done via config files
* database for results
* verified against Matlab cubesat toolkit

## TODOs:
* actuators: magnetorquer
* simulation of perturbation torques from the environment:
    * Gravity gradient torque, solar radiation torque, aerodynamic torque, magnetic torque
* controllers

## Installation:

Use the included env.yml file with anaconda to create a virtual environment including all necessary packages.

## How to use:

Activate the anaconda environment and run the view.py file. Then load a yaml config file by specifying its name and pressing "read file" in the top right. The "default.yaml" should work without issues, possible errors and some info will be generated in the anaconda prompt. Start the simulation by pressing the "start simulation" button. You can then see the simulation results being generated in the other tabs.

## Credit

This project is based on the ADP project done in 2021 at the TU Darmstadt by Thomas, Gerhard, Younes, Felix, Stephan and Nicholas. Their work was based on an earlier prototype by TUDSaT.
