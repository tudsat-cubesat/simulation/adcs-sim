---
time_parameters:
  # simulated_time * real_time_factor =  real_time
  # for simulation without sinchronisation wait time, real_time_factor 
  # should be `real_time_factor = 0`
  simulated_time_start: 1629064800.0
  real_time_factor: 1.0
  simulated_duration: 30.0

pre_simulation:
  outputs: 
    # list of the outputs for the presimulation, with the property
    # name of the main object as the key name. 
    orbit_env_outputs:
      database_presim: 'test.db'
      database_sim: 'test.db'
      orbit_env: true
  components:
    # Only the components specified in this list will be instanced and 
    # run on the presimulation. For parameters different from default
    # add here a `parameters` property and add the custom 
    # subproperties.
    - name: 'orbit'
      class: 'Orbit'
      method: 'propagate_orbit'
      # parameters: 
      # - database: 'predata.db'
    - name: 'environment'
      class: 'Environment'
      method: 'propagate_environment'

simulation:
  outputs: 
    # list of the outputs for the simulation, with the property
    # name of the main object as the key name. 
    simulation_outputs:
      database_presim: 'test.db'
      database_sim: 'test.db'
      orbit_env: false
  components:
    # Only the components specified in this list will be instanced and 
    # run on the simulation. For parameters different from default
    # add here a `parameters` property and add the custom 
    # subproperties.
    - name: 'FluxMag_X'
      class: 'FluxgateMagnetometer'
      start_simulation_time: 0.5
    - name: 'dynamics'
      class: 'Dynamics'
      start_simulation_time: 0.
    - name: 'gyro'
      class: 'MEMS3AxisGyroscope'
      start_simulation_time: 0.5
    - name: 'acceleration'
      class: 'MEMS3AxisAccelerationSensor'
      start_simulation_time: 0.5
    - name: 'earth_sensor'
      class: 'EarthSensor'
      start_simulation_time: 0.5
    - name: 'photodiode'
      class: 'Photodiode'
      start_simulation_time: 0.5
    - name: 'finesunsensor'
      class: 'FineSunSensor'
      start_simulation_time: 0.5
    - name: 'json_grabber'
      class: 'MakeDict'
      start_simulation_time: 0.


Components:
  # This object lists default properties for all classes. New classes 
  # have to be added here to be able to be added in the simulation.
  # This object is for default configuration only. Adding this object
  # to a custom configuration will have no effect. 
  Orbit:
    class: 'Orbit'
    module: 'orbit'
    parameters:
      database: 'test.db'
      outputs: 'orbit_env_outputs'
      sampling_rate: 50.
      epoch: 'sim_time_offset'
      a: 6771.
      i: 51.3
      e: 0.0015
      argpo: 0.
      RAAN: 0.
      mean_anomaly: 0.
  Environment:
    class: 'Environment'
    module: 'environment'
    parameters:
      sim_time_offset: 'sim_time_offset'
      database: 'test.db'
      outputs: 'orbit_env_outputs'
      sampling_rate: 50.
  Dynamics:
    class: 'Dynamics'
    module: 'dynamics'
    parameters:
      outputs: 'simulation_outputs'
      database: 'test.db'
      w_initial: [0.872664626, 0.0, 0.0]
      euler_init: [1.745329252, 0.0, 0.0]
      att_random: true
      sampling_rate: 5.0
      I_xx: 1.0
      I_yy: 1.0
      I_zz: 1.0
      torque_initial: [0.0, 0.0, 0.0]
  FluxgateMagnetometer:
    class: 'FluxgateMagnetometer'
    module: 'magnetometer'
    parameters:
      outputs: 'simulation_outputs'
      name: 'FluxMag_X'
      position: [0, 0, 0]
      orientation: [1, 0, 0, 0]
      database: 'test.db'
      mode: 1
      resolution: 24
      sensor_range: 65000.
      analog_range: 12.
      noise_density: 12.
      bandwidth: 2.
      sampling_rate: 1
      temp_drift: [0.5, 293.15]
      max_zero_error: 5.
      max_scaling_error: 0.05
      max_orthogonality_error: 1
      active: True
      digital: True
  MEMS3AxisGyroscope:
    class: 'MEMS3AxisGyroscope'
    module: 'gyroscope'
    parameters:
      outputs: 'simulation_outputs'
      name: 'gyro'
      position: [0, 0, 0]
      orientation: [1, 0, 0, 0]
      database: 'test.db'
      mode: 1
      digital: True
      sensor_range: [400, -400]
      sensitivity: 0.000047
      sampling_rate: 1
      angular_random_walk: 0.3
      bias_instability: 0.15
  MEMS3AxisAccelerationSensor:
    class: 'MEMS3AxisAccelerationSensor'
    module: 'acceleration_sensor'
    parameters:
      outputs: 'simulation_outputs'
      name: "acceleration"
      position: [0, 0, 0]
      orientation: [1, 0, 0, 0]
      database: 'test.db'
      mode: 1
      digital: True
      sensor_range: [980, -980]
      sensitivity: 0.00001
      sampling_rate: 1
      scale_factor_error: [0.001, 0.001, 0.001]
      zero_offset: 0.001
      random_noise: [0.01, 0.01, 0.01]
  EarthSensor:
    class: 'EarthSensor'
    module: 'earth_sensor'
    parameters:
      outputs: 'simulation_outputs'
      name: 'earth_sensor'
      position: [0.001, 0.001, 0.001]
      orientation: [0, 0, 0.7071, -0.7071]
      database: 'test.db'
      digital: True
      mode: 1
      fov_shape: 'circle'
      fov: 120
      detected_area: 0
      accuracy: [1.5, 3]
      sampling_rate: 1.
      active: True
  FineSunSensor:
    class: 'FineSunSensor'
    module: 'sunsensor'
    parameters:
      outputs: 'simulation_outputs'
      name: 'finesunsensor'
      position: '+X'
      orientation: [1, 0, 0]
      database: 'test.db'
      mode: 1
      accuracy: 0.3
      precision: 0.05
      fov: 120
      max_current: 100
      sampling_rate: 1.
      digital: False
      active: True
  Photodiode:
    class: 'Photodiode'
    module: 'sunsensor'
    parameters:
      outputs: 'simulation_outputs'
      name: 'photodiode'
      position: [1, 1, 1, 1, 1, 1]
      orientation: [1, 0, 0]
      database: 'test.db'
      mode: 1
      accuracy: 0.3
      precision: 0.05
      fov: 140
      max_current: 100
      sampling_rate: 1.
      digital: False
      active: True
  MakeDict:
    class: 'MakeDict'
    module: 'json_grabber'
    parameters:
      json_path: ["./json_communication/actuator.json",
                  "./json_communication/sensor.json"]
      # acceleration.json only as test path
      # - "./json_communication/acceleration.json"
      sampling_rate: 1.
      name: 'json_grabber'
...
