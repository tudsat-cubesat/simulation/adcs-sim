from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
import pyqtgraph as pg
import sqlite3
import utilities
import numpy as np
import collections


class GUI(QWidget):
    def __init__(self, database):
        super().__init__()
        self.database = database
        self.app = QApplication([])
        self.app.setStyle('Material')

        self.layout = QGridLayout(self)



        # get all tables
        conn = sqlite3.connect(self.database)
        c = conn.cursor()
        c.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = c.fetchall()
        c.close()
        # creates a list with the table names as content
        self.sensor_table_names = []
        self.orbit_environment_tables = ['magnetic_field_strength', 'temperature', 'earth_to_sat', 'sat_to_sun',
                                         'earth_to_sun']

        for table_name in tables:
            self.sensor_table_names.append(table_name[0])
        # removes parameter tables and pre calculated tables
        for table_name in self.sensor_table_names:
            if '_parameters' in table_name:
                self.sensor_table_names.remove(table_name)
            # removes pre calculated tables
            for pre_calculated_table in self.orbit_environment_tables:
                if table_name is pre_calculated_table:
                    self.sensor_table_names.remove(table_name)

        self.sensor_buttons()
        self.orbit_buttons()
        self.active_plots = []

    def sensor_buttons(self):
        for i, table in enumerate(self.sensor_table_names):
            button = QPushButton(table, self)
            button.setDefault(False)
            button.clicked.connect(lambda out, table=table: self.function(table))
            self.layout.addWidget(button, 0, i)



    def function(self, i):
        print(i)

    def orbit_buttons(self):
        for i, table in enumerate(self.orbit_environment_tables):
            button = QPushButton(table, self)
            button.setDefault(False)
            button.clicked.connect(lambda out, table=table: self.function1(table))
            self.layout.addWidget(button, 1, i)

    def function1(self, name):
        print(name)

class LivePlot:
    def __init__(self, database, table_name, sampleinterval=0.1, timewindow=10., size=(600,350)):
        # Data stuff
        self._interval = int(sampleinterval*100)

        df = utilities.get_last_line_from_database_table(database, table_name, rows=100)
        self._bufsize = len(df.index)
        self.x = df.pop(['simulation_time'])
        df = df.drop(['simulation_time'], 1)
        df = df.drop(['real_time'], 1)
        self.databuffer = []
        for column in df:
            self.databuffer.append(collections.deque(df[column]))

        self.y = np.zeros(self._bufsize, dtype=float)
        # PyQtGraph stuff
        self.app = QtGui.QApplication([])
        self.app.setStyle('Windows')
        self.plt = pg.plot(title=table_name)
        self.plt.resize(*size)
        self.plt.showGrid(x=True, y=True)
        self.plt.setLabel('left', 'amplitude', 'V')
        self.plt.setLabel('bottom', 'time', 's')
        self.curve = self.plt.plot(self.x, self.y, pen=(255,0,0))
        # QTimer
        self.timer = QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(self._interval)

    def updateplot(self):
        self.databuffer.append(float(utilities.get_last_line_from_database_table('GUI_TEST.db', 'angular_components')[["omega_x"]].to_numpy()))
        self.y[:] = self.databuffer
        self.curve.setData(self.x, self.y)
        self.app.processEvents()








if __name__ == '__main__':
    app = QApplication([])
    mainapp = GUI('testplot.db')
    mainapp.show()
    app.exec_()