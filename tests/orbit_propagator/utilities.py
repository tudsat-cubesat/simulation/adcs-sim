import numpy as np
import pandas as pd
from pyquaternion import Quaternion
import sqlite3
import datetime as dt
from astropy import coordinates as coord, units as u
from astropy.time import Time


def apply_sensitivity(value: float, sensitivity: float):
    """Simulates the sensitivity of the sensor.

    :param value: (double) sensor output without sensitivity
    :param sensitivity: (double) sensitivity of the sensor
    :return: (double) sensor output with sensitivity
    """
    return sensitivity * round(value / sensitivity)


def get_acceleration_frame_transformation(position, orientation, angular_velocity, angular_acceleration,
                                          velocity=[0, 0, 0], acceleration=[0, 0, 0]):
    """Transforms the acceleration into the sensor frame.

    :param position: (numpy array, 1x3 x y z) position of the sensor in the satellite frame
    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the satellite frame
    :param angular_velocity: (numpy array, 1x3 omega_x omega_y omega_z) angular velocity of the satellite in the
                                satellite coordinate frame
    :param angular_acceleration: (numpy array, 1x3 alpha_x alpha_y alpha_z) angular acceleration of the satellite in the
                                satellite coordinate frame
    :param velocity: (numpy array, 1x3 v_x v_y v_z) velocity of the satellite in the satellite frame
    :param acceleration: (numpy array, 1x3 a_x a_y a_z) acceleration of the satellite in the satellite frame
    :return: (numpy array, 1x3 a_x a_y a_z) acceleration of the sensor in the sensor frame
    """
    # _T = transposed, _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # basic principle of this function is the equation a = R_T_dot_dot * p + 2 * R_T_dot * p_dot + R_T * p_dot_dot
    # p is a point in the space and R_T is the transposed rotation matrix
    q_r = orientation[3]
    q_i = orientation[0]
    q_j = orientation[1]
    q_k = orientation[2]

    # split up the angular velocity
    omega_x = angular_velocity[0]
    omega_y = angular_velocity[1]
    omega_z = angular_velocity[2]

    # split up the angular acceleration
    alpha_x = angular_acceleration[0]
    alpha_y = angular_acceleration[1]
    alpha_z = angular_acceleration[2]

    # differentiated quaternions
    q_r_dot = -1 / 2 * (omega_x * q_i + omega_y * q_j + omega_z * q_k)
    q_i_dot = 1 / 2 * (omega_x * q_r + omega_y * q_k - omega_z * q_j)
    q_j_dot = 1 / 2 * (omega_y * q_r + omega_z * q_i - omega_x * q_k)
    q_k_dot = 1 / 2 * (omega_z * q_r + omega_x * q_j - omega_y * q_i)

    # differentiated quaternions
    q_r_dot_dot = -1 / 2 * (alpha_x * q_i + omega_x * q_i_dot + alpha_y * q_j + omega_y * q_j_dot +
                            alpha_z * q_k + omega_z * q_k_dot)
    q_i_dot_dot = 1 / 2 * (alpha_x * q_r + omega_x * q_r_dot + alpha_y * q_k + omega_y * q_k_dot -
                           alpha_z * q_j - omega_z * q_j_dot)
    q_j_dot_dot = 1 / 2 * (alpha_y * q_r + omega_y * q_r_dot + alpha_z * q_i + omega_z * q_i_dot -
                           alpha_x * q_k - omega_x * q_k_dot)
    q_k_dot_dot = 1 / 2 * (alpha_z * q_r + omega_z * q_r_dot + alpha_x * q_j + omega_x * q_j_dot -
                           alpha_y * q_i - omega_y * q_i_dot)

    # transposed rotation matrix
    R_T = np.array(
        [[1 - 2 * (q_j ** 2 + q_k ** 2), 2 * (q_i * q_j + q_k * q_r), 2 * (q_i * q_k - q_j * q_r)],
         [2 * (q_i * q_j - q_k * q_r), 1 - 2 * (q_i ** 2 + q_k ** 2), 2 * (q_j * q_k + q_i * q_r)],
         [2 * (q_i * q_k + q_j * q_r), 2 * (q_j * q_k - q_i * q_r), 1 - 2 * (q_i ** 2 + q_j ** 2)]])

    # differentiated transposed rotation matrix
    R_T_dot = np.array([[-4*(q_j*q_j_dot+q_k*q_k_dot), 2*(q_i_dot*q_j+q_i*q_j_dot+q_k_dot*q_r+q_k*q_r_dot),
                         2*(q_i_dot*q_k+q_i*q_k_dot-q_j_dot*q_r-q_j*q_r_dot)],
                        [2*(q_i_dot*q_j+q_i*q_j_dot-q_k_dot*q_r-q_k*q_r_dot), -4*(q_i_dot*q_i+q_k*q_k_dot),
                         2*(q_j_dot*q_k+q_j*q_k_dot+q_i_dot*q_r+q_i*q_r_dot)],
                        [2*(q_i_dot*q_k+q_i*q_k_dot+q_j_dot*q_r+q_j*q_r_dot),
                         2*(q_j_dot*q_k+q_j*q_k_dot-q_i_dot*q_r-q_i*q_r_dot), -4*(q_i_dot*q_i+q_j_dot*q_j)]])

    # differentiated transposed rotation matrix
    R_T_dot_dot = np.array([[-4*(q_j_dot**2+q_j*q_j_dot_dot+q_k_dot**2+q_k*q_k_dot_dot),
                             2*(q_i_dot_dot*q_j+2*q_i_dot*q_j_dot+q_i*q_j_dot_dot+q_k_dot_dot*q_r+2*q_k_dot*q_r_dot+q_k*q_r_dot_dot),
                             2*(q_i_dot_dot*q_k+2*q_i_dot*q_k_dot+q_i*q_k_dot_dot-q_j_dot_dot*q_r-2*q_j_dot*q_r_dot-q_j*q_r_dot_dot)],
                            [2*(q_i_dot_dot*q_j+2*q_i_dot*q_j_dot+q_i*q_j_dot_dot-q_k_dot_dot*q_r-2*q_i_dot*q_r_dot-q_k*q_r_dot_dot),
                             -4*(q_i_dot_dot*q_i+q_i_dot**2+q_k_dot**2+q_k*q_k_dot_dot),
                             2*(q_j_dot_dot*q_k+2*q_j_dot*q_k_dot+q_j*q_k_dot_dot+q_i_dot_dot*q_r+2*q_i_dot*q_r_dot+q_i*q_r_dot_dot)],
                            [2*(q_i_dot_dot*q_k+q_j*q_k_dot_dot+2*q_i_dot*q_k_dot+q_j_dot_dot*q_r+2*q_j_dot*q_r_dot+q_j*q_r_dot_dot),
                             2*(q_j_dot_dot*q_k+2*q_j_dot*q_k_dot+q_j*q_k_dot_dot-q_i_dot_dot*q_r-2*q_i_dot*q_r_dot-q_i*q_r_dot_dot),
                             -4*(q_i_dot_dot*q_i+q_i_dot**2+q_j_dot**2+q_j_dot_dot*q_j)]])

    # calculate acceleration in the sensor frame
    acceleration_in_sensor_frame = R_T_dot_dot.dot(position) + 2*R_T_dot.dot(velocity) + R_T.dot(acceleration)
    return acceleration_in_sensor_frame


def get_cardan_angular_velocity_transformation(beta: float, gamma: float, W):
    """Transforms the angular velocity of the satellite frame into the sensor frame by using euler coordinates.

    :param beta: (double) rotation of y-axis of the sensor in euler coordinates
    :param gamma: (double) rotation of z-axis of the sensor in euler coordinates
    :param W: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate frame
    :return: (numpy array, 1x3 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # matrix that transforms the angular velocity of the satellite frame
    # into the angular velocity of the sensor frame
    H_1 = np.array([[np.cos(beta) * np.cos(gamma), np.sin(gamma), 0],
                    [-np.cos(beta) * np.sin(gamma), np.cos(gamma), 0],
                    [np.sin(beta), 0, 1]])
    # calculate the angular velocity of the sensor
    W_out = H_1.dot(W)

    return W_out


def get_quaternion_angular_velocity_transformation_non_unit_quaternions(orientation: np.ndarray,
                                                                        angular_velocity: np.ndarray,
                                                                        s=1) -> np.ndarray:
    """Transforms the angular velocity of the satellite frame into the sensor frame by using quaternions.

    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the satellite frame
    :param angular_velocity: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate frame
    :param s: (double) s = ||q||**-2 if q is a unit quaternion then s = 1
    :return: (numpy array, 1x3 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # _T = transposed, _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # basic principle of this function is the equation W_tilde = R_T * R_dot
    # with R_T is the transposed rotation matrix and R_dot is the differentiated rotation matrix
    #                _                             _
    #               |  0        -omega_z     omega_y|
    # and W_tilde = |  omega_z   0          -omega_x| with the angular velocity omega in the sensor frame
    #               |_-omega_y   omega_x     0     _|
    # ===============================================================================================
    # split up the quaternion in it's parts
    q_r = orientation[3]
    q_i = orientation[0]
    q_j = orientation[1]
    q_k = orientation[2]

    # split up the angular velocity
    W_x = angular_velocity[0]
    W_y = angular_velocity[1]
    W_z = angular_velocity[2]

    # differentiated quaternions
    q_r_dot = -1 / 2 * (W_x * q_i + W_y * q_j + W_z * q_k)
    q_i_dot = 1 / 2 * (W_x * q_r + W_y * q_k - W_z * q_j)
    q_j_dot = 1 / 2 * (W_y * q_r + W_z * q_i - W_x * q_k)
    q_k_dot = 1 / 2 * (W_z * q_r + W_x * q_j - W_y * q_i)

    # transposed rotation matrix
    R_T = np.array(
        [[1 - 2 * s * (q_j ** 2 + q_k ** 2), 2 * s * (q_i * q_j + q_k * q_r), 2 * s * (q_i * q_k - q_j * q_r)],
         [2 * s * (q_i * q_j - q_k * q_r), 1 - 2 * s * (q_i ** 2 + q_k ** 2), 2 * s * (q_j * q_k + q_i * q_r)],
         [2 * s * (q_i * q_k + q_j * q_r), 2 * s * (q_j * q_k - q_i * q_r), 1 - 2 * s * (q_i ** 2 + q_j ** 2)]])

    # differentiated rotation matrix
    R_dot = np.array([[-4 * s * (q_j * q_j_dot + q_k * q_k_dot),
                       2 * s * (q_i_dot * q_j + q_i * q_j_dot - q_k * q_r_dot - q_k_dot * q_r),
                       2 * s * (q_i_dot * q_k + q_i * q_k_dot + q_j_dot * q_r + q_j * q_r_dot)],
                      [2 * s * (q_i_dot * q_j + q_i * q_j_dot + q_k * q_r_dot + q_k_dot * q_r),
                       -4 * s * (q_i * q_i_dot + q_k * q_k_dot),
                       2 * s * (q_j_dot * q_k + q_j * q_k_dot - q_i_dot * q_r - q_i * q_r_dot)],
                      [2 * s * (q_i_dot * q_k + q_i * q_k_dot - q_j_dot * q_r - q_j * q_r_dot),
                       2 * s * (q_j_dot * q_k + q_j * q_k_dot + q_i_dot * q_r + q_i * q_r_dot),
                       -4 * s * (q_i * q_i_dot + q_j * q_j_dot)]])

    # matrix of the angular velocity in the sensor frame
    W_tilde = R_T.dot(R_dot)
    # vector of the angular velocity in the sensor frame
    W_out = np.array([W_tilde[2][1], W_tilde[0][2], W_tilde[1][0]])

    return W_out


def get_quaternion_angular_velocity_transformation(orientation: np.ndarray, angular_velocity: np.ndarray) -> np.ndarray:
    """Transforms the angular velocity of the satellite frame into the sensor frame by using quaternions.

    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the
                        satellite frame
    :param angular_velocity: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate
                            frame
    :return: (numpy array, vector 3x1 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # Basic principle of this function is: W_1 = -2(q_r_dot * q - q_r * q_dot - q_dot x q
    #                                                                  _         _
    #                                     _                          _  |  q_dot_r  |
    #                                    |  -q_i    q_r   q_k    -q_j | |  q_dot_i  |
    # that cn be transformed int W_1 = 2*|  -q_j   -q_k   q_r     q_i | |  q_dot_j  |
    #                                    |_ -q_k    q_j  -q_i     q_r_| |_ q_dot_k _|
    #
    # ======================================================================================================
    # split up the quaternion in it's parts
    q_r = orientation[3]
    q_i = orientation[0]
    q_j = orientation[1]
    q_k = orientation[2]

    # split up the angular velocity
    W_x = angular_velocity[0]
    W_y = angular_velocity[1]
    W_z = angular_velocity[2]

    # differentiated quaternions
    q_r_dot = -1 / 2 * (W_x * q_i + W_y * q_j + W_z * q_k)
    q_i_dot = 1 / 2 * (W_x * q_r + W_y * q_k - W_z * q_j)
    q_j_dot = 1 / 2 * (W_y * q_r + W_z * q_i - W_x * q_k)
    q_k_dot = 1 / 2 * (W_z * q_r + W_x * q_j - W_y * q_i)

    # make the transformation matrix
    G = np.array([[-q_i, q_r, q_k, -q_j],
                  [-q_j, -q_k, q_r, q_i],
                  [-q_k, q_j, -q_i, q_r]])

    # make quaternion differention vector
    q_dot = np.array([[q_r_dot],
                      [q_i_dot],
                      [q_j_dot],
                      [q_k_dot]])

    # calculate angular velocity in the sensor frame
    W_out = 2 * G.dot(q_dot)

    return W_out


def get_attitude(vector1, vector2):
    """
    returns the quaternion that rotates one vector into the other.
    (modified code from TUDSat script)
    Args:
        vector1 (numpy ndarray): first vector
        vector2 (numpy ndarray): second vector

    Returns:
        quaternion (pyquaternion.Quaternion): A quaternion that represents a rotation from first to second vector
    """

    unit_vector1 = vector1 / np.linalg.norm(vector1)
    unit_vector2 = vector2 / np.linalg.norm(vector2)

    # check if arrays are equal and if so, return a quaternion that does not rotate any vector
    if np.array_equal(unit_vector1, unit_vector2):
        quaternion = Quaternion([1., 0., 0., 0.])
    else:
        rotation_axis = np.cross(unit_vector1, unit_vector2)
        rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))

        # check if vectors are pointing into the exact opposite direction
        if np.array_equal(rotation_axis, np.zeros(3)):
            # if vectors are pointing towards opposite directions cross product is a zero vector, which is why another
            # method for finding an orthogonal vector is used
            rotation_axis = np.array([1., 1., 1.])
            rotation_axis -= rotation_axis.dot(unit_vector1) * unit_vector1
            rotation_axis /= np.linalg.norm(rotation_axis)
            rotation_angle = np.pi

        # the length of the vector represents the angle of rotation
        quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
    return quaternion


def ned2ecef(lon, lat):
    """Returns quaternion that transforms local north, east, down vector into ECEF vector. (Source:
    https://gssc.esa.int/navipedia/index.php/Transformations_between_ECEF_and_ENU_coordinates
    https://onlinelibrary.wiley.com/doi/pdf/10.1002/9780470099728.app3

    @param lon: (float) geodetic longitude in deg
    @param lat: (float) geodetic lattitude in deg
    @return: (pyquaternion.Quaternion) a quaternion that represents the transformation from the NED to ECEF system
    """
    rot_mat = np.array([[-np.cos(lon) * np.sin(lat), -np.sin(lon), -np.cos(lon) * np.cos(lat)],
                        [-np.sin(lon) * np.sin(lat), np.cos(lon), -np.sin(lon) * np.cos(lat)],
                        [np.cos(lat), 0., -np.sin(lat)]])

    quaternion = Quaternion(matrix=rot_mat)

    return quaternion


def create_table(database: str, name: str, columns: str):
    """ Create a new table in the database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :param columns: (str) names of the columns separated by commas
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # create table, if it doesn't already exists
    try:
        c.execute('create table ' + name + ' (' + columns + ')')
    except:
        print('table ' + name + ' already existed')
        pass

    conn.commit()
    c.close()
    pass


def write_line_into_database_table(database: str, name: str, columns_content: tuple):
    """ Extends database table with one new line.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :param columns_content: (tuple) content that will be written in the table
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # checks how many columns the input has and adapts the sqlite syntax
    question_marks = '?'
    for i in range(len(columns_content) - 1):
        question_marks += ', ?'

    # writes the new line
    c.executemany('insert into ' + name + ' VALUES (' + question_marks + ')',
                  [columns_content])

    conn.commit()
    c.close()
    pass


def get_last_line_from_database_table(database: str, name: str) -> pd.core.frame.DataFrame:
    """ Read the last line of a table in the database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :return: (pandas.core.frame.DataFrame)
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # read last line of Table from database into a pandas dataframe
    last_row = pd.read_sql_query('SELECT * FROM ' + name + ' ORDER BY rowid DESC LIMIT 1;', conn)
    conn.commit()
    c.close()

    return last_row


def get_table_from_database(database: str, name: str) -> pd.core.frame.DataFrame:
    """ Read the table from database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :return: (pandas.core.frame.DataFrame)
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # read  Table from database into a pandas dataframe
    table = pd.read_sql_query('SELECT * FROM ' + name, conn)
    conn.commit()
    c.close()

    return table


def TEME_to_geodetic(x, y, z, t):
    """
    convert coordinates(km) given in TEME frame and a time(julian day)
    to latitude(deg), longitude(deg) and height(km)
    (code from TUDSat code)
    """
    t = Time(t, format="jd")
    # convert from the ECI frame TEME to ITRS which is an ECEF frame
    teme = coord.builtin_frames.TEME(x * u.km, y * u.km, z * u.km,
                                     obstime=t, representation_type='cartesian')
    itrs = teme.transform_to(coord.ITRS(obstime=t)).cartesian
    # ITRS is the default frame for EarthLocation which enables a geodetic representation
    # the WGS84 should be used since it is also used by the magnetic model
    earth_loc = coord.EarthLocation(x=itrs.x, y=itrs.y,
                                    z=itrs.z).to_geodetic(ellipsoid="WGS84")
    lat = earth_loc.lat.degree
    lon = earth_loc.lon.degree
    height = earth_loc.height.to_value(u.km)
    return [lat, lon, height]


def ECEF_to_TEME(x, y, z, t):
    """
    convert the coordinates given in an ECEF to the TEME frame at
    the given time in julian days
    (code from TUDSat code)
    """
    t = Time(t, format="jd")
    ecef = coord.ITRS(x, y, z, obstime=t)
    teme = ecef.transform_to(coord.builtin_frames.TEME(obstime=t))
    return np.array(teme.cartesian.xyz)


def julian_to_decimal(time):
    """
    converts a julian day date to a decimal year
    (code from TUDSat code)
    """
    t = Time(time, format="jd")
    return float(t.to_value("decimalyear"))
