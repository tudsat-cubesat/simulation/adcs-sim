import numpy as np
from numpy import linalg as la
import math
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.spatial.transform import Rotation as R
from pyquaternion import Quaternion
import sympy as sym
import sqlite3
from Sensor import Sensor
import utilities
import datetime as dt
import json
import time
import Arrow3D
import Plot_3D

ori = R.from_euler('zxz', np.array([0, 0, 0]), degrees=True)
ori = np.array(ori.as_quat())
a = Plot_3D.EarthSensor(name='EHS', position=np.array([0.001, 0.001, 0.001]),
                        orientation=np.array([ori[3], ori[0], ori[1], ori[2]]),
                        database='test.db',
                        digital=True, mode=1, FOV=60, accuracy=np.array([2, 1]), sampling_rate=1., active=True)

# Calculate Data for Earth Sensor
sim_time = 0
r_sphere = 1

df = utilities.interpolate_two_rows('test.db', name='position', sim_time=sim_time)
pos_sat = df.to_numpy()[0][1:].reshape(3)

# Read Earth Disk Radius from Database
df = utilities.interpolate_two_rows('test.db', name='radius_ellipsoid', sim_time=sim_time)
r_ell = df.to_numpy().reshape(2)
r_ell = r_ell[1] / 1000

orientation_sat: Quaternion = a.get_orientation_sat(sim_time)

Nadir_R_Sat = a.NadirVector(pos_sat, sim_time)

# Intersection of Nadir with the Unit Sphere
Nadir_Unit = r_sphere * Nadir_R_Sat / la.norm(Nadir_R_Sat)

# Calculation of the Earth Disk Radius r_disk
# Earth is simplified as an ideal globe
rho = np.arcsin(r_ell / (la.norm(pos_sat)))
print('rho', rho * 180 / np.pi)

lam = np.arccos(r_ell / (la.norm(pos_sat)))
d = np.tan(lam) * r_ell
r_disk = np.sin(rho) * d
r_disk_proj = np.sin(rho) * r_sphere
print('Earth Disk', r_disk, 'Earth Disk projection', r_disk_proj, rho * 180 / np.pi, lam * 180 / np.pi)

# Boresight vector in Sat Coordinates
tr_sen_to_sat = Quaternion(a.orientation)
tr_sat_to_sen = tr_sen_to_sat.inverse
v_b = tr_sen_to_sat.rotate([1, 0, 0])

# Intersection between Boresight of the Sensor and the Unit Sphere r_sphere
# Necessary if sensor is not mounted in the center of the satellite
c1, c2, c3, x = sym.symbols('c1,c2,c3,x')
c_fov_mat = np.array(sym.solve(
    [a.position[0] + x * v_b[0] - c1,
     a.position[1] + x * v_b[1] - c2,
     a.position[2] + x * v_b[2] - c3,
     c1 ** 2 + c2 ** 2 + c3 ** 2 - r_sphere ** 2],
    (c1, c2, c3, x)))
for i in range((c_fov_mat.shape[0])):
    if c_fov_mat[i, 3] > 0:
        # c_fov_vec describes the Intersection of the Sensor's Boresight with the Unit Sphere
        c_fov_vec = np.array(c_fov_mat[i, 0:3])
        c_fov_vec = c_fov_vec.astype(float)
        abs_c = la.norm(c_fov_vec)
        print('length of Sensors Intersection Vector', abs_c)

ang_bn = np.arccos((np.matmul(Nadir_R_Sat, v_b)) / (la.norm(Nadir_R_Sat) * la.norm(v_b)))

if ang_bn <= rho - a.FOV / 2:
    print('earth')
elif ang_bn >= rho + a.FOV / 2:
    print('space')
else:
    print('Horizon in FOV')

# Comparison of the EarthDisk Area Vector and the FOV Area Vector
# to determine the area recognized by the Sensor
# Assumption: Circle of Sensor's FOV is smaller than Earth Disk

# d_be describes distance between the Boresight's intersection point and Center of projected Earth Disk
d_be = la.norm(c_fov_vec - Nadir_Unit)

# Threshold Value for the case that the Sensor only recognizes the space
r_fov = r_sphere * np.sin(a.FOV / 2)
d_sp = r_fov + r_disk_proj
# Threshold Value for the case that the Sensor only recognizes the Earth
d_ea = r_disk_proj - r_fov

print('Space', d_sp, 'Earth', d_ea, 'd_be', d_be, 'FOV', r_fov)

"""Plot of the Sensors FOV in the unit sphere and the Earth Globe"""

# Get Data for FOV and Earth
u = np.linspace(0, 2 * np.pi, 100)
u1 = np.linspace(0, 2 * np.pi, 1001)
v = np.linspace(0, np.pi, 100)
circle_fov = np.zeros([len(u1) - 1, 3])

tr_sen_to_sat = Quaternion(a.orientation)
q_1 = Quaternion(axis=[0, 1, 0], angle=a.FOV / 2)
q_2 = tr_sen_to_sat
x = q_1.rotate([1, 0, 0])
x_sat = q_2.rotate(x)

for i in range(len(u1) - 1):
    q_rot = Quaternion(axis=c_fov_vec, angle=u1[i])
    if i == 0:
        circle_fov[i, :] = q_rot.rotate(x_sat)
    else:
        circle_fov[i, :] = q_rot.rotate(circle_fov[i - 1, :])

circle_earth = np.zeros([len(u1) - 1, 3])
rho_1 = Quaternion(axis=[0, 1, 0], angle=rho)
x_r = rho_1.rotate([1, 0, 0])
x_earth = orientation_sat.rotate(x_r)
ang_rho = np.arccos(
    (np.matmul(Nadir_Unit, x_earth)) / (la.norm(Nadir_Unit) * la.norm(x_earth)))
print(ang_rho * 180 / np.pi)

for i in range(len(u1) - 1):
    q_rot = Quaternion(axis=Nadir_Unit, angle=u1[i])
    if i == 0:
        circle_earth[i, :] = q_rot.rotate(x_earth)
    else:
        circle_earth[i, :] = q_rot.rotate(circle_earth[i - 1, :])

c_fov_vec_in = orientation_sat.rotate(c_fov_vec)
Nadir_Unit_in = orientation_sat.rotate(Nadir_Unit)
Nadir_R_Sat_in = orientation_sat.rotate(Nadir_R_Sat)

print('Nadir_Inertial', Nadir_R_Sat_in)

pos_sat_u = pos_sat / 100

pos_sen_in = orientation_sat.rotate(a.position)

soa = np.array([[pos_sat_u[0] + pos_sen_in[0], pos_sat_u[1] + pos_sen_in[1], pos_sat_u[2] + pos_sen_in[2],
                 *c_fov_vec_in * 100],
                [*pos_sat_u, *Nadir_Unit_in],
                [*pos_sat_u, *Nadir_R_Sat_in / 100]])

X, Y, Z, U, V, W = zip(*soa)

# Data for Earth globe and satellite unit sphere
s1 = pos_sat[0] / 100 + r_sphere * np.outer(np.cos(u), np.sin(v))
s2 = pos_sat[1] / 100 + r_sphere * np.outer(np.sin(u), np.sin(v))
s3 = pos_sat[2] / 100 + r_sphere * np.outer(np.ones(np.size(u)), np.cos(v))

g1 = r_ell / 100 * np.outer(np.cos(u), np.sin(v))
g2 = r_ell / 100 * np.outer(np.sin(u), np.sin(v))
g3 = r_ell / 100 * np.outer(np.ones(np.size(u)), np.cos(v))

# Plotting arguments for Earth Frame
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111, projection='3d')
ax.grid(False)

# Earth Inertial Cartesian Coordinate System
arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
sc = 50
a = Arrow3D.Arrow3D([0, sc * 1], [0, 0], [0, 0], **arrow_prop_dict, color='r')
ax.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, sc * 1], [0, 0], **arrow_prop_dict, color='b')
ax.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, 0], [0, sc * 1], **arrow_prop_dict, color='g')
ax.add_artist(a)

# Give them a name:
ax.text(0.0, 0.0, sc * -0.1, r'$0$')
ax.text(sc * 1.1, 0, 0, r'$x$')
ax.text(0, sc * 1.1, 0, r'$y$')
ax.text(0, 0, sc * 1.1, r'$z$')

# Satellite Fixed Coordinate System
arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
sc2 = 50

ax_sat_in = orientation_sat.rotate([sc2 * 1, 0, 0]) + pos_sat_u
ay_sat_in = orientation_sat.rotate([0, sc2 * 1, 0]) + pos_sat_u
az_sat_in = orientation_sat.rotate([0, 0, sc2 * 1]) + pos_sat_u

b = Arrow3D.Arrow3D([pos_sat_u[0], ax_sat_in[0]], [pos_sat_u[1], ax_sat_in[1]], [pos_sat_u[2], ax_sat_in[2]],
                    **arrow_prop_dict, color='r')
ax.add_artist(b)
b = Arrow3D.Arrow3D([pos_sat_u[0], ay_sat_in[0]], [pos_sat_u[1], ay_sat_in[1]], [pos_sat_u[2], ay_sat_in[2]],
                    **arrow_prop_dict, color='b')
ax.add_artist(b)
b = Arrow3D.Arrow3D([pos_sat_u[0], az_sat_in[0]], [pos_sat_u[1], az_sat_in[1]], [pos_sat_u[2], az_sat_in[2]],
                    **arrow_prop_dict, color='g')
ax.add_artist(b)

# Give them a name:
a0_lab_sat_in = orientation_sat.rotate([-0.1, 0, 0]) + pos_sat_u
ax_lab_sat_in = orientation_sat.rotate([sc2 * 1.1, 0, 0]) + pos_sat_u
ay_lab_sat_in = orientation_sat.rotate([0, sc2 * 1.1, 0]) + pos_sat_u
az_lab_sat_in = orientation_sat.rotate([0, 0, sc2 * 1.1]) + pos_sat_u

ax.text(*a0_lab_sat_in, r'$0_{sat}$')
ax.text(*ax_lab_sat_in, r'$x_{sat}$')
ax.text(*ay_lab_sat_in, r'$y_{sat}$')
ax.text(*az_lab_sat_in, r'$z_{sat}$')

# Plot of Satellite and Earth Data in Earth Frame
ax.quiver(X, Y, Z, U, V, W)
ax.plot_surface(s1, s2, s3, linewidth=1, rstride=5, cstride=5, color='0.9', alpha=0.4, edgecolor='0.5')
ax.plot_surface(g1, g2, g3, linewidth=1, rstride=5, cstride=5, color='k', alpha=0.1, edgecolor='k')

# ax.plot3D(pos_sat[0] + circle_fov[:, 0], pos_sat[1] + circle_fov[:, 1], pos_sat[2] + circle_fov[:, 2],
#          color='b')
# ax.plot3D(pos_sat[0] + circle_earth[:, 0], pos_sat[1] + circle_earth[:, 1], pos_sat[2] + circle_earth[:, 2],
#          color='r')

# Plot of the Orbit Ellipsoid
df = utilities.get_table_from_database('test.db', 'position')
r_table = df.to_numpy()
r_table = r_table[:, 2:5] / 100
ax.plot3D(r_table[:, 0], r_table[:, 1], r_table[:, 2], linestyle='dotted', color='k')

# Plot of Magnetic Field Calculation (blue) and pyIGRF magnetic field (red)

# Plotting arguments for Earth Frame
fig3 = plt.figure(figsize=(10, 10))
ax3 = fig3.add_subplot(111, projection='3d')
ax3.grid(False)
ax3.plot_surface(g1, g2, g3, linewidth=1, rstride=5, cstride=5, color='k', alpha=0.1, edgecolor='k')
ax3.plot3D(r_table[:, 0], r_table[:, 1], r_table[:, 2], linestyle='dotted', color='k')

# Earth Inertial Cartesian Coordinate System
arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
sc = 50
a = Arrow3D.Arrow3D([0, sc * 1], [0, 0], [0, 0], **arrow_prop_dict, color='r')
ax3.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, sc * 1], [0, 0], **arrow_prop_dict, color='b')
ax3.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, 0], [0, sc * 1], **arrow_prop_dict, color='g')
ax3.add_artist(a)

ax3.text(0.0, 0.0, sc * -0.1, r'$0$')
ax3.text(sc * 1.1, 0, 0, r'$x$')
ax3.text(0, sc * 1.1, 0, r'$y$')
ax3.text(0, 0, sc * 1.1, r'$z$')

dm = utilities.get_table_from_database('test.db', 'magnetic_field_strength')
dmc = utilities.get_table_from_database('test.db', 'FluxMag_X')
dyn = utilities.get_table_from_database('test.db', 'state_sat')

dyn = dyn.to_numpy()
dyn = dyn[:, 2:6]

igrf_table = dm.to_numpy()
igrf_table = igrf_table[:, 2:5] / 1000

igrf_table = igrf_table + r_table

mc_table = dmc.to_numpy()
mc_table = mc_table[:, 2:5] / 1000



print('Check', np.shape(igrf_table)[0] - 1)

for i in range(0, 270000, 10000):
    igrf = Arrow3D.Arrow3D([r_table[i, 0], igrf_table[i, 0]],
                           [r_table[i, 1], igrf_table[i, 1]],
                           [r_table[i, 2], igrf_table[i, 2]],
                           **arrow_prop_dict, color='k')

    igrf_x = Arrow3D.Arrow3D([r_table[i, 0], igrf_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='k')
    igrf_y = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], igrf_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='k')
    igrf_z = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], igrf_table[i, 2]],
                             **arrow_prop_dict, color='k')
    ax3.add_artist(igrf_x)
    ax3.add_artist(igrf_y)
    ax3.add_artist(igrf_z)

    orientation_sat = Quaternion(dyn[int(i / 5), :])
    mc_vec = mc_table[int(i / 5)]
    mc_sat = Quaternion(np.array([1, 0, 0, 0])).rotate(mc_table[int(i / 5)])
    mc_in = orientation_sat.rotate(mc_sat)
    mc_in = mc_in + r_table[i]

    mgn_sen = Arrow3D.Arrow3D([r_table[i, 0], mc_in[0]],
                              [r_table[i, 1], mc_in[1]],
                              [r_table[i, 2], mc_in[2]],
                              **arrow_prop_dict, color='r')
    # ax3.add_artist(mgn_sen)

    mgn_sen_x = Arrow3D.Arrow3D([r_table[i, 0], mc_in[0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='r')
    mgn_sen_y = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], mc_in[1]],
                             [r_table[i, 2], r_table[i, 2]],
                             **arrow_prop_dict, color='r')
    mgn_sen_z = Arrow3D.Arrow3D([r_table[i, 0], r_table[i, 0]],
                             [r_table[i, 1], r_table[i, 1]],
                             [r_table[i, 2], mc_in[2]],
                             **arrow_prop_dict, color='r')
    ax3.add_artist(mgn_sen_x)
    ax3.add_artist(mgn_sen_y)
    ax3.add_artist(mgn_sen_z)

    # Rotated Satellite coordinate system
    sat_coord = False
    if sat_coord is True:
        sc3 = 10
        ax_sat_in = orientation_sat.rotate([sc3 * 1, 0, 0]) + np.transpose(r_table[i])
        ay_sat_in = orientation_sat.rotate([0, sc3 * 1, 0]) + np.transpose(r_table[i])
        az_sat_in = orientation_sat.rotate([0, 0, sc3 * 1]) + np.transpose(r_table[i])

        b = Arrow3D.Arrow3D([r_table[i, 0], ax_sat_in[0]], [r_table[i, 1], ax_sat_in[1]], [r_table[i, 2], ax_sat_in[2]],
                            **arrow_prop_dict, color='r')
        ax3.add_artist(b)
        b = Arrow3D.Arrow3D([r_table[i, 0], ay_sat_in[0]], [r_table[i, 1], ay_sat_in[1]], [r_table[i, 2], ay_sat_in[2]],
                            **arrow_prop_dict, color='b')
        ax3.add_artist(b)
        b = Arrow3D.Arrow3D([r_table[i, 0], az_sat_in[0]], [r_table[i, 1], az_sat_in[1]], [r_table[i, 2], az_sat_in[2]],
                            **arrow_prop_dict, color='g')
        ax3.add_artist(b)

# z scales the limit of the axes
z = 1
ax.set_xlim(z * [-100, 100])
ax.set_ylim(z * [-100, 100])
ax.set_zlim(z * [-100, 100])

"""Plot in Satellite Fixed Frame"""

fig2 = plt.figure(figsize=(10, 10))
ax2 = fig2.add_subplot(111, projection='3d')

# Plot for Unit Sphere

soa2 = np.array([[0.001, 0.001, 0.001, *c_fov_vec], [0, 0, 0, *Nadir_Unit], [0, 0, 0, *Nadir_R_Sat / 1000]])
X, Y, Z, U, V, W = zip(*soa2)

s1 = r_sphere * np.outer(np.cos(u), np.sin(v))
s2 = r_sphere * np.outer(np.sin(u), np.sin(v))
s3 = r_sphere * np.outer(np.ones(np.size(u)), np.cos(v))

ax2.quiver(X, Y, Z, U, V, W)
ax2.plot_surface(s1, s2, s3, linewidth=1, rstride=5, cstride=5, color='0.9', alpha=0.4, edgecolor='0.5')

ax2.plot3D(circle_fov[:, 0], circle_fov[:, 1], circle_fov[:, 2], color='b')
ax2.plot3D(circle_earth[:, 0], circle_earth[:, 1], circle_earth[:, 2], color='r')

z2 = 2
ax2.set_xlim([z2 * -1, z2 * 1])
ax2.set_ylim([z2 * -1, z2 * 1])
ax2.set_zlim([z2 * -1, z2 * 1])

# Cartesian Coordinate System
arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
sc = 2
a = Arrow3D.Arrow3D([0, sc * 1], [0, 0], [0, 0], **arrow_prop_dict, color='r')
ax2.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, sc * 1], [0, 0], **arrow_prop_dict, color='b')
ax2.add_artist(a)
a = Arrow3D.Arrow3D([0, 0], [0, 0], [0, sc * 1], **arrow_prop_dict, color='g')
ax2.add_artist(a)

# Give them a name:
ax2.text(0.0, 0.0, sc * -0.1, r'$0$')
ax2.text(sc * 1.1, 0, 0, r'$x$')
ax2.text(0, sc * 1.1, 0, r'$y$')
ax2.text(0, 0, sc * 1.1, r'$z$')

plt.show()
