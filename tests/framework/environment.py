import json
import numpy as np
import pandas as pd
import sqlite3
import datetime as dt
import time
from uuid import uuid4
from scipy.spatial.transform import Rotation as R
import utilities
from Sensor import Sensor

class Environment(object):
    def __init__(self,simulated_time_start: float,database: str, 
        sampling_rate: float, active: bool = True):

        self.simulated_time_position = simulated_time_start
        self.database: str = database
        self.test_value: float
        self.sampling_rate = sampling_rate
        self.active = active

        # create table for the output of the environment in the database, if it doesn't already exist
        conn_1 = sqlite3.connect(str(self.database))
        c_1 = conn_1.cursor()
        try:
            c_1.execute('create table test_value (real_time, simulated_time, test_value)')
        except:
            print('table test_value already existed')

        conn_1.commit()
        c_1.close()

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        conn_2 = sqlite3.connect(str(self.database))
        c_2 = conn_2.cursor()
        try:
            c_2.execute('create table test_value_parameters (real_time, simulated_time, changed_parameter, '
                                                      'parameter_value)')
        except:
            pass
        conn_2.commit()
        c_2.close()

    def output_test_value(self, simulated_time: float):
        if self.active is True:
            self.simulated_time_position = simulated_time
            # connect to database and read the test_value of the satellite
            conn_9 = sqlite3.connect(str(self.database))
            c_9 = conn_9.cursor()

            try:
                w_x_1 = np.sin(simulated_time/20)
                
                # write environment output into the database
                c_9.executemany('insert into test_value VALUES (?, ?, ?)',
                                [(str(dt.datetime.now(tz=None)), simulated_time, w_x_1)])

                conn_9.commit()
                c_9.close()
                print("environment: " + str(w_x_1))

            except Exception as e:
                print('No values yet for test_value: ' + str(e))
                raise e