var searchData=
[
  ['bandwidth_0',['bandwidth',['../classmagnetometer_1_1_fluxgate_magnetometer.html#a7d3dd58b2fa5135811d76e23cd34a266',1,'magnetometer::FluxgateMagnetometer']]],
  ['bias_5finstability_1',['bias_instability',['../classgyroscope_1_1_m_e_m_s3_axis_gyroscope.html#a481150fc272571b631f30b2f48410f88',1,'gyroscope::MEMS3AxisGyroscope']]],
  ['bold_2',['BOLD',['../classconfig__loader_1_1bcolors.html#a024c0e81b4170716e3d0d1da7ab0bf9d',1,'config_loader.bcolors.BOLD()'],['../classmain_1_1bcolors.html#acffa02a149752d86fe0f3811d6b835a9',1,'main.bcolors.BOLD()']]],
  ['bottom_5flabel_3',['bottom_label',['../classwidgets_1_1_py_qt_graph2d_plot.html#aefacfe42a0acb4dd9bd65258839f3067',1,'widgets::PyQtGraph2dPlot']]]
];
