from sgp4.api import jday, Satrec, WGS84
import numpy as np
import datetime as dt
import utilities

def get_mean_motion(a: float =None):
    """Calculate the mean motion of an orbit from its semi-major axis. This is a simplified formular and might be
    updated. It is needed if one wants to initialize an orbit with classic Kepler elements because gp4 propagator
    requires mean motion.ssssssssssssssssssssssssss

    :param a: (float) semi-major axis of the orbit in km
    :return: (float) the mean motion of the orbit in minutes/radians
    """
    if (a is None):
        print('You have to enter a semi-major axis.')
    else:
        n0 = np.sqrt(398601.3 / a ** 3) * 60
        return n0

def init_orbit(epoch: tuple, a: float, i: float, e: float, argpo: float, RAAN: float, mean_anomaly: float =0.0,
               deg: bool =True, database: str ='test.db'):
    """Initialize a sgp4.api.Satrec orbit by defining Kepler elements. This function might be useless if
    coefficients can be provided as required by Satrec class.

    :param epoch: (tuple) a tuple in the form (yyyy, mm, dd, hh, mm, ss) that represents the epoch the given
                        kepler elements are referring to
    :param a: (float) semi-major axis of the orbit in km
    :param i: (float) inclination of the orbit either in deg or rad -> default is deg
    :param e: (float) eccentricity of the orbit, must be between 0 and 1
    :param argpo: (float) argument of perigee of the orbit either in deg or rad -> default is deg
    :param RAAN: (float) right ascension of ascending node of the orbit either in deg or rad -> default is deg
    :param mean_anomaly: (float) mean anomaly of the satellite in deg or rad -> deg is default
    :param deg: (bool) default is True, set to False if indicated angles are provided in rad
    :param database: (str) name of the database to connect with
    :return: (sgp4.api.Satrec) an sgp4 orbit that contains all attributes and methods indicated in the documentation
            of sgp4.py
    """
    # create table for the output of the orbit propagator in the database, if it doesn't already exist
    utilities.create_table(database, 'position', 'real_time, simulation_time, x, y, z')

    # epoch: days since 1949 December 31 00:00 UT
    jd, fr = jday(*epoch)
    epoch = jd + fr - 2433281.5
    # bstar: drag coefficient (1/earth radii)
    bstar = 2.8098e-05
    # ndot (NOT USED): ballistic coefficient (revs/day)
    ndot = 0.0
    # nddot (NOT USED): mean motion 2nd derivative (revs/day^3)
    nddot = 0.0
    # no_kozai: mean motion (radians/minute)
    no_kozai = get_mean_motion(a)

    # propagator needs radians values so transform deg in rad
    if deg:
        i = i * np.pi / 180.
        argpo = argpo * np.pi / 180.
        RAAN = RAAN * np.pi / 180.
        mean_anomaly = mean_anomaly * np.pi / 180.

    # initialize orbit
    # WGS84 is used because IGRF model also uses it and it is the most advanced one, satellite number could also be
    # input variable of init method
    sat = Satrec()
    sat.sgp4init(WGS84, 'i', 5, epoch, bstar, ndot, nddot, e, argpo, i, mean_anomaly,
                 no_kozai, RAAN)

    # return orbit
    return sat

if __name__ == "__main__":
    sat = init_orbit(epoch=(2019, 1, 1, 18, 30, 0), a=6771., e=0.0, i=51.3, argpo=0., RAAN=24.3, mean_anomaly=0.0)

    freq = 0.5
    s = 0

    while True:
        # epoch at which the position is required
        jd_sgp4, fr = jday(2019, 1, 1, 12, 0, s)
        e, r, v = sat.sgp4(jd_sgp4, fr)
        s += 1/freq

        # write sensor output into the database
        columns_content = (str(dt.datetime.now(tz=None)), jd_sgp4 + fr, r[0], r[1], r[2])
        utilities.write_line_into_database_table('test.db', 'position', columns_content)