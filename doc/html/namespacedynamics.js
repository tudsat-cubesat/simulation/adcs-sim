var namespacedynamics =
[
    [ "Dynamics", "classdynamics_1_1_dynamics.html", "classdynamics_1_1_dynamics" ],
    [ "delta_t", "namespacedynamics.html#a05c4303e190f991d6c4064bc280fa1b1", null ],
    [ "dynamics", "namespacedynamics.html#a04f5df96bc463af8f42a206f5bbec1b0", null ],
    [ "euler_init", "namespacedynamics.html#a699ef2e10342fa3093767c8aaa1f22e1", null ],
    [ "freq", "namespacedynamics.html#addcd2690b8e1895c0a9e32d99ca3b37f", null ],
    [ "I_xx", "namespacedynamics.html#a531f5e21ad26aa5e34d8540ea547574b", null ],
    [ "I_yy", "namespacedynamics.html#ac982197dac5e8787c0a7e54187fc005b", null ],
    [ "I_zz", "namespacedynamics.html#a3112f267b01a76c4cde843d65519fe39", null ],
    [ "level", "namespacedynamics.html#a27f0ccb48966de89950481c9fb376eac", null ],
    [ "mu", "namespacedynamics.html#a92c52a7ded9d56b5126bc8fd92cf5c1d", null ],
    [ "n_orbits", "namespacedynamics.html#a2119438381e64806742211c8eb4ded15", null ],
    [ "r_earth", "namespacedynamics.html#aa01bbf12f7a07e5e529dffe10581b913", null ],
    [ "torque_initial", "namespacedynamics.html#a0a7c49364360609636ecf400d8e393ca", null ],
    [ "w_initial", "namespacedynamics.html#a45b1072784cacc71dc1f5500357c8fbd", null ]
];