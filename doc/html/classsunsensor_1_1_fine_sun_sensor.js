var classsunsensor_1_1_fine_sun_sensor =
[
    [ "__init__", "classsunsensor_1_1_fine_sun_sensor.html#a92cf4079fa6994c22dedb86e7b72939c", null ],
    [ "get_fine_current_noise", "classsunsensor_1_1_fine_sun_sensor.html#a68cd494eda7ab7bd6019bc13b41c28a3", null ],
    [ "measure_fine_sun_sensor_angles", "classsunsensor_1_1_fine_sun_sensor.html#aa235d558aed8e938a5628561b8c2e1e7", null ],
    [ "output", "classsunsensor_1_1_fine_sun_sensor.html#a17fe02dfaaa13ff8032a8b685b215592", null ],
    [ "run", "classsunsensor_1_1_fine_sun_sensor.html#abf373e04e327e45fda75325dfb1832bd", null ],
    [ "accuracy", "classsunsensor_1_1_fine_sun_sensor.html#aa5cdf5b8ff483170193dbc62818296a9", null ],
    [ "active", "classsunsensor_1_1_fine_sun_sensor.html#af43bf2dc117ed4db49ec3e8a1da9d75b", null ],
    [ "column_names", "classsunsensor_1_1_fine_sun_sensor.html#af65e95360d3a44ffb5f9ad5019abfb54", null ],
    [ "database", "classsunsensor_1_1_fine_sun_sensor.html#a17852906d3e2e4dee21653b3a290aa36", null ],
    [ "digital", "classsunsensor_1_1_fine_sun_sensor.html#a0deebac6e8f171bc7ba7e75f791b89e9", null ],
    [ "fov", "classsunsensor_1_1_fine_sun_sensor.html#a62694928d2fbbf7b1a449e6308e88dbe", null ],
    [ "max_current", "classsunsensor_1_1_fine_sun_sensor.html#af6798357d9b2e95c9bdbf7877c2130cb", null ],
    [ "mode", "classsunsensor_1_1_fine_sun_sensor.html#a65efa3a4ca173cea66ffd42229677669", null ],
    [ "precision", "classsunsensor_1_1_fine_sun_sensor.html#a1191aac5ac594ff4957212c57b8893aa", null ],
    [ "sampling_rate", "classsunsensor_1_1_fine_sun_sensor.html#ad0b1050697d4bdcced96dd89fc55c809", null ]
];