import sqlite3
import numpy
import time
import pandas as pd

# create sqlite
conn = sqlite3.connect('update_database.db')
c = conn.cursor( )
# create table, if it doesn't already exists
try:
    c.execute('create table written_data (a, b, c, d)')

except:
    pass

conn.commit()
c.close()

conn = sqlite3.connect('sqdatabase.db')
cur = conn.cursor()
cur.executemany('INSERT INTO written_data VALUES(?,?,?,?);', [(1, 2, 3, 4)])
conn.commit()
conn.close()
start = time.time()
conn = sqlite3.connect('sqdatabase.db')
cur = conn.cursor()
sqlite_update_query = """Update written_data set b = ?, c = ?, d = ? where a = ?"""
columnValues = (3, 4, 5, 1)
cur.execute(sqlite_update_query, columnValues)
conn.commit()
conn.close()
end = time.time()
print(end-start)
conn = sqlite3.connect('sqdatabase.db')
c = conn.cursor()
# read last line of Table from database into a pandas dataframe
last_row = pd.read_sql_query('SELECT * FROM written_data ORDER BY rowid DESC LIMIT 1;', conn)
conn.commit()
c.close()
print(last_row)