import numpy as np
from numpy import linalg as la
import math
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.spatial.transform import Rotation as R
from pyquaternion import Quaternion
import sympy as sym
import sqlite3
from Sensor import Sensor
import utilities
import datetime as dt
import json
import time


# TODO second EHS+Output Nadir Angle -> Nadir Estimation , overlapping Area, Structure,
# Pitch and Roll d.h. rotation around x,y axis, r-vector and orientantion_sat from database
# adapting code to higher fov

class EarthSensor(Sensor):

    def __init__(self, name: str, position: np.ndarray, orientation: np.ndarray, database: str, digital: bool, mode,
                 FOV: float, accuracy: np.array, sampling_rate: float, active: bool = True):

        super().__init__(name, position, orientation, database, digital, mode, active)

        self.FOV = FOV * np.pi / 180
        self.acc = accuracy
        self.sampling_rate = sampling_rate

        # create table for the output of the sensor in the database, if it doesn't already exist
        utilities.create_table(self.database, self.name, 'real_time, simulation_time, Real_Angle, Est_Angle')

        # create table for the parameters of the sensor in the database, if it doesn't already exist
        utilities.create_table(self.database, self.name + '_parameters',
                               'real_time, simulation_time, changed_parameter,'
                               'parametervalue')

        # create list of all initialisation parameters
        parameter = ["name", "position", "orientation", "database", "digital", "mode", "active",
                     "accuracy", "field_of_view", "sampling_rate"]
        parameter_value = [self.name, self.position.tolist(), self.orientation.tolist(), self.database,
                           self.digital, self.mode, self.active,
                           self.acc.tolist(), self.FOV, self.sampling_rate]

        # modify list of all initialisation parameters for database
        columns_content = (str(dt.datetime.now(tz=None)), 0, json.dumps(parameter), json.dumps(parameter_value))
        # write all initialisation parameters into the database
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def get_orientation_sat(self):
        # Read Orientation of Satellite from Database
        df = utilities.get_last_line_from_database_table('test.db', "state_sat", 1)
        orientation_sat = df.to_numpy().reshape(9)
        orientation_sat = Quaternion(orientation_sat[2:6])
        return orientation_sat

    def set_field_of_view(self, FOV: int):
        """Sets the field of view of the Sensor as an Integer in Degrees
        """
        self.FOV = FOV * np.pi / 180
        # write the new parameters into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, "FOV", json.dumps(self.FOV))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def set_accuracy(self, acc: np.array):
        """Sets the accuracy of the Sensor as an Integer in Degrees
        """
        self.acc = acc
        # write the new parameters into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, "accuracy", json.dumps(self.FOV))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    def set_sampling_rate(self, sampling_rate: int):
        """Sets the sampling rate of the sensor. The sampling rate of the sensor is an Integer in Hz
            """
        self.sampling_rate = sampling_rate
        # write the new parameters into the database
        columns_content = (str(dt.datetime.now(tz=None)), 0, "sampling_rate", json.dumps(self.sampling_rate))
        utilities.write_line_into_database_table(self.database, self.name + '_parameters', columns_content)

    # EarthtoSat is 3x1 np.array in TEME
    # Function returns the Vector SattoEarth in Sat Coordinates
    def NadirVector(self, pos_sat):

        orientation_sat = self.get_orientation_sat()

        Nadir_R_I = -pos_sat
        # r = R.from_quat(self.Ori_Sat)
        # Nadir_R_Sat = r.apply(Nadir_R_I)

        Nadir_R_Sat = orientation_sat.rotate(Nadir_R_I)

        print('Nadir_sat', Nadir_R_Sat)
        return Nadir_R_Sat

    # Calculation of Noise Angles in x, y ,z axis
    def NoiseCalc(self):
        sigma_rad = self.acc[0] * math.pi / 180 / self.acc[1]
        NoiseRMS = np.sqrt(sigma_rad ** 2 / 3)
        print('Noise', NoiseRMS * 180 / np.pi, self.acc[0], sigma_rad)
        return np.array([np.random.normal(loc=0, scale=NoiseRMS),
                         np.random.normal(loc=0, scale=NoiseRMS),
                         np.random.normal(loc=0, scale=NoiseRMS)])

    def EarthinFOV(self, r_ell, r_sphere, pos_sat):

        orientation_sat = self.get_orientation_sat()

        # Center of Earth Disk is defined by the Nadir Vector
        global c_fov_vec
        Nadir_R_Sat = self.NadirVector(pos_sat)

        # Intersection of Nadir with the Unit Sphere
        Nadir_Unit = r_sphere * Nadir_R_Sat / la.norm(Nadir_R_Sat)

        print('!!!!!!!!!!!!', Nadir_R_Sat, Nadir_Unit)

        # Calculation of the Earth Disk Radius r_disk
        # Earth is simplified as an ideal globe
        rho = np.arcsin(r_ell / (la.norm(pos_sat)))
        print('rho', rho * 180 / np.pi)

        lam = np.arccos(r_ell / (la.norm(pos_sat)))
        d = np.tan(lam) * r_ell
        r_disk = np.sin(rho) * d
        r_disk_proj = np.sin(rho) * r_sphere
        print('Earth Disk', r_disk, 'Earth Disk projection', r_disk_proj, rho * 180 / np.pi, lam * 180 / np.pi)

        # Boresight vector in Sat Coordinates
        tr_sat_to_sen = Quaternion(self.orientation)
        tr_sen_to_sat = tr_sat_to_sen.inverse
        v_b = tr_sen_to_sat.rotate([1, 0, 0])

        # Intersection between Boresight of the Sensor and the Unit Sphere r_sphere
        # Necessary if sensor is not mounted in the center of the satellite
        c1, c2, c3, x = sym.symbols('c1,c2,c3,x')
        c_fov_mat = np.array(sym.solve(
            [self.position[0] + x * v_b[0] - c1,
             self.position[1] + x * v_b[1] - c2,
             self.position[2] + x * v_b[2] - c3,
             c1 ** 2 + c2 ** 2 + c3 ** 2 - r_sphere ** 2],
            (c1, c2, c3, x)))
        for i in range((c_fov_mat.shape[0])):
            if c_fov_mat[i, 3] > 0:
                # c_fov_vec describes the Intersection of the Sensor's Boresight with the Unit Sphere
                c_fov_vec = np.array(c_fov_mat[i, 0:3])
                c_fov_vec = c_fov_vec.astype(float)
                abs_c = la.norm(c_fov_vec)
                print('length of Sensors Intersection Vector', abs_c)

        ang_bn = np.arccos((np.matmul(Nadir_R_Sat, v_b)) / (la.norm(Nadir_R_Sat) * la.norm(v_b)))

        if ang_bn <= rho - self.FOV / 2:
            print('earth')
        elif ang_bn >= rho + self.FOV / 2:
            print('space')
        else:
            print('Horizon in FOV')

        # Comparison of the EarthDisk Area Vector and the FOV Area Vector
        # to determine the area recognized by the Sensor
        # Assumption: Circle of Sensor's FOV is smaller than Earth Disk

        # d_be describes distance between the Boresight's intersection point and Center of projected Earth Disk
        d_be = la.norm(c_fov_vec - Nadir_Unit)

        # Threshold Value for the case that the Sensor only recognizes the space
        r_fov = r_sphere * np.sin(self.FOV / 2)
        d_sp = r_fov + r_disk_proj
        # Threshold Value for the case that the Sensor only recognizes the Earth
        d_ea = r_disk_proj - r_fov

        print('Space', d_sp, 'Earth', d_ea, 'd_be', d_be, 'FOV', r_fov)

        """Plot of the Sensors FOV in the unit sphere and the Earth Globe"""

        orientation_sat_earth = orientation_sat.inverse
        u = np.linspace(0, 2 * np.pi, 100)
        u1 = np.linspace(0, 2 * np.pi, 1001)
        v = np.linspace(0, np.pi, 100)
        circle_fov = np.zeros([len(u1) - 1, 3])

        q_1 = Quaternion(axis=[0, 1, 0], angle=self.FOV / 2)
        q_2 = tr_sen_to_sat
        x = q_1.rotate([1, 0, 0])
        x_sat = q_2.rotate(x)

        for i in range(len(u1) - 1):
            q_rot = Quaternion(axis=c_fov_vec, angle=u1[i])
            if i == 0:
                circle_fov[i, :] = q_rot.rotate(x_sat)
            else:
                circle_fov[i, :] = q_rot.rotate(circle_fov[i - 1, :])

        circle_earth = np.zeros([len(u1) - 1, 3])
        rho_1 = Quaternion(axis=[0, 1, 0], angle=rho / 2)
        x_r = rho_1.rotate([-1, 0, 0])
        x_earth = orientation_sat.rotate(x_r)
        ang_rho = np.arccos(
            (np.matmul(Nadir_Unit, x_earth)) / (la.norm(Nadir_Unit) * la.norm(x_earth)))

        for i in range(len(u1) - 1):
            q_rot = Quaternion(axis=Nadir_Unit, angle=u1[i])
            if i == 0:
                circle_earth[i, :] = q_rot.rotate(x_earth)
            else:
                circle_earth[i, :] = q_rot.rotate(circle_earth[i - 1, :])

        c_fov_vec_in = orientation_sat_earth.rotate(c_fov_vec)
        Nadir_Unit_in = orientation_sat_earth.rotate(Nadir_Unit)
        Nadir_R_Sat_in = orientation_sat_earth.rotate(Nadir_R_Sat)

        print('Nadir_Inertial', Nadir_R_Sat_in)
        pos_sat_u = pos_sat / 100

        soa = np.array([[pos_sat_u[0] + 0.001, pos_sat_u[1] + 0.001, pos_sat_u[2] + 0.001, *c_fov_vec_in*100],
                        [*pos_sat_u, *Nadir_Unit_in], [*pos_sat_u, *Nadir_R_Sat_in / 10]])
        theta = np.linspace(0, 2 * np.pi, 201)

        X, Y, Z, U, V, W = zip(*soa)

        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, projection='3d')

        s1 = pos_sat[0] / 100 + r_sphere * np.outer(np.cos(u), np.sin(v))
        s2 = pos_sat[1] / 100 + r_sphere * np.outer(np.sin(u), np.sin(v))
        s3 = pos_sat[2] / 100 + r_sphere * np.outer(np.ones(np.size(u)), np.cos(v))

        g1 = r_ell / 100 * np.outer(np.cos(u), np.sin(v))
        g2 = r_ell / 100 * np.outer(np.sin(u), np.sin(v))
        g3 = r_ell / 100 * np.outer(np.ones(np.size(u)), np.cos(v))

        ax.quiver(X, Y, Z, U, V, W)
        ax.plot_surface(s1, s2, s3, linewidth=1, rstride=5, cstride=5, color='0.9', alpha=0.4, edgecolor='0.5')
        ax.plot_surface(g1, g2, g3, linewidth=1, rstride=5, cstride=5, color='k', alpha=0.1, edgecolor='k')

        ax.plot3D(pos_sat[0] + circle_fov[:, 0], pos_sat[1] + circle_fov[:, 1], pos_sat[2] + circle_fov[:, 2],
                  color='b')
        ax.plot3D(pos_sat[0] + circle_earth[:, 0], pos_sat[1] + circle_earth[:, 1], pos_sat[2] + circle_earth[:, 2],
                  color='r')
        z = 1
        ax.set_xlim(z * [-100, 100])
        ax.set_ylim(z * [-100, 100])
        ax.set_zlim(z * [-100, 100])

        fig2 = plt.figure(figsize=(10, 10))
        ax2 = fig2.add_subplot(111, projection='3d')

        # Plot for Unit Sphere
        soa2 = np.array([[0.001, 0.001, 0.001, *c_fov_vec], [0, 0, 0, *Nadir_Unit], [0, 0, 0, *Nadir_R_Sat / 1000]])
        X, Y, Z, U, V, W = zip(*soa2)

        s1 = r_sphere * np.outer(np.cos(u), np.sin(v))
        s2 = r_sphere * np.outer(np.sin(u), np.sin(v))
        s3 = r_sphere * np.outer(np.ones(np.size(u)), np.cos(v))

        ax2.quiver(X, Y, Z, U, V, W)
        ax2.plot_surface(s1, s2, s3, linewidth=1, rstride=5, cstride=5, color='0.9', alpha=0.4, edgecolor='0.5')

        ax2.plot3D(circle_fov[:, 0], circle_fov[:, 1], circle_fov[:, 2], color='b')
        ax2.plot3D(circle_earth[:, 0], circle_earth[:, 1], circle_earth[:, 2], color='r')

        z2 = 2
        ax2.set_xlim([z2 * -1, z2 * 1])
        ax2.set_ylim([z2 * -1, z2 * 1])
        ax2.set_zlim([z2 * -1, z2 * 1])
        plt.show()

        # Checking if the Circle of the FOV on the Unit Sphere and the Earth Disk are overlapping
        if d_be >= d_sp:
            print('Earth is not recognized by Sensors FOV because Sensor is mounted towards space')
            return False
        elif d_be <= d_ea:
            print('Only Earth is recognized by Sensors FOV')
            return False
        else:
            print('Horizon is recognized')
            return True

    def measureNadirVector(self, pos_sat):
        if self.EarthinFOV(6378.136 * 1000, 1000) is True and \
                self.EarthinFOV(6378.136 * 1000, 1000):

            while self.active is True:
                Nadir_R_Sat = self.NadirVector(pos_sat)

                NoiseAng = self.NoiseCalc()

                nx = Quaternion(axis=[1, 0, 0], angle=NoiseAng[0])
                ny = Quaternion(axis=[0, 1, 0], angle=NoiseAng[1])
                nz = Quaternion(axis=[0, 0, 1], angle=NoiseAng[2])

                n = nx * ny * nz
                print(NoiseAng)
                Nadir_Est_Sat = n.rotate(Nadir_R_Sat)
                angle = np.arccos(
                    (np.matmul(Nadir_R_Sat, Nadir_Est_Sat)) / (la.norm(Nadir_R_Sat) * la.norm(Nadir_Est_Sat)))
                print('Angle', angle * 180 / np.pi)
                print(Nadir_R_Sat)
                print(Nadir_Est_Sat)

    def measureNadirAngle(self, pos_sat, r_ell):
        """ The function measures the angle between the sensors boresight and the nadir angle if the Earth Ellipsoid
        is in the field of view of the sensor.
        With this type it needs two orthogonally mounted sensors two compute a nadir vector estimation"""

        orientation_sat = self.get_orientation_sat()
        Nadir_R_Sat = self.NadirVector(pos_sat)
        sigma_rad = self.acc[0] * math.pi / 180 / self.acc[1]

        # Angle ang_bn between Boresight Vector and the Nadir Vector
        tr_sat_to_sen = Quaternion(self.orientation)
        tr_sen_to_sat = tr_sat_to_sen.inverse
        v_b = tr_sen_to_sat.rotate([1, 0, 0])

        ang_bn = np.arccos((np.matmul(Nadir_R_Sat, v_b)) / (la.norm(Nadir_R_Sat) * la.norm(v_b))) * 180 / np.pi

        if self.EarthinFOV(r_ell, 1, pos_sat) is True:
            ang_bn_est = np.random.normal(loc=ang_bn, scale=sigma_rad)
            print('Real angle', ang_bn, 'Estimated angle', ang_bn_est)
        else:
            ang_bn_est = 'No Data obtained'

        return ang_bn, ang_bn_est

    def output(self, sim_time: float):

        # Read Position of Satellite from Database
        df = utilities.interpolate_two_rows(self.database, name='position', sim_time=sim_time)
        pos_sat = df.to_numpy()[0][1:].reshape(3)
        print('Position_sat', pos_sat)

        # Read Earth Disk Radius from Database
        df = utilities.interpolate_two_rows(self.database, name='radius_ellipsoid', sim_time=sim_time)
        r_ell = df.to_numpy().reshape(2)
        r_ell = r_ell[1] / 1000
        print('Earth Disk Ellipsoid', r_ell)

        # Get output
        ang_real, ang_est = self.measureNadirAngle(pos_sat, r_ell)

        # write sensor output into the database
        columns_content = (str(dt.datetime.now(tz=None)), sim_time, ang_real, ang_est)
        utilities.write_line_into_database_table(self.database, self.name, columns_content)

        return ang_real, ang_est

    def run(self):
        """ Starts sensor in while Loop."""

        while self.active is True:
            # timestamp to simulate sampling rate
            current_time_stamp = time.time()

            # run sensor one time
            self.output(0.1)
            # simulate the sampling time of the sensor
            if time.time() - current_time_stamp < 1 / self.sampling_rate:
                time.sleep(1 / self.sampling_rate - (time.time() - current_time_stamp))
            else:
                print("script to slow for the set sampling rate of the " + self.name)


if __name__ == "__main__":
    import threading

    h_ec = np.sqrt(((6378.136 + 500) * 1000) ** 2 / 3)

    ori = R.from_euler('zxz', np.array([180, 0, 120]), degrees=True)
    ori = np.array(ori.as_quat())
    a = EarthSensor(name='EHS', position=np.array([0.001, 0.001, 0.001]), orientation=np.array([ori[3], ori[0], ori[1], ori[2]]),
                    database='test.db',
                    digital=True, mode=1, FOV=60, accuracy=np.array([2, 1]), sampling_rate=1., active=True)
    a.output(0.1)
