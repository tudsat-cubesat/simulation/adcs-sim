import logging

import numpy as np
import pandas as pd
from pyquaternion import Quaternion
import sqlite3
import datetime as dt
from astropy import coordinates as coord, units as u
from astropy.time import Time
from scipy.spatial.transform import Rotation as R
import logging


# import time

# class bcolors:
#     HEADER = '\033[95m'
#     OKBLUE = '\033[94m'
#     OKCYAN = '\033[96m'
#     OKGREEN = '\033[92m'
#     WARNING = '\033[93m'
#     FAIL = '\033[91m'
#     ENDC = '\033[0m'
#     BOLD = '\033[1m'
#     UNDERLINE = '\033[4m'


def apply_sensitivity(value: float, sensitivity: float):
    """Simulates the sensitivity of the sensor.

    :param value: (double) sensor output without sensitivity
    :param sensitivity: (double) sensitivity of the sensor
    :return: (double) sensor output with sensitivity
    """
    return sensitivity * round(value / sensitivity)


def get_acceleration_frame_transformation(position: np.ndarray, orientation: np.ndarray, angular_velocity: np.ndarray,
                                          angular_acceleration: np.ndarray, velocity: np.ndarray = [0, 0, 0],
                                          acceleration: np.ndarray = [0, 0, 0]) -> np.ndarray:
    """Transforms the acceleration into the sensor frame.

    Velocity and acceleration are differentiations of the position vector of the sensor. Velocity and acceleration of
    the satellite aren't considered.
    :param position: (numpy array, 1x3 x y z) position of the sensor in the satellite frame
    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the
                                                                                                        satellite frame
    :param angular_velocity: (numpy array, 1x3 omega_x omega_y omega_z) angular velocity of the satellite in the
                                satellite coordinate frame
    :param angular_acceleration: (numpy array, 1x3 alpha_x alpha_y alpha_z) angular acceleration of the satellite in the
                                satellite coordinate frame
    :param velocity: (numpy array, 1x3 v_x v_y v_z) velocity of the satellite in the satellite frame
    :param acceleration: (numpy array, 1x3 a_x a_y a_z) acceleration of the satellite in the satellite frame
    :return: (numpy array, 1x3 a_x a_y a_z) acceleration of the sensor in the sensor frame
    """
    # _T = transposed, _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # basic principle of this function is the equation a = R_T_dot_dot * p + 2 * R_T_dot * p_dot + R_T * p_dot_dot
    # p is a point in the space and R_T is the transposed rotation matrix
    q_r = orientation[0]
    q_i = orientation[1]
    q_j = orientation[2]
    q_k = orientation[3]

    # split up the angular velocity
    omega_x = angular_velocity[0]
    omega_y = angular_velocity[1]
    omega_z = angular_velocity[2]
    # split up the angular acceleration
    alpha_x = angular_acceleration[0]
    alpha_y = angular_acceleration[1]
    alpha_z = angular_acceleration[2]
    # differentiated quaternions
    q_r_dot = -1 / 2 * (omega_x * q_i + omega_y * q_j + omega_z * q_k)
    q_i_dot = 1 / 2 * (omega_x * q_r + omega_y * q_k - omega_z * q_j)
    q_j_dot = 1 / 2 * (omega_y * q_r + omega_z * q_i - omega_x * q_k)
    q_k_dot = 1 / 2 * (omega_z * q_r + omega_x * q_j - omega_y * q_i)

    # differentiated quaternions
    q_r_dot_dot = -1 / 2 * (alpha_x * q_i + omega_x * q_i_dot + alpha_y * q_j + omega_y * q_j_dot +
                            alpha_z * q_k + omega_z * q_k_dot)
    q_i_dot_dot = 1 / 2 * (alpha_x * q_r + omega_x * q_r_dot + alpha_y * q_k + omega_y * q_k_dot -
                           alpha_z * q_j - omega_z * q_j_dot)
    q_j_dot_dot = 1 / 2 * (alpha_y * q_r + omega_y * q_r_dot + alpha_z * q_i + omega_z * q_i_dot -
                           alpha_x * q_k - omega_x * q_k_dot)
    q_k_dot_dot = 1 / 2 * (alpha_z * q_r + omega_z * q_r_dot + alpha_x * q_j + omega_x * q_j_dot -
                           alpha_y * q_i - omega_y * q_i_dot)

    # transposed rotation matrix
    R_T = np.array(
        [[1 - 2 * (q_j ** 2 + q_k ** 2), 2 * (q_i * q_j + q_k * q_r), 2 * (q_i * q_k - q_j * q_r)],
         [2 * (q_i * q_j - q_k * q_r), 1 - 2 * (q_i ** 2 + q_k ** 2), 2 * (q_j * q_k + q_i * q_r)],
         [2 * (q_i * q_k + q_j * q_r), 2 * (q_j * q_k - q_i * q_r), 1 - 2 * (q_i ** 2 + q_j ** 2)]])

    # differentiated transposed rotation matrix
    R_T_dot = np.array(
        [[-4 * (q_j * q_j_dot + q_k * q_k_dot), 2 * (q_i_dot * q_j + q_i * q_j_dot + q_k_dot * q_r + q_k * q_r_dot),
          2 * (q_i_dot * q_k + q_i * q_k_dot - q_j_dot * q_r - q_j * q_r_dot)],
         [2 * (q_i_dot * q_j + q_i * q_j_dot - q_k_dot * q_r - q_k * q_r_dot), -4 * (q_i_dot * q_i + q_k * q_k_dot),
          2 * (q_j_dot * q_k + q_j * q_k_dot + q_i_dot * q_r + q_i * q_r_dot)],
         [2 * (q_i_dot * q_k + q_i * q_k_dot + q_j_dot * q_r + q_j * q_r_dot),
          2 * (q_j_dot * q_k + q_j * q_k_dot - q_i_dot * q_r - q_i * q_r_dot), -4 * (q_i_dot * q_i + q_j_dot * q_j)]])

    # differentiated transposed rotation matrix
    R_T_dot_dot = np.array([[-4 * (q_j_dot ** 2 + q_j * q_j_dot_dot + q_k_dot ** 2 + q_k * q_k_dot_dot),
                             2 * (
                                     q_i_dot_dot * q_j + 2 * q_i_dot * q_j_dot + q_i * q_j_dot_dot + q_k_dot_dot * q_r + 2 * q_k_dot * q_r_dot + q_k * q_r_dot_dot),
                             2 * (
                                     q_i_dot_dot * q_k + 2 * q_i_dot * q_k_dot + q_i * q_k_dot_dot - q_j_dot_dot * q_r - 2 * q_j_dot * q_r_dot - q_j * q_r_dot_dot)],
                            [2 * (
                                    q_i_dot_dot * q_j + 2 * q_i_dot * q_j_dot + q_i * q_j_dot_dot - q_k_dot_dot * q_r - 2 * q_i_dot * q_r_dot - q_k * q_r_dot_dot),
                             -4 * (q_i_dot_dot * q_i + q_i_dot ** 2 + q_k_dot ** 2 + q_k * q_k_dot_dot),
                             2 * (
                                     q_j_dot_dot * q_k + 2 * q_j_dot * q_k_dot + q_j * q_k_dot_dot + q_i_dot_dot * q_r + 2 * q_i_dot * q_r_dot + q_i * q_r_dot_dot)],
                            [2 * (
                                    q_i_dot_dot * q_k + q_j * q_k_dot_dot + 2 * q_i_dot * q_k_dot + q_j_dot_dot * q_r + 2 * q_j_dot * q_r_dot + q_j * q_r_dot_dot),
                             2 * (
                                     q_j_dot_dot * q_k + 2 * q_j_dot * q_k_dot + q_j * q_k_dot_dot - q_i_dot_dot * q_r - 2 * q_i_dot * q_r_dot - q_i * q_r_dot_dot),
                             -4 * (q_i_dot_dot * q_i + q_i_dot ** 2 + q_j_dot ** 2 + q_j_dot_dot * q_j)]])

    # calculate acceleration in the sensor frame
    acceleration_in_sensor_frame = R_T_dot_dot.dot(position) + 2 * R_T_dot.dot(velocity) + R_T.dot(acceleration)
    return acceleration_in_sensor_frame


def calc_angular_motion_parameters(database: str):
    """Converts the angular position of the last few time steps into an angular velocity and acceleration

        :param sim_time: (float) simulation time
        """
    # connect to database and read the orientation of the satellites last three steps
    # df = get_last_line_from_database_table(database, 'state_sat', rows=3)
    df = pd.read_json('json_communication/state_sat.json', orient='records',
                      lines=True)

    # convert the orientation of the last three time steps into euler angles, if statements in case of the first two
    # written lines don't exist
    orientations = df[['q_i', 'q_j', 'q_k', 'q_r']].to_numpy(dtype='float')
    omega = df[['w_x', 'w_y', 'w_z']].to_numpy(dtype='float')
    time_stamps = df[['simulation_time']].to_numpy(dtype='float')
    num_rows = np.shape(orientations)[0]
    if num_rows > 2:
        omega_x = omega[-1, 0]
        omega_y = omega[-1, 1]
        omega_z = omega[-1, 2]

        alpha_x = (omega_x - omega[-2, 0]) / (time_stamps[2][0] - time_stamps[1][0])
        alpha_y = (omega_y - omega[-2, 1]) / (time_stamps[2][0] - time_stamps[1][0])
        alpha_z = (omega_z - omega[-2, 2]) / (time_stamps[2][0] - time_stamps[1][0])

    elif num_rows > 1:
        omega_x = omega[-1, 0]
        omega_y = omega[-1, 1]
        omega_z = omega[-1, 2]
        alpha_x, alpha_y, alpha_z = 0, 0, 0

    else:
        omega_x, omega_y, omega_z, alpha_x, alpha_y, alpha_z = 0, 0, 0, 0, 0, 0

    # write angular components into database

    return omega_x, omega_y, omega_z, alpha_x, alpha_y, alpha_z


def get_cardan_angular_velocity_transformation(beta: float, gamma: float, W):
    """Transforms the angular velocity of the satellite frame into the sensor frame by using euler coordinates.

    :param beta: (double) rotation of y-axis of the sensor in euler coordinates
    :param gamma: (double) rotation of z-axis of the sensor in euler coordinates
    :param W: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate frame
    :return: (numpy array, 1x3 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # matrix that transforms the angular velocity of the satellite frame
    # into the angular velocity of the sensor frame
    H_1 = np.array([[np.cos(beta) * np.cos(gamma), np.sin(gamma), 0],
                    [-np.cos(beta) * np.sin(gamma), np.cos(gamma), 0],
                    [np.sin(beta), 0, 1]])
    # calculate the angular velocity of the sensor
    W_out = H_1.dot(W)

    return W_out


def get_quaternion_angular_velocity_transformation_non_unit_quaternions(orientation: np.ndarray,
                                                                        angular_velocity: np.ndarray,
                                                                        s=1) -> np.ndarray:
    """Transforms the angular velocity of the satellite frame into the sensor frame by using quaternions.

    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the satellite frame
    :param angular_velocity: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate frame
    :param s: (double) s = ||q||**-2 if q is a unit quaternion then s = 1
    :return: (numpy array, 1x3 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # _T = transposed, _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # basic principle of this function is the equation W_tilde = R_T * R_dot
    # with R_T is the transposed rotation matrix and R_dot is the differentiated rotation matrix
    #                _                             _
    #               |  0        -omega_z     omega_y|
    # and W_tilde = |  omega_z   0          -omega_x| with the angular velocity omega in the sensor frame
    #               |_-omega_y   omega_x     0     _|
    # ===============================================================================================
    # split up the quaternion in it's parts
    q_r = orientation[0]
    q_i = orientation[1]
    q_j = orientation[2]
    q_k = orientation[3]

    # split up the angular velocity
    W_x = angular_velocity[0]
    W_y = angular_velocity[1]
    W_z = angular_velocity[2]

    # differentiated quaternions
    q_r_dot = -1 / 2 * (W_x * q_i + W_y * q_j + W_z * q_k)
    q_i_dot = 1 / 2 * (W_x * q_r + W_y * q_k - W_z * q_j)
    q_j_dot = 1 / 2 * (W_y * q_r + W_z * q_i - W_x * q_k)
    q_k_dot = 1 / 2 * (W_z * q_r + W_x * q_j - W_y * q_i)

    # transposed rotation matrix
    R_T = np.array(
        [[1 - 2 * s * (q_j ** 2 + q_k ** 2), 2 * s * (q_i * q_j + q_k * q_r), 2 * s * (q_i * q_k - q_j * q_r)],
         [2 * s * (q_i * q_j - q_k * q_r), 1 - 2 * s * (q_i ** 2 + q_k ** 2), 2 * s * (q_j * q_k + q_i * q_r)],
         [2 * s * (q_i * q_k + q_j * q_r), 2 * s * (q_j * q_k - q_i * q_r), 1 - 2 * s * (q_i ** 2 + q_j ** 2)]])

    # differentiated rotation matrix
    R_dot = np.array([[-4 * s * (q_j * q_j_dot + q_k * q_k_dot),
                       2 * s * (q_i_dot * q_j + q_i * q_j_dot - q_k * q_r_dot - q_k_dot * q_r),
                       2 * s * (q_i_dot * q_k + q_i * q_k_dot + q_j_dot * q_r + q_j * q_r_dot)],
                      [2 * s * (q_i_dot * q_j + q_i * q_j_dot + q_k * q_r_dot + q_k_dot * q_r),
                       -4 * s * (q_i * q_i_dot + q_k * q_k_dot),
                       2 * s * (q_j_dot * q_k + q_j * q_k_dot - q_i_dot * q_r - q_i * q_r_dot)],
                      [2 * s * (q_i_dot * q_k + q_i * q_k_dot - q_j_dot * q_r - q_j * q_r_dot),
                       2 * s * (q_j_dot * q_k + q_j * q_k_dot + q_i_dot * q_r + q_i * q_r_dot),
                       -4 * s * (q_i * q_i_dot + q_j * q_j_dot)]])

    # matrix of the angular velocity in the sensor frame
    W_tilde = R_T.dot(R_dot)
    # vector of the angular velocity in the sensor frame
    W_out = np.array([W_tilde[2][1], W_tilde[0][2], W_tilde[1][0]])

    return W_out


def get_quaternion_angular_velocity_transformation(orientation: np.ndarray, angular_velocity: np.ndarray) -> np.ndarray:
    """Transforms the angular velocity of the satellite frame into the sensor frame by using quaternions.

    :param orientation: (numpy array, 1x4 qi qj qk qr) quaternion that describes the rotation of the sensor in the
                        satellite frame
    :param angular_velocity: (numpy array, 1x3 Wx Wy Wz) angular velocity of the satellite in the satellite coordinate
                            frame
    :return: (numpy array, vector 3x1 Wx Wy Wz) vector of the angular velocity in the sensor frame
    """
    # _dot = differentiated, _r = real part, _i, _j, _k = imaginary part
    # Basic principle of this function is: W_1 = -2(q_r_dot * q - q_r * q_dot - q_dot x q
    #                                                                  _         _
    #                                     _                          _  |  q_dot_r  |
    #                                    |  -q_i    q_r   q_k    -q_j | |  q_dot_i  |
    # that cn be transformed int W_1 = 2*|  -q_j   -q_k   q_r     q_i | |  q_dot_j  |
    #                                    |_ -q_k    q_j  -q_i     q_r_| |_ q_dot_k _|
    #
    # ======================================================================================================
    # split up the quaternion in it's parts
    q_r = orientation[0]
    q_i = orientation[1]
    q_j = orientation[2]
    q_k = orientation[3]

    # split up the angular velocity
    W_x = angular_velocity[0]
    W_y = angular_velocity[1]
    W_z = angular_velocity[2]

    # differentiated quaternions
    q_r_dot = -1 / 2 * (W_x * q_i + W_y * q_j + W_z * q_k)
    q_i_dot = 1 / 2 * (W_x * q_r + W_y * q_k - W_z * q_j)
    q_j_dot = 1 / 2 * (W_y * q_r + W_z * q_i - W_x * q_k)
    q_k_dot = 1 / 2 * (W_z * q_r + W_x * q_j - W_y * q_i)

    # make the transformation matrix
    G = np.array([[-q_i, q_r, q_k, -q_j],
                  [-q_j, -q_k, q_r, q_i],
                  [-q_k, q_j, -q_i, q_r]])

    # make quaternion differention vector
    q_dot = np.array([q_r_dot, q_i_dot, q_j_dot, q_k_dot])

    # calculate angular velocity in the sensor frame
    W_out = 2 * G.dot(q_dot)

    return W_out


def get_attitude(vector1, vector2):
    """
    returns the quaternion that rotates one vector into the other.
    (modified code from TUDSat script)
    Args:
        vector1 (numpy ndarray): first vector
        vector2 (numpy ndarray): second vector

    Returns:
        quaternion (pyquaternion.Quaternion): A quaternion that represents a rotation from first to second vector
    """

    unit_vector1 = vector1 / np.linalg.norm(vector1)
    unit_vector2 = vector2 / np.linalg.norm(vector2)

    # check if arrays are equal and if so, return a quaternion that does not rotate any vector
    if np.array_equal(unit_vector1, unit_vector2):
        quaternion = Quaternion([1., 0., 0., 0.])
    else:
        rotation_axis = np.cross(unit_vector1, unit_vector2)
        rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))

        # check if vectors are pointing into the exact opposite direction
        if np.array_equal(rotation_axis, np.zeros(3)):
            # if vectors are pointing towards opposite directions cross product is a zero vector, which is why another
            # method for finding an orthogonal vector is used
            rotation_axis = np.array([1., 1., 1.])
            rotation_axis -= rotation_axis.dot(unit_vector1) * unit_vector1
            rotation_axis /= np.linalg.norm(rotation_axis)
            rotation_angle = np.pi

        # the length of the vector represents the angle of rotation
        quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
    return quaternion


def ned2ecef(lon, lat):
    """Returns quaternion that transforms local north, east, down vector into ECEF vector. (Source:
    https://gssc.esa.int/navipedia/index.php/Transformations_between_ECEF_and_ENU_coordinates
    https://onlinelibrary.wiley.com/doi/pdf/10.1002/9780470099728.app3

    :param lon: (float or list of floats) geodetic longitude in deg
    :param lat: (float or list of floats) geodetic latitude in deg
    :return: (scipy.Rotation) a rotation that represents the transformation from the NED to ECEF system
    """
    to_rad = np.pi / 180

    if (type(lon) == list) or (type(lon) == tuple) or (type(lon) == np.ndarray):
        if len(lon) != len(lat):
            raise ValueError('If list like objects are provided, they have to have the same length!')
        else:
            rot_mat = []
            for i in range(len(lon)):
                rot_mat.append(np.array([[-np.cos(to_rad * lon[i]) * np.sin(to_rad * lat[i]), -np.sin(to_rad * lon[i]),
                                          -np.cos(to_rad * lon[i]) * np.cos(to_rad * lat[i])],
                                         [-np.sin(to_rad * lon[i]) * np.sin(to_rad * lat[i]), np.cos(to_rad * lon[i]),
                                          -np.sin(to_rad * lon[i]) * np.cos(to_rad * lat[i])],
                                         [np.cos(to_rad * lat[i]), 0., -np.sin(to_rad * lat[i])]]))
    else:
        rot_mat = np.array([[-np.cos(to_rad * lon) * np.sin(to_rad * lat), -np.sin(to_rad * lon),
                             -np.cos(to_rad * lon) * np.cos(to_rad * lat)],
                            [-np.sin(to_rad * lon) * np.sin(to_rad * lat), np.cos(to_rad * lon),
                             -np.sin(to_rad * lon) * np.cos(to_rad * lat)],
                            [np.cos(to_rad * lat), 0., -np.sin(to_rad * lat)]])

    r = R.from_matrix(rot_mat)

    return r


def create_table(database: str, name: str, columns: str):
    """ Create a new table in the database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :param columns: (str) names of the columns separated by commas
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # create table, if it doesn't already exists
    try:
        c.execute('create table ' + name + ' (' + columns + ')')

    except:
        logging.info('table ' + name + ' already existed')
        pass

    conn.commit()
    c.close()


def write_line_into_database_table(database: str, name: str, columns_content: tuple):
    """ Extends database table with one new line.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :param columns_content: (tuple) content that will be written in the table
    """
    conn = sqlite3.connect(database)
    conn.execute('PRAGMA synchronous=OFF')
    conn.execute('BEGIN TRANSACTION')
    # checks how many columns the input has and adapts the sqlite syntax
    question_marks = '?'
    if type(columns_content) == dict:
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        c.execute('SELECT * FROM ' + name
            + ' ORDER BY rowid DESC LIMIT 1;')
        last_row = c.fetchone()
        input_keys = columns_content.keys()
        new_row = list()
        for key in last_row.keys():
            if key in input_keys:
                new_row.append(columns_content[key])
            else:
                new_row.append(last_row[key])

        for i in range(len(new_row) - 1):
            question_marks += ', ?'

        # writes the new line
        c.executemany('insert into ' + name + ' VALUES (' + question_marks
             + ')', [new_row])

    elif (type(columns_content[0]) == tuple) or (type(columns_content[0]) == list):
        c = conn.cursor()
        for i in range(len(columns_content[0]) - 1):
            question_marks += ', ?'

        # writes the new lines
        c.executemany('insert into ' + name + ' VALUES (' + question_marks + ')',
                      [*columns_content])
    else:
        c = conn.cursor()
        for i in range(len(columns_content) - 1):
            question_marks += ', ?'

        # writes the new line
        c.executemany('insert into ' + name + ' VALUES (' + question_marks + ')',
                      [columns_content])

    conn.commit()
    c.close()


def get_last_line_from_database_table(database: str, name: str, rows: int = 1) -> pd.core.frame.DataFrame:
    """ Read the last line of a table in the database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :return: (pandas.core.frame.DataFrame)
    :param rows: (int) number of lines that will be red in
    """
    # time_cost = -time.perf_counter()
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # read last line of Table from database into a pandas dataframe
    last_row = pd.read_sql_query('SELECT * FROM ' + name + ' ORDER BY rowid DESC LIMIT ' + str(rows) + ';', conn)
    conn.commit()
    c.close()
    # time_cost += time.perf_counter()
    # logging.debug("get_last_line_from_database_table(name = " + name + ": taken " + 
    #     bcolors.OKCYAN + str(time_cost) + bcolors.ENDC)

    return last_row


def get_table_from_database(database: str, name: str) -> pd.core.frame.DataFrame:
    """ Read the table from database.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :return: (pandas.core.frame.DataFrame)
    """
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # read  Table from database into a pandas dataframe
    table = pd.read_sql_query('SELECT * FROM ' + name, conn)
    conn.commit()
    c.close()

    return table


def TEME_to_geodetic(x, y, z, t, convert_time=True):
    """
    convert coordinates(km) given in TEME frame and a time(julian day)
    to latitude(deg), longitude(deg) and height(km)
    (code from TUDSat code)
    """
    if convert_time:
        t = Time(t, format="jd")
    # convert from the ECI frame TEME to ITRS which is an ECEF frame
    teme = coord.builtin_frames.TEME(x * u.km, y * u.km, z * u.km,
                                     obstime=t, representation_type='cartesian')
    itrs = teme.transform_to(coord.ITRS(obstime=t)).cartesian
    # ITRS is the default frame for EarthLocation which enables a geodetic representation
    # the WGS84 should be used since it is also used by the magnetic model
    earth_loc = coord.EarthLocation(x=itrs.x, y=itrs.y,
                                    z=itrs.z).to_geodetic(ellipsoid="WGS84")
    lat = earth_loc.lat.degree
    lon = earth_loc.lon.degree
    height = earth_loc.height.to_value(u.km)
    return [lat, lon, height]


def ECEF_to_TEME(x, y, z, t, convert_time=True):
    """
    convert the coordinates given in an ECEF to the TEME frame at
    the given time in julian days
    (code from TUDSat code)
    """
    if convert_time:
        t = Time(t, format="jd")
    ecef = coord.ITRS(x, y, z, obstime=t)
    teme = ecef.transform_to(coord.builtin_frames.TEME(obstime=t))
    return np.array(teme.cartesian.xyz).transpose()


def julian_to_decimal(time, convert_time=True):
    """
    converts a julian day date to a decimal year
    (code from TUDSat code)
    """
    if convert_time:
        time = Time(time, format="jd")
    return float(time.to_value("decimalyear"))


def interpolate_two_rows(database: str, name: str, sim_time: float):
    """ Read the two lines of a table in the database that are closest to the sim_time.

    :param database: (str) name of the database
    :param name: (str) name of the table
    :param sim_time: (float) the current simulation time
    :return: (pandas.core.frame.DataFrame)
    """
    # time_cost = -time.perf_counter()
    conn = sqlite3.connect(database)
    c = conn.cursor()
    # get the two closest lines to the given sim_time
    closest_rows = pd.read_sql_query('SELECT * FROM ' + name + ' ORDER BY ABS(simulation_time - ' + str(sim_time)
                                     + ') ASC LIMIT 2;', conn)
    conn.commit()
    c.close()

    # add a third row with sim_time as simulation_time and set simulation time as index
    closest_rows = pd.concat([closest_rows, pd.DataFrame({'simulation_time': [sim_time]})])
    closest_rows.set_index('simulation_time', inplace=True)

    # use pandas interpolate method to interpolate all column values except real time
    closest_rows.interpolate(method='index', inplace=True)
    interpolated = closest_rows.reset_index().dropna(axis=1)

    # return only interpolated row which does not contain the real time any more

    # time_cost += time.perf_counter()
    # logging.debug("interpolate_two_rows(name = " + name + ": taken " + 
    #     bcolors.OKCYAN + str(time_cost) + bcolors.ENDC)
    return interpolated.drop([0, 1])


def get_mean_motion(a: float):
    """Computes the mean motion in rad/minutes as required by the sgp4 propagator.

    :param a: (float) semi-major axis of the orbit in km
    :return: (float) the mean motion on the specified orbit in radians/minutes
    """
    n0 = np.sqrt(398601.3 / a ** 3) * 60
    return n0


def change_sensor_parameter(sensor, parameter: list, new_val: list, sim_time: float = 0):
    """Method that adds change request to respective sensor's parameter table. This method is thought to be called
    by the GUI.

    :param sensor: (Sensor type) the sensor object whose parameter is supposed to be changed
    :param parameter: (list of strings) the attribute names of the parameters that are supposed to be changed
    :param new_val: (list of type(parameter)) the new values for the changed parameters (NO numpy arrays allowed, instead use
                        python list)
    :param sim_time: (float) the simulation time the parameter change is conducted
    """
    con = sqlite3.connect(sensor.database)
    df = pd.read_sql_query('SELECT * FROM ' + sensor.name + '_parameters', con)
    parameter_dict = df[df.columns[2:]].to_dict('records')[-1]

    if type(parameter) != list:
        parameter = [parameter]
        new_val = [new_val]

    for i in range(len(parameter)):
        if parameter[i] not in parameter_dict:
            logging.warning('This sensor does not have any attribute called {}.'.format(parameter[i]))
        else:
            if new_val[i] != '':
                parameter_dict[parameter[i]] = new_val[i]

    columns_content = (str(dt.datetime.now(tz=None)), sim_time, *[str(parameter_dict[i]) for i in parameter_dict])
    write_line_into_database_table(sensor.database, sensor.name + '_parameters', columns_content)


def euler_to_quaternion(euler):
    """
    Convert euler angles to a quaternion with scalar first representation
    """
    # aerospace standard Euler angles sequence Z,Y,state = yaw, pitch, roll
    # 1, psi   - z (yaw,heading)
    # 2, theta - y (pitch)
    # 3, phi   - x (roll)

    # check if pitch is not in [-90, 90] deg domain
    if euler[1] >= np.pi / 2:
        logging.warning(">>> WARNING! Pitch is more than 90 deg. Results may "
                        "not be accurate")

    if euler[1] <= -np.pi / 2:
        logging.warning(">>> WARNING! Pitch is less than -90 deg. Results may "
                        "not be accurate")
    euler = np.array(euler)

    # angles = array([r, p, y])
    c = np.cos(euler / 2.)
    s = np.sin(euler / 2.)

    # from book: Quaternions and Rotation Sequences pg 167
    # scalar first representation
    q0 = c[0] * c[1] * c[2] + s[0] * s[1] * s[2]
    q1 = c[0] * c[1] * s[2] - s[0] * s[1] * c[2]
    q2 = c[0] * s[1] * c[2] + s[0] * c[1] * s[2]
    q3 = s[0] * c[1] * c[2] - c[0] * s[1] * s[2]

    # scalar first
    return np.array([q0, q1, q2, q3])


def quaternion_to_euler(q):
    # from book: Quaternions and Rotation Sequences pg 168
    # assumes q = [q0, q1, q2, q3] (scalar first)

    dcm = quaternion_to_dcm(q)
    a = abs(-dcm[0, 2])
    if a > 1:
        dcm[0, 2] = np.round(a, 5)
    psi = np.arctan2(dcm[0, 1], dcm[0, 0])  # yaw
    theta = np.arcsin(-dcm[0, 2])  # pitch
    phi = np.arctan2(dcm[1, 2], dcm[2, 2])  # roll

    return np.array([psi, theta, phi])


def quatNorm(q):
    # return normalized quaternion
    return q / np.sqrt(np.dot(q, q))


def quaternion_to_dcm(q):
    # from book: Quaternions and Rotation Sequences pg 168
    q0, q1, q2, q3 = q  # [0],q[1],q[2],q[3]

    return np.array([
        [2 * q0 ** 2 - 1 + 2 * q1 ** 2, 2 * (q1 * q2 + q0 * q3),
         2 * (q1 * q3 - q0 * q2)],
        [2 * (q1 * q2 - q0 * q3), 2 * q0 ** 2 - 1 + 2 * q2 ** 2,
         2 * (q2 * q3 + q0 * q1)],
        [2 * (q1 * q3 + q0 * q2), 2 * (q2 * q3 - q0 * q1),
         2 * q0 ** 2 - 1 + 2 * q3 ** 2]
    ])


def interpolate_dataframe(dataframe, sim_time):
    closest_row = dataframe.iloc[(dataframe['simulation_time'] - sim_time).abs().argsort()[:2]]
    # add a third row with sim_time as simulation_time and set simulation time as index
    closest_rows = pd.concat([closest_row, pd.DataFrame({'simulation_time': [sim_time]})])
    closest_rows.set_index('simulation_time', inplace=True)
    # print(closest_rows)

    # use pandas interpolate method to interpolate all column values except real time
    closest_rows.interpolate(method='index', inplace=True)
    interpolated = closest_rows.reset_index().dropna(axis=1)
    # print(interpolated)
    return interpolated.loc[2]
