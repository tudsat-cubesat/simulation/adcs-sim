import time

import numpy as np
import matplotlib.pyplot as plt

a = []
b = []
c = [0]
run_variable = 0
max_error = 0.4
error = 0.5
sampling_rate = 0.5
angular_random_walk = 0.5
bias_instability = 0.5
def get_slow_error(max_error, last_error, sampling_rate):
    """ Returns a walking drift with the boundaries of the max error.

    :param max_error: (float, unit/hr) the upper and lower boundaries of the drift
    :param last_error: (float, unit/s) last output of the function, for first initiation start with 0
    :param sampling_rate: (float, hz) frequency of the sensor
    :return: (float, unit/s) returns error, has to be added to sensor data
    """
    # creates normal distributed error in unit/s
    error = np.random.normal(0, max_error / 3600 / sampling_rate, 1)[0]
    # compares if the error if the error drift with the boundary of the maximum hourly error
    # if it were to cross the boundaries, a change of sign will take place
    if error + last_error >= max_error:
        error_output = - error
    elif error + last_error <= -max_error:
        error_output = - error
    else:
        error_output = + error
    return error_output


j = 0
k = 0

for i in range(1000):
    a.append(np.random.normal(0, 0.5, 1)[0])
    b.append(np.random.normal(0, 0.5, 1)[0])
    k += get_slow_error(angular_random_walk, k, sampling_rate)



    c.append(k)

fig, axs = plt.subplots(3)
fig.suptitle('Vertically stacked subplots')

axs[0].plot(list(range(len(a))), a)
axs[1].plot(list(range(len(b))), b)
axs[2].plot(list(range(len(c))), c)
plt.show()