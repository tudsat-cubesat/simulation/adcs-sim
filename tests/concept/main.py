import threading
import model
import sensor
import time

# instanziert die Klasse
sin_wave = model.SinusModel()
print('simulation erstellt')
# schiebt die funktion "start" der Klasse sin_wave in einen eigenen thread und übergibt der Funktion die inputs über args
t1 = threading.Thread(target=sin_wave.start, args=())
# startet die Klasse im Thread dabei ist start eine Funktion des moduls threading und nicht die funktion der klasse
# aus der Klasse sin_wave heist nur unglücklicherweise gleich
t1.start()
print('thread 1 aktiv')
# es existiert auch ein threadpoolmanager, "ThreadPoolExecutor" der allerdings eine Funktion mehrfach aufruft deswegen
# bin ich mir unsicher, ob man damit auch die selbe Funktion von verschiedenen Klassen aufrufen kann

sensor1 = sensor.ErrorSensor('sensor1', 0, 0.5)
t2 = threading.Thread(target=sensor1.start, args=())
t2.start()
print('thread 2 aktiv')

sensor2 = sensor.ErrorSensor('sensor2', 0, 0.5)
t3 = threading.Thread(target=sensor2.start, args=())
t3.start()
print('thread 3 aktiv')

sensor3 = sensor.ErrorSensor('sensor3', 0.0, 0.5)
t4 = threading.Thread(target=sensor3.start, args=())
t4.start()
print('thread 4 aktiv')

sensor4 = sensor.ErrorSensor('sensor4', 0, 0.5)
t5 = threading.Thread(target=sensor4.start, args=())
t5.start()
print('thread 5 aktiv')

sensor5 = sensor.ErrorSensor('sensor5', 0, 0.5)
t6 = threading.Thread(target=sensor5.start, args=())
t6.start()
print('thread 6 aktiv')
time.sleep(5)

# beim import wird selbst die Klasse des tkintermoduls direkt ausgeführt, dies würde das script automatisch stoppen,
# deswegen steht der import hier unten. Ich hab bisher noch keinen besseren fix gefunden
import tkinter_test



