from pandas import DataFrame
import pandas as pd
import sqlite3
import time
import matplotlib.animation as animation
from tkinter import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import random
from itertools import count
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

class GUI:
    def __init__(self):
        pass

    # verbindet mit Datenbank und erstellt einen Cursor
    conn = sqlite3.connect('concepttest.db')
    c = conn.cursor()
    # erstellt eine Tabelle aus tuple mit den Namen aller Tabellen aus der Datenbank
    c.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = c.fetchall()
    c.close()
    # erstellt eine Tabelle mit den Namen aller Tabellen aus der Datenbank als reine strings
    table_names = []
    for table_name in tables:
        table_names.append(table_name[0])
    # schmeißt das Model aus der liste, damit darin nur noch die sensoren stehen
    table_names.remove('model')

    data = []


    root = Tk()
    figId = plt.figure()
    canvas = FigureCanvasTkAgg(figId, master=root)
    canvas.get_tk_widget().pack()
    canvas.draw()

    # erstellt eine Animation des plots, tkinter update diese in jedem Loop
    def animate(i):
        conn = sqlite3.connect('concepttest.db')
        c = conn.cursor()
        data = pd.read_sql_query("SELECT * FROM sensor1", conn)
        c.close()
        xs = data["time"]
        ys = data["out"]
        print(ys)
        plt.cla()
        plt.plot(xs, ys)

    ani = FuncAnimation(plt.gcf(), animate, interval=1000)


    root.mainloop()
